<?php 

class Dashboard extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Dashboard';
		
		$this->load->model('model_rentitems');
		$this->load->model('model_inward');
		$this->load->model('model_users');
		$this->load->model('model_stores');
		$this->load->model('model_outward');
		$this->load->model('model_invoice');
		$this->load->model('model_scustomers');
		$this->load->model('model_estimate');
	}

	/* 
	* It only redirects to the manage category page
	* It passes the total product, total paid orders, total users, and total stores information
	into the frontend.
	*/
	public function index()
	{
		$this->data['total_products'] = $this->model_rentitems->countTotalProducts();
		$this->data['total_inward'] = $this->model_inward->countTotalInwards();
		$this->data['total_users'] = $this->model_users->countTotalUsers();
		$this->data['total_stores'] = $this->model_stores->countTotalStores();
		$this->data['total_outwards'] = $this->model_outward->countTotalOutwards();
		$this->data['total_invoice'] = $this->model_invoice->countTotalInvoices();
		$this->data['total_estimate'] = $this->model_estimate->countTotalInvoices();
		$this->data['total_suppliers'] = $this->model_scustomers->countTotalSuppliers();
		$this->data['total_sales'] = $this->model_inward->countTotalSumOrders();
		$user_id = $this->session->userdata('id');
		$is_admin = ($user_id == 1) ? true :false;

		$this->data['is_admin'] = $is_admin;
		$this->render_template('dashboard', $this->data);
	}
}