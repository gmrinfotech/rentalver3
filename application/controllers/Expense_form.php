<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense_form extends Admin_Controller {
	var $expense_id	= '';
	function __construct()
	{
		parent::__construct(); 
		$this->load->model('model_ledger');
		$this->load->model('Model_expenditure');
		$this->load->model('model_inward');
		$this->data['page_title'] = 'Expenditure';
	}
	
	function index($id = '')
	{
		$this->data['id'] = $id;
		$this->data['expense_date'] = date("d/m/Y");

		$this->data['item_description'] = '';
		$this->data['amount'] = '';
		$this->data['expense_type'] = 2;
		$this->data['ledger_id'] = '';
		$this->data['opening_balance'] = 0;
		$this->data['collection_amt'] = 0;
		$this->data['credit_amt'] = 0;
		$this->data['debit_amt'] = 0;
		$this->data['closing_balance'] = 0;

		$filter = array();
		$filter['expense_date'] = date("d/m/Y");
		$expense_list = $this->Model_expenditure->get_expense_list($filter);
		$this->data['expense_list'] = $expense_list;

		$opening_balance = 0;
		$collection_amt = 0;
		$credit_amt = 0;
		$debit_amt = 0;
		$closing_balance = 0;

		$opening_balance = $this->Model_expenditure->get_opening_balance(date("d/m/Y"));
		$collection_amt = 0;
		$opening_balance - $collection_amt;
		
		$opening_closing = $this->Model_expenditure->get_opening_closing(date("d/m/Y"));

		if(sizeof($expense_list) > 0)
		{
			foreach ($expense_list as $row) {
				if($row->expense_type == 1)
				{
					$credit_amt += $row->amount;
				}
				if($row->expense_type == 2)
				{
					$debit_amt += $row->amount;
				}
			}
		}
		
		$closing_balance = ($opening_balance + $collection_amt + $credit_amt) - $debit_amt;
 
		$opening_balance = $opening_closing['opening'];
		$closing_balance = $opening_closing['closing'];
		$this->data['credit_amt'] = $credit_amt;
		$this->data['debit_amt'] = $debit_amt;
		$this->data['collection_amt'] = $collection_amt;
		$this->data['opening_balance'] = $opening_balance;
		$this->data['closing_balance'] = $closing_balance; 
		$this->data['ledgers'] = $this->model_ledger->getAllActiveLedger();

		if($id)
		{
			$this->expense_id = $id;
			$expense = $this->Model_expenditure->get_expense($id);
			if(!$expense)
			{
				$this->session->set_flashdata('error', 'Record Not Found');
				$this->render_template('ledger/expense-form', $this->data);
			}
			$this->data['id'] = $expense->id;
			$this->data['expense_date'] = date("d/m/Y", strtotime($expense->expense_date));
			$this->data['ledger_id'] = $expense->ledger_id;
			$this->data['item_description'] = $expense->item_description;
			$this->data['amount'] = $expense->amount;
			
			$exp_type = 1;
			
			if($expense->expense_type == 1 || $expense->expense_type == 3) {
				$exp_type = 1;
			} else {
				$exp_type = 2;
			}
			
			$this->data['expense_type'] = $exp_type;
		}

		if($this->input->post('submit'))
		{
			$this->data['expense_date'] = $this->input->post('expense_date');
			$this->data['ledger_id'] = $this->input->post('ledger');
			$this->data['item_description'] = $this->input->post('item_description');
			$this->data['amount'] = $this->input->post('amount');
			$this->data['expense_type'] = $this->input->post('expense_type');
			$opening_balance = $this->Model_expenditure->get_opening_balance($row->expense_date);
			$this->data['opening_balance'] = $opening_balance;
		}
 
		$this->form_validation->set_rules('ledger', 'Ledger Item', 'trim|required');
		$this->form_validation->set_rules('amount', 'Amount', 'required|numeric');
		$this->form_validation->set_error_delimiters('<p class="val-error">','</p>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->render_template('ledger/expense-form', $this->data);
		}
		else
		{
			$save['id'] = $id;
			$sdate = $this->input->post('expense_date');
			
	    	$date = str_replace('/', '-', $sdate);
	    	 
			$save['expense_date'] = $sdate;
			$save['date_time'] = strtotime($date);
			$save['ledger_id'] = $this->input->post('ledger');
			$save['item_description'] = $this->input->post('item_description');
			$save['amount'] = $this->input->post('amount');
			$save['expense_type'] = $this->input->post('expense_type');

			$create = $this->Model_expenditure->save_expense($save);
			if($create == true) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('Expense_form', 'refresh');
			}
			else {
				$this->session->set_flashdata('errors', 'Error occurred!!');
				redirect('Expense_form', 'refresh');
			}
		}
	}

	function delete_expense($id)
	{
		if($id)
		{
			$expense = $this->Model_expenditure->get_expense($id);
			if (!$expense)
			{
				$this->session->set_flashdata('error', 'Record Not Found');
				redirect(base_url('Expense_form'));
			}
			else
			{
				$this->Model_expenditure->delete_expense($id);
				$this->session->set_flashdata('message', 'Successfully Deleted');
				redirect(base_url('Expense_form'));
			}
		}
		else
		{
			$this->session->set_flashdata('message', 'Successfully Deleted');
			redirect(base_url('Expense_form'));
		}
	}

	function daywise_report()
	{
		$this->data['page_title'] = 'Day wise Report';
		$this->data['from_date'] = date("d/m/Y");
		$this->data['to_date'] = date("d/m/Y");
		if($this->input->post('from_date'))
		{
			$from_date = $this->input->post('from_date');
			$this->data['from_date'] = $this->input->post('from_date');
		}
		if($this->input->post('to_date'))
		{
			$to_date = $this->input->post('to_date');
			$this->data['to_date'] = $this->input->post('to_date');
		}

		$report_data = array();
		$filter = array();
		$filter['from_date'] =$this->data['from_date'];
		$filter['to_date'] = $this->data['to_date'];
		$expense_list = $this->Model_expenditure->get_expense_list($filter);
		if(sizeof($expense_list) > 0)
		{
			foreach ($expense_list as $row) {
				$row_item = array();
				$row_item['category_name'] = $row->name;
				$row_item['item_description'] = $row->item_description;
				$row_item['expense_type'] = $row->expense_type;
				$row_item['amount'] = $row->amount;
				$report_data[$row->expense_date][] = $row_item;
			}
		}
		//$opening_balance =  $this->Model_expenditure->get_opening_balance($filter['from_date']);
		/*$opening_closing = $this->Model_expenditure->get_opening_closing($filter['from_date']);
		$opening_balance = $opening_closing['opening'];
		$closing_balance = $opening_closing['closing'];
		$this->data['opening_balance'] = $opening_balance;*/
		$this->data['report_data'] = $report_data; 
		$this->data['fdate'] = $this->data['from_date'];
		$this->data['tdate'] = $this->data['to_date'];
		$this->data['total_csales'] = $this->Model_expenditure->countTotalCashSumOrders($filter);
		$this->data['total_sales'] = $this->Model_expenditure->countTotalSumOrders($filter);
		
		$this->render_template('ledger/daywise-report', $this->data);
	}

	function monthwise_report()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->data['page_title'] = 'Month wise Report';
		$this->data['active_tab'] = 'expenditure';
		$this->data['active_item'] = 'index';
		$this->data['from_date'] = date("d/m/Y");
		$this->data['to_date'] = date("d/m/Y");
		if($this->input->post('from_date'))
		{
			$from_date = $this->input->post('from_date');
			$this->data['from_date'] = $this->input->post('from_date');
		}
		if($this->input->post('to_date'))
		{
			$to_date = $this->input->post('to_date');
			$this->data['to_date'] = $this->input->post('to_date');
		}

		$report_data = array();
		$credit_data = array();
		$opening_balance_data = array();
		$category_id_list = array();
		$filter = array();
		$filter['from_date'] = date("d/m/Y", strtotime($this->data['from_date']));
		$filter['to_date'] = date("d/m/Y", strtotime($this->data['to_date']));
		$expense_list = $this->Model_expenditure->get_expense_list($filter);
		if(sizeof($expense_list) > 0)
		{
			foreach ($expense_list as $row) {
				$opening_closing = $this->Model_expenditure->get_opening_closing($row->expense_date);
				$opening_balance = $opening_closing['opening'];
				
				//$opening_balance = $this->Model_expenditure->get_opening_balance($row->expense_date);
				///$opening_balance_data[$row->expense_date] = $opening_balance;
				if($row->expense_type == 1)
				{
					$credit_data[$row->expense_date][] = $row->amount;
				}
				if($row->expense_type == 2)
				{
					$report_data[$row->expense_date][$row->ledger_id][] = $row->amount;
					$category_id_list[$row->ledger_id] = $row->name;					
				}
			}
		}
		/*echo "<pre>";
		print_r($opening_balance_data);
		echo "</pre>";*/
		$this->data['credit_data'] = $credit_data;
		$this->data['report_data'] = $report_data;
		$this->data['category_id_list'] = $category_id_list;
		
		$opening_closing = $this->Model_expenditure->get_opening_closing($filter['from_date']);
		$opening_balance = $opening_closing['opening'];
		$closing_balance = $opening_closing['closing'];
		
		//print_r($report_data);
		
		$this->data['opening_balance_data'] = $opening_balance;
		$this->render_template('ledger/monthwise-report', $this->data);
	}
}