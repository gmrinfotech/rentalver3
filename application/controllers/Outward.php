<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Outward extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Outward';

		$this->load->model('model_outward');
		$this->load->model('model_credit'); 
		$this->load->model('model_rentitems');
		$this->load->model('model_driver');
		$this->load->model('model_users');
		$this->load->model('model_employee');
		$this->load->model('model_company');
		$this->load->model('model_scustomers');
		$this->load->model('model_invoiceprefixs');
		$this->load->model('model_boxitems');
		$this->load->library('Sim');
		$this->load->library('mypdf');
	}

	public function index()
	{
		if(!in_array('viewOutward', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Manage Outward';
		$this->render_template('outward/index', $this->data);	
	}

	public function fetchOutwardData()
	{
	
		$result = array('data' => array());
		$data = $this->model_outward->getOutwardData();

		foreach ($data as $key => $value) {
			$user_id = $this->model_users->getUserData($value['user_id']);
			$date = date('d-m-Y', $value['date_time']);
			$time = date('h:i a', $value['date_time']);

			$date_time = $date . ' ' . $time;

		
			//echo $outtedOutward;
			
			
			// button
			$buttons = '';

			//$buttons .= ' <a href="'.base_url('outward/view/'.$value['id']).'" class="btn btn-default viewbutton"><i class="fa fa-eye"></i></a>';
				
			if(in_array('viewOrder', $this->permission)) {
				$buttons .= '<a target="__blank" href="'.base_url('outward/printDiv/'.$value['id']).'" class="btn btn-default printbutton"><i class="fa fa-print"></i></a>';
			}

			if(in_array('updateOrder', $this->permission)) {
					$buttons .= ' <a href="'.base_url('outward/update/'.$value['id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
			}
			if(in_array('viewOrder1', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default smsbutton" onClick="smsFunc('.$value['id'].')" data-toggle="modal" data-target="#smsModal" title="Send SMS"><i class="fa fa-commenting-o "></i></button>';
			}
				
			if(in_array('viewOrder1', $this->permission)) {
				$buttons .= '&nbsp;<button type="button" class="btn btn-default mailbutton"
						onClick="mailFunc('.$value['id'].')" data-toggle="modal" data-target="#mailModal"
					    title="Send Email"><i class="fa fa-envelope-o "></i></button>';
				
			}

		/*	if(in_array('deleteOrder', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default deletebutton" onClick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}*/
			
			$status = $value['total_balanceqty'] == 0 ? '<span class="label label-success">Closed</span>' : '<span class="label label-warning">Open</span>';
			if($value['mop'] == 4) {
				$paid_status = '<span class="label label-warning">Not Paid</span>';
				
			}
			else {
				$paid_status = '<span class="label label-success">Paid</span>';	
			}
 
			$result['data'][$key] = array(
				$value['sdate'],
				$value['odc_no'],
				$value['supplier_name'],
				$value['contNo'],
				$value['total_items'],
				$value['totalrent'], 
				$value['advance'], 
				$paid_status,
				$status,
				$user_id['firstname'],
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}
 	public function getDetailsByOutwardNo()
	{
		$id = $this->input->post('id');
		if($id) {
			$data = $this->model_outward->search($id);
			echo json_encode($data);
		}
	} 
	public function create()
	{
		if(!in_array('createOutward', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->data['page_title'] = 'Add Outward';

		$this->form_validation->set_rules('boxitem_name[]', 'Boxitem name', 'trim|required');
		
	
        if ($this->form_validation->run() == TRUE) {        	
        	$order_id = $this->model_outward->create();
        	
        	if($order_id) {
				//$this->pdfemailcreate($order_id);
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('outward/', 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('outward/create/', 'refresh');
        	}
        }
        else {
            // false case
            $this->data['boxitems'] = $this->model_rentitems->getTypeRentitemsData(1);   
        	$this->data['products'] = $this->model_rentitems->getTypeRentitemsData(2);      	
            $this->data['suppliers'] = $this->model_scustomers->getSupplierData();  
            $this->data['drivers'] = $this->model_driver->getActiveDriver();
         	$this->data['employees'] = $this->model_employee->getActiveEmployee();
             
            $this->render_template('outward/create', $this->data);
        }	
	}
 
 
	public function update($id)
	{
		if(!in_array('updateOutward', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		if(!$id) {
			redirect('dashboard', 'refresh');
		}

		$this->data['page_title'] = 'Update Outward';

		$this->form_validation->set_rules('boxitem_name[]', 'Boxitem name', 'trim|required'); 
        if ($this->form_validation->run() == TRUE) {        	
        	
        	$update = $this->model_outward->update($id);
        	
        	if($update == true) {
        		$this->session->set_flashdata('success', 'Successfully updated');
        		redirect('outward/update/'.$id, 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('outward/update/'.$id, 'refresh');
        	}
        }
        else {
            // false case

        	$result = array();
        	$outward_data = $this->model_outward->getOutwardData($id);

    		$result['outward'] = $outward_data;
    		$outward_items = $this->model_outward->getOutwardItemData($outward_data['id']);

    		foreach($outward_items as $k => $v) {
    			$result['outward_item'][] = $v;
    		}

    		$this->data['outward_data'] = $result;
			 
    		$supid = $outward_data['sid'];
    		$this->data['products'] = $this->model_rentitems->getTypeRentitemsData(2); 
			$this->data['boxitems'] = $this->model_rentitems->getTypeRentitemsData(1); 
    		
    		//$this->data['products'] = $this->model_rentitems->getRentitemsData();      	
            $this->data['suppliers'] = $this->model_scustomers->getSupplierData(); 
            $this->data['drivers'] = $this->model_driver->getActiveDriver(); 
            $this->data['employees'] = $this->model_employee->getActiveEmployee();
 
            $outtedInward = $this->model_outward->getInOutwardData($id); 
            $outtedInward = $outtedInward > 0 ? true : false;
 			$this->data['editen'] = false;
			$today = date('d/m/Y'); 
       		if($outtedInward) {
    			$this->data['editen'] = false;
    		} else if($outward_data['sdate'] == $today) {
				$this->data['editen'] = true;
			} else {
    			$user_id = $this->session->userdata('id');
				$is_admin = ($user_id == 1 || $user_id == 2) ? true :false;
    	 
    			if($is_admin) {
    				//$today = date('d/m/Y'); 
				//	$sdate = str_replace('/', '-', $today);
				// $previous_date = date('d/m/Y', strtotime('-1 day', strtotime($sdate)));
					//if($outward_data['sdate'] == $previous_date) {
		    			$this->data['editen'] = true;
		    	//	}
    			}
    		}
    		
            $this->render_template('outward/edit', $this->data);
        }
	}
 
	public function view($id)
	{
		if(!$id) {
			redirect('dashboard', 'refresh');
		}

		$result = array();
		$outward_data = $this->model_outward->getOutwardData($id);

		$result['outward'] = $outward_data;
		$outward_items = $this->model_outward->getOutwardItemData($outward_data['id']);

		foreach($outward_items as $k => $v) {
			$result['outward_item'][] = $v;
		}

		$this->data['outward_data'] = $result;

		$supid = $outward_data['sid'];
		$this->data['products'] = $this->getSupRentitemData($supid);

		//$this->data['products'] = $this->model_rentitems->getRentitemsData();
		$this->data['suppliers'] = $this->model_scustomers->getSupplierData();
		$this->data['drivers'] = $this->model_driver->getActiveDriver();
	 	$this->data['employees'] = $this->model_employee->getActiveEmployee();
 	
		$this->render_template('outward/view', $this->data);

	}
	
	public function pdfemailcreate($id)
	{ 
		$pdf = new CI_MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$order_data = $this->model_outward->getOutwardData($id);
		$buyeremail = $order_data['semail_id'].",magnumwebtech@gmail.com";
		$this->getPdf($id,$pdf, $order_data); 
 
		$pdf->Output(FCPATH.'/newpdf/'.$order_data['odc_no'].'.pdf', "F");
       // $pdf->Output(FCPATH. '/newpdf/2.pdf', 'F');
        $this->load->library('email');
        $this->email->from('gmr@gstsoftwareincoimbatore.com');
        $this->email->to($buyeremail); 
        $this->email->subject('Invoice From GMR');
        $this->email->message('Invoice Copy From GMR(website.com)');   
        $this->email->attach(FCPATH.'/newpdf/'.$order_data['odc_no'].'.pdf');
        $this->email->send();
		$file=FCPATH.'/newpdf/'.$order_data['odc_no'].'.pdf';
		unlink($file);
	}
	
	public function printDiv($id)
	{ 
		$pdf = new CI_MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$order_data = $this->model_outward->getOutwardData($id);
		$this->getPdf($id,$pdf, $order_data); 
		$pdf->Output($id.'.pdf', 'I'); 
	}
	
	public function getPdf($id, $pdf, $order_data) {
		
		$orders_items = $this->model_outward->getOutwardItemData($order_data['id']);  
		$company_info = $this->model_company->getCompanyData(1);
 		 
		$dc_no = $order_data['odc_no'];
		$buyeraddr= $this->model_scustomers->getSupplierAddress($order_data['sid']);
 
		$i = 0;
		foreach ($orders_items as $key => $value) {
			$itype = $value['itype'];
			$box_id = $value['boxitem_id'];
            $box_name = $value['boxitem_name'];
            $value['boxname'] = $box_name;
            $noofunits = $value['noofunits'];
  
			if($itype == 1) {
				$value['name'] = "-";
				$value['itype'] = "Box"; 
				$orders_items[$i] = $value;
				$i++;
	    		$get_box_item = $this->model_outward->getBoxitemsRentQty($box_id);
					foreach ($get_box_item as $k => $v) { 
						
						 
						$value['boxname'] = "''"; 
						$value['itype'] = ""; 
						$value['rate'] = ""; 
						$value['rate'] = ""; 
						$value['period'] = ""; 
						$value['totalrent'] = ""; 
					 
						$rentitem_name = $v['rentitem_name'];
						$qty = $v['qty']; 
						 
						$totunits = $noofunits * $qty;
						$value['name'] = $rentitem_name;
						$value['noofunits'] = $totunits;
						
				     	$orders_items[$i] = $value; 
					    $i++;
					    continue;
				}
    		} else { 
    			 $value['boxname'] = '-';
    			 $value['itype'] = "Single"; 
			 	 $value['name'] = $value['boxitem_name'];
			 	 $orders_items[$i] = $value;
				$i++;
    		} 
		}
    
		$title="OUTWARD COPY";
		$pdf->createpdf($id, $pdf, $order_data, $orders_items, $company_info, $buyeraddr, 
					$title,$dc_no,true);
		return $pdf;
	}
 
	public function sendSMS()
	{
		$id = $this->input->post('id');
		$response = array();
		if($id) {
			$sent = $this->model_outward->updatesms($id);
			if($sent == true) {
				$response['success'] = true;
				$response['messages'] = "SMS Successfully sent";
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error occurred while sending SMS";
			}
		} else {
			$response['success'] = false;
			$response['messages'] = "Refresh the page again!!";
		}
		echo json_encode($response);
	}
	
	public function sendemail()
	{
		$id = $this->input->post('id');
		$response = array();
		if($id) {
			$sent = $this->pdfemailcreate($id);
			if($sent == true) {
				$response['success'] = true;
				$response['messages'] = "Email Successfully sent";
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error occurred while sending Email";
			}
		} else {
			$response['success'] = false;
			$response['messages'] = "Refresh the page again!!";
		}
		echo json_encode($response);
	}
 
	function fetch_data($orders_items)
	{
		$output = '';
		$total_amt = 0;
		$tax_amt = 0;
		foreach($orders_items as $row) {
			$product_data = $this->model_rentitems->getRentitemsData($row['product_id']);
			$output .= '<tr>
                          <td style="width:110px;border:1px thin #666; font-size:12px; font-weight:normal; font-style:normal;">'.$product_data["name"].'</td>
                          <td style="width:100px;border:1px thin #666"; align="center">'.$row["sku"].'</td>
                          <td style="width:70px;border:1px thin #666"; align="right">'.$row["rate"].'</td>
                          <td style="width:30px;border:1px thin #666"; align="center">'.$row["qty"].'</td>
                          <td style="width:70px;border:1px thin #666"; align="right">'.$row["amount"].'</td>
                          <td style="width:30px;border:1px thin #666"; align="center">'.$row["discount"].'%</td>
                          <td style="width:50px;border:1px thin #666"; align="center">'.$row["gst"].'</td>
                          <td style="width:50px;border:1px thin #666"; align="center">'.$row["sgst"].'</td>
                          <td style="width:50px;border:1px thin #666"; align="center">'.$row["cgst"].'%</td>
                          <td style="width:80px;padding:8px;border:1px thin #666"; align="right">'.$row["totamount"].'</td>
                     </tr>';
			$total_amt += $row['amount'];
			$tax_amt += $row['totamount'];
		}
		$output .= '<tr><td colspan="7" style="border-top:1px thin #666";></td></tr><tr><td colspan="5" align="left"><strong>Total</strong></td><td align="center">'.number_format($tax_amt,2).'</td><td align="center">'.number_format($total_amt,2).'</td></tr>';
	
		return $output;
	}
	

	public function getOutwardDetailsbyId()
	{
		$outward_no = $this->input->post('outward_no');
		if($outward_no) {
			$outward_data = $this->model_outward->getOutwardData($outward_no);
			$outward_data['availOutward'] = $this->model_outward->getAvailableOutwardItems($outward_no);
 
			echo json_encode($outward_data);
		}
	}
	
	public function getOutwardItemValueById()
	{
		$outward_item_id = $this->input->post('outward_item_id');
		$outward_id = $this->input->post('outward_id');

		if($outward_item_id) {
			$product_data = $this->model_outward->getOutwardItemDatabyId($outward_item_id, $outward_id);
			echo json_encode($product_data);
		}
	}
	
	
	public function getAvailableOutwardItems(){
		$outward_no = $this->input->post('outward_no');
		$availOutward = $this->model_outward->getAvailableOutwardItems($outward_no);
		echo json_encode($availOutward);
	}
	
	
	public function getOutwardInwardItemById()
	{
		$this->load->model('model_inward');
		$outward_no = $this->input->post('outward_no');
		if($outward_no) {
			$outward_data = $this->model_outward->getOutwardData($outward_no);
			$outward_data['outwardData'] = $this->model_outward->getInBoxNos($outward_no);
			$outward_data['inwardData'] = $this->model_inward->getInwardItemsForOutwardId($outward_no);
			echo json_encode($outward_data);
		}
	}
 
/*	public function remove()
	{
		if(!in_array('deleteOutward', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$order_id = $this->input->post('order_id');

        $response = array();
        if($order_id) {
            $delete = $this->model_outward->remove($order_id);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Successfully removed"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error in the database while removing the product information";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh the page again!!";
        }

        echo json_encode($response); 
	}
 
	public function getSupRentitemData($supid) {
		if($supid) {
			$products = $this->model_scustomers->getSupplierProducts($supid);
		}
		if(empty($products)) {
			$products = $this->model_rentitems->getRentitemsData();
		}
		
		$products['availRacks'] = $this->model_rackspace->getAvailableRacks(1, 1);
		return $products;
	}  */
	


}