<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Employee';

		$this->load->model('model_employee');
	}

	/* 
	* It only redirects to the manage employee page
	*/
	public function index()
	{
		if(!in_array('viewEmployee', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
	
		$this->render_template('employee/index', $this->data);
	}

	/*
	* Fetches the category value from the category table 
	* this function is called from the datatable ajax function
	*/
	public function fetchEmployeeData()
	{
		$result = array('data' => array());

		$data = $this->model_employee->getEmployeeData();

		foreach ($data as $key => $value) {

			// button
			$buttons = '';

			$buttons = '';
			if(in_array('updateEmployee', $this->permission)) {
				$buttons .= '<a href="'.base_url('employee/update/'.$value['id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
			}
	
			if(in_array('deleteCategory', $this->permission)) {
				//$buttons .= ' <button type="button" class="btn btn-default deletebutton" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
					

			$result['data'][$key] = array(
				$value['employeename'],
				$value['employeephone'],
				//$value['sboxwage'],
				//$value['mboxwage'],
			//	$value['lboxwage'],
			//	$value['bagwage'],
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	public function fetchEmployeeDataById($id){
		$data = $this->model_employee->getEmployeeData($id);
		echo json_encode($data);
	}
	
	public function getEmployeeDataById(){
		$id = $this->input->post('id');
		if($id) {
			$data=$this->model_employee->getEmployeeData($id);
			echo json_encode($data);
		}
	}
	
		
	public function create()
	{
		if(!in_array('createEmployee', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		$this->form_validation->set_rules('employee_name', 'Employee name is required', 'trim|required');		
		//$this->form_validation->set_rules('employeephone', 'Unique Employee Phone is required', 'trim|required|is_unique[employee.employeephone]');
		$this->form_validation->set_rules('employee_phone', 'Employee Phone is required', 'trim|required');
			
		if ($this->form_validation->run() == TRUE) {
	
			$data = array(
					'employeename' => $this->input->post('employee_name'),
					'employeephone' => $this->input->post('employee_phone')
			);
			
			$create = $this->model_employee->create($data);
			if($create == true) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('employee/', 'refresh');
			}
			else {
				$this->session->set_flashdata('errors', 'Error occurred!!');
				redirect('employee/create', 'refresh');
			}
		}
		else {
			$this->render_template('employee/create', $this->data);
		}
	}

	function check_employee_name() {
		if($this->input->post('id'))
			$id = $this->input->post('id');
		else
			$id = '';
		$employee_name = $this->input->post('employee_name');
		$result = $this->model_employee->check_employee_name($id, $employee_name);
		if($result == 0)
			$response = true;
		else {
			$this->form_validation->set_message('check_employee_name' , 'Employee name must be unique');
			$response = false;
		}
		return $response;
	}
	
	public function update($emp_id)
	{
		if(!in_array('updateEmployee', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
	
		if(!$emp_id) {
			redirect('dashboard', 'refresh');
		}
		
		$employee_name = $this->input->post('pnoldname');
		$employee_name_change = $this->input->post('pneditstatus');

		if($employee_name_change == "true") {
			$this->form_validation->set_rules('employee_name', 'employee_name', 'required|callback_check_employee_name');
			$employee_name = $this->input->post('employee_name');
		}
		
	
		//$this->form_validation->set_rules('employee_name', 'Employee name is required', 'trim|required');		
		//$this->form_validation->set_rules('employeephone', 'Unique Employee Phone is required', 'trim|required|is_unique[employee.employeephone]');
		$this->form_validation->set_rules('employee_phone', 'Employee Phone is required', 'trim|required');
		
		if ($this->form_validation->run() == TRUE) {

			$data = array(
					'employeename' => $employee_name,
					'employeephone' => $this->input->post('employee_phone')
			);
				
			$update = $this->model_employee->update($data, $emp_id);
			if($update == true) {
				$this->session->set_flashdata('success', 'Successfully updated');
				redirect('employee/', 'refresh');
			}
			else {
				$this->session->set_flashdata('errors', 'Error occurred!!');
				redirect('employee/update/'.$emp_id, 'refresh');
			}
		}
		else {
			$result = $this->model_employee->getEmployeeData($emp_id);
			$this->data['employee_data'] = $result;
			$this->render_template('employee/edit', $this->data);
		}
	}

	/*
	* It removes the category information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		if(!in_array('deleteCategory', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$employee_id = $this->input->post('employee_id');

		$response = array();
		if($employee_id) {
			$delete = $this->model_employee->remove($employee_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the employee information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}

}