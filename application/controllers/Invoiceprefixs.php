<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoiceprefixs extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Prefix';

		$this->load->model('model_invoiceprefixs');
	}

	/* 
	* It only redirects to the manage product page and
	*/
	public function index()
	{
		if(!in_array('viewPrefix', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$result = $this->model_invoiceprefixs->getPrefixData();

		$this->data['results'] = $result;

		$this->render_template('invoiceprefixs/index', $this->data);
	}

	/*
	* Fetches the brand data from the brand table 
	* this function is called from the datatable ajax function
	*/
	public function fetchPrefixData()
	{
		$result = array('data' => array());

		$data = $this->model_invoiceprefixs->getPrefixData();
		foreach ($data as $key => $value) {

			// button
			$buttons = '';

			if(in_array('viewPrefix', $this->permission)) {
				$buttons .= '<button type="button" class="btn btn-default" onclick="editBrand('.$value['id'].')" data-toggle="modal" data-target="#editBrandModal"><i class="fa fa-pencil"></i></button>';	
			}
			
			/*if(in_array('deletePrefix', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default" onclick="removeBrand('.$value['id'].')" data-toggle="modal" data-target="#removeBrandModal"><i class="fa fa-trash"></i></button>
				';
			}	*/			

			$status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';

			$result['data'][$key] = array(
				$value['inname'],
				$value['inno'],
				$value['outname'],
				$value['outno'],
				$value['name'],
				$value['invno'],
				$status,
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	* It checks if it gets the brand id and retreives
	* the brand information from the brand model and 
	* returns the data into json format. 
	* This function is invoked from the view page.
	*/
	public function fetchPrefixDataById($id)
	{
		if($id) {
			$data = $this->model_invoiceprefixs->getPrefixData($id);
			echo json_encode($data);
		}

		return false;
	}

	public function update($id)
	{
		if(!in_array('updatePrefix', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

		if($id) {
			$this->form_validation->set_rules('edit_inward_name', 'Lot prefix name', 'trim|required');
			$this->form_validation->set_rules('edit_outward_name', 'Outward prefix name', 'trim|required');
			$this->form_validation->set_rules('edit_inv_name', 'Invoice prefix name', 'trim|required');
			$this->form_validation->set_rules('edit_inv_no', 'Invoice current number', 'trim|required');
			$this->form_validation->set_rules('edit_in_no', 'Lot current number', 'trim|required');
			$this->form_validation->set_rules('edit_out_no', 'Outward current number', 'trim|required');
				
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
	        		'inname' => $this->input->post('edit_inward_name'),
	        		'outname' => $this->input->post('edit_outward_name'),
	        		'name' => $this->input->post('edit_inv_name'),
        			'inno' => $this->input->post('edit_in_no'),
        			'outno' => $this->input->post('edit_out_no'),
        			'invno' => $this->input->post('edit_inv_no'),
	        		'active' => $this->input->post('edit_active'),	
	        	);

	        	$update = $this->model_invoiceprefixs->update($data, $id);
	        	if($update == true) {
	        		$response['success'] = true;
	        		$response['messages'] = 'Succesfully updated';
	        	}
	        	else {
	        		$response['success'] = false;
	        		$response['messages'] = 'Error in the database while updated the brand information';			
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}

	/*
	* It removes the brand information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		if(!in_array('deletePrefix', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$brand_id = $this->input->post('brand_id');
		$response = array();
		if($brand_id) {
			$delete = $this->model_invoiceprefixs->remove($brand_id);

			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the brand information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}

}