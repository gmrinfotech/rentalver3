<?php  

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends Admin_Controller 
{	
	public function __construct()
	{
		parent::__construct();
		$this->data['page_title'] = 'Graph';
		$this->load->model('model_reports');
	}

	/* 
    * It redirects to the report page
    * and based on the year, all the orders data are fetch from the database.
    */
	public function index()
	{
		if(!in_array('viewReports', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		
		$today_year = date('Y');

		if($this->input->post('select_year')) {
			$today_year = $this->input->post('select_year');
		}

		$this->data['report_years'] = $this->model_reports->getOrderYear();

		$order_data = $this->model_reports->getOrderData($today_year);
		$final_order_data = array();
		foreach ($order_data as $k => $v) {
			
			if(count($v) > 1) {
				$total_amount_earned = array();
				foreach ($v as $k2 => $v2) {
					if($v2) {
						$total_amount_earned[] = $v2['gross_amount'];						
					}
				}
				$final_order_data[$k] = array_sum($total_amount_earned);	
			}
			else {
				$final_order_data[$k] = 0;	
			}
		}
		
		$poorder_data = $this->model_reports->getPoorderData($today_year);
		$final_poorder_data = array();
		foreach ($poorder_data as $k => $v) {
			
			if(count($v) > 1) {
				$total_amount_earned = array();
				foreach ($v as $k2 => $v2) {
					if($v2) {
						$total_amount_earned[] = $v2['gross_amount'];						
					}
				}
				$final_poorder_data[$k] = array_sum($total_amount_earned);	
			}
			else {
				$final_poorder_data[$k] = 0;	
			}
		}
		$income_data = $this->model_reports->getIncomeData($today_year);
		$final_income_data = array();
		foreach ($income_data as $k => $v) {
			
			if(count($v) > 1) {
				$total_amount_earned = array();
				foreach ($v as $k2 => $v2) {
					if($v2) {
						$total_amount_earned[] = $v2['amount'];						
					}
				}
				$final_income_data[$k] = array_sum($total_amount_earned);	
			}
			else {
				$final_income_data[$k] = 0;	
			}
		}
		$expenses_data = $this->model_reports->getExpensesData($today_year);
		$final_expenses_data = array();
		foreach ($expenses_data as $k => $v) {
			
			if(count($v) > 1) {
				$total_amount_earned = array();
				foreach ($v as $k2 => $v2) {
					if($v2) {
						$total_amount_earned[] = $v2['amount'];						
					}
				}
				$final_expenses_data[$k] = array_sum($total_amount_earned);	
			}
			else {
				$final_expenses_data[$k] = 0;	
			}
		}

		$this->data['selected_year'] = $today_year;
		$this->data['company_currency'] = $this->company_currency();
		$this->data['results'] = $final_order_data;
		$this->data['poresults'] = $final_poorder_data;
		$this->data['incomeresults'] = $final_income_data;
		$this->data['expensesresults'] = $final_expenses_data;
		//$this->data['pl'] = $final_order_data-$final_poorder_data;
		$this->render_template('reports/index', $this->data);
	}
	
	public function rackindex()
	{
		if(!in_array('viewReports', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$floor = 1;
		$chamber = 1;
		
		if($this->input->post('chamber')) {
			$floor = $this->input->post('floor');
			$chamber = $this->input->post('chamber');
		}
		
		if($floor == 1 && $chamber == 1) {
			$racks = 20;
		} else {
			$racks = 24;
		}
		
		$this->load->model('model_inward');
		$fill_data = $this->model_inward->getStackBoxNos($floor, $chamber);
		
		$sbox_array = array_column($fill_data, 'isboxes','rack');
		$mbox_array = array_column($fill_data, 'imboxes','rack');
		$lbox_array = array_column($fill_data, 'ilboxes','rack');
		$bag_array = array_column($fill_data, 'ibags','rack');

	//	echo "<br> <br> Fill Data :";
		//print_r($fill_data);
		
		foreach ($fill_data as $value) {
			$free[$value['rack']] =  600 - ($value['isboxes'] + $value['imboxes'] + $value['ilboxes'] + $value['ibags']);
		}
		
		//echo "<br> <br> sbox before filling:";
		
	//	print_r($sbox_array);
		
		for($i=1; $i<=$racks; $i++) {
			if(!isset($sbox_array[$i])) {
				$sbox_array[$i] = 0;
			}
			
			if(!isset($mbox_array[$i])) {
				$mbox_array[$i] = 0;
			}
			
			if(!isset($lbox_array[$i])) {
				$lbox_array[$i] = 0;
			}
			
			if(!isset($bag_array[$i])) {
				$bag_array[$i] = 0;
			}
			
			if(!isset($sbox_array[$i])) {
				$sbox_array[$i] = 0;
			}
			
			if(!isset($free[$i])) {
				$free[$i] = 600;
			} else {
				$free[$i] = $free[$i] > 0 ? $free[$i] : 0;
			}
		}

		ksort($sbox_array);
		ksort($mbox_array);
		ksort($lbox_array);
		ksort($bag_array);
		ksort($free);

		$this->data['sbox_array'] = $sbox_array;
		$this->data['mbox_array'] = $mbox_array;
		$this->data['lbox_array'] = $lbox_array;
		$this->data['bag_array'] = $bag_array;
		$this->data['free_array'] = $free;
		$this->data['racks'] = $racks;
		
		//$this->data['pl'] = $final_order_data-$final_poorder_data;
		$this->render_template('reports/rackindex', $this->data);
	}
}	