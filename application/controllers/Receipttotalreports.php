<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Receipttotalreports extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Receipt Reports';

		$this->load->model('model_receipttotalreports');
		$this->load->model('model_users');
		$this->load->model('model_ledger');
		//$this->load->model('model_products');
		 $this->load->model('model_company');
		//$this->load->model('model_customers');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewOrder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Receipt Reports';
		$filter = array();
		$filter['from_date'] = date("d/m/Y");
		$filter['to_date'] = date("d/m/Y");
				$data = array();
		$report_data = array();
		$filter = array();
		$filter['from_date'] = date("d/m/Y");
		$filter['to_date'] = date("d/m/Y");
		$orderlist = $this->model_receipttotalreports->get_report_list($filter);
		$this->data['orderlist'] = $orderlist;
		$this->render_template('receipttotalreports/index', $this->data);	
	}

	function reportData()
	{
		
		$data['from_date'] = date("d-m-Y");
		$data['to_date'] = date("d-m-Y");

		if($this->input->post('from_date'))
		{
			$from_date = $this->input->post('from_date');
			$data['from_date'] = $this->input->post('from_date');
			 
		}
		if($this->input->post('to_date'))
		{
			$to_date = $this->input->post('to_date');
			$data['to_date'] = $this->input->post('to_date');
			
		}

		$report_data = array();
		$filter = array();
		
		$filter['from_date'] =$data['from_date'];
		$filter['to_date'] = $data['to_date'];
		
	
		$orderlist = $this->model_receipttotalreports->get_report_list($filter);
		$this->data['orderlist'] = $orderlist;
		$this->render_template('receipttotalreports/index', $this->data);	
	}


}