<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inwardreports extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Inward Reports';

		$this->load->model('model_inwardreports');
		$this->load->model('model_inward');
		$this->load->model('model_outward');
		$this->load->model('model_scustomers');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewOrder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Inward Reports';
		$data = array();
		$data['from_date'] = date("d/m/Y");
		$data['to_date'] = date("d/m/Y"); 
		$filter = array();
		$data['customer'] = "";
		$filter['customer'] = "";
		if($this->input->post('from_date'))
		{
			$from_date = $this->input->post('from_date');
			$data['from_date'] = $this->input->post('from_date');
			 
		}
		if($this->input->post('to_date'))
		{
			$to_date = $this->input->post('to_date');
			$data['to_date'] = $this->input->post('to_date');
			
		}
		if($this->input->post('customer'))
		{
			$customer = $this->input->post('customer');
			$data['customer'] = $this->input->post('customer');
			$filter['customer'] = $customer; 
		}
		
		$filter['from_date'] =$data['from_date'];
		$filter['to_date'] = $data['to_date'];
		
	
		$orderlist = $this->model_inwardreports->get_report_list($filter);
	 	
		$i = 0;
		foreach ($orderlist as $key => $value) {
    		$get_box_item = $this->model_inward->getInwardItemData($value['id']);  
    		$odc_no = $this->model_outward->getOutwardDCNo($value['odc_no']);
    		$pbillno = "";
			foreach ($get_box_item as $k => $v) {  
					$value['name'] = $v['boxitem_name'];
					$value['noofunits'] = $v['noofunits'];
					$value['itype'] = $v['itype'] == 1 ? 'Box' : 'Single';
                    $value['odc_no'] =	$odc_no['odc_no'];
					$billno = $value['idc_no'];
					if($pbillno == $billno) {
						$value['odate'] = "";
						$value['odc_no'] = "";
						$value['idate'] = "";
						$value['idc_no'] = "";
						$value['customer_name'] = "";
						$value['totalrent'] = "";
						$value['advance'] = "";
						$value['supplier_name'] = "";
						$value['ph_no'] = "";
					}
					$pbillno = $billno;
				
			     	$orderlist[$i] = $value; 
				    $i++;
				    continue;
			} 
		}
		$this->data['orderlist'] = $orderlist;
		$this->data['fcustomer'] = $data['customer'];
		$this->data['customers'] = $this->model_scustomers->getSupplierData();
		$this->render_template('inwardreports/index', $this->data);	
	}

}