<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rentitemsreports extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Rent Items Reports';	
	}

    /* 
    * It only redirects to the manage product page
    */
	public function index()
	{
        if(!in_array('viewProduct', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->render_template('rentitemsreports/index', $this->data);	
	}
}