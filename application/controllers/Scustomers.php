<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Scustomers extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Customers';
		
		$this->load->model('model_scustomers');
		$this->load->model('model_rentitems');
		$this->load->model('model_outward');
		$this->load->model('model_outwardreports');
		$this->load->model('model_stores');
		$this->load->model('model_inward');
	}

    /* 
    * It only redirects to the manage product page
    */
	public function index()
	{
        if(!in_array('viewCustomer', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->render_template('scustomers/index', $this->data);	
	}

    /*
    * It Fetches the products data from the product table 
    * this function is called from the datatable ajax function
    */
	public function fetchSupplierData()
	{
		$result = array('data' => array());

		$data = $this->model_scustomers->getSupplierData();

		foreach ($data as $key => $value) {

            $store_data = $this->model_stores->getStoresData($value['supl_id']);
			// button
            $buttons = '';
            if(in_array('updateCustomer', $this->permission)) {
    			$buttons .= '<a href="'.base_url('scustomers/update/'.$value['supl_id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
            }

           /* if(in_array('deleteCustomer', $this->permission)) { 
				$buttons .= ' <button type="button" class="btn btn-default deletebutton" onclick="removeFunc('.$value['supl_id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
            }*/	

			$result['data'][$key] = array(
				$value['supp_name'],
				//$value['supp_address'], 
				$value['ph_no'], 
				$value['credit_limit'],
				$value['wallet'],
				$value['credit_limit']-$value['wallet'],
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}	
	
	public function getSupplierData()
	{
		$supl_id = $this->input->post('supl_id');
		if($supl_id) {
			$supplier_data = $this->model_scustomers->getSupplierData($supl_id);
			echo json_encode($supplier_data);
		}
	}

    /*
    * If the validation is not valid, then it redirects to the create page.
    * If the validation for each input field is valid then it inserts the data into the database 
    * and it stores the operation message into the session flashdata and display on the manage product page
    */
	public function create()
	{
		if(!in_array('createCustomer', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		
		$this->form_validation->set_rules('supp_name', 'Supplier name', 'trim|required');
		//$this->form_validation->set_rules('gst_no', 'Enter GST/Pan Number', 'trim|required|is_unique[suppliers.gst_no]');
		$this->form_validation->set_rules('ph_no', 'Enter Phone Number', 'trim|required');
		
		/*$this->form_validation->set_rules('supp_address', 'Address line 1', 'min_length[5]|max_length[28]');
		$this->form_validation->set_rules('supp_address2', 'Address line 2', 'max_length[28]');
		$this->form_validation->set_rules('supp_address3', 'Address line 3', 'max_length[28]');
		$this->form_validation->set_rules('supp_address4', 'Address line 4', 'max_length[28]');*/

        if ($this->form_validation->run() == TRUE) {

        	$create = $this->model_scustomers->create();
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('scustomers/', 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('scustomers/create', 'refresh');
        	}
        }
        else {
        	$this->data['products'] = $this->model_rentitems->getRentitemsData();
            $this->render_template('scustomers/create', $this->data);
        }	
	}
	
	function creport()
	{
		$this->data['page_title'] = 'Customer wise Report';
		$this->data['from_date'] = date("d/m/Y");
		$this->data['to_date'] = date("d/m/Y");
		$this->data['customer'] = "";
		
		if($this->input->post('from_date'))
		{
			$from_date = $this->input->post('from_date');
			$this->data['from_date'] = $this->input->post('from_date');
		}
		if($this->input->post('to_date'))
		{
			$to_date = $this->input->post('to_date');
			$this->data['to_date'] = $this->input->post('to_date');
		}
		$customer = 0; 
	   
		if($this->input->post('customer'))
		{
	   		 $customer = $this->input->post('customer');
	   		 $this->data['customer'] = $this->input->post('customer');
		}
 
		$report_data = array();
		$filter = array();
		$filter['from_date'] = $this->data['from_date'];
		$filter['to_date'] = $this->data['to_date'];
		$filter['customer'] = $this->data['customer'];
		$orders_items = $this->model_scustomers->get_report_list($filter); 
		
		$pbillno = "";
		$i = 0;
			foreach ($orders_items as $k => $value) {   
					$value['itype'] = $value['itype'] == 1 ? 'Box' : 'Single';
 					$value['name'] = $value['boxitem_name'];
					$billno = $value['odc_no'];
					if($pbillno == $billno) {
						$value['sdate'] = "";
						$value['odc_no'] = "";
						$value['customer_name'] = "";
						$value['totalrent'] = "";
						$value['discount'] = "";
						$value['finalamt'] = "";
						$value['advance'] = "";
						$value['supplier_name'] = "";
						$value['ph_no'] = "";
					}
					$pbillno = $billno;
				
			     	$orders_items[$i] = $value; 
				    $i++;
				    continue;
			} 
 
		
	 	/*$order_data = $this->model_inward->getInwardData(1); 
		$orders_items = $this->model_inward->getInwardItemData($order_data['id']);  
	    
		$dc_no = $order_data['idc_no'];
	 
		$i = 0;
		foreach ($orders_items as $key => $value) {
			$itype = $value['itype'];
			$box_id = $value['boxitem_id'];
            $box_name = $value['boxitem_name'];
            $value['boxname'] = $box_name;
            $noofunits = $value['noofunits'];
  			$outunits =  $value['outunits'];
  
			if($itype == 1) {
				$value['name'] = "-";
				$value['itype'] = "Box"; 
				$orders_items[$i] = $value;
				$i++;
	    		$get_box_item = $this->model_inward->getBoxitemsRentQty($box_id);
					foreach ($get_box_item as $k => $v) { 
						
						 
						$value['boxname'] = "''"; 
						$value['itype'] = ""; 
						$value['rate'] = ""; 
						$value['rate'] = ""; 
						$value['period'] = ""; 
						$value['gperiod'] = "";  
						$value['totalrent'] = ""; 
					 
						$rentitem_name = $v['rentitem_name'];
						$qty = $v['qty']; 
 
						$totunits = $noofunits * $qty;
						$value['name'] = $rentitem_name;
						$value['noofunits'] = $totunits;
						$value['outunits'] = $outunits * $qty;
						
				     	$orders_items[$i] = $value; 
					    $i++;
					    continue;
				}
    		} else { 
    			 $value['boxname'] = '-';
    			 $value['itype'] = "Single"; 
			 	 $value['name'] = $value['boxitem_name'];
			 	 $orders_items[$i] = $value;
				$i++;
    		} 
		}*/
		
		 
	 	$this->data['orderlist'] = $orders_items;
	    
	    $this->data['from_date'] = $filter['from_date'];
	    $this->data['to_date'] = $filter['to_date'];
	    $this->data['customers'] = $this->model_scustomers->getSupplierData();
		$this->data['fcustomer'] = $this->data['customer'];
		$this->render_template('scustomers/creport', $this->data);
	}
 
	public function update($supplier_id)
	{      
        if(!in_array('updateCustomer', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

        if(!$supplier_id) {
            redirect('dashboard', 'refresh');
        }
		
      	$this->form_validation->set_rules('supp_name', 'Supplier name', 'trim|required');
		$this->form_validation->set_rules('ph_no', 'Enter Phone Number', 'trim|required');
		
	/*	$this->form_validation->set_rules('supp_address', 'Address line 1', 'min_length[5]|max_length[28]');
		$this->form_validation->set_rules('supp_address2', 'Address line 2', 'max_length[28]');
		$this->form_validation->set_rules('supp_address3', 'Address line 3', 'max_length[28]');
		$this->form_validation->set_rules('supp_address4', 'Address line 4', 'max_length[28]');*/
		
		
        if ($this->form_validation->run() == TRUE) {
            // true case
         $data = array(
      			'supp_name' => $this->input->post('supp_name'),
        		'supp_address' => $this->input->post('supp_address'),
        		'supp_address2' => $this->input->post('supp_address2'),
        		'supp_address3' => $this->input->post('supp_address3'),
        		'supp_address4' => $this->input->post('supp_address4'),
        		'gst_no' => $this->input->post('gst_no'),
        		'ph_no' => $this->input->post('ph_no'),
				'email_id' => $this->input->post('email_id'),
			    'state' => $this->input->post('customer_state'),
				'credit' => $this->input->post('credit'),
				'credit_limit' => $this->input->post('credit_limit'),
				'credit_days' => $this->input->post('credit_days'),
			   	'old_balance' => $this->input->post('old_balance'),
				'wallet' => $this->input->post('credit_limit'),
				'state_code' => $this->input->post('customer_state_code') 
            );

            $update = $this->model_scustomers->update($data, $supplier_id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Successfully updated');
                redirect('scustomers/', 'refresh');
            }
            else {
                $this->session->set_flashdata('errors', 'Error occurred!!');
                redirect('scustomers/update/'.$supplier_id, 'refresh');
            }
        }
        else {
              
        	$supplier_data = $this->model_scustomers->getSupplierData($supplier_id);
        	//print_r($supplier_data);
        	$result['supplier'] = $supplier_data;
         
        	$this->data['supplier_data'] = $result;
        	$this->data['products'] = $this->model_rentitems->getRentitemsData();

            $this->render_template('scustomers/edit', $this->data); 
        }   
	}

	public function custdetailsinsert()
	{
   		$data = array(
			'supp_name' => $this->input->get('mcust_name'),
			'supp_address' => $this->input->get('mcust_address'),
			'supp_address2' => $this->input->get('mcust_address2'),
			'supp_address3' => $this->input->get('mcust_address3'),
			'supp_address4' => $this->input->get('mcust_address4'),
			'gst_no' => $this->input->get('mgst_no'),
			'ph_no' => $this->input->get('mph_no'),
			'credit' => 1,
			'credit_limit' => 10000,
			'credit_days' => 30,
			'old_balance' => 0,
			'wallet' => 10000,
			'email_id' => $this->input->get('memail_id'),
   		    'state' => 'Tamil Nadu',
   		    'state_code' => 33
		);
		
		$insertid = $this->model_scustomers->createcustomer($data);

		if($insertid == '000' || $insertid == '0000')
		{
			$cid = $insertid; 
			 echo "Fail/".$cid;
		} else if($insertid == true) {
			echo "Success/".$insertid; 
		} else {
			echo "Failure";
		} 
	}
	
    /*
    * It removes the data from the database
    * and it returns the response into the json format
    */
	public function remove()
	{
        if(!in_array('deleteCustomer', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
        
        $supplier_id = $this->input->post('supl_id');

        $response = array();
        if($supplier_id) {
            $delete = $this->model_scustomers->remove($supplier_id);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Successfully removed"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error in the database while removing the supplier information";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh the page again!!";
        }

        echo json_encode($response);
	}
}
