<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Driver';

		$this->load->model('model_driver');
	}

	/* 
	* It only redirects to the manage driver page
	*/
	public function index()
	{

		if(!in_array('viewVehicle', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$result = $this->model_driver->getActiveDriver();
		
		$this->data['results'] = $result;

		$this->render_template('driver/index', $this->data);	
	}	

	public function getDriverNames(){
		$keyword=$this->input->post('keyword');
		$data=$this->model_driver->GetRow($keyword);
		echo json_encode($data);
	}
	
	public function fetchDriverDataById($id){
		$data=$this->model_driver->getDriverData($id);
		echo json_encode($data);
	}

	public function getDriverDataById(){
		$id = $this->input->post('id');
		if($id) {
			$data=$this->model_driver->getDriverData($id);
			echo json_encode($data);
		}
	}
	
	/*
	* Fetches the category value from the category table 
	* this function is called from the datatable ajax function
	*/
	public function fetchDriverData()
	{
		$result = array('data' => array());

		$data = $this->model_driver->getDriverData();

		foreach ($data as $key => $value) {

			// button
			$buttons = '';

			if(in_array('updateVehicle', $this->permission)) {
				$buttons .= '<button type="button" class="btn btn-default editbutton" onclick="editFunc('.$value['id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';
			}

			/*	if(in_array('deleteVehicle', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default deletebutton" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
			*/		

			$result['data'][$key] = array(
				$value['truckno'],
				$value['drivername'],
				$value['driverphone'],
			
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	* Its checks the category form validation 
	* and if the validation is successfully then it inserts the data into the database 
	* and returns the json format operation messages
	*/
	public function create()
	{
		if(!in_array('createVehicle', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();
		
		$this->form_validation->set_rules('driver_name', 'Driver name is required', 'trim|required');		
		$this->form_validation->set_rules('driver_phone', 'Unique Driver Phone is required', 'trim|required|is_unique[driver.driverphone]');

		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'drivername' => $this->input->post('driver_name'),
        		'driverphone' => $this->input->post('driver_phone'),	
        		'truckno'=> $this->input->post('truck_no'),	
        	);

        	$create = $this->model_driver->create($data);
        	if($create == true) {
        		$response['success'] = true;
        		$response['messages'] = 'Succesfully created';
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the driver information';			
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }

        echo json_encode($response);
	}


	function check_truck_no() {
		if($this->input->post('id'))
			$id = $this->input->post('id');
		else
			$id = '';
		$truck_no = $this->input->post('edit_truck_no');
		$result = $this->model_driver->check_truck_no($id, $truck_no);
		if($result == 0)
			$response = true;
		else {
			$this->form_validation->set_message('check_truck_no' , 'Truck No must be unique');
			$response = false;
		}
		return $response;
	}
	
	
	/*
	* Its checks the category form validation 
	* and if the validation is successfully then it updates the data into the database 
	* and returns the json format operation messages
	*/
	public function update($id)
	{

		if(!in_array('updateVehicle', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

		if($id) {
				
			$truck_no = $this->input->post('pnoldname');
			$truck_no_change = $this->input->post('pneditstatus');

			if($truck_no_change == "true") {
				$this->form_validation->set_rules('edit_truck_no', 'edit_truck_no', 'required|callback_check_truck_no');
				$truck_no = $this->input->post('edit_truck_no');
			}

			$this->form_validation->set_rules('edit_driver_name', 'Driver name is required', 'trim|required');		
			$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
	        		'drivername' => $this->input->post('edit_driver_name'),
        			'truckno'=> $truck_no,	
        			 'driverphone' => $this->input->post('edit_driver_phone')
	        	);

	        	$update = $this->model_driver->update($data, $id);
	        	if($update == true) {
	        		$response['success'] = true;
	        		$response['messages'] = 'Succesfully updated';
	        	}
	        	else {
	        		$response['success'] = false;
	        		$response['messages'] = 'Error in the database while updated the brand information';			
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}

	/*
	* It removes the category information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		if(!in_array('deleteVehicle', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$driver_id = $this->input->post('driver_id');

		$response = array();
		if($driver_id) {
			$delete = $this->model_driver->remove($driver_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the driver information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}

}