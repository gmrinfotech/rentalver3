<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Company';

		$this->load->model('model_company');
	}

    /* 
    * It redirects to the company page and displays all the company information
    * It also updates the company information into the database if the 
    * validation for each input field is successfully valid
    */
	public function index()
	{  
        if(!in_array('updateCompany', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
        
		$this->form_validation->set_rules('company_name', 'Company name', 'trim|required');
		$this->form_validation->set_rules('address1', 'Address', 'trim|required');
        $this->form_validation->set_error_delimiters('<p class="val-error">','</p>');
		$address1 = $this->input->post('address1');
		$address2 = $this->input->post('address2');
		$address3 = $this->input->post('address3');
		$address4 = $this->input->post('address4');
		$address5 = $this->input->post('address5');
		
		$address = $address1 . "," . $address2. "," . $address3. "," . $address4. "," . $address5;
		
		$company_data = $this->model_company->getCompanyData(1);
		$logo = $company_data['image'];
        if ($this->form_validation->run() == TRUE) {
            // true case

	
        	$data = array(
        		'company_name' => $this->input->post('company_name'),
				'company_gstno' => $this->input->post('company_gstno'),
        		'address' => rtrim($address,","),
        		'phone' => $this->input->post('phone'),
        		'email' => $this->input->post('email'),
        		'country' => $this->input->post('country'),
        		'message' => $this->input->post('message'),
                'currency' => $this->input->post('currency'),
        		'forname' =>  $this->input->post('forname'),
        		'title' =>  $this->input->post('title'),
				'bankactive' => $this->input->post('bankactive'),
        		'printCopy2' => $this->input->post('printCopy2checkid'),
                'printCopy3' => $this->input->post('printCopy3checkid'),
        		'printCopy4' =>  $this->input->post('printCopy4checkid'),
        		'printCopy5' =>  $this->input->post('printCopy5checkid'),
				'printCopy1Title' => $this->input->post('printCopy1Title'),
        		'printCopy2Title' => $this->input->post('printCopy2Title'),
                'printCopy3Title' => $this->input->post('printCopy3Title'),
        		'printCopy4Title' =>  $this->input->post('printCopy4Title'),
        		'printCopy5Title' =>  $this->input->post('printCopy5Title'),
				'productcodeenable' =>  $this->input->post('productcodeenable'),
				'enableproddesc' =>  $this->input->post('productdescenable'),
				'decimalpoints' =>  $this->input->post('decimalpoints'),
        		'discountType' =>  $this->input->post('discountType'),
				'bankname' => $this->input->post('bankname'),
                'accno' => $this->input->post('accno'),
        		'ifsccode' =>  $this->input->post('ifsccode'),
        		'showbal' =>  $this->input->post('showbal'),
        		'chooseCust' =>  $this->input->post('chooseCust'),
        		'branchname' =>  $this->input->post('branchname'),
        		'composition' =>  $this->input->post('composition'),
        		'thermalprint' =>  $this->input->post('thermalprint'),
        		'printcorder' =>  $this->input->post('printcorder'),
        		'A4print' =>  $this->input->post('A4print'),
				'hsncode' =>  $this->input->post('hsncode'),
				'smscode' =>  $this->input->post('smscode'),
				'smsenable' =>  $this->input->post('smsenable'),
				'smssenderid' =>  $this->input->post('smssenderid'),
				'emaildirect' =>  $this->input->post('emaildirect'),
        		'taxsplitup' =>  $this->input->post('taxsplitup')
        		//'discountType' =>  $this->input->post('discountType')
        	);
        	
        	if($_FILES['product_image']['size'] > 0) {
        		$upload_image = $this->upload_image();

        		$upload_image = array('image' => $upload_image);
        	
        		if($logo != "<p>You did not select a file to upload.</p>" ||
        			$logo != "<p>The upload path does not appear to be valid.</p>") {
						unlink($logo);
        			}
        		$this->model_company->update($upload_image, 1);
        	}

        	$update = $this->model_company->update($data, 1);
        	if($update == true) {
        		$this->session->set_flashdata('success', 'Successfully updated');
        		redirect('company/', 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('company/index', 'refresh');
        	}
        }
        else {

            // false case
            $this->data['currency_symbols'] = $this->currency();
            $company_data = $this->model_company->getCompanyData(1);
            
            if($company_data['image'] == "<p>You did not select a file to upload.</p>" ||
        			$company_data['image'] == "<p>The upload path does not appear to be valid.</p>")
            	$company_data['image'] = "assets/images/product_image/imgUpload.jpg";
             
            
            $this->data['company_data'] = $company_data;
            $company_address = $company_data['address'];
            
        	$companyaddrArray = explode(',', $company_address);
        	$this->data['company_addr'] = $companyaddrArray;
			$this->render_template('company/index', $this->data);			
        }
	}
	
	public function getDiscType()
	{
		$disc_type = $this->model_company->getDiscoutType();
		echo json_encode($disc_type);
	}
	
	/*
	 * This function is invoked from another function to upload the image into the assets folder
	 * and returns the image path
	 */
	public function upload_image()
	{
		// assets/images/product_image
		$config['upload_path'] = 'assets/images/logo';
		$config['file_name'] =  uniqid();
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '1000';
	
		// $config['max_width']  = '1024';s
		// $config['max_height']  = '768';
	
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('product_image'))
		{
			$error = $this->upload->display_errors();
			return $error;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$type = explode('.', $_FILES['product_image']['name']);
			$type = $type[count($type) - 1];
	
			$path = $config['upload_path'].'/'.$config['file_name'].'.'.$type;
			return ($data == true) ? $path : false;
		}
	}
	
	public function gmradmin()
	{
		if(!in_array('gmradmin', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
	
		$save = $this->input->post('save');
 
		if ($save == 1) {
			// true case
			$data = array(
					'showpprice' => $this->input->post('showpprice'),
					'subsidy' => $this->input->post('subsidy'), 
					'opticals' => $this->input->post('opticals'),
					'warehouse' => $this->input->post('warehouse'),
					'enableestimate' => $this->input->post('enableestimate'),
					'salesinvpayment' => $this->input->post('salesinvpayment'),
					'custreports' => $this->input->post('custreports'),
					'enablepurchase' =>  $this->input->post('enablepurchase'),
					'enablesales' =>  $this->input->post('enablesales'),
					'custpayment' =>  $this->input->post('custpayment'),
					'purpayment' =>  $this->input->post('purpayment'),
					'csales' => $this->input->post('csales'),
					'mobservice' => $this->input->post('mobservice'),
					'corder' => $this->input->post('corder'),
					'hotel' =>  $this->input->post('hotel'),
					'attendance' =>  $this->input->post('attendance'),
					'redeempoints' => $this->input->post('redeempoints')
			);

			$update = $this->model_company->updateadmin($data, 1);
			if($update == true) {
				$this->session->set_flashdata('success', 'Successfully updated');
				redirect('company/gmradmin', 'refresh');
			}
			else {
				$this->session->set_flashdata('errors', 'Error occurred!!');
				redirect('company/gmradmin', 'refresh');
			}
		}
		else {
 
			$company_data = $this->model_company->getCompanyData(1);
 
			$this->data['company_data'] = $company_data;
	 
			$this->render_template('company/gmradmin', $this->data);
		}
	}
}