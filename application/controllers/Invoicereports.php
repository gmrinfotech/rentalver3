<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoicereports extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Invoice Reports';

		$this->load->model('model_invoicereports');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewOrder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Invoice Reports';
		$filter = array();
		$filter['from_date'] = date("d/m/Y");
		$filter['to_date'] = date("d/m/Y");
				$data = array();
		$report_data = array();
		$filter = array();
		$filter['from_date'] = date("d/m/Y");
		$filter['to_date'] = date("d/m/Y");
		$orderlist = $this->model_invoicereports->get_report_list($filter);
		$this->data['orderlist'] = $orderlist;
		$this->render_template('invoicereports/index', $this->data);	
	}

	function reportData()
	{
		
		$data['from_date'] = date("d-m-Y");
		$data['to_date'] = date("d-m-Y");

		if($this->input->post('from_date'))
		{
			$from_date = $this->input->post('from_date');
			$data['from_date'] = $this->input->post('from_date');
			 
		}
		if($this->input->post('to_date'))
		{
			$to_date = $this->input->post('to_date');
			$data['to_date'] = $this->input->post('to_date');
			
		}

		$report_data = array();
		$filter = array();
		
		$filter['from_date'] =$data['from_date'];
		$filter['to_date'] = $data['to_date'];
		
	
		$orderlist = $this->model_invoicereports->get_report_list($filter);
		$this->data['orderlist'] = $orderlist;
		$this->render_template('invoicereports/index', $this->data);	
	}

}