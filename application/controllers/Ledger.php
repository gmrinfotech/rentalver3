<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ledger extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Ledger';

		$this->load->model('model_ledger');
	}

	/* 
	* It only redirects to the manage category page
	*/
	public function index()
	{

		if(!in_array('viewCategory', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$this->render_template('ledger/index', $this->data);	
	}	

	/*
	* It checks if it gets the category id and retreives
	* the category information from the category model and 
	* returns the data into json format. 
	* This function is invoked from the view page.
	*/
	public function fetchLedgerDataById($id) 
	{
		if($id) {
			$data = $this->model_ledger->getLedgerData($id);
			echo json_encode($data);
		}

		return false;
	}
	public function getLedgerValueById()
	{
		$product_id = $this->input->post('product_id');
		if($product_id) {
			$product_data = $this->model_ledger->getLedgerData($product_id);
			echo json_encode($product_data);
		}
	}

	/*
	* Fetches the category value from the category table 
	* this function is called from the datatable ajax function
	*/
	public function fetchLedgerData()
	{
		$result = array('data' => array());

		$data = $this->model_ledger->getLedgerData();

		foreach ($data as $key => $value) {
			$ledgername_data = $this->model_ledger->getActiveLedgernameData($value['under']);
			// button
			$buttons = '';

			if(in_array('updateLedger', $this->permission)) {
				$buttons .= '<button type="button" class="btn btn-default editbutton" onclick="editFunc('.$value['id'].')" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></button>';
			}

			/* 	if(in_array('deleteLedger', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default deletebutton" onclick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
				*/
			if($value['expense_type']==1)
			{
			    $expense_type="CR";
			}
			else 
			{
			  $expense_type="DR";  
			} 
		
			
			$status = ($value['active'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';

			$result['data'][$key] = array(
				$value['name'],
				//$value['under'],
				$ledgername_data['name'],
				$expense_type,
				$status,
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	* Its checks the category form validation 
	* and if the validation is successfully then it inserts the data into the database 
	* and returns the json format operation messages
	*/
	public function create()
	{
		
		if(!in_array('createLedger', $this->permission)) {
			redirect('dashboard', 'refresh');
		}

		$response = array();

	    $this->form_validation->set_rules('ledger_name', 'Ledger name', 'trim|required|is_unique[ledger.name]');
		
		$this->form_validation->set_error_delimiters('<p class="text-danger">','</p>');

        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'name' => $this->input->post('ledger_name'),
				'under' => $this->input->post('under_category'),
				'expense_type' => $this->input->post('expense_type'),
        		'active' => $this->input->post('active'),	
        	);

        	$create = $this->model_ledger->create($data);
        	if($create == true) {
        		$response['success'] = true;
        		$response['messages'] = 'Succesfully created';
        	}
        	else {
        		$response['success'] = false;
        		$response['messages'] = 'Error in the database while creating the brand information';			
        	}
        }
        else {
        	$response['success'] = false;
        	foreach ($_POST as $key => $value) {
        		$response['messages'][$key] = form_error($key);
        	}
        }

        echo json_encode($response);
	}

	function check_ledger_name() {
		if($this->input->post('id'))
			$id = $this->input->post('id');
		else
			$id = '';
		$ledger_name = $this->input->post('edit_ledger_name');
		$result = $this->model_ledger->check_ledger_name($id, $ledger_name);
		if($result == 0)
			$response = true;
		else {
			$this->form_validation->set_message('check_ledger_name' , 'Ledger name must be unique');
			$response = false;
		}
		return $response;
	}
	
	
	/*
	* Its checks the category form validation 
	* and if the validation is successfully then it updates the data into the database 
	* and returns the json format operation messages
	*/
	public function update($id)
	{  
		$response = array(); 
		if($id) {
		
			$ledger_name = $this->input->post('pnoldname');
        	$ledger_name_change = $this->input->post('pneditstatus');
			
			if($ledger_name_change == "true") {
				$this->form_validation->set_rules('edit_ledger_name', 'edit_ledger_name', 'trim|required|callback_check_ledger_name');
	         	$ledger_name = $this->input->post('edit_ledger_name');
       		} else {
			 	$this->form_validation->set_rules('edit_ledger_name', 'Ledger name', 'required');
			}
			$this->form_validation->set_error_delimiters('<p class="val-error">','</p>');
	        if ($this->form_validation->run() == TRUE) {
	        	$data = array(
	        		'name' => $ledger_name,
					'under' => $this->input->post('edit_under_ledger'),
					'expense_type' => $this->input->post('edit_expense_type'),
	        		'active' => $this->input->post('edit_active'),	
	        	);

	        	$update = $this->model_ledger->update($data, $id);
	        	if($update == true) {
	        		$response['success'] = true;
	        		$response['messages'] = 'Succesfully updated';
	        	}
	        	else {
	        		$response['success'] = false;
	        		$response['messages'] = 'Error in the database while updated the Ledger information';			
	        	}
	        }
	        else {
	        	$response['success'] = false;
	        	foreach ($_POST as $key => $value) {
	        		$response['messages'][$key] = form_error($key);
	        	}
	        }
		}
		else {
			$response['success'] = false;
    		$response['messages'] = 'Error please refresh the page again!!';
		}

		echo json_encode($response);
	}

	/*
	* It removes the category information from the database 
	* and returns the json format operation messages
	*/
	public function remove()
	{
		if(!in_array('deleteLedger', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
		
		$category_id = $this->input->post('category_id');

		$response = array();
		if($category_id) {
			$delete = $this->model_ledger->remove($category_id);
			if($delete == true) {
				$response['success'] = true;
				$response['messages'] = "Successfully removed";	
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error in the database while removing the brand information";
			}
		}
		else {
			$response['success'] = false;
			$response['messages'] = "Refersh the page again!!";
		}

		echo json_encode($response);
	}

}