<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inward extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Inward';
		$this->load->model('model_outward');
		$this->load->model('model_inward');
		$this->load->model('model_users');
		$this->load->model('model_rentitems');
		$this->load->model('model_driver');
		$this->load->model('model_company');
		$this->load->model('model_scustomers');
		$this->load->model('model_employee');
		//$this->load->model('model_subscustomers');
		$this->load->model('model_invoiceprefixs');
		$this->load->library('Sim');
		$this->load->library('mypdf');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewInward', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Manage Inward';
		$this->render_template('inward/index', $this->data);	
	}

	/*
	* Fetches the orders data from the orders table 
	* this function is called from the datatable ajax function
	*/
	public function fetchInwardData()
	{
		$result = array('data' => array());

		$data = $this->model_inward->getInwardData();

		foreach ($data as $key => $value) {
			$user_id = $this->model_users->getUserData($value['user_id']);
			$date = date('d-m-Y', $value['date_time']);
			$time = date('h:i a', $value['date_time']);

			$odc_no = $this->model_outward->getOutwardDCNo($value['odc_no']);

			$date_time = $date . ' ' . $time;

			// button
			$buttons = '';
		if(in_array('viewOrder', $this->permission)) {
				$buttons .= '<a target="__blank" href="'.base_url('inward/printDiv/'.$value['id']).'" class="btn btn-default printbutton"><i class="fa fa-print"></i></a>';
			}
		/*	if(in_array('viewOrder', $this->permission)) {
				$buttons .= '<a target="__blank" href="'.base_url('inward/printDiv/'.$value['id']).'" class="btn btn-default printbutton"><i class="fa fa-print"></i></a>';
			}

		    if(in_array('updateOrder', $this->permission)) {
				$buttons .= ' <a href="'.base_url('inward/`/'.$value['id']).'" class="btn btn-default"><i class="fa fa-money"></i></a>';
			}*/
			
			if(in_array('updateOrder', $this->permission)) {
				$buttons .= ' <a href="'.base_url('inward/update/'.$value['id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
			}

			if(in_array('viewOrder1', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default smsbutton" onClick="smsFunc('.$value['id'].')" data-toggle="modal" data-target="#smsModal" title="Send SMS"><i class="fa fa-commenting-o "></i></button>';
			}
			
			if(in_array('viewOrder1', $this->permission)) {
				$buttons .= '&nbsp;<button type="button" class="btn btn-default mailbutton"
						onClick="mailFunc('.$value['id'].')" data-toggle="modal" data-target="#mailModal"
					    title="Send Email"><i class="fa fa-envelope-o "></i></button>';
			
			}
		/*  if(in_array('deleteOrder', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default deletebutton" onClick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}*/

			$result['data'][$key] = array(
				$value['idate'],
				$value['idc_no'],
				$odc_no['odc_no'],
				//$value['idate'], 
				$value['supplier_name'],
			    $value['contNo'],
				//$value['drivername'],
				//$date_time,
				$value['total_items'],
				$value['totalrent'],
				$user_id['firstname'],
				$buttons
			);
		} // /foreach
		echo json_encode($result);
	}

	/*
	* If the validation is not valid, then it redirects to the create page.
	* If the validation for each input field is valid then it inserts the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function create()
	{
		if(!in_array('createInward', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->data['page_title'] = 'Add Inward';

		$this->form_validation->set_rules('boxitem_name[]', 'Item name', 'trim|required');
		
	
        if ($this->form_validation->run() == TRUE) {        	
        	
        	$order_id = $this->model_inward->create();
        	
        	if($order_id) {
				//$this->pdfemailcreate($order_id);
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('inward/update/'.$order_id, 'refresh');
        		//redirect('inward/', 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('inward/create/', 'refresh');
        	}
        }
        else {
            // false case
        	$this->data['outwardDC'] = $this->model_outward->getPresentOutwarditemsData(); 
        	$this->data['products'] = $this->model_rentitems->getRentitemsData();      
        		
            $this->data['suppliers'] = $this->model_scustomers->getSupplierData();  
            $this->data['drivers'] = $this->model_driver->getActiveDriver();
            $this->data['employees'] = $this->model_employee->getActiveEmployee();

            $this->render_template('inward/create', $this->data);
        }	
	}

	/*
	* If the validation is not valid, then it redirects to the edit orders page 
	* If the validation is successfully then it updates the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function update($id)
	{
		if(!in_array('updateInward', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		if(!$id) {
			redirect('dashboard', 'refresh');
		}

		$this->data['page_title'] = 'Update Inward';

		$this->form_validation->set_rules('boxitem_name[]', 'Item name', 'trim|required');
		
	
        if ($this->form_validation->run() == TRUE) {        	
        	
        	$update = $this->model_inward->update($id);
        	
        	if($update == true) {
        		$this->session->set_flashdata('success', 'Successfully updated');
        		redirect('inward/update/'.$id, 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('inward/update/'.$id, 'refresh');
        	}
        }
        else {
            // false case

        	$result = array();
        	$inward_data = $this->model_inward->getInwardData($id);
        	$odc_no = $this->model_outward->getOutwardDCNo($inward_data['odc_no']);
       
    		$result['inward'] = $inward_data;
    		$result['inward']['odc_no'] = $odc_no['odc_no'];
    	    $result['inward']['odc_id'] = $inward_data['odc_no'];
    	    
    		$inward_items = $this->model_inward->getInwardItemData($inward_data['id']);

    		foreach($inward_items as $k => $v) {
    		//	$v['unitname'] = $this->model_outward->getUnitsName($v['units']);
    		//	$v['floorname'] = $this->model_outward->getFloorName($v['floor']);
    		//	$v['chambername'] = $this->model_outward->getChamberName($v['chamber']);
    			$result['inward_item'][] = $v;
    		}
    		
    		$estedInward = $this->model_inward->getInEstData($id);
            
           	$estedInward = $estedInward > 0 ? true : false;
 			$this->data['editen'] = false;
			$today = date('d/m/Y');  
    		if($estedInward) {
    			$this->data['editen'] = false;
    		} else if($inward_data['idate'] == $today) {
				$this->data['editen'] = true;
			} else {
    			$user_id = $this->session->userdata('id');
				$is_admin = ($user_id == 1 || $user_id == 2) ? true :false;
    			if($is_admin) {
	    			//$today = date('d/m/Y');  
					//$sdate = str_replace('/', '-', $today);
					//$previous_date = date('d/m/Y', strtotime('-1 day', strtotime($sdate)));
					//if($inward_data['idate'] == $previous_date) {
		    			$this->data['editen'] = true;
		    		//}  
    			}
    		}
    		
    		$this->data['inward_data'] = $result;
    	    $this->data['employees'] = $this->model_employee->getActiveEmployee();
           
            $this->render_template('inward/edit', $this->data);
        }
	}

	/*
	* It removes the data from the database
	* and it returns the response into the json format
	*/
	public function remove()
	{
		if(!in_array('deleteInward', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$order_id = $this->input->post('order_id');

        $response = array();
        if($order_id) {
            $delete = $this->model_inward->remove($order_id);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Successfully removed"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error in the database while removing the product information";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh the page again!!";
        }

        echo json_encode($response); 
	}
    
	public function sendSMS()
	{
		$id = $this->input->post('id');
		$response = array();
		if($id) {
			$sent = $this->model_inward->updatesms($id);
			if($sent == true) {
				$response['success'] = true;
				$response['messages'] = "SMS Successfully sent";
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error occurred while sending SMS";
			}
		} else {
			$response['success'] = false;
			$response['messages'] = "Refresh the page again!!";
		}
		echo json_encode($response);
	}
	
	public function sendemail()
	{
		$id = $this->input->post('id');
		$response = array();
		if($id) {
			$sent = $this->pdfemailcreate($id);
			if($sent == true) {
				$response['success'] = true;
				$response['messages'] = "Email Successfully sent";
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error occurred while sending Email";
			}
		} else {
			$response['success'] = false;
			$response['messages'] = "Refresh the page again!!";
		}
		echo json_encode($response);
	}
	
	public function getPdf($id,$pdf, $order_data)
	{  
		$orders_items = $this->model_inward->getInwardItemData($order_data['id']);  
		$company_info = $this->model_company->getCompanyData(1);
 		 
		$dc_no = $order_data['idc_no'];
		$buyeraddr= $this->model_scustomers->getSupplierAddress($order_data['sid']);
 
		$i = 0;
		foreach ($orders_items as $key => $value) {
			$itype = $value['itype'];
			$box_id = $value['boxitem_id'];
            $box_name = $value['boxitem_name'];
            $value['boxname'] = $box_name;
            $noofunits = $value['noofunits'];
  			$outunits =  $value['outunits'];
  
			if($itype == 1) {
				$value['name'] = "-";
				$value['itype'] = "Box"; 
				$orders_items[$i] = $value;
				$i++;
	    		$get_box_item = $this->model_inward->getBoxitemsRentQty($box_id);
					foreach ($get_box_item as $k => $v) { 
						
						 
						$value['boxname'] = "''"; 
						$value['itype'] = ""; 
						$value['rate'] = ""; 
						$value['rate'] = ""; 
						$value['period'] = ""; 
						$value['gperiod'] = "";  
						$value['totalrent'] = ""; 
					 
						$rentitem_name = $v['rentitem_name'];
						$qty = $v['qty']; 
 
						$totunits = $noofunits * $qty;
						$value['name'] = $rentitem_name;
						$value['noofunits'] = $totunits;
						$value['outunits'] = $outunits * $qty;
						
				     	$orders_items[$i] = $value; 
					    $i++;
					    continue;
				}
    		} else { 
    			 $value['boxname'] = '-';
    			 $value['itype'] = "Single"; 
			 	 $value['name'] = $value['boxitem_name'];
			 	 $orders_items[$i] = $value;
				$i++;
    		} 
		}

	//	print_r($orders_items);
		// create new PDF document
		//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		$title="INWARD COPY";
		$pdf->createpdf($id, $pdf, $order_data, $orders_items, $company_info, $buyeraddr, 
					$title,$dc_no,false);
		
		return $pdf; 
	}
    
	public function printDiv($id)
	{ 
		$pdf = new CI_MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$order_data = $this->model_inward->getInwardData($id);
		$this->getPdf($id,$pdf, $order_data); 
		$pdf->Output($id.'.pdf', 'I'); 
	}
	
	public function pdfemailcreate($id)
	{
		$pdf = new CI_MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$order_data = $this->model_inward->getInwardData($id);
		$buyeremail = $order_data['semail_id'].",magnumwebtech@gmail.com";
		$this->getPdf($id,$pdf, $order_data); 
		$pdf->Output(FCPATH.'/newpdf/'.$order_data['idc_no'].'.pdf', "F");
       // $pdf->Output(FCPATH. '/newpdf/2.pdf', 'F');
        $this->load->library('email');
        $this->email->from('ksk@gstsoftwareincoimbatore.com', 'kskcoldstorage.com');
        $this->email->to($buyeremail); 
          $this->email->subject('Invoice From GMR');
        $this->email->message('Invoice Copy From GMR(website.com)');   
        $this->email->attach(FCPATH.'/newpdf/'.$order_data['idc_no'].'.pdf');
        $this->email->send();
		$file=FCPATH.'/newpdf/'.$order_data['idc_no'].'.pdf';
		unlink($file);
		
	}
}