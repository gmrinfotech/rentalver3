<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Outwardreports extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Outward Reports';

		$this->load->model('model_outwardreports');
		$this->load->model('model_outward');
		$this->load->model('model_scustomers');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewOrder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Outward Reports';
		$data = array();
		$data['from_date'] = date("d/m/Y");
		$data['to_date'] = date("d/m/Y"); 
		
		$filter = array();
		$data['customer'] = "";
		$filter['customer'] = "";
		if($this->input->post('from_date'))
		{
			$from_date = $this->input->post('from_date');
			$data['from_date'] = $this->input->post('from_date');
			 
		}
		if($this->input->post('to_date'))
		{
			$to_date = $this->input->post('to_date');
			$data['to_date'] = $this->input->post('to_date');
			
		}
		if($this->input->post('customer'))
		{
			$customer = $this->input->post('customer');
			$data['customer'] = $this->input->post('customer');
			$filter['customer'] = $customer; 
		}
		
		$filter['from_date'] = $data['from_date'];
		$filter['to_date'] = $data['to_date'];
 
		$orderlist = $this->model_outwardreports->get_report_list($filter);
 		
		$i = 0;
		foreach ($orderlist as $key => $value) {
    		$get_box_item = $this->model_outward->getOutwardItemData($value['id']);  
    		$pbillno = "";
			foreach ($get_box_item as $k => $v) {  
					$value['name'] = $v['boxitem_name'];
					$value['noofunits'] = $v['noofunits'];
					$value['itype'] = $v['itype'] == 1 ? 'Box' : 'Single';
 
					$billno = $value['odc_no'];
					if($pbillno == $billno) {
						$value['sdate'] = "";
						$value['odc_no'] = "";
						$value['customer_name'] = "";
						$value['totalrent'] = "";
						$value['advance'] = "";
						$value['supplier_name'] = "";
						$value['ph_no'] = "";
					}
					$pbillno = $billno;
				
			     	$orderlist[$i] = $value; 
				    $i++;
				    continue;
			} 
		}
  	
		$this->data['orderlist'] = $orderlist;
		$this->data['fcustomer'] = $data['customer'];
		$this->data['customers'] = $this->model_scustomers->getSupplierData();
		$this->render_template('outwardreports/index', $this->data);	
	}
}