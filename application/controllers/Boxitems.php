<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Boxitems extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Box Items';

		$this->load->model('model_boxitems'); 
		$this->load->model('model_category');
		$this->load->model('model_stores');
		$this->load->model('model_attributes');
		$this->load->model('model_invoiceprefixs');
		$this->load->model('model_rentitems');
	}

	
	public function getBoxItemValueById()
	{
		$product_id = $this->input->post('product_id');
		if($product_id) {
			$product_data = $this->model_boxitems->getBoxitemsData($product_id);
			echo json_encode($product_data);
		}
	}
	
	public function getBoxRentItem()
	{
		$boxitem_id = $this->input->post('boxitem_id');
		if($boxitem_id) {
			$boxitem_data = $this->model_boxitems->getBoxitems($boxitem_id);
			echo json_encode($boxitem_data);
		}
	}
	
	public function getBoxItems()
	{  
		$boxitem_data = $this->model_boxitems->getBoxitemsData();
		echo json_encode($boxitem_data); 
	}
	
    /* 
    * It only redirects to the manage product page
    */
	public function index()
	{
        if(!in_array('viewProduct', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->render_template('boxitems/index', $this->data);	
	}

    /*
    * It Fetches the products data from the product table 
    * this function is called from the datatable ajax function
    */
	public function fetchBoxitemsData()
	{
		$result = array('data' => array());

		$data = $this->model_boxitems->getBoxitemsData();

		
		foreach ($data as $key => $value) {

			$bitems = $this->model_boxitems->getBoxitems($value['id']);
			$titems = count($bitems);
			// button
            $buttons = '';
            if(in_array('updateProduct', $this->permission)) {
    			$buttons .= '<a href="'.base_url('boxitems/update/'.$value['id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
            }
 
			$result['data'][$key] = array(
				$value['code'],	 
				$value['name'],
				$value['minrentaldays'],	 
				$value['rate'],
				$value['rateperday'],
				$titems,
				$buttons
			);
		} // /foreach
		echo json_encode($result);
	}	
 
	public function create()
	{
		if(!in_array('createProduct', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
 
		$this->form_validation->set_rules('boxitem_name', 'Box item name', 'trim|required|is_unique[boxitems.name]');

        if ($this->form_validation->run() == TRUE) {
       
        	$create = $this->model_boxitems->create();
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('boxitems/', 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('boxitems/create', 'refresh');
        	}
        }
        else {
        	$this->data['products'] = $this->model_rentitems->getTypeRentitemsData(1);
            $this->render_template('boxitems/create', $this->data);
        }
	}

    function check_product_name() {
    	if($this->input->post('id'))
    		$id = $this->input->post('id');
    	else
    		$id = '';
    		$product_name = $this->input->post('boxitem_name');
    		$result = $this->model_boxitems->check_product_name($id, $product_name);
    	if($result == 0)
    		$response = true;
    	else {
    		$this->form_validation->set_message('check_product_name' , 'Box item name must be unique');
    		$response = false;
    	}
    	return $response;
    }
 
	public function update($product_id)
	{      
        if(!in_array('updateProduct', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

        if(!$product_id) {
            redirect('dashboard', 'refresh');
        }
        
        $product_name = $this->input->post('pnoldname');
        $product_name_change = $this->input->post('pneditstatus');
       
	 	if($product_name_change == "true") {
        	$this->form_validation->set_rules('boxitem_name', 'boxitem_name', 'required|callback_check_product_name');
        	$product_name = $this->input->post('boxitem_name');
        }

        $this->form_validation->set_rules('boxitem_code', 'boxitem_code', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
    
        	$data = array(
				'code' => $this->input->post('boxitem_code'), 
				'name' => $product_name,
	        	'minrentaldays' => $this->input->post('minrentaldays'),  
				'rate' => $this->input->post('rate'),  
				'rateperday' => $this->input->post('rateperday')
			);
		
            $update = $this->model_boxitems->update($data, $product_id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Successfully updated');
                redirect('boxitems/', 'refresh');
            }
            else {
                $this->session->set_flashdata('errors', 'Error occurred!!');
                redirect('boxitems/update/'.$product_id, 'refresh');
            }
        }
        else {
            $this->data['products'] = $this->model_rentitems->getTypeRentitemsData(1);
 
	        $result = array();
        	$product_data = $this->model_boxitems->getBoxitemsData($product_id);

    		$result['box'] = $product_data;
    		$bitems = $this->model_boxitems->getBoxitems($product_data['id']);

    		foreach($bitems as $k => $v) {
    			$result['box_item'][] = $v;
    		}
 
    		$this->data['box_data'] = $result; 
            $this->render_template('boxitems/edit', $this->data); 
        }   
	}
 
    /*
    * It removes the data from the database
    * and it returns the response into the json format
    */
	public function remove()
	{
        if(!in_array('deleteProduct', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
        
        $product_id = $this->input->post('product_id');

        $response = array();
        if($product_id) {
            $delete = $this->model_boxitems->remove($product_id);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Successfully removed"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error in the database while removing the product information";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh the page again!!";
        }

        echo json_encode($response);
	}

}