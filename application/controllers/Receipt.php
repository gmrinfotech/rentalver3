<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Receipt extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Receipt';

		$this->load->model('model_receipt');
		$this->load->model('model_products');
		$this->load->model('model_ledger');
		
		$this->load->model('model_customers');
		$this->load->model('model_users');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewLedger', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Manage Receipt';
		$this->render_template('receipt/index', $this->data);	
	}

	/*
	* Fetches the orders data from the orders table 
	* this function is called from the datatable ajax function
	*/
	public function fetchReceiptData()
	{
	
		$result = array('data' => array());

		$data = $this->model_receipt->getReceiptDataByLedger();

		foreach ($data as $key => $value) {
		$user_id = $this->model_users->getUserData($value['user_id']);


			// button
			$buttons = '';

			/*if(in_array('viewLedger', $this->permission)) {
				$buttons .= '<a target="__blank" href="'.base_url('receipt/printDiv/'.$value['id']).'" class="btn btn-default"><i class="fa fa-print"></i></a>';
			}*/

			if(in_array('updateLedger', $this->permission)) {
				$buttons .= ' <a href="'.base_url('receipt/update/'.$value['id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
			}

			/*	if(in_array('deleteLedger', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default" onClick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}
*/
		

			$result['data'][$key] = array(
				$value['sdate'],
				$value['receipt_no'],
				$value['income'],
				$value['capital'],
				$user_id['firstname'],
				$value['others'],
				//$date_time,
				
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	* If the validation is not valid, then it redirects to the create page.
	* If the validation for each input field is valid then it inserts the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function create()
	{
		if(!in_array('createLedger', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->data['page_title'] = 'Add Receipt';

		$this->form_validation->set_rules('receipt_no', 'Receipt No', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="val-error">','</p>');
	
        if ($this->form_validation->run() == TRUE) {        	
        	
        	$order_id = $this->model_receipt->create();
        	
        	if($order_id) {
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('receipt/update/'.$order_id, 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('error', 'Error occurred!!');
        		redirect('receipt/create/', 'refresh');
        	}
        }
        else {
            // false case
          	//$this->data['ledgers'] = $this->model_ledger->getActiveLedger("1,3");  
			$this->data['ledgers'] = $this->model_ledger->getActiveLedgerCrDr("1");     	
            $this->data['customers'] = $this->model_customers->getCustomerData();  
          
            $this->render_template('receipt/create', $this->data);
        }	
	}

	/*
	* It gets the product id passed from the ajax method.
	* It checks retrieves the particular product data from the product id 
	* and return the data into the json format.
	*/
	public function getProductValueById()
	{
		$product_id = $this->input->post('product_id');
		if($product_id) {
			$product_data = $this->model_products->getProductData($product_id);
			echo json_encode($product_data);
		}
	}
 public function getLedgerValueById()
	{
		$product_id = $this->input->post('product_id');
		if($product_id) {
			$product_data = $this->model_ledger->getLedgerData($product_id);
			echo json_encode($product_data);
		}
	}
	
	

    public function getCustomerData()
	{
		$cust_id = $this->input->post('cust_id');
		if($cust_id) {
			 $customer_data = $this->model_customers->getProductData($cust_id);
			echo json_encode($customer_data);
		}
	}
	/*
	* It gets the all the active product inforamtion from the product table 
	* This function is used in the order page, for the product selection in the table
	* The response is return on the json format.
	*/
	public function getTableProductRow()
	{           
		$products = $this->model_ledger->getActiveLedger("1,3");
		echo json_encode($products);
	}

	function get_featured_json()
	{ 
		$product_data = $this->model_ledger->getActiveLedger("1,3");  
		$this->data['featured_products'] = $product_data;
		
		$this->load->view('billing/featured-products', $this->data);
	}

	/*
	* If the validation is not valid, then it redirects to the edit orders page 
	* If the validation is successfully then it updates the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function update($id)
	{
		if(!in_array('updateLedger', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		if(!$id) {
			redirect('dashboard', 'refresh');
		}

		$this->data['page_title'] = 'Update Receipt';

		$this->form_validation->set_rules('product[]', 'Product name', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="val-error">','</p>');
	
        if ($this->form_validation->run() == TRUE) {        	
        	
        	$update = $this->model_receipt->update($id);
        	
        	if($update == true) {
        		$this->session->set_flashdata('success', 'Successfully updated');
        		redirect('receipt/update/'.$id, 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('error', 'Error occurred!!');
        		redirect('receipt/update/'.$id, 'refresh');
        	}
        }
        else {
            // false case
         
        	$result = array();
        	$orders_data = $this->model_receipt->getReceiptData($id);

    		$result['order'] = $orders_data;
    		$orders_item = $this->model_receipt->getOrdersItemData($orders_data['id']);

    		foreach($orders_item as $k => $v) {
    			$result['order_item'][] = $v;
    		}

    		$this->data['order_data'] = $result;

        	$this->data['ledgers'] = $this->model_ledger->getActiveLedger("1,3");          	
 			$this->data['customers'] = $this->model_customers->getCustomerData(); 
            $this->render_template('receipt/edit', $this->data);
        }
	}

	/*
	* It removes the data from the database
	* and it returns the response into the json format
	*/
	public function remove()
	{
		if(!in_array('deleteLedger', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$order_id = $this->input->post('order_id');

        $response = array();
        if($order_id) {
            $delete = $this->model_receipt->remove($order_id);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Successfully removed"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error in the database while removing the product information";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh the page again!!";
        }

        echo json_encode($response); 
	}
}