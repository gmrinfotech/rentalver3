<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Estimate extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Inward';
		$this->load->model('model_estimate');
		$this->load->model('model_outward');
		$this->load->model('model_users');
		$this->load->model('model_inward');
		$this->load->model('model_rentitems');
		$this->load->model('model_driver');
		$this->load->model('model_company');
		$this->load->model('model_scustomers');
		$this->load->model('model_invoiceprefixs');
		$this->load->library('Sim');
		$this->load->library('mypdf');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewEstimate', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Manage Estimate';
		$this->render_template('estimate/index', $this->data);	
	}

	/*
	* Fetches the orders data from the orders table 
	* this function is called from the datatable ajax function
	*/
	public function fetchInvoiceData()
	{
		$result = array('data' => array());

		$data = $this->model_estimate->getInvoiceData();

		foreach ($data as $key => $value) {
			$user_id = $this->model_users->getUserData($value['user_id']);
			$idc_no = $this->model_outward->getOutwardDCNo($value['odc_no']);
			
			// button
			$buttons = '';
			
			/*$date = date('d-m-Y', $value['sdate']);
			$time = date('h:i a', $value['sdate']);

			$idc_no = $this->model_outward->getOutwardDCNo($value['idc_no']);

			$date_time = $date . ' ' . $time;

			*/
			if($value['balance'] <= 0) {
				$paid_status = '<span class="label label-success">Paid</span>';	
			}
			else {
				$paid_status = '<span class="label label-warning">Not Paid</span>';
			}

			if(in_array('viewEstimate', $this->permission)) {
				$buttons .= '<a target="__blank" href="'.base_url('estimate/printDiv/'.$value['id']).'" class="btn btn-default printbutton"><i class="fa fa-print"></i></a>';
			}

			if(in_array('updateEstimate', $this->permission)) {
				$buttons .= ' <a href="'.base_url('estimate/update/'.$value['id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
			}

			if(in_array('viewEstimate1', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default smsbutton" onClick="smsFunc('.$value['id'].')" data-toggle="modal" data-target="#smsModal" title="Send SMS"><i class="fa fa-commenting-o "></i></button>';
			}
			
			if(in_array('viewEstimate1', $this->permission)) {
				$buttons .= '&nbsp;<button type="button" class="btn btn-default mailbutton"
						onClick="mailFunc('.$value['id'].')" data-toggle="modal" data-target="#mailModal"
					    title="Send Email"><i class="fa fa-envelope-o "></i></button>';
				//$buttons .= '&nbsp;<a target="__blank" href="'.base_url('orders/pdfemailcreate/'.$value['id']).'" class="btn btn-default mailbutton" title="Send Email"><i class="fa fa-envelope-o"></i></a>';
			}
		/*  if(in_array('deleteEstimate', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default deletebutton" onClick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}*/
			
			if($value['invno'] != "") {
				$eno = $value['invno'];
				$billno =  $value['invoice_id'].' <span class="label label-warning">'.$eno.'</span>';
			} else {
				$billno = $value['invoice_id'];
			}

			$result['data'][$key] = array(
				$billno,
				$value['sdate'],
				$idc_no['odc_no'],
				$value['odate'],
				$value['supplier_name'],
				//$value['total_items'],
				//$value['total_sboxes'],
				$value['net_amount'],
				$value['amtpaid'],
				$value['balance'],
				$value['itotalcbal'],
				$paid_status,
				$user_id['firstname'],
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	 * If the validation is not valid, then it redirects to the create page.
	 * If the validation for each input field is valid then it inserts the data into the database
	 * and it stores the operation message into the session flashdata and display on the manage group page
	 */
	public function create()
	{
		if(!in_array('createEstimate', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
	
		$this->data['page_title'] = 'Add Estimate';
	
		$this->form_validation->set_rules('outward_no', 'Outward No', 'trim|required');
	
		if ($this->form_validation->run() == TRUE) {
			 
			$order_id = $this->model_estimate->create();
			 
			if($order_id) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('estimate/update/'.$order_id, 'refresh');
				//redirect('inward/', 'refresh');
			}
			else {
				$this->session->set_flashdata('errors', 'Error occurred!!');
				redirect('estimate/create/', 'refresh');
			}
		}
		else {
			// false case
			$this->data['outwardDC'] = $this->model_outward->getClosedOutwarditemsData();
		
			$this->render_template('estimate/create', $this->data);
		}
	}
	
	/*
	* If the validation is not valid, then it redirects to the edit orders page 
	* If the validation is successfully then it updates the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function update($id)
	{
		if(!in_array('updateEstimate', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		if(!$id) {
			redirect('dashboard', 'refresh');
		}
		if (isset($_POST['convertSales_button'])) {  
			$this->createorder(); 
		}  
		else  {  
			$this->data['page_title'] = 'Update Estimate';
			
			$this->form_validation->set_rules('outward_no', 'Outward No', 'trim|required');
			
	        if ($this->form_validation->run() == TRUE) {     
	        	$update = $this->model_estimate->update($id);
	        	
	        	if($update == true) {
	        		$this->session->set_flashdata('success', 'Successfully updated');
	        		redirect('estimate/update/'.$id, 'refresh');
	        	}
	        	else {
	        		$this->session->set_flashdata('errors', 'Error occurred!!');
	        		redirect('estimate/update/'.$id, 'refresh');
	        	}
	        }
	        else {
	            // false case
	
	        	$result = array();
	        	$this->data['outwardDC'] = $this->model_outward->getClosedOutwarditemsData();
	        	$invoice_data = $this->model_estimate->getInvoiceData($id);
	
	        	$outward_data = $this->model_outward->getOutwardData($invoice_data['odc_no']);
	        	
	    		$result['invoice'] = $invoice_data;
	    		$result['invoice']['outward_data'] = $outward_data;
	
	    		$invoice_items = $this->model_estimate->getInvoiceItemData($invoice_data['id']);
	
	    		foreach($invoice_data as $k => $v) {
	    			$result['invoice_item'][] = $v;
	    		}
	
	    		$this->data['invoice_data'] = $result;
	    		
	       	 	$this->render_template('estimate/edit', $this->data);
	        }
		}
	}
	
	public function getDetailsByEstNo()
	{
		$id = $this->input->post('id');
		if($id) {
			$data = $this->model_estimate->search($id);
			echo json_encode($data);
		}
	} 
	
	public function sendSMS()
	{
		$id = $this->input->post('id');
		$response = array();
		if($id) {
			$sent = $this->model_estimate->updatesms($id);
			if($sent == true) {
				$response['success'] = true;
				$response['messages'] = "SMS Successfully sent";
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error occurred while sending SMS";
			}
		} else {
			$response['success'] = false;
			$response['messages'] = "Refresh the page again!!";
		}
		echo json_encode($response);
	}
		
	public function createorder()
	{
		
		if(!in_array('createOrder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->data['page_title'] = 'Create Invoice';

		$this->form_validation->set_rules('outward_no', 'Outward No', 'trim|required');
        if ($this->form_validation->run() == TRUE) {     
        	$this->load->model('model_invoice');   	
        	$order_id = $this->model_invoice->create();
        	
        	if($order_id) {
			   // $this->pdfemailcreate($order_id);
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('invoice/update/'.$order_id, 'refresh');
        	}
        	else {
				$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('invoice/create/', 'refresh');
        	}
        }
        else {
	       // false case
			$this->data['outwardDC'] = $this->model_outward->getClosedOutwarditemsData();
		
			$this->render_template('invoice/create', $this->data);
        }
	}
	
	public function sendemail()
	{
		$id = $this->input->post('order_id');
		$response = array();
		if($id) {
			$sent = $this->pdfemailcreate($id);
			if($sent == true) {
				$response['success'] = true;
				$response['messages'] = "Email Successfully sent";
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error occurred while sending Email";
			}
		} else {
			$response['success'] = false;
			$response['messages'] = "Refresh the page again!!";
		}
		echo json_encode($response);
	}
 
	public function printDiv($id)
	{
		$pdf = new CI_MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$order_data = $this->model_estimate->getInvoiceData($id);
		$this->getPdf($id, $pdf, $order_data);
		//	$pdf->ob_end_clean();
		$pdf->Output($id.'.pdf', 'I');
	}
	
	public function getInwardPdf($id,$pdf, $order_data)
	{  
		$orders_items = $this->model_inward->getInwardItemData($order_data['id']);  
		$company_info = $this->model_company->getCompanyData(1);
 		 
		$dc_no = $order_data['idc_no'];
		$buyeraddr= $this->model_scustomers->getSupplierAddress($order_data['sid']);
 
		$i = 0;
		foreach ($orders_items as $key => $value) {
			$itype = $value['itype'];
			$box_id = $value['boxitem_id'];
            $box_name = $value['boxitem_name'];
            $value['boxname'] = $box_name;
            $noofunits = $value['noofunits'];
  			$outunits =  $value['outunits'];
  
			if($itype == 1) {
				$value['name'] = "-";
				$value['itype'] = "Box"; 
				$orders_items[$i] = $value;
				$i++;
	    		$get_box_item = $this->model_inward->getBoxitemsRentQty($box_id);
					foreach ($get_box_item as $k => $v) { 
						
						 
						$value['boxname'] = "''"; 
						$value['itype'] = ""; 
						$value['rate'] = ""; 
						$value['rate'] = ""; 
						$value['period'] = ""; 
						$value['gperiod'] = "";  
						$value['totalrent'] = ""; 
					 
						$rentitem_name = $v['rentitem_name'];
						$qty = $v['qty']; 
 
						$totunits = $noofunits * $qty;
						$value['name'] = $rentitem_name;
						$value['noofunits'] = $totunits;
						$value['outunits'] = $outunits * $qty;
						
				     	$orders_items[$i] = $value; 
					    $i++;
					    continue;
				}
    		} else { 
    			 $value['boxname'] = '-';
    			 $value['itype'] = "Single"; 
			 	 $value['name'] = $value['boxitem_name'];
			 	 $orders_items[$i] = $value;
				$i++;
    		} 
		}

	//	print_r($orders_items);
		// create new PDF document
		//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		$title="INWARD COPY";
		$pdf->createpdf($id, $pdf, $order_data, $orders_items, $company_info, $buyeraddr, 
					$title,$dc_no,false);
		
		return $pdf; 
	}
	
	public function pdfemailcreate($id)
	{
		$pdf = new CI_MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$order_data = $this->model_estimate->getInvoiceData($id);
		$buyeremail = $order_data['semail_id'].",magnumwebtech@gmail.com";
		$this->getPdf($id,$pdf,$order_data); 
		$pdf->Output(FCPATH.'/newpdf/'.$order_data['idc_no'].'.pdf', "F");
       // $pdf->Output(FCPATH. '/newpdf/2.pdf', 'F');
        $this->load->library('email');
        $this->email->from('gmr@gstsoftwareincoimbatore.com', 'gmrcoldstorage.com');
        $this->email->to($buyeremail); 
        $this->email->subject('Invoice From GMR');
        $this->email->message('Invoice Copy From GMR(website.com)');   
        $this->email->attach(FCPATH.'/newpdf/'.$order_data['idc_no'].'.pdf');
        $this->email->send();
		$file=FCPATH.'/newpdf/'.$order_data['idc_no'].'.pdf';
		unlink($file); 
	}
	
	public function getPdf($id, $pdf, $order_data)
	{  
		$orders_items = $this->model_estimate->getInvoiceItemData($id); 
		$company_info = $this->model_company->getCompanyData(1);
 
		$buyeraddr= $this->model_scustomers->getSupplierAddress($order_data['sid']);
 
		$outward_data = $this->model_outward->getOutwardData($order_data['odc_no']);
		$outward_data['outwardData'] = $this->model_outward->getInBoxNos($order_data['odc_no']);
	 
		$title="ESTIMATE COPY";
		$pdf->createinvoicepdf($id, $pdf, $order_data, $outward_data, $orders_items, $company_info, $buyeraddr, 
					$title,false);
		
		if(isset($orders_items) and !empty($orders_items)) {
			foreach($orders_items as $row) {
				$inid = $row['inward_id'];
				$order_data = $this->model_inward->getInwardData($inid);
				$this->getInwardPdf($inid,$pdf,$order_data); 
			}
		}
		
		return $pdf; 
	}
  
}