<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Credit extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Outward';
		$this->load->model('model_inward');
		$this->load->model('model_outward');
		$this->load->model('model_credit');
		$this->load->model('model_driver');
		$this->load->model('model_company');
		$this->load->model('model_scustomers');
		$this->load->model('model_subscustomers');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewPoorder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Manage Credit';
		$this->render_template('credit/index', $this->data);	
	}

	/*
	* Fetches the orders data from the orders table 
	* this function is called from the datatable ajax function
	*/
	public function fetchCreditData()
	{
		$result = array('data' => array());

		$data = $this->model_credit->getCreditData();

		foreach ($data as $key => $value) {
			// button
			$buttons = '';
	
			if(in_array('updateOrder', $this->permission)) {
				$buttons .= ' <a href="'.base_url('credit/update/'.$value['id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
			}

			$result['data'][$key] = array(
				$value['crd_no'],
				$value['idc_no'],
				$value['sdate'],
				$value['paydate'],
				$value['supplier_name'],
				//$date_time,
				
				$value['totaldays'],
				$value['creditamt'],
				$value['interest'],
				$value['total_amount'],
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	* If the validation is not valid, then it redirects to the create page.
	* If the validation for each input field is valid then it inserts the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function create()
	{
		if(!in_array('createPoorder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->data['page_title'] = 'Add Credit';

		$this->form_validation->set_rules('creditamt', 'Credit Amount', 'trim|required');
		$this->form_validation->set_rules('interest', 'Interest %', 'trim|required');
		
        if ($this->form_validation->run() == TRUE) {        	
        	
        	$order_id = $this->model_credit->create();
        	
        	if($order_id) {
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('credit/update/'.$order_id, 'refresh');
        		//redirect('outward/', 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('credit/create/', 'refresh');
        	}
        }
        else {
            // false case
        	$this->data['inwardDC'] = $this->model_inward->getInwardData(); 
        	$this->data['suppliers'] = $this->model_scustomers->getSupplierData();  

            $this->render_template('credit/create', $this->data);
        }	
	}

	/*
	* If the validation is not valid, then it redirects to the edit orders page 
	* If the validation is successfully then it updates the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function update($id)
	{
		if(!in_array('updatePoorder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		if(!$id) {
			redirect('dashboard', 'refresh');
		}

		$this->data['page_title'] = 'Update Credit';

		$this->form_validation->set_rules('datepicker', 'Repayment Date', 'trim|required');
		$this->form_validation->set_rules('totaldays', 'Total days', 'trim|required');
		$this->form_validation->set_rules('creditamt', 'Credit Amount', 'trim|required');
		$this->form_validation->set_rules('interest', 'Interest %', 'trim|required');
		$this->form_validation->set_rules('total_amount', 'Total Amount', 'trim|required');

        if ($this->form_validation->run() == TRUE) {        	
        	
        	$update = $this->model_credit->update($id);
        	
        	if($update == true) {
        		$this->session->set_flashdata('success', 'Successfully updated');
        		redirect('credit/update/'.$id, 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('credit/update/'.$id, 'refresh');
        	}
        }
        else {
            // false case

        	$result = array();
        	$credit_data = $this->model_credit->getCreditData($id);

    		$this->data['credit_data'] = $credit_data;
    		$this->data['inwardDC'] = $this->model_inward->getInwardData();
    		 
            $this->data['suppliers'] = $this->model_scustomers->getSupplierData(); 
            $this->render_template('credit/edit', $this->data);
        }
	}
}