<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentbyoutward extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Payment';

		$this->load->model('model_paymentbyoutward');
		$this->load->model('model_rentitems');
		$this->load->model('model_boxitems');
		$this->load->model('model_ledger');
		$this->load->model('model_scustomers');
		$this->load->model('model_outward');
		$this->load->model('model_users');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewPayments', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Manage Payment';
		$this->render_template('paymentbyoutward/index', $this->data);	
	}

	/*
	* Fetches the orders data from the orders table 
	* this function is called from the datatable ajax function
	*/
	public function fetchReceiptData()
	{
	
		$result = array('data' => array());

		$data = $this->model_paymentbyoutward->getPaymentData();

		foreach ($data as $key => $value) {

			$user_id = $this->model_users->getUserData($value['user_id']);

			// button
			$buttons = '';

			$buttons .= '<a target="__blank" href="'.base_url('paymentbyoutward/printDiv/'.$value['id']).'" class="btn btn-default printbutton" title="Print"><i class="fa fa-print"></i></a>';
		

			$result['data'][$key] = array(
				$value['pdate'],
				$value['payment_id'],
				$value['outwardno'],
				$value['cname'],
				$value['cmob'],
				number_format($value['cbal'], 2),
				number_format($value['cpaid'], 2),
				number_format($value['cbal'] - $value['cpaid'], 2),
				$user_id['firstname'],
				$buttons
			
				//$date_time,
				
				//$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	* If the validation is not valid, then it redirects to the create page.
	* If the validation for each input field is valid then it inserts the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function create()
	{
		if(!in_array('createPayments', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->data['page_title'] = 'Add Payment';

		$this->form_validation->set_rules('payment_no', 'Payment No', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="val-error">','</p>');
	
        if ($this->form_validation->run() == TRUE) {        	
        	
        	$order_id = $this->model_paymentbyoutward->create();

        	if($order_id) {
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('paymentbyoutward/', 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('error', 'Error occurred!!');
        		redirect('paymentbyoutward/create/', 'refresh');
        	}
        }
        else {
            // false case
        	$this->data['ledgers'] = $this->model_ledger->getLedgerData();      	
            $this->data['customers'] = $this->model_scustomers->getSupplierData();  
            $this->data['outward_no'] = $this->model_outward->getPendingOutwardNos();
            $this->render_template('paymentbyoutward/create', $this->data);
        }	
	}


	/*
	* It gets the product id passed from the ajax method.
	* It checks retrieves the particular product data from the product id 
	* and return the data into the json format.
	*/
	public function getProductValueById()
	{
		$product_id = $this->input->post('product_id');
		if($product_id) {
			$product_data = $this->model_products->getProductData($product_id);
			echo json_encode($product_data);
		}
	}
 public function getLedgerValueById()
	{
		$product_id = $this->input->post('product_id');
		if($product_id) {
			$product_data = $this->model_ledger->getLedgerData($product_id);
			echo json_encode($product_data);
		}
	}
	
	

    public function getCustomerData()
	{
		$cust_id = $this->input->post('cust_id');
		if($cust_id) {
			 $customer_data = $this->model_scustomers->getProductData($cust_id);
			echo json_encode($customer_data);
		}
	}
	/*
	* It gets the all the active product inforamtion from the product table 
	* This function is used in the order page, for the product selection in the table
	* The response is return on the json format.
	*/
	public function getTableProductRow()
	{           
		$products = $this->model_ledger->getAllActiveLedger();
		echo json_encode($products);
	}


	/*
	* If the validation is not valid, then it redirects to the edit orders page 
	* If the validation is successfully then it updates the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function update($id)
	{
		if(!in_array('updatePayments', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		if(!$id) {
			redirect('dashboard', 'refresh');
		}

		$this->data['page_title'] = 'Update Payment';

		$this->form_validation->set_rules('product[]', 'Product name', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="val-error">','</p>');
	
        if ($this->form_validation->run() == TRUE) {        	
        	
        	$update = $this->model_receipt->update($id);
        	
        	if($update == true) {
        		$this->session->set_flashdata('success', 'Successfully updated');
        		redirect('paymentbyoutward/update/'.$id, 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('error', 'Error occurred!!');
        		redirect('paymentbyoutward/update/'.$id, 'refresh');
        	}
        }
        else {
            // false case
         	$result = array();
        	$orders_data = $this->model_paymentbyoutward->getPaymentData($id);

    		$result['order'] = $orders_data;
    		$orders_item = $this->model_paymentbyoutward->getOrdersItemData($orders_data['id']);

    		foreach($orders_item as $k => $v) {
    			$result['order_item'][] = $v;
    		}

    		$this->data['order_data'] = $result;

        	$this->data['ledgers'] = $this->model_ledger->getLedgerData();          	
 			$this->data['customers'] = $this->model_scustomers->getCustomerData(); 
            $this->render_template('paymentbyoutward/edit', $this->data);
        }
	}
	
	public function printdiv($id) {
		
		$this->load->library('mypdf');
	
		$order_data = $this->model_paymentbyoutward->getPaymentData($id);
 
		$company_info = $this->model_company->getCompanyData(1);
	
		$printTitle = "Outward Receipt Copy";
		$invtitle = "Receipt No : ";
	
		$pdf = new CI_MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
	
		$pdf->createreceiptbyinvpdf($id, $pdf, $order_data, $company_info,'' , $printTitle, $invtitle, 'outwardno');
	
		$pdf->Output($id.'.pdf', 'I');
	
	}
}