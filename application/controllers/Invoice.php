<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Inward';
		$this->load->model('model_invoice');
		$this->load->model('model_outward');
		$this->load->model('model_inward');
		$this->load->model('model_rentitems');
		$this->load->model('model_driver');
		$this->load->model('model_company');
		$this->load->model('model_scustomers');
		$this->load->model('model_invoiceprefixs');
		$this->load->library('Sim');
		$this->load->library('mypdf');
	}

	/* 
	* It only redirects to the manage order page
	*/
	public function index()
	{
		if(!in_array('viewOrder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
		$this->data['page_title'] = 'Manage Invoice';
		$this->render_template('invoice/index', $this->data);	
	}

	/*
	* Fetches the orders data from the orders table 
	* this function is called from the datatable ajax function
	*/
	public function fetchInvoiceData()
	{
		$result = array('data' => array());

		$data = $this->model_invoice->getInvoiceData();

		foreach ($data as $key => $value) {

			 
			$idc_no = $this->model_outward->getOutwardDCNo($value['odc_no']);
			
			// button
			$buttons = '';
			
			/*$date = date('d-m-Y', $value['sdate']);
			$time = date('h:i a', $value['sdate']);

			$idc_no = $this->model_outward->getOutwardDCNo($value['idc_no']);

			$date_time = $date . ' ' . $time;

			*/

			if(in_array('viewOrder', $this->permission)) {
				$buttons .= '<a target="__blank" href="'.base_url('invoice/printDiv/'.$value['id']).'" class="btn btn-default printbutton"><i class="fa fa-print"></i></a>';
			}

			if(in_array('updateOrder', $this->permission)) {
				$buttons .= ' <a href="'.base_url('invoice/update/'.$value['id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
			}

			if(in_array('viewOrder', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default smsbutton" onClick="smsFunc('.$value['id'].')" data-toggle="modal" data-target="#smsModal" title="Send SMS"><i class="fa fa-commenting-o "></i></button>';
			}
			
			if(in_array('viewOrder', $this->permission)) {
				$buttons .= '&nbsp;<button type="button" class="btn btn-default mailbutton"
						onClick="mailFunc('.$value['id'].')" data-toggle="modal" data-target="#mailModal"
					    title="Send Email"><i class="fa fa-envelope-o "></i></button>';
				//$buttons .= '&nbsp;<a target="__blank" href="'.base_url('orders/pdfemailcreate/'.$value['id']).'" class="btn btn-default mailbutton" title="Send Email"><i class="fa fa-envelope-o"></i></a>';
			}
		/*  if(in_array('deleteOrder', $this->permission)) {
				$buttons .= ' <button type="button" class="btn btn-default deletebutton" onClick="removeFunc('.$value['id'].')" data-toggle="modal" data-target="#removeModal"><i class="fa fa-trash"></i></button>';
			}*/

		    if($value['eorderno'] != "") {
				$eno = $value['eorderno'];
				$billno =  $value['invoice_id'].' <span class="label label-warning">'.$eno.'</span>';
			} else {
				$billno = $value['invoice_id'];
			}
			
			$result['data'][$key] = array(
				$billno,
				$value['sdate'],
				$idc_no['odc_no'],
				$value['odate'],
				$value['supplier_name'],
				$value['total_items'],
				//$value['total_sboxes'],
				$value['net_amount'],
				$value['ibalance'],
				$buttons
			);
		} // /foreach

		echo json_encode($result);
	}

	/*
	 * If the validation is not valid, then it redirects to the create page.
	 * If the validation for each input field is valid then it inserts the data into the database
	 * and it stores the operation message into the session flashdata and display on the manage group page
	 */
	public function create()
	{
		if(!in_array('createOrder', $this->permission)) {
			redirect('dashboard', 'refresh');
		}
	
		$this->data['page_title'] = 'Add Invoice';
	
		$this->form_validation->set_rules('outward_no', 'Outward No', 'trim|required');
	
		if ($this->form_validation->run() == TRUE) {
			 
			$order_id = $this->model_invoice->create();
			 
			if($order_id) {
				$this->session->set_flashdata('success', 'Successfully created');
				redirect('invoice/update/'.$order_id, 'refresh');
				//redirect('inward/', 'refresh');
			}
			else {
				$this->session->set_flashdata('errors', 'Error occurred!!');
				redirect('invoice/create/', 'refresh');
			}
		}
		else {
			// false case
			$this->data['outwardDC'] = $this->model_outward->getClosedOutwarditemsData();
		
			$this->render_template('invoice/create', $this->data);
		}
	}
	
	/*
	* If the validation is not valid, then it redirects to the edit orders page 
	* If the validation is successfully then it updates the data into the database 
	* and it stores the operation message into the session flashdata and display on the manage group page
	*/
	public function update($id)
	{
		if(!in_array('updateOrder', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		if(!$id) {
			redirect('dashboard', 'refresh');
		}

		$this->data['page_title'] = 'Update Invoice';
		
		$this->form_validation->set_rules('outward_no', 'Outward No', 'trim|required');
		
        if ($this->form_validation->run() == TRUE) {        	
        	
        	$update = $this->model_invoice->update($id);
        	
        	if($update == true) {
        		$this->session->set_flashdata('success', 'Successfully updated');
        		redirect('invoice/update/'.$id, 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('invoice/update/'.$id, 'refresh');
        	}
        }
        else {
            // false case

        	$result = array();
        	$this->data['outwardDC'] = $this->model_outward->getClosedOutwarditemsData();
        	$invoice_data = $this->model_invoice->getInvoiceData($id);

        	$outward_data = $this->model_outward->getOutwardData($invoice_data['odc_no']);
        	
    		$result['invoice'] = $invoice_data;
    		$result['invoice']['outward_data'] = $outward_data;

    		$invoice_items = $this->model_invoice->getInvoiceItemData($invoice_data['id']);

    		foreach($invoice_data as $k => $v) {
    			$result['invoice_item'][] = $v;
    		}

    		$this->data['invoice_data'] = $result;
    		
       	 	$this->render_template('invoice/edit', $this->data);
        }
	}

	public function sendSMS()
	{
		$id = $this->input->post('id');
		$response = array();
		if($id) {
			$sent = $this->model_invoice->updatesms($id);
			if($sent == true) {
				$response['success'] = true;
				$response['messages'] = "SMS Successfully sent";
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error occurred while sending SMS";
			}
		} else {
			$response['success'] = false;
			$response['messages'] = "Refresh the page again!!";
		}
		echo json_encode($response);
	}
	
	public function sendemail()
	{
		$id = $this->input->post('order_id');
		$response = array();
		if($id) {
			$sent = $this->pdfemailcreate($id);
			if($sent == true) {
				$response['success'] = true;
				$response['messages'] = "Email Successfully sent";
			}
			else {
				$response['success'] = false;
				$response['messages'] = "Error occurred while sending Email";
			}
		} else {
			$response['success'] = false;
			$response['messages'] = "Refresh the page again!!";
		}
		echo json_encode($response);
	}
 
	public function printDiv($id)
	{
		$pdf = new CI_MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$order_data = $this->model_invoice->getInvoiceData($id);
		$this->getPdf($id, $pdf,$order_data);
		//	$pdf->ob_end_clean();
		$pdf->Output($id.'.pdf', 'I');
	}
	
	public function pdfemailcreate($id)
	{
		$pdf = new CI_MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$order_data = $this->model_invoice->getInvoiceData($id);
		$buyeremail = $order_data['semail_id'].",magnumwebtech@gmail.com";
		$this->getPdf($id,$pdf,$order_data); 
		$pdf->Output(FCPATH.'/newpdf/'.$order_data['idc_no'].'.pdf', "F");
       // $pdf->Output(FCPATH. '/newpdf/2.pdf', 'F');
        $this->load->library('email');
        $this->email->from('gmr@gstsoftwareincoimbatore.com', 'gmrcoldstorage.com');
        $this->email->to($buyeremail); 
          $this->email->subject('Invoice From GMR');
        $this->email->message('Invoice Copy From GMR(website.com)');   
        $this->email->attach(FCPATH.'/newpdf/'.$order_data['idc_no'].'.pdf');
        $this->email->send();
		$file=FCPATH.'/newpdf/'.$order_data['idc_no'].'.pdf';
		unlink($file); 
	}
 
	public function getPdf($id, $pdf, $order_data)
	{  
		$orders_items = $this->model_invoice->getInvoiceItemData($id); 
		$company_info = $this->model_company->getCompanyData(1);
 
		$buyeraddr= $this->model_scustomers->getSupplierAddress($order_data['sid']);
 
		$outward_data = $this->model_outward->getOutwardData($order_data['odc_no']);
		$outward_data['outwardData'] = $this->model_outward->getInBoxNos($order_data['odc_no']);
		
		$title="INVOICE COPY";
		$pdf->createinvoicepdf($id, $pdf, $order_data, $outward_data, $orders_items, $company_info, $buyeraddr, 
					$title,true);
		
		return $pdf; 
	}
	
	function fetch_data($orders_items)
	{
		$output = '';
		$total_amt = 0;
		$tax_amt = 0;
		foreach($orders_items as $row) {
			$product_data = $this->model_rentitems->getRentitemsData($row['product_id']);
			$output .= '<tr>
                          <td style="width:110px;border:1px thin #666; font-size:12px; font-weight:normal; font-style:normal;">'.$product_data["name"].'</td>
                          <td style="width:100px;border:1px thin #666"; align="center">'.$row["sku"].'</td>
                          <td style="width:70px;border:1px thin #666"; align="right">'.$row["rate"].'</td>
                          <td style="width:30px;border:1px thin #666"; align="center">'.$row["qty"].'</td>
                          <td style="width:70px;border:1px thin #666"; align="right">'.$row["amount"].'</td>
                          <td style="width:30px;border:1px thin #666"; align="center">'.$row["discount"].'%</td>
                          <td style="width:50px;border:1px thin #666"; align="center">'.$row["gst"].'</td>
                          <td style="width:50px;border:1px thin #666"; align="center">'.$row["sgst"].'</td>
                          <td style="width:50px;border:1px thin #666"; align="center">'.$row["cgst"].'%</td>
                          <td style="width:80px;padding:8px;border:1px thin #666"; align="right">'.$row["totamount"].'</td>
                     </tr>';
			$total_amt += $row['amount'];
			$tax_amt += $row['totamount'];
		}
		$output .= '<tr><td colspan="7" style="border-top:1px thin #666";></td></tr><tr><td colspan="5" align="left"><strong>Total</strong></td><td align="center">'.number_format($tax_amt,2).'</td><td align="center">'.number_format($total_amt,2).'</td></tr>';
	
		return $output;
	}
	

}