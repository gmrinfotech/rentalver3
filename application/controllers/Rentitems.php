<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rentitems extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Rent Items';

		$this->load->model('model_rentitems'); 
		$this->load->model('model_category');
		$this->load->model('model_stores');
		$this->load->model('model_attributes');
		$this->load->model('model_invoiceprefixs');
	}

	
	public function getRentItemValueById()
	{
		$product_id = $this->input->post('product_id');
		if($product_id) {
			$product_data = $this->model_rentitems->getRentitemsData($product_id);
			echo json_encode($product_data);
		}
	}
	
	public function getRentitemsData()
	{ 
		$this->load->model('model_boxitems'); 
		$products['boxitems'] = $this->model_rentitems->getTypeRentitemsData(1);
		echo json_encode($products);
	}
	
	public function getBoxedRentItemData()
	{
		$product_id = $this->input->post('product_id');
		$box_id = $this->input->post('box_id');
		if($product_id) {
			$this->load->model('model_boxitems');
			$product_data = $this->model_boxitems->getBoxedRentItemData($product_id,$box_id);
			echo json_encode($product_data);
		}
	}
	
	public function getTypeRentitemsData()
	{
		$type = $this->input->post('type'); 
		$product_data = $this->model_rentitems->getTypeRentitemsData($type);
		echo json_encode($product_data);
	} 
    /* 
    * It only redirects to the manage product page
    */
	public function index()
	{
        if(!in_array('viewProduct', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->render_template('rentitems/index', $this->data);	
	}

    /*
    * It Fetches the products data from the product table 
    * this function is called from the datatable ajax function
    */
	public function fetchRentitemsData()
	{
		$result = array('data' => array());

		$data = $this->model_rentitems->getRentitemsData();

		foreach ($data as $key => $value) {

			// button
            $buttons = '';
            if(in_array('updateProduct', $this->permission)) {
    			$buttons .= '<a href="'.base_url('rentitems/update/'.$value['id']).'" class="btn btn-default editbutton"><i class="fa fa-pencil"></i></a>';
            }

            $availability = ($value['availability'] == 1) ? '<span class="label label-success">Active</span>' : '<span class="label label-warning">Inactive</span>';

            $type = $value['type'] == 1 ? 'Box' : 'Single';
			$result['data'][$key] = array(
				$value['name'],		
				$this->getUnitsName($value['units']),	
				$type, 	
				$value['totalqty'], 
				$value['rentedqty'], 
				$value['availableqty'],
				$value['minrentaldays'],
				$value['rate'], 
				$value['rateperday'],  
				$buttons
			);
		} // /foreach
		echo json_encode($result);
	}	
 
	public function create()
	{
		if(!in_array('createProduct', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

		$this->form_validation->set_rules('rentitem_name', 'Rent item name', 'trim|required|is_unique[rentitems.name]');
		$this->form_validation->set_rules('rentitem_code', 'Rent item code', 'trim|required|is_unique[rentitems.code]');
        if ($this->form_validation->run() == TRUE) {
        	$data = array(
        		'code' => $this->input->post('rentitem_code'),
        		'name' => $this->input->post('rentitem_name'),
        		'units' => $this->input->post('units'),
				'availableqty' => $this->input->post('availableqty'),
        		'type' => $this->input->post('type'),
        		'totalqty' => $this->input->post('totalqty'),
        		'rentedqty' => $this->input->post('rentedqty'), 
        		'minrentaldays' => $this->input->post('minrentaldays'),
        		'rate' => $this->input->post('rate'),  
        	    'rateperday' => $this->input->post('rateperday'),  
				'availability' => $this->input->post('availability'),
        		'categoryid' => $this->input->post('category'),
				'description' => $this->input->post('description'),
        	);
		
        		$mcode = $this->input->post('manualcode'); 
			 	 if($mcode != 1) {
			 	 	$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
					$pcode= $company_Prefix['pcode'];
			 	 	$this->pcodenewno($pcode);
			 	 } 
			 	 
        	$create = $this->model_rentitems->create($data);
        	if($create == true) {
        		$this->session->set_flashdata('success', 'Successfully created');
        		redirect('rentitems/', 'refresh');
        	}
        	else {
        		$this->session->set_flashdata('errors', 'Error occurred!!');
        		redirect('rentitems/create', 'refresh');
        	}
        }
        else {
        	$this->data['category'] = $this->model_category->getActiveCategroy();    
            $this->render_template('rentitems/create', $this->data);
        }	
	}

	public function pcodenewno($pcode) {

		$pcode++;
		$data = array(
			'pcode' => $pcode,
		);
		$update = $this->model_invoiceprefixs->update($data, 1);
	}
	
    function check_product_name() {
    	if($this->input->post('id'))
    		$id = $this->input->post('id');
    	else
    		$id = '';
    	$product_code = $this->input->post('rentitem_name');
    	$result = $this->model_rentitems->check_product_name($id, $product_code);
    	if($result == 0)
    		$response = true;
    	else {
    		$this->form_validation->set_message('check_product_name' , 'Rent item name must be unique');
    		$response = false;
    	}
    	return $response;
    }
 
    function check_product_code() {
    	if($this->input->post('id'))
    		$id = $this->input->post('id');
    	else
    		$id = '';
    	$product_id = $this->input->post('rentitem_code');
    	$result = $this->model_rentitems->check_product_code($id, $product_id);
    	if($result == 0)
    		$response = true;
    	else {
    		$this->form_validation->set_message('check_product_code' , 'Rent item name must be unique');
    		$response = false;
    	}
    	return $response;
    }
    
	public function update($product_id)
	{      
        if(!in_array('updateProduct', $this->permission)) {
            redirect('dashboard', 'refresh');
        }

        if(!$product_id) {
            redirect('dashboard', 'refresh');
        }
        
		$product_code = $this->input->post('pcoldname');
		$product_code_change = $this->input->post('pceditstatus');
		if($product_code_change == "true") {
			$this->form_validation->set_rules('rentitem_code', 'Rent Item code', 'trim|required|callback_check_product_code');
			$product_code = $this->input->post('rentitem_code');
		}
				
        $product_name = $this->input->post('pnoldname');
        $product_name_change = $this->input->post('pneditstatus');
       
	 	if($product_name_change == "true") {
        	$this->form_validation->set_rules('rentitem_name', 'Rent Item Name', 'required|callback_check_product_name');
        	$product_name = $this->input->post('rentitem_name');
        }

        $this->form_validation->set_rules('units', 'units', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            // true case
            
            $data = array(
                'name' => $product_name,
                'code' => $product_code,
        		'units' => $this->input->post('units'),
				'availableqty' => $this->input->post('availableqty'),
           		'type' => $this->input->post('type'),
        		'totalqty' => $this->input->post('totalqty'),
        		'rentedqty' => $this->input->post('rentedqty'), 
            	'minrentaldays' => $this->input->post('minrentaldays'),
        		'rate' => $this->input->post('rate'), 
                'rateperday' => $this->input->post('rateperday'),  
            	'categoryid' => $this->input->post('category'),
				'availability' => $this->input->post('availability')
            );

            $update = $this->model_rentitems->update($data, $product_id);
            if($update == true) {
                $this->session->set_flashdata('success', 'Successfully updated');
                redirect('rentitems/', 'refresh');
            }
            else {
                $this->session->set_flashdata('errors', 'Error occurred!!');
                redirect('rentitems/update/'.$product_id, 'refresh');
            }
        }
        else {
          	$this->data['category'] = $this->model_category->getActiveCategroy();   
            $product_data = $this->model_rentitems->getRentitemsData($product_id);
	        $this->data['product_data'] = $product_data;
            $this->render_template('rentitems/edit', $this->data); 
        }   
	}
	
	public function getUnitsName($unitid) {
		$unit = "Nox";
		if($unitid == 1) {
			$unit = "Nos";
		} else if($unitid == 2) {
			$unit = "M-Box";
		} else if($unitid == 3) {
			$unit = "L-Box";
		} else if($unitid == 4) {
			$unit = "Bag";
		} else {
			$unit = "S-Box";
		}
		return $unit;
	}

    /*
    * It removes the data from the database
    * and it returns the response into the json format
    */
	public function remove()
	{
        if(!in_array('deleteProduct', $this->permission)) {
            redirect('dashboard', 'refresh');
        }
        
        $product_id = $this->input->post('product_id');

        $response = array();
        if($product_id) {
            $delete = $this->model_rentitems->remove($product_id);
            if($delete == true) {
                $response['success'] = true;
                $response['messages'] = "Successfully removed"; 
            }
            else {
                $response['success'] = false;
                $response['messages'] = "Error in the database while removing the product information";
            }
        }
        else {
            $response['success'] = false;
            $response['messages'] = "Refersh the page again!!";
        }

        echo json_encode($response);
	}

}