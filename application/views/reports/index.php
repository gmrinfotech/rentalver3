 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reports
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Graph</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

        <div class="col-md-12 col-xs-12">
          <form class="form-inline" action="<?php echo base_url('reports/') ?>" method="POST">
            <div class="form-group">
              <label for="date">Year</label>
              <select class="form-control" name="select_year" id="select_year">
                <?php foreach ($report_years as $key => $value): ?>
                  <option value="<?php echo $value ?>" <?php if($value == $selected_year) { echo "selected"; } ?>><?php echo $value; ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>

        <br /> <br />


        <div class="col-md-12 col-xs-12">

          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('success'); ?>
            </div>
          <?php elseif($this->session->flashdata('error')): ?>
            <div class="alert alert-error alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('error'); ?>
            </div>
          <?php endif; ?>
		
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Total Purchase And Sales- Report</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:250px"></canvas>
                
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Total Paid Orders - Report Data</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="datatables" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Month - Year</th>
                  <th>Sales Amount</th>
                  <th>Incomes</th>
                  <th>Expenses</th>
                  <th>Purchase Amount</th>
                   <th>Profit/Loss</th>
                </tr>
                </thead>
                <tbody>

                  <?php foreach ($results as $k => $v): ?>
                    <tr>
                      <td><?php echo $k; ?></td>
                      <td><?php                      
                        echo $company_currency .' ' . $results[$k];
                        
                      
                      ?></td>
                       <td><?php                      
                        echo $company_currency .' ' . $incomeresults[$k];
                      ?></td>
                         <td><?php                      
                        echo $company_currency .' ' . $expensesresults[$k];
                      ?></td>
                       <td><?php                      
                        echo $company_currency .' ' . $poresults[$k];
                      ?></td>
                         <td><?php                      
                        echo $company_currency .' ' . (($results[$k]+$incomeresults[$k])-($poresults[$k]+$expensesresults[$k]));
                      ?></td>
                    </tr>
                  <?php endforeach ?>
                  
                </tbody>
                <tbody>
                  <tr>
                    <th>Total Amount</th>
                    <th>
                     
                      <?php echo array_sum($results); ?>
                      
                    </th>
                      <th>
                     
                      <?php echo array_sum($incomeresults); ?>
                      
                    </th>
                      <th>
                     
                      <?php echo array_sum($expensesresults); ?>
                      
                    </th>
                      <th>
                     
                      <?php echo array_sum($poresults); ?>
                      
                    </th>
                      <th>
                     
                      <?php 
					  echo (array_sum($results)+array_sum($incomeresults))-(array_sum($poresults)+array_sum($expensesresults));
					   ?>
                      
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <script type="text/javascript">
	
 	$(document).ready(function() {
 		$("#reportNav>a")[0].click();
	  $("#reportNav").addClass('active');
	  $("#manageReportNav").addClass('active');
    }); 
	
	
    var report_data = <?php echo '[' . implode(',', $results) . ']'; ?>;
	var po_report_data = <?php echo '[' . implode(',', $poresults) . ']'; ?>;
	var income_report_data = <?php echo '[' . implode(',', $incomeresults) . ']'; ?>;
	var expense_report_data = <?php echo '[' . implode(',', $expensesresults) . ']'; ?>;


    var areaChartData = {
    	      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    	      datasets: [ 
    	        {
    	          data                : report_data
    	        },
    	        {
    	          data                : po_report_data
    	        }
    			,
    	        {
    	          data                : income_report_data
    	        }
    				,
    	        {
    	          data                : expense_report_data
    	        }
    	      ]
    	    }
    
	/*$(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */
	 

    //-------------
    //- BAR CHART -
    //-------------
   /* var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    barChartData.datasets[0].fillColor   = '#00a65a';
    barChartData.datasets[0].strokeColor = '#00a65a';
    barChartData.datasets[0].pointColor  = '#00a65a';
	
	barChartData.datasets[1].fillColor   = '#ec4444';
    barChartData.datasets[1].strokeColor = '#ec4444';
    barChartData.datasets[1].pointColor  = '#ec4444';
	
	barChartData.datasets[2].fillColor   = '#85DCFF';
    barChartData.datasets[2].strokeColor = '#85DCFF';
    barChartData.datasets[2].pointColor  = '#85DCFF';
	
	barChartData.datasets[3].fillColor   = '#FACBEF';
    barChartData.datasets[3].strokeColor = '#FACBEF';
    barChartData.datasets[3].pointColor  = '#FACBEF';

	
    var barChartOptions                  = { 
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
	   showInLegend: true, 
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toUpperCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

	//barChartOptions.datasetFill = false
	//barChart.Bar(barChartData, barChartOptions)
	
  })*/
  
  var bctx = document.getElementById("barChart").getContext("2d");

  var barChartOptions1 = {
        barValueSpacing: 40,
        scales: {
            xAxes: [{
				gridLines: { display: false },
			}],
			yAxes: [{
                ticks: {
                    min: 0,
                    suggestedMax: 100,
                    beginAtZero: true
                }
            }]
        }
    }

 	var barChartData1 = areaChartData
  	barChartData1.datasets[0].backgroundColor   = '#00a65a';
	barChartData1.datasets[1].backgroundColor   = '#ec4444';
	barChartData1.datasets[2].backgroundColor   = '#85DCFF';
	barChartData1.datasets[3].backgroundColor   = '#FACBEF';
	
	barChartData1.datasets[0].label   = 'Sales';
	barChartData1.datasets[1].label   = 'Purchase';
	barChartData1.datasets[2].label   = 'Income';
	barChartData1.datasets[3].label   = 'Expenses';

	var myBarChart = new Chart(bctx, {
	    type: 'bar',
	    data: barChartData1,
	    options: barChartOptions1
	});
  </script>
