 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reports
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Graph</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

        <div class="col-md-12 col-xs-12">
          <form class="form-inline" action="<?php echo base_url('reports/rackindex') ?>" method="POST">
            <div class="form-group">
              <label for="floor">Floor</label>
              <select class="form-control" name="floor" id="floor">
                	<option value="1">G - Floor</option>
					<option value="2">1 - Floor</option>
					<option value="3">2 - Floor</option>
					<option value="4">3 - Floor</option>
              </select>
              &nbsp;&nbsp;&nbsp;
              <label for="floor">Chamber</label>
              <select class="form-control" name="chamber" id="chamber">
                <option value="1">Chamber 1</option>
				<option value="2">Chamber 2</option>
				<option value="3">Chamber 3</option>
				<option value="4">Chamber 4</option>
              </select>
              
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>

        <br /> <br /> <br />
        <div class="col-md-12 col-xs-12">
          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('success'); ?>
            </div>
          <?php elseif($this->session->flashdata('error')): ?>
            <div class="alert alert-error alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('error'); ?>
            </div>
          <?php endif; ?>
		
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Rack Space - Report</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="chart">
				<canvas id="stchart" style="height:250px"></canvas>              
              </div>
              <br>

              
              <div class="form-group col-md-8 col-xs-12 pull pull-left" style="display: none">
					<label for="product_code" class="col-sm-5 control-label"
						style="text-align: left; font-size: 16px; height: 34px; padding-top: 5px; background-color: #abdce4;">
						Enter Free Space to check box wise :</label>
						<div class="col-sm-7">
					<input type="text" class="form-control" id="free"
							name="free" autocomplete="off" onkeyup="checkRackSpace()">
					</div>
				</div>
              
              		<label class="col-md-12 col-xs-12 pull pull-left" style="text-align: left;
							font-size: 19px;height: 34px;padding-top: 5px;color: #231eff;padding-left: 0px;">
							Available Space to Fill </label>
							<div class="col-md-12 col-xs-12 pull pull-left"
								style="border: solid 1px black; padding-top: 12px; margin-bottom: 12px;">	
								<div class="col-md-3 col-xs-12 pull pull-left">						
									<div class="form-group">
										<label for="product_code" class="col-sm-5 control-label"
											style="text-align: left; font-size: 16px; height: 34px; padding-top: 5px; background-color: #abdce4;">
											S Boxes</label>
											<div class="col-sm-7">
										<input type="text" readonly class="form-control" id="tsboxes"
												name="tsboxes" autocomplete="off">
										</div>
									</div>
								</div>
								<div class="col-md-3 col-xs-12 pull pull-left">
									
									<div class="form-group">
										<label for="product_code" class="col-sm-5 control-label"
											style="text-align: left; font-size: 16px; height: 34px; padding-top: 5px; background-color: #abdce4;">
											M Boxes</label>
											<div class="col-sm-7">
										<input type="text" readonly class="form-control" id="tmboxes"
												name="tmboxes" autocomplete="off">
										</div>
									</div>
								</div>
								<div class="col-md-3 col-xs-12 pull pull-left">
									
									<div class="form-group">
										<label for="product_code" class="col-sm-5 control-label"
											style="text-align: left; font-size: 16px; height: 34px; padding-top: 5px; background-color: #abdce4;">
											L Boxes</label>
											<div class="col-sm-7">
										<input type="text" readonly class="form-control" id="tlboxes"
												name="tlboxes" autocomplete="off">
										</div>
									</div>
								</div>
								<div class="col-md-3 col-xs-12 pull pull-left">
									
									<div class="form-group">
										<label for="product_code" class="col-sm-5 control-label"
											style="text-align: left; font-size: 16px; height: 34px; padding-top: 5px; background-color: #abdce4;">
											Bags</label>
											<div class="col-sm-7">
										<input type="text" readonly class="form-control" id="tbags"
												name="tbags" autocomplete="off">
										</div>
									</div>
								</div>

							</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- col-md-12 -->
      </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <script type="text/javascript">
	
	$(document).ready(function() {
	  $("#reportNav").addClass('active');
	  $("#manageRackNav").addClass('active');

		    var canvas = document.getElementById("stchart");
		    var ctx = canvas.getContext("2d");

		    canvas.onclick = function(evt) {
		      var activePoints = schart.getElementsAtEvent(evt);
		      if (activePoints[4]) {
		        var chartData = activePoints[4]['_chart'].config.data;
		        var idx = activePoints[4]['_index'];

		        var label = chartData.labels[idx];
		        var value = chartData.datasets[4].data[idx];

		        $("#free").val(value);
		        checkRackSpace();
		      }
		    };
    }); 
	

    var sboxes = <?php echo '[' . implode(',', $sbox_array) . ']'; ?>;
	var mboxes = <?php echo '[' . implode(',', $mbox_array) . ']'; ?>;
	var lboxes = <?php echo '[' . implode(',', $lbox_array) . ']'; ?>;
	var bags = <?php echo '[' . implode(',', $bag_array) . ']'; ?>;
	var free = <?php echo '[' . implode(',', $free_array) . ']'; ?>;

	var areaChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      datasets: [ 
        {
          data                : sboxes
        },
        {
          data                : mboxes
        }
		,
        {
          data                : lboxes
        }
		,
        {
          data                : bags
        }
      ]
    }

	  /**** Stacked Charts**************/ 

	/* First chart stack chart*/
	
	var numberWithCommas = function(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

/*	var sbox = [40, 47, 44, 38, 27, 31, 25, 40, 47, 44, 38, 27, 31, 25, 40, 47, 44, 38, 27, 31, 25, 27, 31, 25];
	var mbox = [10, 12, 7, 5, 4, 6, 8, 10, 12, 7, 5, 4, 6, 8,10, 12, 7, 5, 4, 6, 8, 27, 31, 25, 27, 31, 25];
	var lbox = [17, 11, 22, 18, 12, 7, 5, 17, 11, 22, 18, 12, 7, 5, 17, 11, 22, 18, 12, 7, 5, 27, 31, 25];
	var bag =  [17, 11, 22, 18, 12, 7, 5, 17, 11, 22, 18, 12, 7, 5, 17, 11, 22, 18, 12, 7, 5, 27, 31, 25];
	var freea = [17, 11, 22, 18, 12, 7, 5, 17, 11, 22, 18, 12, 7, 5,17, 11, 22, 18, 12, 7, 5, 27, 31, 25];*/

	var racks = <?php echo '[' . implode(',', range(1, $racks)) . ']'; ?>;

	// Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

	//var bar_ctx = document.getElementById('bar-chart');
	var stchart  = $('#stchart').get(0).getContext('2d')
	var schart = new Chart(stchart, {
		type: 'bar',
		data: {
			labels: racks,
			datasets: [
			{
				label: 'S-Box',
				data: sboxes,
					hoverBackgroundColor: "#01BB05",
					backgroundColor: "#62EB62",
					hoverBorderWidth: 0
			},
			{
				label: 'M-Boxes',
				data: mboxes,
						hoverBackgroundColor: "#EF25F9",
						backgroundColor: "#F19FF7",
						hoverBorderWidth: 0
			},
			{
				label: 'L-Box',
				data: lboxes,
						hoverBackgroundColor: "#2FA5E5",
						backgroundColor: "#9DCDEB",
						hoverBorderWidth: 0
			},
			{
				label: 'Bags',
				data: bags,
						hoverBackgroundColor: "#FF956A",
						backgroundColor: "#FDC1A7",
						hoverBorderWidth: 0
			},
			{
				label: 'Free',
				data: free,
					hoverBackgroundColor: "#C1C1C1",
					backgroundColor: "#F2F0F2",
					hoverBorderWidth: 0
			}
			]
		},
		options: {
			animation: {
				duration: 10,
			},
			tooltips: {
				mode: 'label',
				callbacks: {
					label: function(tooltipItem, data) { 
						return formtooltip(tooltipItem, data); 
					}
				}
			},
			scales: {
				xAxes: [{ 
					barThickness : 30,    // To set width of the bar
					//categorySpacing: 5, // To give space between bars.
					stacked: true, 
					
					gridLines: { display: false },
				 	scaleLabel: {
				        display: true,
				        labelString: 'Racks'
			        }
				}],
				yAxes: [{ 
					stacked: true, 
					ticks: {
						min: 1,
						callback: function(value) { return numberWithCommas(value); },
					}, 
					scaleLabel: {
				        display: true,
				        labelString: 'Space'
			        }
				}],
			}, // scales
			legend: {display: true}
		} // options
	});

	function formtooltip(tooltipItem, data) {
		var unit = data.datasets[tooltipItem.datasetIndex].label;
		var val = numberWithCommas(tooltipItem.yLabel)
		var ttp = unit + ": " + val;	
		
	    if(unit == "S-Box") {
	    	val = val;
	    	ttp = unit + ": " + val;
	    } else if(unit == "M-Box") {
	    	val = val/2;
	    	ttp = unit + ": " + val;
	    } else if(unit == "L-Box") {
	    	val = val/4;
	    	ttp = unit + ": " + val;
	    } else if(unit == "Bags") {
	    	val = val/4;
	    	ttp = unit + ": " + val;
	    } else if(unit == "Free") {
    	 	var sboxes = val;
			var mboxes = parseInt((Number(sboxes)/2));
			var lboxes = parseInt((Number(sboxes)/4));
	    	ttp = "Free for (S-Box : " + sboxes + ",  M-Box :  " + mboxes + ",  L-Box/Bag :  " + lboxes + ")";
	    } 
		return ttp;	
	}
	
	function checkRackSpace() {
		  var free = converttonumber($("#free").val());
		  var sboxes = free;
		  var mboxes = parseInt((Number(free)/2));
		  var lboxes = parseInt((Number(free)/4));

		  $("#tsboxes").val(sboxes);
		  $("#tmboxes").val(mboxes);
		  $("#tlboxes").val(lboxes);
		  $("#tbags").val(lboxes);

    }

/*	var areaChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      datasets: [ 
        {
			data 			  : [33,77,44]
        },
        {
          data                : [37,07,49]
        }
		,
        {
          data                : [65,27,34]
        }
		,
        {
          data                : [13,76,44]
        }
      ]
    }
	
    var barChartOptions = { 
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
	  showInLegend: true, 
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toUpperCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }
	
	 /** Bar chart 1.x ***/
	 
	/*var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData1
    barChartData.datasets[0].fillColor   = '#00a65a';
    barChartData.datasets[0].strokeColor = '#00a65a';
    barChartData.datasets[0].pointColor  = '#00a65a';
	
	barChartData.datasets[1].fillColor   = '#ec4444';
    barChartData.datasets[1].strokeColor = '#ec4444';
    barChartData.datasets[1].pointColor  = '#ec4444';
	
	barChartData.datasets[2].fillColor   = '#85DCFF';
    barChartData.datasets[2].strokeColor = '#85DCFF';
    barChartData.datasets[2].pointColor  = '#85DCFF';
	
	barChartData.datasets[3].fillColor   = '#FACBEF';
    barChartData.datasets[3].strokeColor = '#FACBEF';
    barChartData.datasets[3].pointColor  = '#FACBEF';

    $(function () {
		barChartOptions.datasetFill = false
		barChart.Bar(barChartData, barChartOptions)
	
    })*/
	
   /****************** Bar chart 2.x ***************/
  /*  var bctx = document.getElementById("barChart").getContext("2d");

	var barChartOptions1 = {
        barValueSpacing: 40,
        scales: {
            xAxes: [{
				gridLines: { display: false },
			}],
			yAxes: [{
                ticks: {
                    min: 0,
                }
            }]
        }
    }
    
	var barChartData1 = areaChartData
    barChartData1.datasets[0].backgroundColor   = '#00a65a';
	barChartData1.datasets[1].backgroundColor   = '#ec4444';
	barChartData1.datasets[2].backgroundColor   = '#85DCFF';
	barChartData1.datasets[3].backgroundColor   = '#FACBEF';
	
	barChartData1.datasets[0].label   = 'Sales';
	barChartData1.datasets[1].label   = 'Income';
	barChartData1.datasets[2].label   = 'Expenses';
	barChartData1.datasets[3].label   = 'Purchase';*/

	
 /**Dummy Data**/
/*	var data = {
    labels: ["Chocolate", "Vanilla", "Strawberry"],
    datasets: [
        {
            label: "Harpo",
            backgroundColor: "blue",
            data: [3,7,4]
        },
        {
            label: "Chico",
            backgroundColor: "red",
            data: [4,3,5]
        },
        {
            label: "Groucho",
            backgroundColor: "green",
            data: [7,2,6]
        }
    ]
}; 
	
var myBarChart = new Chart(bctx, {
    type: 'bar',
    data: barChartData1,
    options: barChartOptions1
});*/

		/* Second chart stack chart*/
	 /*var ctx  = $('#chart').get(0).getContext('2d')
  var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
  
	labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
 datasets: [
  {
    label: 'Low',
    data: [
				150, 52, 78,45,47,85,150
			],
    backgroundColor: '#D6E9C6' // green
  },
  {
    label: 'Moderate',
    data: [
				154,78,25,14,85,89,45
			],
    backgroundColor: '#FAEBCC' // yellow
  },
  {
    label: 'High',
    data: [
				50,12,45,17,82,96,86
			],
    backgroundColor: '#EBCCD1' // red
  }
]
  },
  options: {
  scales: {
    xAxes: [{ stacked: true }],
    yAxes: [{ stacked: true }]
  }
}
});*/

	/* Third chart stack chart */
 /* var ctx1  = $('#chart1').get(0).getContext('2d')
	var barChartData1 = {
		labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
		datasets: [{
			label: 'Dataset 1',
			backgroundColor: 'rgb(255, 99, 132)',
			data: [
				150, 52, 78,45,47,85,150
			]
		}, {
			label: 'Dataset 2',
			backgroundColor: '#FACBEF',
			data: [
				154,78,25,14,85,89,45
			]
		}, {
			label: 'Dataset 3',
			backgroundColor: '#ec4444',
			data: [
				50,12,45,17,82,96,86
			]
		}]

	};
	
	window.myBar = new Chart(ctx1, {
		type: 'bar',
		data: barChartData1,
		options: {
			title: {
				display: true,
				text: 'Chart.js Bar Chart - Stacked'
			},
			tooltips: {
				mode: 'index',
				intersect: true
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					stacked: true
				}]
			}
		}
	});*/
  
  </script>
