

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage
       Rent Item 
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Rent Item</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>


        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Edit Rent Item</h3>
          </div>
          <!-- /.box-header -->
          <form role="form" action="<?php base_url('users/update') ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">

                <?php echo validation_errors(); ?>
		<div class="col-md-6 col-xs-12 pull pull-left">

		<div class="form-group">
                  <label for="rentitem_code">Item Code*</label>
                    <div class="input-group margin-bottom-10">
	        	  <input type="hidden" id="pceditstatus" name="pceditstatus" value="false"></input>
	        	  <input type="hidden" id="pcoldname" name="pcoldname" value="<?php echo $product_data['code']; ?>"></input>
                  <input type="text" class="form-control" readonly id="rentitem_code" name="rentitem_code" 
                  placeholder="Enter product code" required value="<?php echo $product_data['code']; ?>" onkeypress="handleKeyPress(event)"
                   autocomplete="off"/>    
                          <div class="input-group-addon">
                          <span class="glyphicon glyphicon-pencil pceditMode pceditspan" id="pceditspan"></span>
                          <span class="glyphicon glyphicon-ok hide pcnonEditMode pcokspan" id="pcokspan"></span>
	               		  </div>               
                     </div> 
			     </div>
	        <div class="form-group">
        	<label for="name">Item name*</label>
	        <div class="input-group margin-bottom-10">
	        	  <input type="hidden" id="pneditstatus" name="pneditstatus" value="false"></input>
	        	  <input type="hidden" id="pnoldname" name="pnoldname" value="<?php echo $product_data['name']; ?>"></input>
	              <input type="text" class="form-control editField" readonly id="rentitem_name" 
	              name="rentitem_name" placeholder="Enter product name" required 
	              value="<?php echo html_escape($product_data['name']); ?>"  autocomplete="off"/>
	                <div class="input-group-addon">
	                  <span class="glyphicon glyphicon-pencil pneditMode pneditspan" id="pneditspan"></span>
	                  <span class="glyphicon glyphicon-ok hide pnnonEditMode pnokspan" id="pnokspan"></span>
	                </div>         
	        </div> 
	        </div> 
                            <div class="col-md-12 col-xs-12 pl0 pr0">
                       <div class="col-md-6 col-xs-12 pull pull-left md6form-group pl0 plr0"> 
                           <label for="price">Units*</label>
                   <select class="form-control" id="units" name="units" style="width: 100%;" required  placeholder="Enter Unit"  >
                         <?php if ($product_data['units'] == 1) {?>
	                     	<option value="<?php echo $product_data['units'] ?>">Nos</option> 
                   		<?php } ?> 					
				  </select>     
               </div>
    			<div class="col-md-6 col-xs-12 pull pull-right md6form-group pr0 plr0"> 
                           <label for="price">Type</label>
                 		<select class="form-control" id="type" name="type" style="width: 100%;" required >
							<?php if($product_data['type'] == 1) { ?>
								<option value="<?php echo $product_data['type'] ?>">Box</option>
		                      	<option value="2">Single</option>     
	                   		<?php } else if($product_data['type'] == 2) { ?>
	                   			<option value="<?php echo $product_data['type'] ?>">Single</option>
		                      	<option value="1">Box</option>
                        
	                   		<?php } else { ?>
		                      	<option value="1">Box</option>
								<option value="2">Single</option>
	                   		<?php } ?>
						</select>
							 
						 </div> 
                        </div>
                       <div class="col-md-12 col-xs-12 pl0 pr0">
                     	<div class="col-md-6 col-xs-12 pull pull-left md6form-group pl0 plr0">
                           <label for="price">Total Qty*</label>
                           <input type="text" class="form-control" id="totalqty" name="totalqty" placeholder="Enter total Qty" required 
                           value="<?php echo $product_data['totalqty'] ?>" autocomplete="off" onfocusout="fillavalqty()" onkeypress="return IsNumeric(event, 'error1');" onpaste="return false;" ondrop = "return false;" />
                           <span id="error1" style="color: Red;display:none">* Input digits (0 - 9)</span>
                        </div>
                        <div class="col-md-6 col-xs-12 pull pull-right md6form-group pr0 plr0"> 
                          <label for="price">Minimum Rental Days</label>
                           <input type="text" class="form-control" id="minrentaldays" name="minrentaldays" placeholder="Min Rental Days" 
                           required autocomplete="off" onkeypress="return IsNumeric(event, 'error2');" onpaste="return false;" 
                           value="<?php echo $product_data['minrentaldays'] ?>"  onkeyup="calculateRatePerDay()" ondrop = "return false;" />
                           <span id="error2" style="color: Red;display:none">* Input digits (0 - 9)</span>
                       </div> 
                        </div>
                        <div class="col-md-12 col-xs-12 pl0 pr0">
                       <div class="col-md-6 col-xs-12 pull pull-left md6form-group pl0 plr0">
                       <label for="price">Rate For Min Days</label>
                           <input type="text" class="form-control" id="rate" name="rate" placeholder="Rate" required 
                           autocomplete="off" onkeypress="return IsNumeric(event, 'error3');" onpaste="return false;" 
                            value="<?php echo $product_data['rate'] ?>"  onkeyup="calculateRatePerDay()" ondrop = "return false;" />
                           <span id="error3" style="color: Red;display:none ">* Input digits (0 - 9)</span> 
                        </div>
						<div class="col-md-6 col-xs-12 pull pull-right md6form-group pr0 plr0"> 
                           <label for="price">Rate Per Day</label>
                           <input type="text" class="form-control" id="rateperday" name="rateperday" placeholder="Rate Per Day" required readonly 
                          value="<?php echo $product_data['rateperday'] ?>"  autocomplete="off" onkeypress="return IsNumeric(event, 'error4');" onpaste="return false;" ondrop = "return false;" />
                           <span id="error4" style="color: Red;display:none ">* Input digits (0 - 9)</span> 
                        </div> 
                        </div>
						<div class="col-md-6 col-xs-12 pull pull-left md6form-group pl0 plr0">
                           <label for="price">Available Qty</label>
                           <input type="text" class="form-control" id="availableqty" name="availableqty" readonly 
                           value="<?php echo $product_data['availableqty']; ?>" placeholder="Available Qty" required 
                           autocomplete="off"/> 
                        </div>
						<div class="col-md-6 col-xs-12 pull pull-right md6form-group pr0 plr0"> 
                           <label for="price">Rented Qty</label>
                           <input type="text" class="form-control" id="rentedqty" name="rentedqty" 
                           value="<?php echo $product_data['rentedqty']; ?>" placeholder="Rented Qty" required readonly
                           autocomplete="off"/> 
                        </div> 
                     </div>
                     <div class="col-md-6 col-xs-12 pull pull-left">
		      			 <div class="form-group">
                           <label for="category">Category*</label>  &nbsp; 
                          <!-- <span class="input-group-paddon" title="Add New Category">
	                            <a href="#" data-toggle="modal" data-target="#addModal">
	                            	<i class="fa fa-plus-circle"></i>
	                            </a>
                            </span>-->
									 
                           <select class="form-control select_group" id="category" name="category" required >
                              <?php foreach ($category as $k => $v): ?>
                                  <option value="<?php echo $v['id'] ?>" <?php if($product_data['categoryid'] == $v['id']) { echo "selected='selected'"; } ?> ><?php echo $v['name'] ?></option>
                              <?php endforeach ?>
                           </select>
                        </div>
                     
                      <div class="form-group">
                           <label for="store">Availability</label>
                           <select class="form-control" id="availability" name="availability">
                                <option value="1" <?php if($product_data['availability'] == 1) { echo "selected='selected'"; } ?>>Yes</option>
                 			    <option value="2" <?php if($product_data['availability'] != 1) { echo "selected='selected'"; } ?>>No</option>
                           </select>
                        </div>
                       <div class="form-group">
                           <label for="description">Description</label>
                           <textarea type="text" class="form-control" id="description" name="description" placeholder="Enter 
                              description" autocomplete="off"><?php echo $product_data['description']; ?>
                           </textarea>
                        </div>
                        
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Save Changes</button>
                     <a href="<?php echo base_url('rentitems/') ?>" class="btn btn-warning">Back</a>
                  </div>
               </form>
               <!-- /.box-body -->
      	    </div>
            <!-- /.box -->
         </div>
         <!-- col-md-12 -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
  
  $(document).ready(function() {
    $(".select_group").select2();
    $("#description").wysihtml5();
    $("#mainRentitemsNav>a")[0].click();
    $("#mainRentitemsNav").addClass('active');
    $("#manageRentitemsNav").addClass('active');


	//Disable button and prevent double submitting
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	});
	

    $('.pneditspan').click( function() {
           $(this).parents().siblings('input').prop('readonly', false);
           $('.pneditMode').addClass('hide');
           $('.pnnonEditMode').toggleClass('hide');
    });

    $('.pnokspan').click( function() {
           $(this).parents().siblings('input').prop('readonly', true);
           $('.pneditMode').removeClass('hide');
           $('.pnnonEditMode').toggleClass('hide');
           var pnoldtext = $('#pnoldname').val();
           var pnnewtext = $('#rentitem_name').val();
           if(pnoldtext != pnnewtext) {  
          		$('#pneditstatus').val("true");
           } else {
        	   $('#pneditstatus').val("false");
           } 
    });
	$('.pceditspan').click( function() { 
	      $(this).parents().siblings('input').prop('readonly', false);
	      $('.pceditMode').addClass('hide');
	      $('.pcnonEditMode').toggleClass('hide');
	});
	
	$('.pcokspan').click( function() {
	      $(this).parents().siblings('input').prop('readonly', true);
	      $('.pceditMode').removeClass('hide');
	      $('.pcnonEditMode').toggleClass('hide');
	    
		  var pcoldtext = $('#pcoldname').val();
		  var pcnewtext = $('#rentitem_code').val();
		  if(pcoldtext != pcnewtext) {  
		     $('#pceditstatus').val("true");
		  } else {
		   	 $('#pceditstatus').val("false");
		  }  
     }); 
});
  function fillavalqty() {
	  var totalqty = $("#totalqty").val(); 
	  var availqty =  $("#availableqty").val();
	  var rentedqty =  $("#rentedqty").val();
	  
	  $("#availableqty").val(totalqty-rentedqty);
	  $("#rentedqty").val(rentedqty);
	  
  }
  function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode 
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;
 
        return true;
    } 
</script>