<?php 
		$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
		$pcodenew = $company_Prefix['pcode'];
		$pcode = $pcodenew;
?>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Manage  Item 
      </h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active"> Item</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
     	 <div class="col-md-12 col-xs-12">
            <div id="messages"></div>
            <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php elseif($this->session->flashdata('error')): ?>
            <div class="alert alert-error alert-dismissible" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <?php echo $this->session->flashdata('error'); ?>
            </div>
            <?php endif; ?>
            <div class="box">
            	<div class="box-header">
                  <h3 class="box-title">Add  Item</h3>
               </div>
               <!-- /.box-header -->
               <form role="form" action="<?php base_url('users/create') ?>" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                     <?php echo validation_errors(); ?>
                     <div class="col-md-6 col-xs-12 pull pull-left">
                    	<div class="form-group">
                           <label for="rentitem_name">Item code*</label>
	                    <div class="input-group margin-bottom-10"> 
		                 	<input type="text" class="form-control" id="rentitem_code" name="rentitem_code"  value="<?php echo $pcode;?>" 
		                       placeholder="Enter product code" required readonly autocomplete="off" onkeypress="handleKeyPress(event)"/> 
		                         	<input type="hidden" class="form-control" id="rentitem_code_hidden" name="rentitem_code_hidden"  value="<?php echo $pcode;?>" 
		                       placeholder="Enter product code" required readonly autocomplete="off" onkeypress="handleKeyPress(event)"/> 
		                       <div class="input-group-addon">
		                           <input id="manualcode" name="manualcode" value="1" type="checkbox" title="Click to enter code manually"/> 
			               	  </div>               
		                </div>
			    	 </div>
 
                        <div class="form-group">
                           <label for="rentitem_name">Item name*</label>
                           <input type="text" class="form-control" id="rentitem_name" name="rentitem_name" placeholder="Enter  item name" required autocomplete="off"/>
                        </div> 
                            <div class="col-md-12 col-xs-12 pl0 pr0">
                       <div class="col-md-6 col-xs-12 pull pull-left md6form-group pl0 plr0"> 
                           <label for="price">Units*</label>
                         	<select class="form-control" id="units" name="units" style="width: 100%;" required  placeholder="Enter Unit" >
								<option value="1">Nos</option> 
							</select>
                        </div>
                        <div class="col-md-6 col-xs-12 pull pull-right md6form-group pr0 plr0"> 
                           <label for="price">Type</label>
                       	 	<select class="form-control" id="type" name="type"
								style="width: 100%;" required>
								<option value="1">Box</option>
								<option value="2">Single</option> 
							</select>
						 </div> 
                        </div>
                       <div class="col-md-12 col-xs-12 pl0 pr0">
                     	<div class="col-md-6 col-xs-12 pull pull-left md6form-group pl0 plr0">
                           <label for="price">Total Qty*</label>
                           <input type="text" class="form-control" id="totalqty" name="totalqty" placeholder="Enter total Qty" required 
                           autocomplete="off" onfocusout="fillavalqty()"  onkeypress="return IsNumeric(event, 'error1');" onpaste="return false;" ondrop = "return false;" />
                           <span id="error1" style="color: Red;display:none">* Input digits (0 - 9)</span>
                        </div>
                        <div class="col-md-6 col-xs-12 pull pull-right md6form-group pr0 plr0"> 
                          <label for="price">Minimum Rental Days</label>
                           <input type="text" class="form-control" id="minrentaldays" name="minrentaldays" placeholder="Min Rental Days" 
                           required autocomplete="off" onkeypress="return IsNumeric(event, 'error2');" onpaste="return false;" 
                           onkeyup="calculateRatePerDay()" ondrop = "return false;" />
                           <span id="error2" style="color: Red;display:none">* Input digits (0 - 9)</span>
                       </div> 
                        </div>
                        <div class="col-md-12 col-xs-12 pl0 pr0">
                       <div class="col-md-6 col-xs-12 pull pull-left md6form-group pl0 plr0">
                       <label for="price">Rate For Min Days</label>
                           <input type="text" class="form-control" id="rate" name="rate" placeholder="Rate" required 
                           autocomplete="off" onkeypress="return IsNumeric(event, 'error3');" onpaste="return false;" 
                            onkeyup="calculateRatePerDay()" ondrop = "return false;" />
                           <span id="error3" style="color: Red;display:none ">* Input digits (0 - 9)</span> 
                        </div>
						<div class="col-md-6 col-xs-12 pull pull-right md6form-group pr0 plr0"> 
                           <label for="price">Rate Per Day</label>
                           <input type="text" class="form-control" id="rateperday" name="rateperday" placeholder="Rate Per Day" required readonly 
                           autocomplete="off" onkeypress="return IsNumeric(event, 'error4');" onpaste="return false;" ondrop = "return false;" />
                           <span id="error4" style="color: Red;display:none ">* Input digits (0 - 9)</span> 
                        </div> 
                        </div>
						<div class="col-md-6 col-xs-12 pull pull-left md6form-group pl0 plr0">
                           <label for="price">Available Qty</label>
                           <input type="text" class="form-control" id="availableqty" name="availableqty" readonly placeholder="Available Qty" required 
                           autocomplete="off"/> 
                        </div>
						<div class="col-md-6 col-xs-12 pull pull-right md6form-group pr0 plr0"> 
                           <label for="price">Rented Qty</label>
                           <input type="text" class="form-control" id="rentedqty" name="rentedqty" placeholder="Rented Qty" required readonly
                           autocomplete="off"/> 
                        </div> 
                     </div>
                     <div class="col-md-6 col-xs-12 pull pull-left">
		      			 <div class="form-group">
                           <label for="category">Category*</label>  &nbsp; 
                           <span class="input-group-paddon" title="Add New Category">
	                            <a href="#" data-toggle="modal" data-target="#addModal">
	                            	<i class="fa fa-plus-circle"></i>
	                            </a>
                            </span>
									 
                           <select class="form-control select_group" id="category" name="category" required >
                              <?php foreach ($category as $k => $v): ?>
                              <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                              <?php endforeach ?>
                           </select>
                        </div>
                     
                      <div class="form-group">
                           <label for="store">Availability</label>
                           <select class="form-control" id="availability" name="availability">
                              <option value="1">Yes</option>
                              <option value="2">No</option>
                           </select>
                        </div>
                       <div class="form-group">
                           <label for="description">Description</label>
                           <textarea type="text" class="form-control" id="description" name="description" placeholder="Enter 
                              description" autocomplete="off">
                           </textarea>
                        </div>
                        
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Save Changes</button>
                     <a href="<?php echo base_url('rentitems/') ?>" class="btn btn-warning">Back</a>
                  </div>
               </form>
               <!-- /.box-body -->
      	    </div>
            <!-- /.box -->
         </div>
         <!-- col-md-12 -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Category</h4>
      </div>

      <form role="form" action="<?php echo base_url('category/create') ?>" method="post" id="createForm">

        <div class="modal-body">

          <div class="form-group">
            <label for="brand_name">Category Name*</label>
            <input type="text" class="form-control" id="category_name" name="category_name" required placeholder="Enter category name" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="active">Status</label>
            <select class="form-control" id="active" name="active">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
        	<button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  $(document).ready(function() {

	var base_url = "<?php echo base_url(); ?>";
	  
    $(".select_group").select2();
    $("#description").wysihtml5();
    $("#mainRentitemsNav>a")[0].click();
    $("#mainRentitemsNav").addClass('active');
    $("#addRentitemsNav").addClass('active');

	//Disable button and prevent double submitting
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	});
    $('#manualcode').change(function() {
        var curVal = $("#rentitem_code_hidden").val();
    	if(this.checked) {
    		$("#rentitem_code").removeAttr("readOnly");
    		$("#rentitem_code").val("");
    	} else {
    		$("#rentitem_code").attr('readOnly','readOnly');
    		$("#rentitem_code").val(curVal);
    	}    
    });
 // submit the create from 
    $("#createForm").unbind('submit').on('submit', function() {
      var form = $(this);

      // remove the text-danger
      $(".text-danger").remove();

      $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: form.serialize(), // /converting the form data into array and sending it to server
        dataType: 'json',
        success:function(response) { 
          if(response.success === true) { 
			$('#category')
            	    .append('<option value="' + response.createid + '" selected="selected">' + response.name + '</option>');
          	    
            // hide the modal
            $("#addModal").modal('hide');

            // reset the form
            $("#createForm")[0].reset();
            $("#createForm .form-group").removeClass('has-error').removeClass('has-success');

          } else {

            if(response.messages instanceof Object) {
              $.each(response.messages, function(index, value) {
                var id = $("#"+index);

                id.closest('.form-group')
                .removeClass('has-error')
                .removeClass('has-success')
                .addClass(value.length > 0 ? 'has-error' : 'has-success');
                
                id.after(value);

              });
            } else {
              $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
              '</div>');
            }
          }
        }
      }); 

      return false;
    });
});

  function fillavalqty() {
	  var totalqty = $("#totalqty").val();
	  $("#availableqty").val(totalqty);
	  $("#rentedqty").val(0);
	  
  }
	function handleKeyPress(e){
		 var key=e.keyCode || e.which;
		  if (key==13){
			event.preventDefault();
			document.getElementById("product_name").focus();
		}
	}

    </script>