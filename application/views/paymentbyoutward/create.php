<?php
		$preinvoice_id = $this->model_paymentbyoutward->getInsertData(1);
		$invoicenew= $preinvoice_id['id'];
		$invoicenew++; 
		$payment_no = '0'.$invoicenew;
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php if(in_array('createPayments', $this->permission)): ?>
          <a href="<?php echo base_url('paymentbyoutward/') ?>" class="btn btn-primary">Manage Payment</a>
        <?php endif; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Payment</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

<!--value="01-Jan-2018"-->
        <div class="box">
        <div class="col-sm-6">
          <div class="box-header">
            <h3 class="box-title">Add Payment By Outward</h3>
          </div>
          </div>
       
          <!-- /.box-header -->
          <form role="form" action="<?php base_url('paymentbyoutward/create') ?>" method="post" class="form-horizontal">
              <div class="box-body">
                <?php echo validation_errors(); 
	
				?>
 		  <div class="col-sm-6">
          <div class="box-header" style="float:right">
         	<label>Date :</label>
			<input type="text" style="border: 1px solid #ccc;padding-left: 5px;" name="selected_date"  value=<?php echo date("d/m/Y");?> id="datepicker"/>
          </div>
          </div>
				<div class="col-md-4 col-xs-12 pull pull-left">
                    <div class="form-group">
                    <label for="payment_no" class="col-sm-5 control-label" style="text-align:left;">Payment No*</label>
                    <div class="col-sm-7">	
                      <input type="text" class="form-control" id="payment_no" name="payment_no" readonly="readonly" value="<?php echo $payment_no;?>" placeholder="Enter Payment No" required   autocomplete="off">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="text-align:left;">Outward No</label>
                      <div class="col-sm-7">	
                    <!-- input type="text" id="invoiceNo" name="invoiceNo" class="form-control"
                    	 required placeholder="Enter Invoice No" >        
                    <ul class="dropdown-menu txtinvoiceNo" style="margin-left:15px;margin-right:0px;"
                    	 role="menu" aria-labelledby="dropdownMenu"  id="DropdowninvoiceNo"></ul-->
                    <select class="form-control select_group" id="outwardnoid" name="outwardnoid" onchange="getOutwardPayDetails()">
                         <option value="">&nbsp;</option>
                         <?php foreach ($outward_no as $k => $v): ?>
                         	<option value="<?php echo $v['id'] ?>"><?php echo $v['odc_no'] ?></option>
                         <?php endforeach ?>
                     </select>
                      <input type="hidden" class="form-control" id="outwardno" name="outwardno" readonly="readonly"
                      required autocomplete="off">
                  	</div>
                  </div>
                    <div class="form-group">
                    <label class="col-sm-5 control-label" style="text-align:left;">Customer Name</label>
                     <div class="col-sm-7">	
                      <input type="text" class="form-control" id="custname" name="custname" readonly="readonly"
                      placeholder="Customer Name" required autocomplete="off">
                       <input type="hidden" class="form-control" id="custid" name="custid" readonly="readonly"
                      placeholder="Customer Id" required autocomplete="off">
                    </div>
                   </div>
                   <div class="form-group">
                    <label class="col-sm-5 control-label" style="text-align:left;">Mobile No</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" readonly autocomplete="off">
                    </div>
                  </div>
                  </div>
					<div class="col-md-4 col-xs-12 pull pull-left">
                    	<div class="form-group">
                        <label class="col-sm-5 control-label" style="text-align:left;">Estimate Amt</label>
                         <div class="col-sm-7">	
                          <input type="text" class="form-control" id="totalrent" name="totalrent" readonly="readonly"
                          placeholder="Estimate Amount"  autocomplete="off">
                        </div>
                       </div>
                       <div class="form-group">
                        <label class="col-sm-5 control-label" style="text-align:left;">Advance Paid</label>
                         <div class="col-sm-7">	
                          <input type="text" class="form-control" id="advance" name="advance" readonly="readonly"
                          placeholder="Advance Amount"  autocomplete="off">
                        </div>
                       </div>
                    
                        <div class="form-group">
                        <label class="col-sm-5 control-label" style="text-align:left;">Balance</label>
                         <div class="col-sm-7">	
                          <input type="text" class="form-control" id="balance" name="balance" readonly="readonly"
                          placeholder="Balance Amount"  autocomplete="off">
                        </div>
                       </div>
                   
                      
                        <div class="form-group">
                        <label class="col-sm-5 control-label" style="text-align:left;">Paid</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="payment" required name="payment" placeholder="Payment amount" autocomplete="off">
                        </div>
                       </div>
                      
                 </div>
                     <div class="col-md-4 col-xs-12 pull pull-left">
                     
                     <div class="form-group">
                            <label class="col-sm-5 control-label" style="text-align:left;">Mode of Pay</label>
                            <div class="col-sm-7">
                                   <select class="form-control" data-row-id="row_1" id="mop" name="mop" style="width:100%;"
                                     onchange="editCustomerEntry()" required > 
                                   <option value="1">Cash</option>
                                   <option value="2">Cheque</option>
                                   <option value="3">Card</option>
                                   <option value="4">Credit</option>
                                   <option value="5">UPI</option>
                                   <option value="6">Netbanking</option>
                                  
                             </select>
                            </div>
                          </div>
                     
                      <div class="form-group">
                        <label class="col-sm-5 control-label" style="text-align:left;">Others</label>
                        <div class="col-sm-7">
                          <input type="text" class="form-control" id="others" name="others" placeholder="Enter Others" autocomplete="off">
                        </div>
                      </div>
                     </div>
				</div>
                <br /> <br/>
                
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Make Payment</button>
                <a href="<?php echo base_url('paymentbyoutward/') ?>" class="btn btn-warning">View</a>
              </div>
              </div>
              <!-- /.box-body -->

            </form>
              <!-- /.box-body -->

            
            </form>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>


<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {
    $(".select_group").select2();
   	$(".select_group1").select2({
      dropdownParent: $("#myModal")
    });
	$("#mainPaymentsNav>a")[0].click();
    $("#mainPaymentsNav").addClass('active menu-open');
    $("#addInvPaymentNav").addClass('active');

    //Disable button and prevent double submitting as in ksk
    $('form').submit(function () {
    	$(this).find(':submit').attr('disabled', 'disabled');
    });

	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	}); 
  });
 
  function getOutwardPayDetails() {
      var id = $('#outwardnoid').val();
      if (id == "") {
      	$("#custname").val("");
          $("#custid").val("");
          $("#mobile").val("");
          $("#balance").val("");
          $("#payment").val(""); 
      } else {
          $.ajax({
            // url: base_url + 'orders/getDetailsByinvoiceNo',
  		  url: base_url + 'outward/getDetailsByOutwardNo',
              type: 'post',
              data: {
              	id: id
              },
              dataType: 'json',
              success: function(response) {
              	$("#custname").val(response.supplier_name);
      	        $("#custid").val(response.sid);
				$("#outwardno").val(response.odc_no);
				 $("#mobile").val(response.ph_no);
				$("#totalrent").val(response.totalrent);
				$("#advance").val(response.advance);
				$("#balance").val(converttonumber(response.totalrent) - converttonumber(response.advance));
      	        
      	        $("#payment").val(converttonumber(response.totalrent) - converttonumber(response.advance));             
              }
          })
      }
  }
    
</script>