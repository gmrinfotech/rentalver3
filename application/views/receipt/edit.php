<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    <?php if(in_array('createLedger', $user_permission)): ?>
          <a href="<?php echo base_url('receipt/create') ?>" class="btn btn-primary">Add Receipt</a>
    <?php endif; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Receipt</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
		
<!--value="01-Jan-2018"-->
        <div class="box">
        <div class="col-sm-6">
          <div class="box-header">
            <h3 class="box-title">Edit Receipt</h3>
          </div>
          </div>
       
          <!-- /.box-header -->
          <form role="form" action="<?php base_url('receipt/update') ?>" method="post" class="form-horizontal">
              <div class="box-body">

                <?php echo validation_errors(); 
	
				?>
   <div class="col-sm-6">
          <div class="box-header" style="float:right">
         	<label>Date :</label>
			<input type="text" style="border: 1px solid #ccc;padding-left: 5px;" name="selected_date"  value=<?php echo date("d/m/Y");?> id="datepicker"/>
          </div>
          </div>
				<div class="col-md-4 col-xs-12 pull pull-left">
                    <div class="form-group">
                    <label class="col-sm-5 control-label" style="text-align:left;">Receipt No*</label>
                    <div class="col-sm-7">	
                      <input type="text" class="form-control" id="receipt_no" name="receipt_no" placeholder="Enter receipt No"  
                      value="<?php echo $order_data['order']['receipt_no']; ?>" readonly="readonly"    autocomplete="off">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="text-align:left;">Other Details</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" id="others" name="others"  value="<?php echo $order_data['order']['others'] ?>" placeholder="Enter Others" autocomplete="off">
                    </div>
                  </div>
                  </div>

                  <div id="no-more-tables">
								<h2>&nbsp</h2>
                <table class="table table-bordered" id="product_info_table_gst">
                  <thead>
                    <tr>
                      <th style="width:25%">Ledger</th>
                      <th style="width:10%">Amount</th>
                      
                      <th style="width:5%"><button type="button" id="add_row" class="btn btn-default"><i class="fa fa-plus"></i></button></th>
                    </tr>
                  </thead>

          <tbody>
                      <?php if(isset($order_data['order_item'])): ?>
                      <?php $x = 1; ?>
                      <?php foreach ($order_data['order_item'] as $key => $val): ?>
                        <?php //print_r($v); ?>
                       <tr id="row_<?php echo $x; ?>">
                       <td data-title="Ledger">
                        <select class="form-control select_group product" data-row-id="row_<?php echo $x; ?>" id="product_<?php echo $x; ?>" required name="product[]" style="width:100%;" onchange="getLedgerData(<?php echo $x; ?>)">
                            <option value=""></option>
							<?php foreach ($ledgers as $k => $v): ?>
                              <option value="<?php echo $v['id'] ?>" <?php if($val['ledger_id'] == $v['id']) { echo "selected='selected'"; } ?>><?php echo $v['name'] ?></option>
                            <?php endforeach ?>
                          </select>
                        </td>
                        <td data-title="Amount">  <input type="text" name="amount[]" id="amount_<?php echo $x; ?>" class="form-control" value="<?php echo $val['amount'] ?>"   autocomplete="off">
                        <input type="hidden" name="under_value[]" id="under_value_<?php echo $x; ?>" class="form-control" autocomplete="off"  value="<?php echo $val['under'] ?>"></td>
                          <td><button type="button" class="btn btn-default" onclick="removeRow('<?php echo $x; ?>')"><i class="fa fa-close"></i></button></td>
                          
                     </tr>
                      <?php $x++; ?>
                     <?php endforeach; ?>
                   <?php endif; ?>
                   </tbody>
                </table></div>

                <br /> <br/>

    
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
               <button type="submit" class="btn btn-primary">Update</button>
                <a href="<?php echo base_url('receipt/') ?>" class="btn btn-warning">View</a>
              </div>
            </form>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>


<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {
    $(".select_group").select2();
   	$(".select_group1").select2({
      dropdownParent: $("#myModal")
    });
    $("#mainLedgerJournalNav>a")[0].click();
    $("#mainLedgerJournalNav").addClass('active menu-open');
    $("#manageRecieptNav").addClass('active');
   	

    //Disable button and prevent double submitting as in ksk
    $('form').submit(function () {
    	$(this).find(':submit').attr('disabled', 'disabled');
    });

	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	});
    // Add new row in the table 
    $("#add_row").unbind('click').bind('click', function() {
      var table = $("#product_info_table_gst");
	  var withinSt = $("#product_info_table_gst");
      var count_table_tbody_tr = $("#product_info_table_gst tbody tr").length;
      var row_id = count_table_tbody_tr + 1;

      $.ajax({
          url: base_url + '/receipt/getTableProductRow/',
          type: 'post',
          dataType: 'json',
          success:function(response) {
            
              // console.log(reponse.x);
               var html = '<tr id="row_'+row_id+'">'+
                   '<td> <select class="form-control select_group product" data-row-id="'+row_id+'" id="product_'+row_id+'" name="product[]" style="width:100%;"'+
				   'onchange="getLedgerData('+row_id+')">'+
                        '<option value=""></option>';
                        $.each(response, function(index, value) {
                          html += '<option value="'+value.id+'">'+value.name+'</option>';             
                        });
                        
                     html += '</select></td>'+ 
                    '<td><input type="text" name="amount[]" id="amount_'+row_id+'" class="form-control" >'+
     			    '<input type="hidden" name="under_value[]" id="under_value_'+row_id+'" class="form-control"></td>'+
                    '<td><button type="button" class="btn btn-default" onclick="removeRow(\''+row_id+'\')"><i class="fa fa-close"></i></button></td>'+
                    '</tr>';

                if(count_table_tbody_tr >= 1) {
                $("#product_info_table_gst tbody tr:last").after(html);  
              }
              else {
                $("#product_info_table_gst tbody").html(html);
              }

              $(".product").select2();
			  chooseTable();
          }
        });
      return false;
    });
  });
 
</script>