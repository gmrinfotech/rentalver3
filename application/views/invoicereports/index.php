

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Reports Invoice
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active">Invoice Reports</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
 <?php
			$from_date = date("d/m/Y");
			$to_date = date("d/m/Y");
		?>
   	 	<?php
		$from_date = array('name'=>'from_date', 'id'=>'from_date', 'value' => set_value('from_date', $from_date), 'class'=>'form-control default-date-picker', 'required' => '');
		$to_date = array('name'=>'to_date', 'id'=>'to_date', 'value' => set_value('to_date', $to_date), 'class'=>'form-control default-date-picker', 'required' => '');
		?>
       
         <div>
          <form class="form-inline" action="<?php echo base_url('invoicereports/reportData') ?>" method="POST">
            <div class="form-group">
           <label>From :</label>
			<?php echo form_input($from_date); ?>
           <label>To :</label>
		 	<?php echo form_input($to_date); ?>
            </div>
            <button btn btn-primary repSubBtntype="submit" class="btn btn-primary repSubBtn">Submit</button>
          </form>
        </div>
 		<br>
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Invoice Reports</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Date</th>
                <th>Invoice no</th>
                <th>Customer Name</th>
                <th>Phone Number</th>
                <th>Amount</th>
              	<th>Advance</th>
              	<th>Balance</th
              ></tr>
              </thead>
  				<tbody>
				  <?php if(count($orderlist) > 0): ?>
                      <?php foreach($orderlist as $key => $row): ?>
                          <tr>
                          	<td><?php echo $row['sdate']; ?></td>
                            <td><?php echo $row['invoice_id']; ?></td>
                          <!--  <td><?php/* echo date("dd/mm/YYYY", strtotime($row['sdate']));*/ ?></td>-->
                             
                            <td><?php echo $row['supplier_name'];?></td>
                            <td><?php echo $row['supplier_phone'];?></td>
                           
                            
                            <td><?php echo $row['net_amount']; ?></td>
                            <td><?php echo $row['amtpaid']; ?></td>
                           <td><?php echo $row['ibalance']; ?></td>
                          </tr>
                      <?php endforeach; ?>
                  <?php endif; ?>
            </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php if(in_array('deletePoorder', $user_permission)): ?>
<!-- remove brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove Order</h4>
      </div>

      <form role="form" action="<?php echo base_url('invoicereports/remove') ?>" method="post" id="removeForm">
        <div class="modal-body">
          <p>Do you really want to remove?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>



<script type="text/javascript">
	$(".default-date-picker").datepicker({
		  format: 'dd/mm/yyyy',
		  autoclose: true
	});  

var manageTable;
var base_url = "<?php echo base_url();  ?>";

$(document).ready(function() {
	$("#reportNav>a")[0].click();
  $("#reportNav").addClass('active');
  $("#manageInvoicereportsNav").addClass('active');
  // initialize the datatable 
    
  manageTable = $('#manageTable').DataTable({
	   dom: 'Bfrtip',
	    buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
 		],
        'order': []
 
  });
});

// remove functions 
function removeFunc(id)
{
  if(id) {
    $("#removeForm").on('submit', function() {

      var form = $(this);

      // remove the text-danger
      $(".text-danger").remove();

      $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: { order_id:id }, 
        dataType: 'json',
        success:function(response) {

          manageTable.ajax.reload(null, false); 

          if(response.success === true) {
            $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
            '</div>');

            // hide the modal
            $("#removeModal").modal('hide');

          } else {

            $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
            '</div>'); 
          }
        }
      }); 

      return false;
    });
  }
}


</script>
