<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Manage Outward</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><i
					class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Outward</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12 col-xs-12">

				<div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible"
					role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <div class="box">
					<div class="col-sm-6">
						<div class="box-header">
							<h3 class="box-title">View Outward</h3>
						</div>
					</div>

					<!-- /.box-header -->
					<form role="form" action="<?php base_url('outward/create') ?>"
						method="post" class="form-horizontal">
						<div class="box-body">

                	<?php echo validation_errors ();?>
   						<div class="col-sm-6">
								<div class="box-header" style="float: right">
									<label>Date :</label> <input type="text"
										style="border: 1px solid #ccc; padding-left: 5px;"
										name="selected_date"  value="<?php echo $outward_data['outward']['sdate'];?>"
										id="datepicker" />
								</div>
							</div>
							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Outward DC No*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="invoice_no"
											name="invoice_no" placeholder="Enter Invoice No" required
											value="<?php echo $outward_data['outward']['idc_no'] ?>" readonly autocomplete="off">
									</div>
								</div>
							<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Truck No</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="cont_no"
											name="cont_no" placeholder="Enter Container No" required
											autocomplete="off" value="<?php echo $outward_data['outward']['contNo'] ?>">
									</div>
								</div>
	

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Driver Name</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="driver_name"
											name="driver_name" placeholder="Enter Driver Name" required
											autocomplete="off" value="<?php echo $outward_data['outward']['drivername'] ?>">
									</div>
								</div>
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Driver Phone</label>
									<div class="col-sm-7">
										<input type="hidden" class="form-control" id="driver_id"
											name="driver_id" placeholder="Enter Driver Phone" required
											autocomplete="off" value="<?php echo $outward_data['outward']['did'] ?>"/> 
										<input type="text" class="form-control"
											id="driver_phone" name="driver_phone" value="<?php echo $outward_data['outward']['driverphone'] ?>"
											placeholder="Enter Driver Phone" required autocomplete="off" />
									</div>
								</div>
						

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Terms of Outward</label>
									<div class="col-sm-7">
										<select class="form-control" data-row-id="row_1" id="tod"
											name="tod" style="width: 100%;" required>
											<?php if ($outward_data['outward']['tod'] == 1) {?>
						                     	<option value="<?php echo $outward_data['outward']['tod'] ?>">By Transport</option>
						                      	<option value="2">By Hand</option>
					                   		<?php } else if($outward_data['outward']['tod'] == 2) { ?>
												<option value="<?php echo $outward_data['outward']['tod'] ?>">By Hand</option>
						                      	<option value="1">By Transport</option>
						                     <?php } else { ?>
						                      	<option value="1">By Transport</option>
						                      	<option value="2">By Hand</option>
					                   		<?php } ?>
										</select>
									</div>
								</div>

							</div>
							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">

									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Customer Name*</label>
									<div class="col-sm-7">

										<input type="hidden" class="form-control" id="customer_id"
											name="customer_id" autocomplete="off" value="<?php echo $outward_data['outward']['sid'] ?>"> 
										<input type="text"
											class="form-control" id="customer_name" name="customer_name"
											placeholder="Enter Customer Name" required autocomplete="off" 
											value="<?php echo $outward_data['outward']['supplier_name'] ?>"/>
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Address</label>
									<div class="col-sm-7">

										<input type="text" class="form-control" id="customer_address"
											name="customer_address" placeholder="Enter Customer Address"
											autocomplete="off" value="<?php echo $outward_data['outward']['supplier_address'] ?>">
									</div>
								</div>
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Phone*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_phone"
											name="customer_phone" pattern="^[0-9]{10,12}$"
											placeholder="Enter Customer Phone" required
											autocomplete="off" value="<?php echo $outward_data['outward']['ph_no'] ?>">
									</div>
								</div>
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">GST/PAN</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_gst"
											name="customer_gst" placeholder="Enter Customer GST/PAN"
											autocomplete="off" value="<?php echo $outward_data['outward']['gst_no'] ?>">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Email</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_email"
											name="customer_email"
											pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
											placeholder="Enter Customer Email" autocomplete="off" value="<?php echo $outward_data['outward']['email_id'] ?>">
									</div>
								</div>
								
								<div class="form-group">
								 <label class="col-sm-5 control-label"
										style="text-align: left;">Default</label>
								 <div class="col-sm-7">
									<select class="form-control" onchange="getRents()"
										id="default" name="default" style="width: 100%;" required>
										  <?php if ($outward_data['outward']['default'] == 1) {?>
						                     	<option value="<?php echo $outward_data['outward']['default'] ?>">Monthly</option>
						                      	<option value="2">Season</option>
						                  <?php } else { ?>
						                      	<option value="2">Season</option>
						                      	<option value="1">Monthly</option>
					                   	  <?php } ?>
									</select>
									</div>
								</div>

							</div>

							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label for="brands" class="col-sm-5 control-label"
										style="text-align: left;">State</label>
									<div class="col-sm-7">
										<input type="text" id="customer_state" name="customer_state"
											value="Tamil Nadu" class="form-control"
											placeholder="Enter State" value="<?php echo $outward_data['outward']['state'] ?>">
										<ul class="dropdown-menu txtstatename"
											style="margin-left: 15px; margin-right: 0px;" role="menu"
											aria-labelledby="dropdownMenu" id="DropdownStateName"></ul>
									</div>
								</div>
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">State Code</label>
									<div class="col-sm-7">
										<input type="text" class="form-control"
											id="customer_state_code" name="customer_state_code"
											value="33" placeholder="Enter State Code" autocomplete="off" value="<?php echo $outward_data['outward']['state_code'] ?>">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Mode of Pay</label>
									<div class="col-sm-7">
										<select class="form-control" data-row-id="row_1" id="mop"
											name="mop" style="width: 100%;" required>
											<?php if ($outward_data['outward']['mop'] == 1) {?>
						                     	<option value="<?php echo $outward_data['outward']['mop'] ?>">Cash</option>
						                      	<option value="2">Cheque</option>
						                        <option value="3">Card</option>
						                        <option value="4">Credit</option>
						                        
					                   		<?php } else if($outward_data['outward']['mop'] == 2) { ?>
												<option value="<?php echo $outward_data['outward']['mop'] ?>">Cheque</option>
						                      	<option value="1">Cash</option>
						                        <option value="3">Card</option>
						                        <option value="4">Credit</option>
						                        
					                   		<?php } else if($outward_data['outward']['mop'] == 3) { ?>
					                   			<option value="<?php echo $outward_data['outward']['mop'] ?>">Card</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="4">Credit</option>
						                    <?php } else if($outward_data['outward']['mop'] == 4) { ?>
					                   			<option value="<?php echo $outward_data['outward']['mop'] ?>">Credit</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="3">Card</option> 
					                   		<?php } else { ?>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                        <option value="3">Card</option>
						                        <option value="4">Credit</option>
					                   		<?php } ?>

										</select>
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Credit Given</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="credit_given"
											name="credit_given" placeholder="Enter Credit Given"
											autocomplete="off" value="<?php echo $outward_data['outward']['credit'] ?>">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Interest</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="interest"
											name="interest" placeholder="Interest as per bank"
											autocomplete="off" value="<?php echo $outward_data['outward']['interest'] ?>">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Employee</label>
									<div class="col-sm-7">
										<select class="form-control select_group employee"
											data-row-id="row_1" id="employee" required name="employee"
											style="width: 100%;" onchange="getemployeeData()">
												<option value=""></option>
											<?php foreach ($employees as $k => $v): ?>
					                          	<option value="<?php echo $v['id'] ?>" 
						                          	<?php if($outward_data['outward']['empid'] == $v['id'])
						                          	 { echo "selected='selected'"; } ?>><?php echo $v['employeename'] ?>
					                          	</option>
					                       <?php endforeach ?>
				                          </select>
									</div>
								</div>
							</div>
					
							<label class="col-md-12 col-xs-12 pull pull-left" style="text-align: left;
							font-size: 19px;height: 34px;padding-top: 5px;color: #231eff;padding-left: 0px;">
							Available Space to Fill and Employee Wages </label>
							<div class="col-md-12 col-xs-12 pull pull-left"
								style="border: solid 1px black; padding-top: 12px; margin-bottom: 12px;">	
								<div class="col-md-2 col-xs-12 pull pull-left">						
									<div class="form-group">
									<div class="col-sm-12">
									<b>	SBox Rack Space : </b> <input type="text" readonly class="form-control" id="tsboxes"
												name="tsboxes" autocomplete="off" value=<?php echo $srackbalspace;?>>
										<input type="hidden" class="form-control" id="enterednoofsboxes"
												name="enterednoofsboxes" autocomplete="off"> <br>
									<b>	SBox Wages : </b> <input type="text" readonly class="form-control" id="sbox_wage"
												name="sbox_wage" autocomplete="off"
												value="<?php echo $employeewage['sboxwage']?>">
										</div>
									</div>
								</div>
								<div class="col-md-2 col-xs-12 pull pull-left">
									<div class="form-group">
											<div class="col-sm-12">
									<b>	MBox Rack Space : </b> <input type="text" readonly class="form-control" id="tmboxes"
												name="tmboxes" autocomplete="off" value=<?php echo intval($srackbalspace/2);?>>
									<br><b>	MBox Wages : </b> <input type="text" readonly class="form-control" id="mbox_wage"
												name="mbox_wage" autocomplete="off"
												value="<?php echo $employeewage['mboxwage']?>">
									</div>
									</div>
								</div>
								<div class="col-md-2 col-xs-12 pull pull-left">
									
									<div class="form-group">
										<div class="col-sm-12">
										<b>	LBox Rack Space : </b> <input type="text" readonly class="form-control" id="tlboxes"
												name="tlboxes" autocomplete="off" value=<?php echo intval($srackbalspace/4);?>>
										<br><b>	LBox Wages : </b> <input type="text" readonly class="form-control" id="lbox_wage"
												name="lbox_wage" autocomplete="off" 
												value="<?php echo $employeewage['lboxwage']?>">	
										</div>
									</div>
								</div>
								<div class="col-md-2 col-xs-12 pull pull-left">
									<div class="form-group">
											<div class="col-sm-12">
										<b> Bag Rack Space : </b> <input type="text" readonly class="form-control" id="tbags"
												name="tbags" autocomplete="off" value=<?php echo intval($srackbalspace/4);?>>
										<br><b>	Bag Wages : </b> <input type="text" readonly class="form-control" id="bag_wage"
												name="bag_wage" autocomplete="off"
												value="<?php echo $employeewage['bagwage']?>">		
										</div>
									</div>
								</div>
								
								<div class="col-md-4 col-xs-12 pull pull-left">
								<br>
									<label for="gross_amount" class="col-sm-5 control-label"
											style="text-align: left;font-size: 18px;height: 34px;padding-top: 5px;background-color: #ec8282;">
											Total In Items</label>
									<div class="col-sm-5">
										<input type="hidden" class="form-control" id="row_index"
											name="row_index" placeholder="row_index" required
											autocomplete="off" value="<?php echo $outward_data['outward']['total_items'] ?>" /> 
										<input type="text"
											class="form-control" readonly id="total_items" name="total_items"
											autocomplete="off" value="<?php echo $outward_data['outward']['total_items'] ?>" />
									</div>
								</div>
							</div>
							<div id="no-more-tables">
							<h2>&nbsp</h2>
							<table class="table table-bordered" id="product_info_table_gst">
								<thead>
									<tr>
										<th style="width: 20%">Product Name</th>

										<th style="width: 7%">Unit</th>
										<th style="width: 6%; background: #fbd377 !important;">Kg/Unit</th>
										<th style="width: 9%;">Floor</th>
										<th style="width: 9%;">Chamber</th>
										<th style="width: 5%;">Rack</th>
										<th style="width: 6%; background: #19f38f !important;">No.Units</th>
										<th style="width: 7%; background: #fbd377 !important;">Rent
											for</th>
										<th style="width: 8%; background: #19f38f !important;">Rent</th>
										<th style="width: 8%; background: #19f38f !important;">Est
											Period</th>
										<th style="width: 8%">Est Rent</th>
									</tr>
								</thead>

								<tbody>
								  <?php if(isset($outward_data['outward_item'])): ?>
			                      <?php $x = 1; ?>
			                      <?php foreach ($outward_data['outward_item'] as $key => $val): ?>
			                        <?php //print_r($v); ?>
                       				<tr id="row_<?php echo $x; ?>">
										<td data-title="Product Name">
			                        <select class="form-control select_group product" data-row-id="row_<?php echo $x; ?>" id="product_<?php echo $x; ?>" required name="product[]" style="width:100%;" onchange="getCommonProductData(<?php echo $x; ?>)">
			                        <option value=""></option>
			                            <?php foreach ($products as $k => $v): ?>
			                          	  <?php if($v['name']): ?>
			                              	<option value="<?php echo $v['id'] ?>" <?php if($val['product_id'] == $v['id']) { echo "selected='selected'"; } ?>><?php echo $v['name'] ?></option>
			                           	   <?php endif; ?>
			                            <?php endforeach ?>
			                          </select>
			                           <input type="hidden" name="name[]" id="name_<?php echo $x; ?>"
											class="form-control" autocomplete="off" value="<?php echo $val['product_name'] ?>">
                       				
											</td>
										<td data-title="Unit">
									 <select class="form-control" data-row-id="row_<?php echo $x; ?>"
											id="units_<?php echo $x; ?>" name="units[]" style="width: 100%;" required
											onchange="getInEditTotalRent(<?php echo $x; ?>)">
											<?php if ($val['units'] == 1) {?>
						                     	<option value="<?php echo $val['units'] ?>">S-Box</option>
												<option value="2">M-Box</option>
												<option value="3">L-Box</option>
												<option value="4">Bag</option>
						                        
					                   		<?php } else if($val['units'] == 2) { ?>
												<option value="<?php echo $val['units'] ?>">M-Box</option>
						                      	<option value="1">S-Box</option>
												<option value="3">L-Box</option>
												<option value="4">Bag</option>
					                   		<?php } else if($val['units'] == 3) { ?>
					                   			<option value="<?php echo $val['units'] ?>">L-Box</option>
						                      	<option value="1">S-Box</option>
												<option value="2">M-Box</option>
												<option value="4">Bag</option>
					                   		<?php } else { ?>
					                   			<option value="4">Bag</option>
					                   			<option value="1">S-Box</option>
												<option value="2">M-Box</option>
												<option value="3">L-Box</option>
												
					                   		<?php } ?>

										</select></td>
										<td data-title="Kg/Unit">
										<input type="text" name="kg[]" id="kg_<?php echo $x; ?>"
											class="form-control" autocomplete="off"
											onkeyup="getInEditTotalRent(<?php echo $x; ?>)" value="<?php echo $val['kg'] ?>" ></td>
										
										<td data-title="Floor">
										<select class="form-control" id="floor_<?php echo $x; ?>" name="floor[]"
												style="width: 100%;" required onchange="fillRack(<?php echo $x; ?>)">
												<?php if ($val['floor'] == 1) { ?>
						                         <option value="<?php echo $val['floor'] ?>">G - Floor</option>
												<option value="2">1 - Floor</option>
												<option value="3">2 - Floor</option>
												<option value="4">3 - Floor</option>
						                      <?php } else  if ($val['floor'] == 2) { ?>
						                         <option value="<?php echo $val['floor'] ?>">1 - Floor</option>
						                         <option value="1">G - Floor</option>
												<option value="3">2 - Floor</option>
												<option value="4">3 - Floor</option>
					                           <?php } else  if ($val['floor'] == 3) { ?>
						                           <option value="<?php echo $val['floor'] ?>">2 - Floor</option>
						                           <option value="1">G - Floor</option>
												<option value="2">1 - Floor</option>
												<option value="4">3 - Floor</option>
						                       <?php } else { ?>
						                        <option value="4">3 - Floor</option>
						                      	<option value="1">G - Floor</option>
												<option value="2">1 - Floor</option>
												<option value="3">2 - Floor</option>									
					                      	  <?php } ?>
											</select>
										</td>
										<td data-title="Chamber">
											<select class="form-control" id="chamber_<?php echo $x; ?>" name="chamber[]" data-row-id="row_<?php echo $x; ?>"
												style="width: 100%;" required onchange="fillRack(<?php echo $x; ?>)">
												<?php if ($val['chamber'] == 1) { ?>
						                           <option value="<?php echo $val['chamber'] ?>">Chamber 1</option>
						                           <option value="2">Chamber 2</option>
												   <option value="3">Chamber 3</option>
												   <option value="4">Chamber 4</option>
						                      <?php } else  if ($val['chamber'] == 2) { ?>
						                           <option value="<?php echo $val['chamber'] ?>">Chamber 2</option>
						                           <option value="1">Chamber 1</option>
													<option value="3">Chamber 3</option>
													<option value="4">Chamber 4</option>
					                           <?php } else  if ($val['chamber'] == 3) { ?>
						                           <option value="<?php echo $val['chamber'] ?>">Chamber 3</option>
						                           <option value="1">Chamber 1</option>
													<option value="2">Chamber 2</option>
													<option value="4">Chamber 4</option>
						                       <?php } else { ?>
						                      		<option value="4">Chamber 4</option>
													<option value="1">Chamber 1</option>
													<option value="2">Chamber 2</option>
													<option value="3">Chamber 3</option>
					                      	  <?php } ?>
											</select>
										</td>
							
										<td data-title="Rack">
											<select class="form-control"
												id="rack_<?php echo $x; ?>" name="rack[]" style="width: 100%;"required" 
												onchange="fillAvailSpace(<?php echo $x; ?>)">
												 <?php foreach ($racks as $k => $v): ?>
					                           		<option value="<?php echo $v['rack'] ?>" <?php if($v['rack'] == $val['rack']) { echo "selected='selected'"; } ?>><?php echo $v['rack'] ?></option>
					                            <?php endforeach ?>
											</select>
										
											<select class="form-control" 
												id="rackbal_<?php echo $x; ?>" name="rackbal[]" style="width: 100%; display:none" required>
												 <?php foreach ($racks as $k => $v): ?>
					                               	<option value="<?php echo $v['rack'] ?>" <?php if($v['rack'] == $val['rack']) { echo "selected='selected'"; } ?>><?php echo $v['rackbalspace'] ?></option>
					                            <?php endforeach ?>
	
											</select>
										</td>
										<td data-title="No.Units">
										<input type="text" name="noofunits[]" id="noofunits_<?php echo $x; ?>"
											class="form-control" onkeyup="getInEditTotalRent(<?php echo $x; ?>, true, true)"  value="<?php echo $val['noofunits'] ?>" >
                                            <input type="hidden" name="wage[]" id="wage_<?php echo $x; ?>"
											class="form-control" required value="<?php echo $val['wage'] ?>">
                                            <input type="hidden" name="noofsboxes[]" id="noofsboxes_<?php echo $x; ?>"
											class="form-control" value="<?php echo $val['noofsboxes'] ?>">
                                            <input type="hidden" name="onoofsboxes[]" id="onoofsboxes_<?php echo $x; ?>"
											class="form-control" required value="<?php echo $val['noofsboxes'] ?>" ></td>
										<td data-title="Rent for">
										<select class="form-control" data-row-id="row_<?php echo $x; ?>"
											id="rentunit_<?php echo $x; ?>" name="rentunit[]" style="width: 100%;"
											required onchange="getInEditTotalRent(<?php echo $x; ?>, false)">
												<?php if ($val['rentunit'] == 1) {?>
						                     	<option value="<?php echo $val['rentunit'] ?>">Month</option>
												<option value="2">Days</option>
												<option value="3">Kg</option>
						                        
					                   		<?php } else if($val['rentunit'] == 2) { ?>
												<option value="<?php echo $val['rentunit'] ?>">Days</option>
						                      	<option value="1">Month</option>
												<option value="3">Kg</option>
					                   		
					                   		<?php } else { ?>
					                   			<option value="3">Kg</option>
					                   			<option value="1">Month</option>
					                   			<option value="2">Days</option>
					                   		<?php } ?>
												
										</select></td>
										<td data-title="Rent">
										<input type="hidden" name="pcode[]" id="pcode_<?php echo $x; ?>"
											class="form-control" autocomplete="off"> 
										  <input type="text"
											name="rent[]" id="rent_<?php echo $x; ?>" class="form-control"  value="<?php echo $val['rent'] ?>" 
											autocomplete="off" onkeyup="getInEditTotalRent(<?php echo $x; ?>, false)"></td>
										<td data-title="Est Period">
										<input type="text" name="period[]" id="period_<?php echo $x; ?>"
											class="form-control" autocomplete="off"  value="<?php echo $val['period'] ?>" 
											onkeyup="getInEditTotalRent(<?php echo $x; ?>, false)"></td>
										<td data-title="Est Rent">
										<input type="text" name="totamount[]" id="totamount_<?php echo $x; ?>"
											class="form-control" readonly autocomplete="off" value="<?php echo $val['totalrent'] ?>" ></td>
									</tr>
									<?php $x++; ?>
                    			<?php endforeach; ?>
                  				<?php endif; ?>
								</tbody>
							</table>
							</div>
							<br />

							<div>
								<!-- New Format -->
								<div class="col-md-12 col-xs-12 pull-right bottomDiv">

									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Outward Charges</label> 
											<input
												type="text" class="form-control" id="rent_amount"
												name="rent_amount" readonly autocomplete="off" value="<?php echo $outward_data['outward']['outwardcharge'] ?>">
 											<input type="hidden" class="form-control" id="total_noofsboxes"
												name="total_noofsboxes" readonly autocomplete="off" value="<?php echo $outward_data['outward']['total_noofsboxes'] ?>">
											<input type="hidden" class="form-control" id="total_balanceqty"
												name="total_balanceqty" readonly autocomplete="off" value="<?php echo $outward_data['outward']['total_balanceqty'] ?>">
										</div>
										</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Total Wages</label> 
											<input
												type="text" class="form-control" id="wages"
												name="wages" readonly autocomplete="off"
												value="<?php echo $outward_data['outward']['wages'] ?> ">
										</div>
									</div>
									
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Cleaning Charge</label> <input
												type="text" class="form-control" id="cleaningcharge"
												name="cleaningcharge" autocomplete="off" value="<?php echo $outward_data['outward']['cleaningcharge'] ?>"
												onkeyup="calculateNet()" />
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Vehicle Transfer Charge</label> <input
												type="text" class="form-control" id="vehtranscharge"
												name="vehtranscharge" autocomplete="off" value="<?php echo $outward_data['outward']['vehtranscharge'] ?>"
												onkeyup="calculateNet()">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Commission Charge</label> <input
												type="text" class="form-control" id="comcharge" value="<?php echo $outward_data['outward']['comcharge'] ?>"
												name="comcharge" autocomplete="off" onkeyup="calculateNet()">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gst_amount" id="gstsublbl">Untimed Loading Charge</label>
											<input type="text" class="form-control" id="untimedloading" value="<?php echo $outward_data['outward']['untimedloading'] ?>"
												name="untimedloading" autocomplete="off"
												onkeyup="calculateNet()">

										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Total Amount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="total_amount"
											value="<?php echo $outward_data['outward']['totalamount'] ?>"
												name="total_amount" readonly autocomplete="off">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Discount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="disc_amount" value="<?php echo $outward_data['outward']['discount'] ?>"
												name="disc_amount" autocomplete="off" onkeyup="calculateNet()">
										</div>
									</div>

									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Net Amount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="net_amount" value="<?php echo $outward_data['outward']['totalrent'] ?>"
												name="net_amount" readonly autocomplete="off">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="amtpaid">Advance Amount Paid</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="amtpaid" value="<?php echo $outward_data['outward']['advance'] ?>"
												name="amtpaid" onkeyup="calculateRBalance()"
												autocomplete="off" required>
										</div>
									</div>

									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="balance">Balance</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="balance" value="<?php echo $outward_data['outward']['balance'] ?>"
												name="balance" value="0" readonly autocomplete="off">
										</div>
									</div>
								</div>
							</div>
					</div>
				<!-- /.box-body -->

				<div class="box-footer">
					
					<a target="__blank" href="<?php echo base_url() . 'outward/printDiv/'.$outward_data['outward']['id'] ?>" class="btn btn-success" >Print</a>
					<a href="<?php echo base_url('outward/') ?>" class="btn btn-warning">Back</a>
				</div>
				</form>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- col-md-12 -->
</div>
<!-- /.row -->


</section>
<!-- /.content -->
</div>


<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {
    $(".select_group").select2();
    $(".select_group1").select2({
      dropdownParent: $("#myModal")
    });
    $(".select_group2").select2({
        dropdownParent: $("#myModal1")
    });
    $("#mainOutwardNav>a")[0].click();
    $("#mainOutwardNav").addClass('active');
    $("#manageOutwardNav").addClass('active');
    

	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	});

</script>