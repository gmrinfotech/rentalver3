<?php
$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
$invoiceprefix= $company_Prefix['inname']; 
$invoicenew = $company_Prefix['inno'];
$bill_no =$invoiceprefix.$invoicenew;
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Manage Outward</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><i
					class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Outward</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible"
					role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        		<div class="box">
					<div class="col-sm-12">
						<div class="box-header">
							<h3 class="box-title">Add Outward</h3>
						</div>
					</div>

					<!-- /.box-header -->
					<form role="form" action="<?php base_url('outward/create') ?>"
						method="post" class="form-horizontal" autocomplete="off">
						<div class="box-body">
              
   						  <div class="col-sm-6" style="float: left;color:red;font-weight:bold"> 
   						   <?php echo validation_errors ();?>
   						   </div>
   						   <div class="col-sm-6">
								<div class="box-header" style="float: right">
									<label>Date :</label> <input type="text"
										style="border: 1px solid #ccc; padding-left: 5px;"
										name="selected_date" value=<?php echo date("d/m/Y");?>
										id="datepicker" />
								</div>
							</div>
							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Lot No*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="invoice_no"
											name="invoice_no" placeholder="Enter Invoice No" required
											value="<?php echo $bill_no;?>" readonly autocomplete="off">
									</div>
								</div>

							    <div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Truck No</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="cont_no"
											name="cont_no" placeholder="Enter Container No" required
											autocomplete="off">
										 <a href="#" data-toggle="modal" data-target="#myModal1">Pick
										 Vehicle No</a>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Driver Phone</label>
									<div class="col-sm-7">
										<input type="hidden" class="form-control" id="driver_id"
											name="driver_id" placeholder="Enter Driver Phone" required
											autocomplete="off" /> <input type="text" class="form-control"
											id="driver_phone" name="driver_phone"
											placeholder="Enter Driver Phone" required autocomplete="off" /> 
									</div>
								</div> 
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Driver Name</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="driver_name"
											name="driver_name" placeholder="Enter Driver Name" required
											autocomplete="off">
									</div>
								</div>
  								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Terms of Outward</label>
									<div class="col-sm-7">
										<select class="form-control" data-row-id="row_1" id="tod"
											name="tod" style="width: 100%;" required>
											<option value="1">By Transport</option>
											<option value="2">By Hand</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-12 pull pull-left"> 
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Mode of Pay</label>
									<div class="col-sm-7">
										<select class="form-control" data-row-id="row_1" id="mop"
											name="mop" style="width: 100%;" required>
											<option value="1">Cash</option>
											<option value="2">Cheque</option>
											<option value="3">Card</option>
											<option value="4">Credit</option>

										</select>
									</div>
								</div>
								
								<div class="form-group"> 
									<label class="col-sm-5 control-label"
										style="text-align: left;">Customer Name*</label>
									<div class="col-sm-7">

										<input type="hidden" class="form-control" id="customer_id"
											name="customer_id" autocomplete="off"> <input type="text"
											class="form-control" id="customer_name" name="customer_name"
											placeholder="Enter Customer Name" required autocomplete="off" />
										<a href="#" data-toggle="modal" data-target="#myModal">Pick Customer</a>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Address</label>
									<div class="col-sm-7">

										<input type="text" class="form-control" id="customer_address"
											name="customer_address" placeholder="Enter Customer Address"
											autocomplete="off">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Phone*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_phone"
											name="customer_phone" pattern="^[0-9]{10,12}$"
											placeholder="Enter Customer Phone" required
											autocomplete="off">
									</div>
								</div>
				 
							</div>

							<div class="col-md-4 col-xs-12 pull pull-left">
											<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">GST/PAN</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_gst"
											name="customer_gst" placeholder="Enter Customer GST/PAN"
											autocomplete="off">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Email</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_email"
											name="customer_email"
											pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
											placeholder="Enter Customer Email" autocomplete="off">
									</div>
								</div> 
								
								<div class="form-group">
									<label for="brands" class="col-sm-5 control-label"
										style="text-align: left;">State</label>
									<div class="col-sm-7">
										<input type="text" id="customer_state" name="customer_state"
											value="Tamil Nadu" class="form-control"
											placeholder="Enter State">
										<ul class="dropdown-menu txtstatename"
											style="margin-left: 15px; margin-right: 0px;" role="menu"
											aria-labelledby="dropdownMenu" id="DropdownStateName"></ul>
									</div>
									<input type="hidden" class="form-control"
											id="customer_state_code" name="customer_state_code"
											value="33" placeholder="Enter State Code" autocomplete="off">
								</div>
					 
								
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Employee</label>
									<div class="col-sm-7">
										<select class="form-control select_group employee"
											data-row-id="row_1" id="employee" required name="employee"
											style="width: 100%;">
												<option value=""></option>
											    <?php foreach ($employees as $k => $v): ?>
					                              <option value="<?php echo $v['id'] ?>"><?php echo $v['employeename'] ?></option>
					                            <?php endforeach ?>
				                          </select>
									</div>
							 	 </div>
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Total Items</label>
									<div class="col-sm-7">
								  <input type="text" class="form-control" id="row_index" name="row_index" placeholder="row_index" required autocomplete="off" value="1"/> 
							      <input type="hidden" readonly
												class="form-control" id="total_items" name="total_items"
												autocomplete="off" value="0" />
								</div>
							</div>
 							 </div>
							<div id="no-more-tables">
							<h2>&nbsp</h2>
							<table class="table table-bordered" id="product_info_table_gst">
								<thead>
									<tr>
										<th style="width: 20%">Box Item</th>
										<th style="width: 20%">Item Name</th>
										<th style="width: 9%">Units</th> 
										<th style="width: 9%">Avl Qty</th> 
										<th style="width: 9%">Min R-Days</th> 
										<th style="width: 7%; background: #fbd377 !important;">Rate/Day</th>
										<th style="width: 6%; background: #19f38f !important;">No.Units</th> 
										<th style="width: 8%; background: #19f38f !important;">Period</th>
										<th style="width: 8%">Rent</th>
										<th style="width: 3%">Remove</th>
									</tr>
								</thead>

								<tbody>
									<tr id="row_1">
									  <td data-title="Box Item Name"> 
											<select class="form-control select_group product"
												data-row-id="row_1" id="boxitem_name_1" required name="boxitem_name[]"
												style="width: 100%;" onchange="getCommonRentItemData(1)">
													<option value=""></option>
												    <?php foreach ($boxitems as $k => $v): ?>
						                              <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
						                            <?php endforeach ?>
					                          </select> 
												<input type="hidden" name="bname[]" id="bname_1"
												class="form-control" autocomplete="off">										
									   </td>
										<td data-title="Item Name"> 
											<select class="form-control select_group product"
												data-row-id="row_1" id="rentitem_name_1" required name="rentitem_name[]"
												style="width: 100%;" onchange="getCommonProductData(1)">
													<option value=""></option>
												    <?php foreach ($products as $k => $v): ?>
						                              <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
						                            <?php endforeach ?>
					                        </select>  
											<input type="hidden" name="rname[]" id="rname_1"
												class="form-control" autocomplete="off">
										</td>
										<td data-title="Unit">
											<input type="text" name="units[]" id="units_1" readonly
											class="form-control" autocomplete="off"></td>
										</td>
										<td data-title="Avl Qty">
										<input type="text" name="avlqty[]" id="avlqty_1" readonly
											class="form-control" autocomplete="off"></td>
										<td data-title="Min R-Days">
										<input type="text" class="form-control" id="minrentaldays_1" name="minrentaldays[]" readonly
											 required autocomplete="off"/> 
										</td>
										<td data-title="Rate/Day">
											 <input type="text" class="form-control" id="rate_1" name="rate[]" 
											 readonly data-row-id="row_1"/> 	 
										</td>
							 
										<td data-title="No.Units">
											<input type="text" name="noofunits[]" id="noofunits_1" value="1"
											class="form-control" required onkeyup="getTotalRent(1)"> 
											<input type="hidden" name="balanceqty[]" id="balanceqty_1"
											class="form-control" placeholder="balanceqty"></td> 
										<td data-title="Period">
										<input type="text" name="period[]" id="period_1"
											class="form-control" autocomplete="off"
											onkeyup="getTotalRent(1)" value="0"></td>
										<td data-title="Rent">
										<input type="text" name="totamount[]" id="totamount_1"
											class="form-control" readonly autocomplete="off"  value="0"></td>
										<td data-title="Remove">
										<button type="button" class="btn btn-default"
												onclick="removeRow('1')">
												<i class="fa fa-close"></i>
										</button></td>
									</tr>
								</tbody>
							</table>
							</div>
							<br />
							<div>
								&nbsp;
								<button type="button" id="add_row" disabled class="btn btn-default">
									<i class="fa fa-plus"></i>&nbsp; Add New Row
								</button>
							</div>
							<div>
								<!-- New Format -->
								<div class="col-md-12 col-xs-12 pull-right bottomDiv">
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Outward Charges</label> 
											<input
												type="text" class="form-control" id="rent_amount"
												name="rent_amount" readonly autocomplete="off">
											
										    <input
												type="hidden" class="form-control" id="total_noofsboxes"
												name="total_noofsboxes" readonly autocomplete="off" placeholder="total_noofsboxes">
											<input
												type="hidden" class="form-control" id="total_balanceqty"
												name="total_balanceqty" readonly autocomplete="off" placeholder="total_balanceqty">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Total Wages</label> 
											<input
												type="text" class="form-control" id="wages"
												name="wages" autocomplete="off" onkeyup="calculateNet()">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Cleaning Charge</label> <input
												type="text" class="form-control" id="cleaningcharge"
												name="cleaningcharge" autocomplete="off"
												onkeyup="calculateNet()" />
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Vehicle Transfer Charge</label> <input
												type="text" class="form-control" id="vehtranscharge"
												name="vehtranscharge" autocomplete="off"
												onkeyup="calculateNet()">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Commission Charge</label> <input
												type="text" class="form-control" id="comcharge"
												name="comcharge" autocomplete="off" onkeyup="calculateNet()">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gst_amount" id="gstsublbl">Untimed Loading Charge</label>
											<input type="text" class="form-control" id="untimedloading"
												name="untimedloading" autocomplete="off"
												onkeyup="calculateNet()">

										</div>
									</div>

									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Total Amount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="total_amount"
												name="total_amount" readonly autocomplete="off">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Discount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="disc_amount"
												name="disc_amount" autocomplete="off" onkeyup="calculateNet()">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Net Amount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="net_amount"
												name="net_amount" readonly autocomplete="off">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="amtpaid">Amount Paid</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" required class="form-control" id="amtpaid"
												name="amtpaid" onkeyup="calculateRBalance()"
												autocomplete="off" value="0">
										</div>
									</div>

									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="balance">Balance</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="balance"
												name="balance" value="0" readonly autocomplete="off">
										</div>
									</div>
								</div>
							</div> 
						</div> 
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Create Outward</button>
					<a href="<?php echo base_url('outward/') ?>" class="btn btn-warning">Back</a>
				</div>
				</form>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- col-md-12 -->

		 <!-- Pop up msg dialog -->
            <div class="modal fade" id="popup_detail_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="vertical-alignment-helper">
			        <div class="modal-dialog vertical-align-center">
			            <div class="modal-content" style="width: 350px;">
			                <div class="modal-header" style="background: #3c8dbc; color:white">
			                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
			                    </button>
			                     <h4 class="modal-title" id="myModalLabel">Message Box</h4>
			                </div>
			                <div class="modal-body" id="popup_detail_model_body"></div>
			                <div class="modal-footer">
			                    <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
			                </div>
			            </div>
			        </div>
			    </div>
			</div> 
			 <!-- Pop up msg dialog -->
</div>
<!-- /.row -->


</section>
<!-- /.content -->
</div>
<!-- popup form -->
<div id="myModal" class="modal fade" aria-labelledby="myModalLabel"
	aria-hidden="true" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true"></button>
				<h4 class="modal-title">Pick Customer</h4>
			</div>
			<div class="modal-body" id="myModalBody">
				<table class="table table-bordered">
					<tr>
						<td>Customer Name</td>
						<td><select class="form-control select_group1" id="supp_name"
							name="supp_name" style="width: 100%;" onchange="getSuplData()"
							required>
								<option value=""></option>
                            <?php foreach ($suppliers as $k => $v): ?>
                              <option value="<?php echo $v['supl_id'] ?>"><?php echo $v['supp_name'] ?></option>
                            <?php endforeach ?>
                         </select></td>
					</tr>
					<tr>
						<td>Customer Address</td>
						<input type="hidden" name="supp_name_hid" id="supp_name_hid"
							readonly class="form-control">
						<input type="hidden" name="supp_hid" id="supp_hid" readonly
							class="form-control">
						<td><input type="text" name="supp_address" id="supp_address"
							readonly class="form-control"></td>
					</tr>
					<tr>
						<td>GST Number/Pan Number</td>
						<td><input type="text" name="gst_no" id="gst_no"
							class="form-control" readonly autocomplete="off"></td>
					</tr>
					<tr>
						<td>Phone Number</td>
						<td><input type="text" name="ph_no" id="ph_no"
							class="form-control" readonly autocomplete="off"></td>
					</tr>
					<tr>
						<td>Email Id</td>
						<td><input type="text" name="email_id" id="email_id"
							class="form-control" readonly autocomplete="off" /></td>
					</tr>
					<tr>
						<td>State</td>
						<td><input type="text" name="state" id="state"
							class="form-control" readonly autocomplete="off" /></td>
					</tr>
					<tr>
						<td>State Code</td>
						<td><input type="text" name="state_code" id="state_code"
							class="form-control" readonly autocomplete="off" /></td>
					</tr>
				</table>

				<div id="alert-msg"></div>
			</div>
			<div class="modal-footer">
				<input class="btn btn-default" id="submit" name="submit"
					data-dismiss="modal" type="button" value="OK"
					onclick="selectSupl()" /> <input class="btn btn-default"
					type="button" data-dismiss="modal" value="Close"
					onclick="clearCus()" />
			</div>
            <?php echo form_close(); ?>            
        </div>
	</div>
</div>

<div id="myModal1" class="modal fade" aria-labelledby="myModalLabel"
	aria-hidden="true" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true"></button>
				<h4 class="modal-title">Pick Vehicle</h4>
			</div>
			<div class="modal-body" id="myModalBody1">
				<table class="table table-bordered">
					<tr>
						<td>Truck No</td>
						<td><select class="form-control select_group2" id="truckno"
							name="truckno" style="width: 100%;"
							onchange="getDriverData()" required>
								<option value=""></option>
                            <?php foreach ($drivers as $k => $v): ?>
                              <option value="<?php echo $v['id'] ?>"><?php echo $v['truckno'] ?></option>
                            <?php endforeach ?>
                         </select></td>
					
					</tr>
					
					<tr>
						<td>Driver Phone</td>
						<td><input type="text" name="driverphone" id="driverphone"
							class="form-control" readonly autocomplete="off"></td>
					</tr>
					<tr>
						<td>Driver Name</td>
						<input type="hidden" name="driver_hid" id="driver_hid" readonly
							class="form-control">
						<input type="hidden" name="truck_no_hid" id="truck_no_hid"
							readonly class="form-control">
						<td><input type="text" name="drivername" id="drivername" readonly
							class="form-control"></td>
					</tr>
				
				</table>

				<div id="alert-msg"></div>
			</div>
			<div class="modal-footer">
				<input class="btn btn-default" id="submit" name="submit"
					data-dismiss="modal" type="button" value="OK"
					onclick="selectDriver()" /> <input class="btn btn-default"
					type="button" data-dismiss="modal" value="Close"
					onclick="clearDri()" />
			</div>
            <?php echo form_close(); ?>            
        </div>
	</div>
</div>

<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {
	  $("#mainOutwardNav>a")[0].click();
    $("#mainOutwardNav").addClass('active');
    $("#addOutwardNav").addClass('active');

    // Add new row in the table 
    $("#add_row").unbind('click').bind('click', function() {
       addoutwardrow();
   });
  });
  //Gst Operations
  // get the product information from the server
  function getCommonProductData(row_id)
  {
	  getRentItemData(row_id);
	   var btn = $("#add_row");
	   btn.removeAttr("disabled");
  }

  function getCommonRentItemData(row_id)
  {
	  getBoxRentItemData(row_id);
	   var btn = $("#add_row");
	   btn.removeAttr("disabled");
  }
  

</script>