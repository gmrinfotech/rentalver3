<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage Prefix
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Prefix</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <?php if(in_array('createPrefix', $user_permission)): ?>
        <!--  <button class="btn btn-primary" data-toggle="modal" data-target="#addBrandModal">Add Brand</button>
          <br /> <br />-->
        <?php endif; ?>

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Manage Prefix</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Lot Prefix</th>
                <th>Lot Current No</th>
                <th>Outward Prefix</th>
                <th>Out Current No</th>
                <th>Invoice Prefix</th>
                <th>Invoice Current No</th>
                <th>Status</th>
                <?php if(in_array('updatePrefix', $user_permission)): ?>
                  <th>Action</th>
                <?php endif; ?>
              </tr>
              </thead>

            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
 
<!-- edit brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="editBrandModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Prefix</h4>
      </div>

      <form role="form" action="<?php echo base_url('invoiceprefixs/update') ?>" method="post" id="updateBrandForm">

        <div class="modal-body">
          <div id="messages"></div>
          <div class="form-group">
            <label for="brand_name">Lot Prefix Name</label>
            <input type="text" class="form-control" id="edit_inward_name" name="edit_inward_name" placeholder="Enter lot prefix name" autocomplete="off">
          </div>
          
         <div class="form-group">
            <label for="brand_name">Lot Current No</label>
            <input type="text" class="form-control" id="edit_in_no" name="edit_in_no" placeholder="Enter lot prefix no" autocomplete="off">
          </div>
          
            <div class="form-group">
            <label for="brand_name">Outward Prefix Name</label>
            <input type="text" class="form-control" id="edit_outward_name" name="edit_outward_name" placeholder="Enter outward prefix name" autocomplete="off">
          </div>
          
          
            <div class="form-group">
            <label for="brand_name">Outward Current No</label>
            <input type="text" class="form-control" id="edit_out_no" name="edit_out_no" placeholder="Enter outward current no" autocomplete="off">
          </div>
          
          <div class="form-group">
            <label for="edit_brand_name">Invoice Prefix Name</label>
            <input type="text" class="form-control" id="edit_inv_name" name="edit_inv_name" placeholder="Enter invoice prefix name" autocomplete="off">
          </div>

          <div class="form-group">
            <label for="edit_brand_name">Invoice Current No</label>
            <input type="text" class="form-control" id="edit_inv_no" name="edit_inv_no" placeholder="Enter invoice current no" autocomplete="off">
          </div>
          
          <div class="form-group">
            <label for="edit_active">Status</label>
            <select class="form-control" id="edit_active" name="edit_active">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Update</button>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
 
<script type="text/javascript">
var manageTable;

$(document).ready(function() {
	$("#mainSettingsNav>a")[0].click();
  $("#mainSettingsNav").addClass('active');
  $("#prefixNav").addClass('active');

  // initialize the datatable 
  manageTable = $('#manageTable').DataTable({
    'ajax': 'fetchPrefixData',
    'order': []
  });
 });

function editBrand(id)
{ 
  $.ajax({
    url: 'fetchPrefixDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {
      $("#edit_inv_name").val(response.name);
      $("#edit_inward_name").val(response.inname);
      $("#edit_outward_name").val(response.outname);
      $("#edit_inv_no").val(response.invno);
      $("#edit_in_no").val(response.inno);
      $("#edit_out_no").val(response.outno);
      $("#edit_active").val(response.active);

      // submit the edit from 
      $("#updateBrandForm").unbind('submit').bind('submit', function() {
        var form = $(this);

        // remove the text-danger
        $(".text-danger").remove();

        $.ajax({
          url: form.attr('action') + '/' + id,
          type: form.attr('method'),
          data: form.serialize(), // /converting the form data into array and sending it to server
          dataType: 'json',
          success:function(response) {

            manageTable.ajax.reload(null, false); 

            if(response.success === true) {
              $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
              '</div>');


              // hide the modal
              $("#editBrandModal").modal('hide');
              // reset the form 
              $("#updateBrandForm .form-group").removeClass('has-error').removeClass('has-success');

            } else {

              if(response.messages instanceof Object) {
                $.each(response.messages, function(index, value) {
                  var id = $("#"+index);

                  id.closest('.form-group')
                  .removeClass('has-error')
                  .removeClass('has-success')
                  .addClass(value.length > 0 ? 'has-error' : 'has-success');
                  
                  id.after(value);

                });
              } else {
                $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
                '</div>');
              }
            }
          }
        }); 

        return false;
      });

    }
  });
}

</script>
