

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Reports Outward
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active">Outward Reports</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
 <?php
			$from_date = date("d/m/Y");
			$to_date = date("d/m/Y");
		?>
   	 	<?php
		$from_date = array('name'=>'from_date', 'id'=>'from_date', 'value' => set_value('from_date', $from_date), 'class'=>'form-control default-date-picker', 'required' => '');
		$to_date = array('name'=>'to_date', 'id'=>'to_date', 'value' => set_value('to_date', $to_date), 'class'=>'form-control default-date-picker', 'required' => '');
		?>
       
  <div>
          <form class="form-inline" action="<?php echo base_url('outwardreports') ?>" method="POST">
            <div class="form-group">
           <label>From :</label>
			<?php echo form_input($from_date); ?>
           <label>To :</label>
		 	<?php echo form_input($to_date); ?>
            </div>
                  	   <label>Customer Name :</label>
            <select class="form-control" name="customer" id="customer" style="width:150px !important">
                			<option value="" <?php if($fcustomer == "") { echo "selected='selected'"; } ?>>All</option>
                            <?php foreach ($customers as $k => $v): ?> 
                             <option value="<?php echo $v['supl_id'] ?>" 
                             <?php if($fcustomer == $v['supl_id']) { echo "selected='selected'"; } ?> >
                             <?php echo $v['supp_name'] ?></option> 
                            <?php endforeach ?>
                            <option value="000" <?php if($fcustomer == "000") { echo "selected='selected'"; } ?>>Unregistered Customers</option>
              </select>
            <button btn btn-primary repSubBtntype="submit" class="btn btn-primary repSubBtn">Submit</button>
          </form>
        </div>
        
        <br>
         
        <div class="box"> 
          <!-- /.box-header -->
          <div class="box-body">
           <div id="flip-scroll">
            <table id="manageTable" class="table table-bordered table-striped cf">
              <thead class="cf">
              <tr>
                <th>Outward no</th>
                <th>Date</th>
                <th>Customer Name</th>
                <th>Phone Number</th>
                <th>Type</th>
                <th>Box/Rent Item</th>
                <th>No of Units</th>
                <th>Est Rent</th>
              	<th>Advance Paid</th> 
              </tr>
              </thead>
  				<tbody>
				  <?php if(count($orderlist) > 0): ?>
                      <?php foreach($orderlist as $key => $row): ?>
                          <tr>
                            <td><?php echo $row['odc_no']; ?></td> 
                            <td><?php echo $row['sdate']; ?></td>
                            <td><?php echo $row['supplier_name'];?></td>
                            <td><?php echo $row['ph_no'];?></td>
                            <td><?php echo $row['itype']; ?></td> 
                            <td><?php echo $row['name']; ?></td> 
                            <td><?php echo $row['noofunits']; ?></td> 
                            <td><?php echo $row['totalrent']; ?></td>
                            <td><?php echo $row['advance']; ?></td> 
                          </tr>
                      <?php endforeach; ?>
                  <?php endif; ?>
            </tbody>
            </table> </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
 

<script type="text/javascript">
	$(".default-date-picker").datepicker({
		  format: 'dd/mm/yyyy',
		  autoclose: true
	});  

var manageTable;
var base_url = "<?php echo base_url();  ?>";

$(document).ready(function() {
	$("#reportNav>a")[0].click();
  $("#reportNav").addClass('active');
  $("#manageOutwardreportsNav").addClass('active');
  // initialize the datatable 
    
  manageTable = $('#manageTable').DataTable({
	    dom: 'Bfrtip',
	    bSort : false, 
	    bFilter: false,
	    buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
 		],
        'order': []
 
  });
});

</script>
