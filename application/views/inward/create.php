<?php
$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
$invoiceprefix= $company_Prefix['inname'];
//$preinvoice_id = $this->model_inward->getInsertData ( 1 );
//$invoicenew = $preinvoice_id ['id'];
$invoicenew = $company_Prefix['inno'];
$bill_no =$invoiceprefix.$invoicenew;
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Manage Inward</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><i
					class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Inward</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12 col-xs-12">

				<div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible"
					role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <div class="box">
					<div class="col-sm-12">
						<div class="box-header">
							<h3 class="box-title">Add Inward</h3>
						</div>
					</div>

					<!-- /.box-header -->
					<form role="form" action="<?php base_url('inward/create') ?>"
						method="post" class="form-horizontal">
						<div class="box-body">
							<div class="col-sm-6" style="float: left;color:red;font-weight:bold"> 
			                <?php 											
								echo validation_errors (); 
							?>
							</div>
   							<div class="col-sm-6">
								<div class="box-header" style="float: right">
									<label>Inward Date :</label> <input type="text"
										style="border: 1px solid #ccc; padding-left: 5px;"
										name="selected_date" value=<?php echo date("d/m/Y");?>
										id="datepicker" />
								</div>
							</div>
							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Inward No*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="invoice_no"
											name="invoice_no" placeholder="Enter Invoice No" required
											value="<?php echo $bill_no;?>" readonly autocomplete="off">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Outward No*</label>
									<div class="col-sm-7">
										<select class="form-control select_group"
											id="outward_no" required name="outward_no"
											style="width:100%;" onchange="getOutwardDetails()">
												<option value=""></option>
				                            <?php foreach ($outwardDC as $k => $v): ?>
				                              <option value="<?php echo $v['id'] ?>"><?php echo $v['odc_no'].'-'.$v['ph_no'] ?></option>
				                            <?php endforeach ?>
				                          </select>

									</div>
								</div>
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Outward Date</label>
									<div class="col-sm-7">
												<input type="text" class="form-control" id="odate"
											name="odate" required
											readonly autocomplete="off">
									</div>
								</div>
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Customer Name</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="isupl_name"
											name="isupl_name" placeholder="Customer Name"
											autocomplete="off">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Customer Phone</label>
									<div class="col-sm-7">
									    	<input type="text" class="form-control" id="isupl_phone"
											name="isupl_phone" placeholder="Customer Phone"
											autocomplete="off">
											<input type="hidden" class="form-control" id="sid"
											name="sid" placeholder="Customer Phone"
											autocomplete="off">
                                             <input type="hidden" class="form-control" id="semail"
											name="semail" placeholder="semail"
											autocomplete="off">
									</div>
								</div>
	
							</div>
							<div class="col-md-4 col-xs-12 pull pull-left"> 				
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Mode of Pay</label>
									<div class="col-sm-7">
									<select class="form-control" data-row-id="row_1" id="mop"
											name="mop" style="width: 100%;" required>
											<option value="1">Cash</option>
											<option value="2">Cheque</option>
											<option value="3">Card</option>
											<option value="4">Credit</option>
                                            <option value="5">UPI</option>
                                            <option value="6">Netbanking</option>

										</select>
									</div>
								</div>
				
							 	<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Truck No</label>
									<div class="col-sm-7">
										<input type="hidden" class="form-control" id="driver_id"
											name="driver_id" required
											autocomplete="off" />
										<input type="text" class="form-control" id="cont_no"
											name="cont_no" placeholder="Enter Container No" required
											autocomplete="off">
											<a href="#" data-toggle="modal" data-target="#myModal1">Pick
											Vehicle</a>
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Driver Name</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="driver_name"
											name="driver_name" placeholder="Enter Driver Name" required
											autocomplete="off">
									</div>
								</div>
								
								<div class="form-group">
									 <label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Driver Phone</label>
									<div class="col-sm-7">
									 <input type="text" class="form-control"
											id="driver_phone" name="driver_phone"
											placeholder="Enter Driver Phone" required autocomplete="off" />
									</div>
								</div>
								
							</div>

							<div class="col-md-4 col-xs-12 pull pull-left">
							 
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Terms of Inward</label>
									<div class="col-sm-7">
										<select class="form-control" id="tod"
											name="tod" style="width: 100%;" required>
											<option value="1">By Transport</option>
											<option value="2">By Hand</option>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Total days</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="totaldays"
											name="totaldays" placeholder="Total days" required
											autocomplete="off">
									</div>
								</div>
		 					
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Employee</label>
									<div class="col-sm-7">
										<select class="form-control select_group employee"
											  id="employee" required name="employee"
											style="width: 100%;">
												<option value=""></option>
											    <?php foreach ($employees as $k => $v): ?>
					                              <option value="<?php echo $v['id'] ?>"><?php echo $v['employeename'] ?></option>
					                            <?php endforeach ?>
				                          </select>
									</div>
								</div>
                                <div class="form-group">
                                        <label class="col-sm-5 control-label" style="text-align:left;">Labours </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="others" name="others" placeholder="Enter Labour Details" autocomplete="off">
                                        </div> 
                                  	</div>
									<div class="form-group">
										<label for="gross_amount" class="col-sm-5 control-label"
											style="text-align: left;">Total In Items</label>
										<div class="col-sm-7">
												<input type="hidden" class="form-control" id="row_index"
												name="row_index" placeholder="row_index" required
												autocomplete="off" value="1" /> 
											<input type="text" readonly
												class="form-control" id="total_items" name="total_items"
												autocomplete="off" value="0" />
										</div>
									</div> 
							</div>
  
							<table class="table table-bordered" id="product_info_table_gst">
								<thead>
									<tr>
									 	<th style="width: 8%">Item Type</th>  
										<th style="width: 20%">Box/Item Name</th>
										<th style="width: 5%">Type</th>
										<th style="width: 5%">Units</th> 
										<th style="width: 9%">Min R-Days</th> 
										<th style="width: 7%; background: #fbd377 !important;">Rate</th>
										<th style="width: 7%; background: #fbd377 !important;">Rate/Day</th>
										<th style="width: 6%; background: #19f38f !important;">Out Units</th> 
										<th style="width: 5%; background: #19f38f !important;">In Units</th>
										<th style="width: 8%; background: #19f38f !important;">Grace Period</th>
										<th style="width: 8%; background: #19f38f !important;">Period in Days</th>
										<th style="width: 8%">Rent</th> 
									</tr>
								</thead>

								<tbody>
									<tr id="row_1">
										<td data-title="Type">
											<select class="form-control" id="itype_1" name="itype[]"
												style="width: 100%;" required>
												<option value="1">Box</option>
												<option value="2">Single</option> 
											</select>
										</td>
									  <td data-title="Box/Item Name">  
					                          	<input type="hidden" name="boxitem_name[]" id="boxitem_name_1"
												class="form-control" readonly autocomplete="off">	
												<input type="hidden" name="boxitem_id[]" id="boxitem_id_1"
												class="form-control" readonly autocomplete="off">	
												<input type="text" name="bname[]" id="bname_1"
												class="form-control" autocomplete="off">										
									   </td>
									 	<td data-title="Type">
											<select class="form-control" id="type_1" name="type[]"  
												style="width: 100%;" required>
												<option value="1">Main</option>
												<option value="2">Side</option> 
											</select>
										</td>
										<td data-title="Unit">
											<input type="text" name="units[]" id="units_1" readonly
											class="form-control"></td>
										</td>
						 
										<td data-title="Min R-Days">
										<input type="text" class="form-control" id="minrentaldays_1" name="minrentaldays[]" readonly
											 required autocomplete="off"/> 
										</td>
										<td data-title="Rate">
											 <input type="text" class="form-control" id="rate_1" name="rate[]" 
											 readonly data-row-id="row_1"/> 	 
										</td> 
										<td data-title="Rate/Day">
											 <input type="text" class="form-control" id="rateperday_1" name="rateperday[]" 
											 readonly data-row-id="row_1"/> 	 
										</td> 
										 <td data-title="Out Units">
											<input type="text" name="outunits[]" id="outunits_1" value="1"
											class="form-control" required readonly>  
										<td data-title="In Units">
											<input type="text" name="noofunits[]" id="noofunits_1" value="1"
											class="form-control" required autocomplete="off" onkeyup="getTotalRent(1)"> 
											<input type="hidden" name="balanceqty[]" id="balanceqty_1"
											class="form-control" placeholder="balanceqty"></td> 
										<td data-title="Grace Period">
										<input type="text" class="form-control" id="grace_period_1"
												name="grace_period[]" placeholder="Grace Period" required
												autocomplete="off" value="0" onkeyup="getTotalRent(1)"></td>
										<td data-title="Period">
										<input type="text" name="period[]" id="period_1"
											class="form-control" autocomplete="off"
											onkeyup="getTotalRent(1)" value="0"></td> 
										<td data-title="Rent">
										<input type="text" name="totamount[]" id="totamount_1"
											class="form-control" readonly autocomplete="off"  value="0"></td> 
									</tr>
								</tbody>
							</table> 
				 
							<div>
								<!-- New Format -->
								<div class="col-md-12 col-xs-12 pull-right bottomDiv">

									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Inward Charges</label> 
											<input
												type="text" class="form-control" id="rent_amount"
												name="rent_amount" readonly autocomplete="off">
										    <input
												type="hidden" class="form-control" id="total_balanceqty"
												name="total_balanceqty" readonly autocomplete="off">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Total Wages</label> 
											<input
												type="text" class="form-control" id="wages" value="0" onkeyup="calculateNet()"
												name="wages" autocomplete="off">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Cleaning Charge</label> <input
												type="text" class="form-control" id="cleaningcharge" value="0"
												name="cleaningcharge" autocomplete="off"
												onkeyup="calculateNet()" />
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Vehicle Transfer Charge</label> <input
												type="text" class="form-control" id="vehtranscharge" value="0"
												name="vehtranscharge" autocomplete="off"
												onkeyup="calculateNet()">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gst_amount" id="gstsublbl">Untimed Loading Charge</label>
											<input type="text" class="form-control" id="untimedloading" value="0"
												name="untimedloading" autocomplete="off"
												onkeyup="calculateNet()">

										</div>
									</div>
									
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Total Amount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="total_amount"
												name="total_amount" readonly autocomplete="off">
										</div>
									</div> 
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Discount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="disc_amount"
												name="disc_amount" autocomplete="off" onkeyup="calculateNet()">
										</div>
									</div>

									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Net Amount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="net_amount"
												name="net_amount" readonly autocomplete="off">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="amtpaid">Amount Paid</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" required class="form-control" id="amtpaid"
												name="amtpaid" onkeyup="calculateRBalance()"
												autocomplete="off" value="0">
										</div>
									</div>

									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="balance">Balance</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="balance"
												name="balance" value="0" readonly autocomplete="off">
										</div>
									</div>
								    <div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Outward Advance Paid</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="advance"
												name="advance" readonly autocomplete="off">
										</div>
									</div>
								<!--  <div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="balance">Balance To Return to Customer</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="cbalance"
												name="cbalance" value="0" readonly autocomplete="off">
										</div>
									</div> -->  
								</div> 
							</div>
						</div>
					
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Create Inward</button>
					<a href="<?php echo base_url('inward/') ?>" class="btn btn-warning">Back</a>
				</div>
				</form>
				 
			</div>
			<!-- /.box -->
		</div>
		<!-- col-md-12 -->
		
		 <!-- Pop up msg dialog -->
            <div class="modal fade" id="popup_detail_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="vertical-alignment-helper">
			        <div class="modal-dialog vertical-align-center">
			            <div class="modal-content" style="width: 350px;">
		                <div class="modal-header" style="background: #3c8dbc; color:white">
		                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		                    </button>
		                     <h4 class="modal-title" id="myModalLabel">Message Box</h4>
		                </div>
		                <div class="modal-body" id="popup_detail_model_body"></div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
		                </div>
		            </div>
		        </div>
		    </div>
		</div> 
		 <!-- Pop up msg dialog -->
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
 

<div id="myModal1" class="modal fade" aria-labelledby="myModalLabel"
	aria-hidden="true" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true"></button>
				<h4 class="modal-title">Pick Vehicle</h4>
			</div>
			<div class="modal-body" id="myModalBody1">
				<table class="table table-bordered">
					<tr>
						<td>Truck No</td>
						<td><select class="form-control select_group2" id="truckno"
							name="truckno" style="width: 100%;"
							onchange="getDriverData()" required>
								<option value=""></option>
                            <?php foreach ($drivers as $k => $v): ?>
                              <option value="<?php echo $v['id'] ?>"><?php echo $v['truckno'] ?></option>
                            <?php endforeach ?>
                         </select></td>
					</tr>
					<tr>
						<td>Driver Phone</td>
						<td><input type="text" name="driverphone" id="driverphone"
							class="form-control" readonly autocomplete="off"></td>
					</tr>
					<tr>
						<td>Driver Name</td>
						<input type="hidden" name="driver_hid" id="driver_hid" readonly
							class="form-control">
						<input type="hidden" name="truck_no_hid" id="truck_no_hid"
							readonly class="form-control">
						<td><input type="text" name="drivername" id="drivername" readonly
							class="form-control"></td>
					</tr>
				
				</table>

				<div id="alert-msg"></div>
			</div>
			<div class="modal-footer">
				<input class="btn btn-default" id="submit" name="submit"
					data-dismiss="modal" type="button" value="OK"
					onclick="selectDriver()" /> <input class="btn btn-default"
					type="button" data-dismiss="modal" value="Close"
					onclick="clearDri()" />
			</div>
            <?php echo form_close(); ?>            
        </div>
	</div>
</div>

<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {

    $(".select_group").select2();
    $(".select_group1").select2({
      dropdownParent: $("#myModal")
    });
    $(".select_group2").select2({
        dropdownParent: $("#myModal1")
    });

    $(".select_group3").select2({
        dropdownParent: $("#myModal2")
    });

//  	$('#productname').autocomplete({
//		source: "<?php /*echo base_url + 'orders/search/?';*/ ?>",
//     });
    $("#mainInwardNav>a")[0].click();
    $("#mainInwardNav").addClass('active');
    $("#addInwardNav").addClass('active');

	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		}).on('change', function(){
			calculateDays($('#odate').val(), $('#datepicker').val());
    });

	//Disable button and prevent double submitting
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	});
	
  
    // Add new row in the table 
    $("#add_row").unbind('click').bind('click', function() {
       addinwardrow();
   });
  });
 

</script>