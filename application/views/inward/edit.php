<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Manage Inward</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><i
					class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Inward</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12 col-xs-12">

				<div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible"
					role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <div class="box">
					<div class="col-sm-12">
						<div class="box-header">
							<h3 class="box-title">Edit Inward</h3>
						</div>
					</div>

					<!-- /.box-header -->
					<form role="form" action="<?php base_url('inward/create') ?>"
						method="post" class="form-horizontal" autocomplete="off" >
						<div class="box-body">
							<div class="col-sm-6" style="float: left;color:red;font-weight:bold"> 
			                <?php 											
								echo validation_errors (); 
							?>
							</div>
   							<div class="col-sm-6">
								<div class="box-header" style="float: right">
									<label>Inward Date :</label> <input type="text"
										style="border: 1px solid #ccc; padding-left: 5px;"
										name="selected_date"  value="<?php echo $inward_data['inward']['idate'];?>"
										id="datepicker" />
								</div>
							</div>
							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Inward No*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="invoice_no"
											name="invoice_no" placeholder="Enter Invoice No" required
											value="<?php echo $inward_data['inward']['idc_no'] ?>" readonly autocomplete="off">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Outward No*</label>
									<div class="col-sm-7">
											<input type="text" class="form-control" id="outward_no"
											name="outward_no" placeholder="Enter Outward No" required
											value="<?php echo $inward_data['inward']['odc_no']?>" readonly autocomplete="off">
										   <input type="hidden" class="form-control" id="outward_id"
											name="outward_id" placeholder="Enter Outward No" required
											value="<?php echo $inward_data['inward']['odc_id']?>" readonly autocomplete="off">
										 
									</div>
								</div>
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Outward Date</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="odate"
											name="odate" required value="<?php echo $inward_data['inward']['odate'] ?>"
											readonly autocomplete="off">
									</div>
								</div>
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Customer Name</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="isupl_name"
											name="isupl_name" placeholder="Customer Name"
											autocomplete="off" value="<?php echo $inward_data['inward']['supplier_name'] ?>">
										<input type="hidden" class="form-control" id="sid"
											name="sid" placeholder="Customer Phone"
											autocomplete="off" value="<?php echo $inward_data['inward']['sid'] ?>">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Customer Phone</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="isupl_phone"
											name="isupl_phone" placeholder="Customer Phone"
											autocomplete="off" value="<?php echo $inward_data['inward']['ph_no'] ?>">
                                             <input type="hidden" class="form-control" id="semail"
											name="semail" placeholder="semail"
											autocomplete="off" value="<?php echo $inward_data['inward']['email_id'] ?>">
									</div>
								</div> 
							</div>
							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Mode of Pay</label>
									<div class="col-sm-7">
										<select class="form-control" data-row-id="row_1" id="mop"
											name="mop" style="width: 100%;" required>
											<?php if ($inward_data['inward']['mop'] == 1) {?>
						                     	<option value="<?php echo $inward_data['inward']['mop'] ?>">Cash</option>
						                      	<option value="2">Cheque</option>
						                        <option value="3">Card</option>
						                        <option value="4">Credit</option>
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option>
					                   		<?php } else if($inward_data['inward']['mop'] == 2) { ?>
												<option value="<?php echo $inward_data['inward']['mop'] ?>">Cheque</option>
						                      	<option value="1">Cash</option>
						                        <option value="3">Card</option>
						                        <option value="4">Credit</option>
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option>
						                        
					                   		<?php } else if($inward_data['inward']['mop'] == 3) { ?>
					                   			<option value="<?php echo $inward_data['inward']['mop'] ?>">Card</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="4">Credit</option>
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option>
						                    <?php } else if($inward_data['inward']['mop'] == 4) { ?>
					                   			<option value="<?php echo $inward_data['inward']['mop'] ?>">Credit</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="3">Card</option> 
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option>
					                   		 <?php } else if($inward_data['inward']['mop'] == 5) { ?>
					                   			<option value="<?php echo $inward_data['inward']['mop'] ?>">UPI</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="3">Card</option> 
                                                <option value="4">Credit</option>
                                           		<option value="6">Netbanking</option>
					                   		 <?php } else if($inward_data['inward']['mop'] == 6) { ?>
					                   			<option value="<?php echo $inward_data['inward']['mop'] ?>">Netbanking</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="3">Card</option> 
                                                <option value="4">Credit</option>
                                                <option value="5">UPI</option>
					                   		<?php } else { ?>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                        <option value="3">Card</option>
						                        <option value="4">Credit</option>
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option>
					                   		<?php } ?>

										</select>
									</div>
								</div>
							 
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Truck No</label>
									<div class="col-sm-7">
										<input type="hidden" class="form-control" id="driver_id"
											name="driver_id" placeholder="Enter Driver Phone" required
											autocomplete="off" value="<?php echo $inward_data['inward']['did'] ?>"/> 
												<input type="text" class="form-control" id="cont_no"
											name="cont_no" placeholder="Enter Container No" required
											autocomplete="off" value="<?php echo $inward_data['inward']['contNo'] ?>">
								
										<a href="#" data-toggle="modal" data-target="#myModal1">Pick
											Vehicle</a>
									</div>
								</div> 
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Driver Name</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="driver_name"
											name="driver_name" placeholder="Enter Driver Name" required
											autocomplete="off" value="<?php echo $inward_data['inward']['drivername'] ?>">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Driver Phone</label>
									<div class="col-sm-7">
										<input type="text" class="form-control"
											id="driver_phone" name="driver_phone" value="<?php echo $inward_data['inward']['driverphone'] ?>"
											placeholder="Enter Driver Phone" required autocomplete="off" />
									
									</div>
								</div>

							</div>

							<div class="col-md-4 col-xs-12 pull pull-left">
		
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Terms of Inward</label>
									<div class="col-sm-7">
										<select class="form-control" id="tod"
											name="tod" style="width: 100%;" required>
											<?php if ($inward_data['inward']['tod'] == 1) {?>
						                     	<option value="<?php echo $inward_data['inward']['tod'] ?>">By Transport</option>
						                      	<option value="2">By Hand</option>
					                   		<?php } else if($inward_data['inward']['tod'] == 2) { ?>
												<option value="<?php echo $inward_data['inward']['tod'] ?>">By Hand</option>
						                      	<option value="1">By Transport</option>
						                     <?php } else { ?>
						                      	<option value="1">By Transport</option>
						                      	<option value="2">By Hand</option>
					                   		<?php } ?>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Total days</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="totaldays"
											name="totaldays" placeholder="Total days" required
											autocomplete="off" value="<?php echo $inward_data['inward']['totaldays'] ?>">
									</div>
								</div>
	  							
								<div class="form-group">
									<label class="col-sm-5 control-label"
										style="text-align: left;">Employee</label>
									<div class="col-sm-7">
										<select class="form-control select_group employee"
											data-row-id="row_1" id="employee" required name="employee"
											style="width: 100%;" onchange="getemployeeData()">
												<option value=""></option>
											<?php foreach ($employees as $k => $v): ?>
					                          	<option value="<?php echo $v['id'] ?>" 
						                          	<?php if($inward_data['inward']['empid'] == $v['id'])
						                          	 { echo "selected='selected'"; } ?>><?php echo $v['employeename'] ?>
					                          	</option>
					                       <?php endforeach ?>
				                          </select>
									</div>
								</div>
                                <div class="form-group">
                                        <label class="col-sm-5 control-label" style="text-align:left;">Labours </label>
                                        <div class="col-sm-7">
                                        <input type="text" class="form-control" id="others" name="others"  value="<?php echo $inward_data['inward']['others'] ?>" placeholder="Enter Labour Details" autocomplete="off">
                                        </div> 
                                   </div> 
									<div class="form-group">
										<label for="gross_amount" class="col-sm-5 control-label"
											style="text-align: left;">Total In Items</label>
										<div class="col-sm-7">
											<input type="hidden" class="form-control" id="row_index"
												name="row_index" placeholder="row_index" required
												autocomplete="off" value="1" /> 
										    <input type="text" readonly
												class="form-control" id="total_items" name="total_items"
												autocomplete="off" value="<?php echo $inward_data['inward']['total_items'] ?>"/>
										</div>
								</div>
							</div>
							
							<table class="table table-bordered" id="product_info_table_gst">
								<thead>
									<tr>
									 	<th style="width: 8%">Item Type</th>  
										<th style="width: 20%">Box/Item Name</th>
										<th style="width: 8%">Type</th>
										<th style="width: 8%">Units</th> 
										<th style="width: 9%">Min R-Days</th> 
										<th style="width: 7%; background: #fbd377 !important;">Rate</th>
										<th style="width: 7%; background: #fbd377 !important;">Rate/Day</th>
										<th style="width: 6%; background: #19f38f !important;">Out Units</th> 
										<th style="width: 7%; background: #19f38f !important;">In Units</th>
										<th style="width: 8%; background: #19f38f !important;">Grace Period</th>
										<th style="width: 8%; background: #19f38f !important;">Period in Days</th>
										<th style="width: 8%">Rent</th> 
									</tr>
								</thead>

								<tbody>
								  <?php if(isset($inward_data['inward_item'])): ?>
			                      <?php $x = 1; ?>
			                      <?php foreach ($inward_data['inward_item'] as $key => $val): ?>
									<tr id="row_<?php echo $x; ?>">
                       					<td data-title="IType"> 
											<select class="form-control" data-row-id="row_<?php echo $x; ?>"
											id="itype_<?php echo $x; ?>" name="itype[]" style="width: 100%;"
											onchange="getItems(<?php echo $x; ?>)"required>
												<?php if ($val['itype'] == 1) {?>
						                     	<option value="<?php echo $val['itype'] ?>">Box</option>
												<option value="2">Single</option>  
					                   		<?php } else if($val['itype'] == 2) { ?>
												<option value="<?php echo $val['itype'] ?>">Single</option>
						                      	<option value="1">Box</option>  
					                   		<?php } ?> 
										</select></td>
										<td data-title="Box/Item Name">
					                          	<input type="hidden" name="boxitem_name[]" id="boxitem_name_<?php echo $x; ?>"
												class="form-control" readonly autocomplete="off" value="<?php echo $val['outward_item_id'] ?>" >	
												<input type="hidden" name="boxitem_id[]" id="boxitem_id_<?php echo $x; ?>"
												class="form-control" readonly autocomplete="off" value="<?php echo $val['boxitem_id'] ?>" >	
												<input type="text" name="bname[]" id="bname_<?php echo $x; ?>"
												class="form-control" autocomplete="off" value="<?php echo $val['boxitem_name'] ?>" >										
                       				 </td>
                       				 	<td data-title="Type"> 
											<select class="form-control" data-row-id="row_<?php echo $x; ?>"
											id="type_<?php echo $x; ?>" name="type[]" style="width: 100%;"
											required>
												<?php if ($val['type'] == 1) {?>
						                     	<option value="<?php echo $val['type'] ?>">Main</option>
												<option value="2">Side</option>  
					                   		<?php } else if($val['type'] == 2) { ?>
												<option value="<?php echo $val['type'] ?>">Side</option>
						                      	<option value="1">Main</option>  
					                   		<?php } ?> 
										</select></td>
						             <td data-title="Unit">
										<input type="text" name="units[]" id="units_<?php echo $x; ?>" readonly
											class="form-control" autocomplete="off"
											value="<?php echo $val['units'] ?>" ></td>
										</td> 
										<td data-title="Min R-Days">
										<input type="text" class="form-control" id="minrentaldays_<?php echo $x; ?>" name="minrentaldays[]" readonly
											 value="<?php echo $val['minrentaldays'] ?>" required autocomplete="off"/> 
										</td>
										<td data-title="Rate">
											 <input type="text" class="form-control" id="rate_<?php echo $x; ?>" name="rate[]" 
											 value="<?php echo $val['rate'] ?>" readonly data-row-id="row_<?php echo $x; ?>"/> 	 
										</td>
										<td data-title="Rate/Day">
											 <input type="text" class="form-control" id="rateperday_<?php echo $x; ?>" name="rateperday[]" 
											 readonly value="<?php echo $val['rateperday'] ?>" /> 	 
										</td> 
										 <td data-title="Out Units">
											<input type="text" name="outunits[]" id="outunits_<?php echo $x; ?>"  
											class="form-control" required readonly value="<?php echo $val['outunits'] ?>">  
										<td data-title="No.Units">
										<input type="text" name="noofunits[]" id="noofunits_<?php echo $x; ?>"
											class="form-control" value="<?php echo $val['noofunits'] ?>" onkeyup="getTotalRent(<?php echo $x; ?>)">
									    <input type="hidden" name="balanceqty[]" id="balanceqty_<?php echo $x; ?>"
											class="form-control" value="<?php echo $val['balanceqty'] ?>"></td>
										<td data-title="Grace Period">
										<input type="text" class="form-control" id="grace_period_<?php echo $x; ?>"  value="<?php echo $val['gperiod'] ?>"
												name="grace_period[]" placeholder="Grace Period" required onkeyup="getTotalRent(<?php echo $x; ?>)"
												autocomplete="off" value="0"></td>
										<td data-title="Period">
											<input type="text" name="period[]" id="period_<?php echo $x; ?>"
											class="form-control" autocomplete="off"  value="<?php echo $val['period'] ?>" 
											onkeyup="getTotalRent(<?php echo $x; ?>)"></td>
										<td data-title="Rent">
									 		<input type="text" name="totamount[]" id="totamount_<?php echo $x; ?>"
											class="form-control" readonly autocomplete="off" value="<?php echo $val['totalrent'] ?>" ></td>
									 
									</tr>
									<?php $x++; ?>
                    			<?php endforeach; ?>
                  				<?php endif; ?>
								</tbody>
										
							</table>
							  
							<div>
								<!-- New Format -->
								<div class="col-md-12 col-xs-12 pull-right bottomDiv">

									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Inward Charges</label> 
											<input
												type="text" class="form-control" id="rent_amount"
												name="rent_amount" readonly autocomplete="off"
												value="<?php echo $inward_data['inward']['inwardcharge'] ?>">
										    <input
												type="hidden" class="form-control" id="total_balanceqty"
												name="total_balanceqty" readonly autocomplete="off"
												value="<?php echo $inward_data['inward']['total_balanceqty'] ?>">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Total Wages</label> 
											<input
												type="text" class="form-control" id="wages"
												name="wages" autocomplete="off" onkeyup="calculateNet()"
												value="<?php echo $inward_data['inward']['wages'] ?> ">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Cleaning Charge</label> <input
												type="text" class="form-control" id="cleaningcharge"
												name="cleaningcharge" autocomplete="off"
												onkeyup="calculateNet()" 
												value="<?php echo $inward_data['inward']['cleaningcharge'] ?>"/>
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Vehicle Transfer Charge</label>
											 <input
												type="text" class="form-control" id="vehtranscharge"
												name="vehtranscharge" autocomplete="off"
												onkeyup="calculateNet()"
												value="<?php echo $inward_data['inward']['vehtranscharge'] ?>">
										</div>
									</div>
									
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gst_amount" id="gstsublbl">Untimed Loading Charge</label>
											<input type="text" class="form-control" id="untimedloading"
												name="untimedloading" autocomplete="off"
												onkeyup="calculateNet()"
												value="<?php echo $inward_data['inward']['untimedloading'] ?>">

										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Total Amount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="total_amount"
												name="total_amount" readonly autocomplete="off"
												value="<?php echo $inward_data['inward']['totalamount'] ?>">
										</div>
									</div>
								
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Discount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="disc_amount"
												name="disc_amount" autocomplete="off" onkeyup="calculateNet()"
						                        value="<?php echo $inward_data['inward']['discount'] ?>">
										</div>
									</div>
									
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Net Amount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="net_amount"
												name="net_amount" readonly autocomplete="off"
												value="<?php echo $inward_data['inward']['totalrent'] ?>">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="amtpaid">Amount Paid</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" required class="form-control" id="amtpaid"
												name="amtpaid" onkeyup="calculateRBalance()"
												value="<?php echo $inward_data['inward']['amtpaid'] ?>"
												autocomplete="off">
										</div>
									</div>

									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="balance">Balance</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="balance"
												value="<?php echo $inward_data['inward']['balance'] ?>"
												name="balance" value="0" readonly autocomplete="off">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="net_amount">Outward Advance Paid</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="advance"
												name="advance" readonly autocomplete="off"
												value="<?php echo $inward_data['inward']['advance'] ?>">
										</div>
									</div>
									<!--     <div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="balance">Balance To Return to Customer</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="cbalance" 
											value="<?php echo $inward_data['inward']['cbalance'] ?>"
												name="cbalance" value="0" readonly autocomplete="off">
										</div>
									</div> -->
								</div>
							</div>
						</div>
					
				<!-- /.box-body -->

				<div class="box-footer">
					<a target="__blank" href="<?php echo base_url() . 'inward/printDiv/'.$inward_data['inward']['id'] ?>" class="btn btn-success" >Print</a>
					<?php if($editen):?> 
					<button type="submit" class="btn btn-primary">Update Inward</button>
					<?php endif?>
					<a href="<?php echo base_url('inward/') ?>" class="btn btn-warning">Back</a>
				</div>
				</form>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- col-md-12 -->
		
		 <!-- Pop up msg dialog -->
            <div class="modal fade" id="popup_detail_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			    <div class="vertical-alignment-helper">
			        <div class="modal-dialog vertical-align-center">
			            <div class="modal-content" style="width: 350px;">
		                <div class="modal-header" style="background: #3c8dbc; color:white">
		                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		                    </button>
		                     <h4 class="modal-title" id="myModalLabel">Message Box</h4>
		                </div>
		                <div class="modal-body" id="popup_detail_model_body"></div>
		                <div class="modal-footer">
		                    <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
		                </div>
		            </div>
		        </div>
		    </div>
		</div> 
		 <!-- Pop up msg dialog -->
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
 

<div id="myModal1" class="modal fade" aria-labelledby="myModalLabel"
	aria-hidden="true" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true"></button>
				<h4 class="modal-title">Pick Vehicle</h4>
			</div>
			<div class="modal-body" id="myModalBody1">
				<table class="table table-bordered">
					<tr>
						<td>Truck No</td>
						<td><select class="form-control select_group2" id="truckno"
							name="truckno" style="width: 100%;"
							onchange="getDriverData()" required>
								<option value=""></option>
                            <?php foreach ($drivers as $k => $v): ?>
                              <option value="<?php echo $v['id'] ?>"><?php echo $v['truckno'] ?></option>
                            <?php endforeach ?>
                         </select></td>
					</tr>
					<tr>
						<td>Driver Phone</td>
						<td><input type="text" name="driverphone" id="driverphone"
							class="form-control" readonly autocomplete="off"></td>
					</tr>
					<tr>
						<td>Driver Name</td>
						<input type="hidden" name="driver_hid" id="driver_hid" readonly
							class="form-control">
						<input type="hidden" name="truck_no_hid" id="truck_no_hid"
							readonly class="form-control">
						<td><input type="text" name="drivername" id="drivername" readonly
							class="form-control"></td>
					</tr>
				
				</table>

				<div id="alert-msg"></div>
			</div>
			<div class="modal-footer">
				<input class="btn btn-default" id="submit" name="submit"
					data-dismiss="modal" type="button" value="OK"
					onclick="selectDriver()" /> <input class="btn btn-default"
					type="button" data-dismiss="modal" value="Close"
					onclick="clearDri()" />
			</div>
            <?php echo form_close(); ?>            
        </div>
	</div>
</div>

<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {
    $(".select_group").select2();
    $(".select_group1").select2({
      dropdownParent: $("#myModal")
    });
    $(".select_group2").select2({
        dropdownParent: $("#myModal1")
    });

    $(".select_group3").select2({
        dropdownParent: $("#myModal2")
    });

 
//  	$('#productname').autocomplete({
//		source: "<?php /*echo base_url + 'orders/search/?';*/ ?>",
//     });
    $("#mainInwardNav>a")[0].click();
    $("#mainInwardNav").addClass('active');
    $("#manageInwardNav").addClass('active');

	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		}).on('change', function(){
			calculateDays($('#odate').val(), $('#datepicker').val());
    });


	//Disable button and prevent double submitting
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	});
  
    // Add new row in the table 
    $("#add_row").unbind('click').bind('click', function() {
       addinwardrow();
   });
  });
 

	

</script>