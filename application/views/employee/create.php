
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage
      Employees
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Employees</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>


        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Add Employees</h3>
          </div>
          <!-- /.box-header -->
          <form role="form" action="<?php base_url('employees/create') ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">

                <?php echo validation_errors(); ?>
  				<div class="col-md-6 col-xs-12 pull pull-left">
   
                <div class="form-group">
            <label>Employee Name*</label>
            <input type="text" class="form-control" id="employee_name" name="employee_name" required placeholder="Enter employee name" autocomplete="off">
          </div>
            <div class="form-group">
            <label>Employee Phone*</label>
            <input type="text" class="form-control" id="employee_phone" name="employee_phone" required placeholder="Enter employee phone" autocomplete="off">
          </div>
       <!--  <div class="form-group">
            <label>SBox Wage</label>
            <input type="text" class="form-control" id="sbox_wage" name="sbox_wage" placeholder="Enter SBox Wage" autocomplete="off">
          </div>
           <div class="form-group">
            <label>MBox Wage</label>
            <input type="text" class="form-control" id="mbox_wage" name="mbox_wage" placeholder="Enter MBox Wage" autocomplete="off">
          </div>
           <div class="form-group">
            <label>LBox Wage</label>
            <input type="text" class="form-control" id="lbox_wage" name="lbox_wage" placeholder="Enter LBox Wage" autocomplete="off">
          </div>
           <div class="form-group">
            <label>Bag Wage</label>
            <input type="text" class="form-control" id="bag_wage" name="bag_wage" placeholder="Enter Bag Wage" autocomplete="off">
          </div> -->    

              
			</div>
 
               </div>
            

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save Changes</button>
                <a href="<?php echo base_url('employee/') ?>" class="btn btn-warning">Back</a>
              </div>
            </form>
              </div>
              <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
  $(document).ready(function() {
    $(".select_group").select2();
    $("#mainemployeeNav>a")[0].click();
    $("#mainemployeeNav").addClass('active');
    $("#addemployeeNav").addClass('active');

	//Disable button and prevent double submitting
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	});

});
  


/*	var exp=/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	if (!exp.test(obj.value)){
		alert("Please enter valid "+msg);
		obj.focus();
		return true;		
	}else
		return false;*/
</script>