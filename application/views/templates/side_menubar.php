<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        
        <li id="dashboardMainMenu">
          <a href="<?php echo base_url('dashboard') ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        
         <!--  <li class="treeview">
          <a class="treetitle">
            <span>Services</span>
          </a>
        </li> -->
        
       <?php if(in_array('createVehicle', $user_permission) || in_array('updateVehicle', $user_permission) || in_array('viewVehicle', $user_permission) || in_array('deleteVehicle', $user_permission)): ?>
            <li id="driverNav">
              <a href="<?php echo base_url('driver/') ?>">
                <i class="fa fa-id-card-o"></i> <span>Vehicle</span>
              </a>
            </li>
         <?php endif; ?>
          <?php if(in_array('createEmployee', $user_permission) || in_array('updateEmployee', $user_permission) || in_array('viewEmployee', $user_permission) || in_array('deleteEmployee', $user_permission)): ?>
             <li class="treeview" id="mainemployeeNav">
              <a href="#">
                <i class="fa fa-user-o"></i>
                <span>Employee</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createEmployee', $user_permission)): ?>
                  <li id="addemployeeNav"><a href="<?php echo base_url('employee/create') ?>">
                  <i class="fa fa-circle-o"></i> Add Employee</a></li>
                <?php endif; ?>
                <?php if(in_array('updateEmployee', $user_permission) || in_array('viewEmployee', $user_permission) || in_array('deleteEmployee', $user_permission)): ?>
                <li id="manageemployeeNav"><a href="<?php echo base_url('employee') ?>">
                	<i class="fa fa-circle-o"></i> Manage Employee</a></li>
                <?php endif; ?>
              </ul>
            </li>
            
         <?php endif; ?>

          <?php if(in_array('createProduct', $user_permission) || in_array('updateProduct', $user_permission) || in_array('viewProduct', $user_permission) || in_array('deleteProduct', $user_permission)): ?>
            <li class="treeview" id="mainRentitemsNav">
              <a href="#">
                <i class="fa fa-snowflake-o"></i>
                <span>Rent Items</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createProduct', $user_permission)): ?>
                  <li id="addRentitemsNav"><a href="<?php echo base_url('rentitems/create') ?>"><i class="fa fa-circle-o"></i> Add Rent Items</a></li>
                <?php endif; ?>
                <?php if(in_array('updateProduct', $user_permission) || in_array('viewProduct', $user_permission) || in_array('deleteProduct', $user_permission)): ?>
                <li id="manageRentitemsNav"><a href="<?php echo base_url('rentitems') ?>"><i class="fa fa-circle-o"></i> Manage Rent Items</a></li>
                <?php endif; ?>
              <li id="categoryNav">
              <a href="<?php echo base_url('category/') ?>">
                <i class="fa fa-files-o"></i> <span>Category</span></a></li>
              </ul>
            </li>
          <?php endif; ?>
          <!-- 
                  <?php if(in_array('createProduct', $user_permission) || in_array('updateProduct', $user_permission) || in_array('viewProduct', $user_permission) || in_array('deleteProduct', $user_permission)): ?>
            <li class="treeview" id="mainBoxitemsNav">
              <a href="#">
                <i class="fa fa-snowflake-o"></i>
                <span>Box Items</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createProduct', $user_permission)): ?>
                  <li id="addBoxitemsNav"><a href="<?php echo base_url('boxitems/create') ?>"><i class="fa fa-circle-o"></i> Add Box Items</a></li>
                <?php endif; ?>
                <?php if(in_array('updateProduct', $user_permission) || in_array('viewProduct', $user_permission) || in_array('deleteProduct', $user_permission)): ?>
                <li id="manageBoxitemsNav"><a href="<?php echo base_url('boxitems') ?>"><i class="fa fa-circle-o"></i> Manage Box Items</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?> -->
            <?php if(in_array('createCustomer', $user_permission) || in_array('updateCustomer', $user_permission) || in_array('viewCustomer', $user_permission) || in_array('deleteCustomer', $user_permission)): ?>
            <li class="treeview" id="mainSupplierNav">
              <a href="#">
                <i class="fa fa-cube"></i>
                <span>Customers</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createCustomer', $user_permission)): ?>
                  
                <li id="addSupplierNav"><a href="<?php echo base_url('scustomers/create') ?>"><i class="fa fa-circle-o"></i> Add Customers</a></li>
                <?php endif; ?>
                <?php if(in_array('updateCustomer', $user_permission) || in_array('viewCustomer', $user_permission) || in_array('deleteCustomer', $user_permission)): ?>
                <li id="manageSupplierNav"><a href="<?php echo base_url('scustomers') ?>"><i class="fa fa-circle-o"></i> Manage Customers</a></li>
                <?php endif; ?>
                <li id="manageSupplierRep"><a href="<?php echo base_url('scustomers/creport') ?>"><i class="fa fa-circle-o"></i>Customer Report</a></li>
            
              </ul>
            </li>
          <?php endif; ?>
           <?php if(in_array('createSupplier1', $user_permission) || in_array('updateSupplier1', $user_permission) || in_array('viewSupplier1', $user_permission) || in_array('deleteSupplier1', $user_permission)): ?>
            <li class="treeview" id="mainSubSupplierNav">
              <a href="#">
                <i class="fa fa-american-sign-language-interpreting"></i>
                <span>Sub Customers</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createSupplier', $user_permission)): ?>
                  
                <li id="addSubSupplierNav"><a href="<?php echo base_url('subscustomers/create') ?>"><i class="fa fa-circle-o"></i> Add SubCustomers</a></li>
                <?php endif; ?>
                <?php if(in_array('updateSupplier', $user_permission) || in_array('viewSupplier', $user_permission) || in_array('deleteSupplier', $user_permission)): ?>
                <li id="manageSubSupplierNav"><a href="<?php echo base_url('subscustomers') ?>"><i class="fa fa-circle-o"></i> Manage SubCustomer</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>
		  
          <?php if(in_array('createOutward', $user_permission) || in_array('updateOutward', $user_permission) || in_array('viewOutward', $user_permission) || in_array('deleteOutward', $user_permission)): ?>
            <li class="treeview" id="mainOutwardNav">
              <a href="#">
                <i class="fa fa-arrow-circle-right"></i>
                <span>Outward</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createOutward', $user_permission)): ?>
                  <li id="addOutwardNav"><a href="<?php echo base_url('outward/create') ?>"><i class="fa fa-circle-o"></i> Add Outward</a></li>
                <?php endif; ?>
                <?php if(in_array('updateOutward', $user_permission) || in_array('viewOutward', $user_permission) || in_array('deleteOutward', $user_permission)): ?>
                <li id="manageOutwardNav"><a href="<?php echo base_url('outward') ?>"><i class="fa fa-circle-o"></i> Manage Outward</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>
		  
		<?php if(in_array('createInward', $this->permission) || in_array('updateInward', $this->permission) || in_array('viewInward', $this->permission) || in_array('deleteInward', $this->permission)): ?>
            <li class="treeview" id="mainInwardNav">
              <a href="#">
                <i class="fa fa-arrow-circle-left"></i>
                <span>Inward</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createInward', $this->permission)): ?>
                  <li id="addInwardNav"><a href="<?php echo base_url('inward/create') ?>"><i class="fa fa-circle-o"></i> Add Inward</a></li>
                <?php endif; ?>
                <?php if(in_array('updateInward', $this->permission) || in_array('viewInward', $this->permission) || in_array('deleteInward', $this->permission)): ?>
                <li id="manageInwardNav"><a href="<?php echo base_url('inward') ?>"><i class="fa fa-circle-o"></i> Manage Inward</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>
		   <?php if(in_array('createEstimate', $user_permission) || in_array('updateEstimate', $user_permission) || in_array('viewEstimate', $user_permission) || in_array('deleteEstimate', $user_permission)): ?>
            <li class="treeview" id="mainEstimateNav">
              <a href="#">
                <i class="fa fa-sitemap"></i>
                <span>Estimate</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
               <?php if(in_array('createEstimate', $user_permission)): ?>
                  <li id="addEstimateNav"><a href="<?php echo base_url('estimate/create') ?>"><i class="fa fa-circle-o"></i> Add Estimate</a></li>
                <?php endif; ?>
                
                <?php if(in_array('updateEstimate', $user_permission) || in_array('viewEstimate', $user_permission) || in_array('deleteEstimate', $user_permission)): ?>
                <li id="manageEstimateNav"><a href="<?php echo base_url('estimate') ?>"><i class="fa fa-circle-o"></i> Manage Estimate</a></li>
                <?php endif; ?>
              </ul>
            </li>
    	 <?php endif; ?>
         <?php if(in_array('createOrder', $user_permission) || in_array('updateOrder', $user_permission) || in_array('viewOrder', $user_permission) || in_array('deleteOrder', $user_permission)): ?>
            <li class="treeview" id="mainInvoiceNav">
              <a href="#">
                <i class="fa fa-sitemap"></i>
                <span>Invoice</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
             <!-- <?php if(in_array('createOrder', $user_permission)): ?>
                  <li id="addInvoiceNav"><a href="<?php echo base_url('invoice/create') ?>"><i class="fa fa-circle-o"></i> Add Invoice</a></li>
                <?php endif; ?> -->
                
                <?php if(in_array('updateOrder', $user_permission) || in_array('viewOrder', $user_permission) || in_array('deleteOrder', $user_permission)): ?>
                <li id="manageInvoiceNav"><a href="<?php echo base_url('invoice') ?>"><i class="fa fa-circle-o"></i> Manage Invoice</a></li>
                <?php endif; ?>
              </ul>
            </li>
    	 <?php endif; ?>

    	 <?php if(in_array('createOrder1', $user_permission) || in_array('updateOrder1', $user_permission) || in_array('viewOrder1', $user_permission) || in_array('deleteOrder1', $user_permission)): ?>
            <li class="treeview" id="mainCreditNav">
              <a href="#">
                <i class="fa fa-inr"></i>
                <span>Credit Service</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
               <?php if(in_array('createOrder', $user_permission)): ?>
                  <li id="addCreditNav"><a href="<?php echo base_url('credit/create') ?>"><i class="fa fa-circle-o"></i> Add Credit</a></li>
                <?php endif; ?>
                
                <?php if(in_array('updateOrder', $user_permission) || in_array('viewOrder', $user_permission) || in_array('deleteOrder', $user_permission)): ?>
                <li id="manageCreditNav"><a href="<?php echo base_url('credit') ?>"><i class="fa fa-circle-o"></i> Manage Credit</a></li>
                <?php endif; ?>
              </ul>
            </li>
    	 <?php endif; ?>
    	 
    	 <!-- <li class="treeview">
          <a class="treetitle">
            <span>Transactions</span>
          </a>
        </li>
        
		<?php if(in_array('createProduct', $user_permission) || in_array('updateProduct', $user_permission) || in_array('viewProduct', $user_permission) || in_array('deleteProduct', $user_permission)): ?>
            <li class="treeview" id="mainProductNav">
              <a href="#">
                <i class="fa fa-product-hunt"></i>
                <span>Products</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createProduct', $user_permission)): ?>
                  <li id="addProductNav"><a href="<?php echo base_url('products/create') ?>"><i class="fa fa-circle-o"></i> Add Product</a></li>
                <?php endif; ?>
                <?php if(in_array('updateProduct', $user_permission) || in_array('viewProduct', $user_permission) || in_array('deleteProduct', $user_permission)): ?>
                <li id="manageProductNav"><a href="<?php echo base_url('products') ?>"><i class="fa fa-circle-o"></i> Manage Products</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>
          
		<?php if(in_array('createCustomer', $user_permission) || in_array('updateCustomer', $user_permission) || in_array('viewCustomer', $user_permission) || in_array('deleteCustomer', $user_permission)): ?>
            <li class="treeview" id="mainCustomerNav">
              <a href="#">
                <i class="fa fa-cubes"></i>
                <span>Customers</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createCustomer', $user_permission)): ?>
                  
                   <li id="addCustomerNav"><a href="<?php echo base_url('customers/create') ?>"><i class="fa fa-circle-o"></i> Add Customers</a></li>
                <?php endif; ?>
                <?php if(in_array('updateCustomer', $user_permission) || in_array('viewCustomer', $user_permission) || in_array('deleteCustomer', $user_permission)): ?>
                <li id="manageCustomerNav"><a href="<?php echo base_url('customers') ?>"><i class="fa fa-circle-o"></i> Manage Customers</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>
    
           <?php if(in_array('createPoorder', $user_permission) || in_array('updatePoorder', $user_permission) || in_array('viewPoorder', $user_permission) || in_array('deletePoorder', $user_permission)): ?>
            <li class="treeview" id="mainPoordersNav">
              <a href="#">
                <i class="fa fa-shopping-bag"></i>
                <span>Purchase</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createPoorder', $user_permission)): ?>
                  <li id="addPoorderNav"><a href="<?php echo base_url('poorders/create') ?>"><i class="fa fa-circle-o"></i> Add Purchase</a></li>
                <?php endif; ?>
                <?php if(in_array('updatePoorder', $user_permission) || in_array('viewPoorder', $user_permission) || in_array('deletePoorder', $user_permission)): ?>
                <li id="managePoordersNav"><a href="<?php echo base_url('poorders') ?>"><i class="fa fa-circle-o"></i> Manage Purchase</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>
          
        
          
    <?php if(in_array('createOrder', $user_permission) || in_array('updateOrder', $user_permission) || in_array('viewOrder', $user_permission) || in_array('deleteOrder', $user_permission)): ?>
            <li class="treeview" id="mainOrdersNav">
              <a href="#">
                <i class="fa fa-shopping-cart"></i>
                <span>Sales</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createOrder', $user_permission)): ?>
                  <li id="addOrderNav"><a href="<?php echo base_url('orders/create') ?>"><i class="fa fa-circle-o"></i> Add Sales</a></li>
                <?php endif; ?>
                <?php if(in_array('updateOrder', $user_permission) || in_array('viewOrder', $user_permission) || in_array('deleteOrder', $user_permission)): ?>
                <li id="manageOrdersNav"><a href="<?php echo base_url('orders') ?>"><i class="fa fa-circle-o"></i> Manage Sales</a></li>
                <?php endif; ?>
              </ul>
            </li>
    <?php endif; ?> -->
   

          
 		<?php if(in_array('createLedger', $user_permission) || in_array('updateLedger', $user_permission) || in_array('viewLedger', $user_permission) || in_array('deleteLedger', $user_permission)): ?>
            <li class="treeview" id="mainLedgerJournalNav">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>Accounting</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>                </span>              </a>
              <ul class="treeview-menu">
              
                <?php if(in_array('createLedger', $user_permission)): ?>
                  <li id="manageLedgerNav"><a href="<?php echo base_url('ledger') ?>"><i class="fa fa-circle-o"></i> Manage Ledger</a></li>
                <?php endif; ?>
			 
                           
                <?php if(in_array('createLedger', $user_permission)): ?>
                  <li id="manageExpenditureNav"><a href="<?php echo base_url('Expense_form') ?>">
                  <i class="fa fa-circle-o"></i> Manage Expenditure</a></li>
                <?php endif; ?>
                 <?php if(in_array('createLedger', $user_permission)): ?>
                  <li id="managedaywiseNav"><a href="<?php echo base_url('Expense_form/daywise_report') ?>"><i class="fa fa-circle-o"></i>Day wise report</a></li>
               
                <?php endif; ?>
                  
        
              </ul>
            </li>
          <?php endif; ?>

			<?php if(in_array('createPayments', $user_permission) || in_array('updatePayments', $user_permission) || in_array('viewPayments', $user_permission) || in_array('deletePayments', $user_permission)): ?>
            <li class="treeview" id="mainPaymentsNav">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Payments</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>                </span>              </a>
              <ul class="treeview-menu">
             	<?php if(in_array('updatePayments', $user_permission) || in_array('viewPayments', $user_permission) || in_array('deletePayments', $user_permission)): ?>
              
                  <li id="addInvPaymentNav"><a href="<?php echo base_url('Paymentbyoutward/create') ?>"><i class="fa fa-circle-o"></i> Outward Payment</a></li>
 
                  <li id="addEstPaymentNav"><a href="<?php echo base_url('Paymentbyestimate/create') ?>"><i class="fa fa-circle-o"></i> Estimate Payment</a></li>
    
              <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>
        <!-- <li class="header">User Create</li> -->
  		<?php if($user_permission): ?>
          <?php if(in_array('createUser', $user_permission) || in_array('updateUser', $user_permission) || in_array('viewUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>
            <li class="treeview" id="mainUserNav">
            <a href="#">
              <i class="fa fa-users"></i>
              <span>Users</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(in_array('createUser', $user_permission)): ?>
              <li id="createUserNav"><a href="<?php echo base_url('users/create') ?>"><i class="fa fa-circle-o"></i> Add User</a></li>
              <?php endif; ?>

              <?php if(in_array('updateUser', $user_permission) || in_array('viewUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>
              <li id="manageUserNav"><a href="<?php echo base_url('users') ?>"><i class="fa fa-circle-o"></i> Manage Users</a></li>
            <?php endif; ?>
            </ul>
          </li>
          <?php endif; ?>

          <?php if(in_array('createGroup', $user_permission) || in_array('updateGroup', $user_permission) || in_array('viewGroup', $user_permission) || in_array('deleteGroup', $user_permission)): ?>
            <li class="treeview" id="mainGroupNav">
              <a href="#">
                <i class="fa fa-object-group"></i>
                <span>Groups</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if(in_array('createGroup', $user_permission)): ?>
                  <li id="addGroupNav"><a href="<?php echo base_url('groups/create') ?>"><i class="fa fa-circle-o"></i> Add Group</a></li>
                <?php endif; ?>
                <?php if(in_array('updateGroup', $user_permission) || in_array('viewGroup', $user_permission) || in_array('deleteGroup', $user_permission)): ?>
                <li id="manageGroupNav"><a href="<?php echo base_url('groups') ?>"><i class="fa fa-circle-o"></i> Manage Groups</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>


           <?php if(in_array('viewReports', $user_permission) || in_array('viewPoorderreports', $user_permission) || in_array('viewOrderreports', $user_permission)):  ?>
            <li class="treeview" id="reportNav">
              <a href="#">
                <i class="glyphicon glyphicon-stats"></i>
                <span>Reports</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
               <!--  <?php if(in_array('viewReports', $user_permission)): ?>
                <li id="managePoorderreportsNav"><a href="<?php echo base_url('poorderreports') ?>"><i class="fa fa-circle-o"></i>Purchase Reports</a></li>
                <li id="manageOrdersreportsNav"><a href="<?php echo base_url('ordersreports') ?>"><i class="fa fa-circle-o"></i>Sales Reports</a></li>
                <li id="manageStockreportsNav"><a href="<?php echo base_url('productreports') ?>"><i class="fa fa-circle-o"></i>Stock Reports</a></li>
               <?php endif; ?> -->
                <?php if(in_array('viewReports', $user_permission)): ?>
                 <li id="manageOutwardreportsNav"><a href="<?php echo base_url('outwardreports') ?>"><i class="fa fa-circle-o"></i>Outward Reports</a></li>
                 <li id="manageInwardreportsNav"><a href="<?php echo base_url('inwardreports') ?>"><i class="fa fa-circle-o"></i>Inward Reports</a></li>
                <li id="manageInvoicereportsNav"><a href="<?php echo base_url('invoicereports') ?>"><i class="fa fa-circle-o"></i>Invoice Reports</a></li>
                <li id="manageRentitemreportsNav"><a href="<?php echo base_url('rentitemsreports') ?>"><i class="fa fa-circle-o"></i>Rent Item Reports</a></li>
             	<!--<li id="manageRackNav"><a href="<?php echo base_url('reports\rackindex') ?>"> <i class="glyphicon glyphicon-stats"></i> <span>Rack Graph</span></a></li>-->
     			<!-- <li id="manageReportNav"><a href="<?php echo base_url('reports') ?>"> <i class="fa fa-bar-chart"></i> <span>Graph</span></a></li> -->
             
                <li id="manageReceiptreportssingleNav"><a href="<?php echo base_url('receipttotalreports') ?>"><i class="fa fa-circle-o"></i>Receipt Reports</a></li>
                <li id="manageVoucherreportssingleNav"><a href="<?php echo base_url('vouchertotalreports') ?>"><i class="fa fa-circle-o"></i>Expenses Reports</a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?>

         <!-- <li class="header">Settings</li> -->

        <?php if(in_array('updateSetting', $user_permission) || in_array('viewProfile', $user_permission) || in_array('updateCompany', $user_permission) || in_array('createAttribute', $user_permission) || in_array('updateAttribute', $user_permission) || in_array('viewAttribute', $user_permission) || in_array('deleteAttribute', $user_permission)||in_array('createStore', $user_permission) || in_array('updateStore', $user_permission) || in_array('viewStore', $user_permission) || in_array('deleteStore', $user_permission)): ?>
          <li class="treeview" id="mainSettingsNav">
              <a href="#">
                <i class="fa fa-cogs"></i>
                <span>Settings</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
           <ul class="treeview-menu">
                       
       <!--    <li id="brandNav">
              <a href="<?php echo base_url('brands/') ?>">
                <i class="glyphicon glyphicon-tags"></i> <span>Brands</span>
              </a>
            </li>
            
          <?php if(in_array('createCategory', $user_permission) || in_array('updateCategory', $user_permission) || in_array('viewCategory', $user_permission) || in_array('deleteCategory', $user_permission)): ?>
            <li id="categoryNav">
              <a href="<?php echo base_url('category/') ?>">
                <i class="fa fa-files-o"></i> <span>Category</span>
              </a>
            </li> -->  
          <?php endif; ?>
          <li id="passwordNav"><a href="<?php echo base_url('users/setting/') ?>"><i class="fa fa-dollar"></i> <span>Change Password</span></a></li>
          <li id="profileNav"><a href="<?php echo base_url('users/profile/') ?>"><i class="fa fa-user-o"></i> <span>Profile</span></a></li>
          <li id="companyNav"><a href="<?php echo base_url('company/') ?>"><i class="fa fa-files-o"></i> <span>Company</span></a></li>
          <!--<li id="attributeNav">
            <a href="<?php/* echo base_url('attributes/')*/ ?>">
              <i class="fa fa-files-o"></i> <span>Attributes</span>
            </a>
          </li>-->
              <li id="storeNav">
              <a href="<?php echo base_url('stores/') ?>">
                <i class="fa fa-files-o"></i> <span>Stores</span>
              </a>
            </li>
         	<li id="prefixNav">
              <a href="<?php echo base_url('invoiceprefixs/') ?>">
                <i class="fa fa-files-o"></i> <span>Prefix</span>
              </a>
            </li>
          </ul>
          </li>
        <?php endif; ?>

        <?php endif; ?>
        <!-- user permission info
        <li><a href="<?php echo base_url('auth/logout') ?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li> -->

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
