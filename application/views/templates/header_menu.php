<header class="main-header">
    <!-- Logo -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>ADN</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
   	 <a href="<?php echo base_url('auth/logout') ?>" class="logout" role="button">
      	<i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a>
      </a>
     
     <!-- <a class="logout username"> 
      	<i class="glyphicon glyphicon-user"></i><span>Logout</span></a>
      </a>
      
      <a class="logo headername" style="background: transparent; width: 50%; text-align: left; font-weight: 500;">
      	<span class="logo-lg">Dashboard</span>
      </a> --> 
      
    </nav>
  </header>
  
<script type="text/javascript">
   $(document).ready(function() {

    //var name = userdata('id');
		setHeaderTitle("Dashboard")
	});
</script>
  <!-- Left side column. contains the logo and sidebar -->
  