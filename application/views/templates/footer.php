
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
  	
 	 <a href="http://www.gmrinfotech.in" target="_blank" alt="GMR INFOTECH"> <b>Version GMR 1.0</b> </a>
    </div>
    <strong>Copyright &copy; 2018-<?php echo date('Y') ?>.</strong> All rights reserved.&nbsp;&nbsp;&nbsp;<a href="http://www.magnumwebtech.com" target="_blank" alt="Magnum Web Technologies">Maintained By MWT </a>
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->



</body>
</html>
    <script src="<?php echo base_url('assets/bower_components/datatables/media/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bower_components/datatables.net-buttons/js/buttons.flash.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bower_components/jszip/dist/jszip.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bower_components/pdfmake/build/pdfmake.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bower_components/pdfmake/build/vfs_fonts.js') ?>"></script>
	<script src="<?php echo base_url('assets/bower_components/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/bower_components/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>