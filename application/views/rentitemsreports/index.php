<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
       Rent Items Reports
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Rent Items Reports</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

      

        <div class="box">
          <div class="box-header">
            <h3 class="box-title"> Rent Items Reports</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="manageTable" class="table table-bordered table-striped">
              <thead>
              <tr>
              	<th>Product Name</th>
                <th>Units</th>
                <th>S-Box Kg</th>
                <th>M-Box Kg</th>
                <th>L-Box Kg</th>
                <th>Bag Kg</th>
                <th>Availability</th>
              </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
	$('#datepicker1').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	});
	$('#datepicker2').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	});
	

var manageTable;
var base_url = "<?php echo base_url(); ?>";

$(document).ready(function() {
	$("#reportNav>a")[0].click();
  $("#reportNav").addClass('active');
  $("#manageRentitemreportsNav").addClass('active');

  // initialize the datatable 
  manageTable = $('#manageTable').DataTable({
  
  'ajax': base_url + 'rentitems/fetchRentitemsData',
	   dom: 'Bfrtip',
	    buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
 		],
  
      'order': []
  });

});
</script>
