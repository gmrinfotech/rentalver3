<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper invoicepage">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Manage Estimate</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><i
					class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Estimate</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12 col-xs-12"> 
				<div id="messages"></div> 
        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible"
					role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <div class="box">
					<div class="col-sm-12">
						<div class="box-header">
							<h3 class="box-title">Edit Estimate</h3>
						</div>
					</div>

					<!-- /.box-header -->
					<form role="form" action="<?php base_url('estimate/update') ?>"
						method="post" class="form-horizontal invoicepageform">
						<div class="box-body">
						 
						<div class="col-sm-6">
		               	 <?php echo validation_errors (); ?>  
						</div>
   							<div class="col-sm-6">
								<div class="box-header" style="float: right">
									<label>Date :</label> <input type="text"
										style="border: 1px solid #ccc; padding-left: 5px;"
										name="selected_date" value="<?php echo $invoice_data['invoice']['sdate'];?>"
										id="datepicker" />
								</div>
							</div>
							 
							<div class="col-md-12 col-xs-12 pull pull-left">
							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Estimate No*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="invoice_no"
											name="invoice_no" placeholder="Enter Invoice No" required
											readonly autocomplete="off" value="<?php echo $invoice_data['invoice']['invoice_id'];?>">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Lot No*</label>
									<div class="col-sm-7">
										<!-- select class="form-control select_group"
											id="outward_no" required name="outward_no"
											style="width:100%;" onchange="getInwardOutwardDetails()">
												<option value=""></option>
			                            <!--?php foreach ($outwardDC as $k => $v): ?>
			                              <option value="<!--?php echo $v['id'] ?>" <!--?php if($invoice_data['invoice']['idc_no'] == $v['id']) 
			                              		{echo "selected='selected'";} ?>><!--?php echo $v['idc_no'] ?></option>
			                            <!?php endforeach ?>
			                          </select -->
			                          <input type="text" class="form-control" id="idc_no"
											name="idc_no" placeholder="Enter Invoice No" required
											readonly autocomplete="off" value="<?php echo $invoice_data['invoice']['outward_data']['odc_no'];?>">
								      <input type="hidden" class="form-control" id="outward_no"
											name="outward_no" placeholder="Enter Invoice No" required
											readonly autocomplete="off" value="<?php echo $invoice_data['invoice']['odc_no'];?>">
								
									</div>
								</div>
								
							<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Mode of Pay</label>
									<div class="col-sm-7">
										<select class="form-control" data-row-id="row_1" id="mop"
											name="mop" style="width: 100%;" required>
											<?php if ($invoice_data['invoice']['mop'] == 1) {?>
						                     	<option value="<?php echo $invoice_data['invoice']['mop'] ?>">Cash</option>
						                      	<option value="2">Cheque</option>
						                        <option value="3">Card</option>
						                        <option value="4">Credit</option>
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option>
					                   		<?php } else if($invoice_data['invoice']['mop'] == 2) { ?>
												<option value="<?php echo $invoice_data['invoice']['mop'] ?>">Cheque</option>
						                      	<option value="1">Cash</option>
						                        <option value="3">Card</option>
						                        <option value="4">Credit</option>
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option> 
					                   		<?php } else if($invoice_data['invoice']['mop'] == 3) { ?>
					                   			<option value="<?php echo $invoice_data['invoice']['mop'] ?>">Card</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="4">Credit</option>
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option>
						                    <?php } else if($invoice_data['invoice']['mop'] == 4) { ?>
					                   			<option value="<?php echo $invoice_data['invoice']['mop'] ?>">Credit</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="3">Card</option> 
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option>
					                   		 <?php } else if($invoice_data['invoice'] == 5) { ?>
					                   			<option value="<?php echo $invoice_data['invoice']['mop'] ?>">UPI</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="3">Card</option> 
                                                <option value="4">Credit</option>
                                           		<option value="6">Netbanking</option>
					                   		 <?php } else if($invoice_data['invoice']['mop'] == 6) { ?>
					                   			<option value="<?php echo $invoice_data['invoice']['mop'] ?>">Netbanking</option>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                      	<option value="3">Card</option> 
                                                <option value="4">Credit</option>
                                                <option value="5">UPI</option>
					                   		<?php } else { ?>
						                      	<option value="1">Cash</option>
						                      	<option value="2">Cheque</option>
						                        <option value="3">Card</option>
						                        <option value="4">Credit</option>
                                                <option value="5">UPI</option>
                                           		<option value="6">Netbanking</option>
					                   		<?php } ?> 
										</select>
									</div>
								</div>
							<div class="form-group">
								<label for="gross_amount" class="col-sm-5 control-label"
									style="text-align: left; font-size: 16px; height: 34px; 
									padding-top: 5px; background-color: #ec8282;">
									Total Items</label>
								<div class="col-sm-7">
									 <input type="hidden" class="form-control" id="estimateid" name="estimateid" 
									 value="<?php echo $invoice_data['invoice']['id'] ?>" required autocomplete="off" /> 
									 <input type="hidden" class="form-control" id="eorderno" name="eorderno" 
									 value="<?php echo $invoice_data['invoice']['invoice_id'] ?>" required autocomplete="off" /> 
									<input type="text" 
										class="form-control" readonly id="total_items" name="total_items"
										autocomplete="off" value="<?php echo $invoice_data['invoice']['total_items'];?>"/>
								</div>
							</div>
							</div>
							<div class="col-md-4 col-xs-12 pull pull-left">
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Driver Name</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="driver_name"
											readonly name="driver_name" placeholder="Enter Driver Name" required
											autocomplete="off" value="<?php echo $invoice_data['invoice']['outward_data']['drivername'];?>"/>
									</div>
								</div>
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Driver Phone</label>
									<div class="col-sm-7">
										<input type="hidden" class="form-control" id="driver_id"
											name="driver_id" placeholder="Enter Driver Phone" required
											autocomplete="off" value="<?php echo $invoice_data['invoice']['outward_data']['did'];?>"/> 
										<input type="text" class="form-control"
											readonly id="driver_phone" name="driver_phone"
											placeholder="Enter Driver Phone" required autocomplete="off" 
											value="<?php echo $invoice_data['invoice']['outward_data']['driverphone'];?>"/>
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Customer Name*</label>
									<div class="col-sm-7">
										<input type="hidden" class="form-control" id="customer_id"
											name="customer_id" autocomplete="off" value="<?php echo $invoice_data['invoice']['outward_data']['sid'];?>"/>
									    <input type="text"
											readonly class="form-control" id="customer_name" name="customer_name"
											placeholder="Enter Customer Name" required autocomplete="off"
											value="<?php echo $invoice_data['invoice']['outward_data']['supplier_name'];?>"/>
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Phone*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_phone"
											name="customer_phone" pattern="^[0-9]{10,12}$"
											placeholder="Enter Customer Phone" required readonly
											autocomplete="off" value="<?php echo $invoice_data['invoice']['outward_data']['ph_no'];?>"/>
									</div>
								</div>
								
								
							</div>

							<div class="col-md-4 col-xs-12 pull pull-left">
						<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Address</label>
									<div class="col-sm-7">

										<input type="text" class="form-control" id="customer_address"
											name="customer_address" placeholder="Enter Customer Address"
											readonly autocomplete="off" value="<?php echo $invoice_data['invoice']['outward_data']['supplier_address'];?>"/>
									</div>
								</div>
							
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">GST/PAN</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_gst"
											name="customer_gst" readonly placeholder="Enter Customer GST/PAN"
											autocomplete="off" value="<?php echo $invoice_data['invoice']['outward_data']['gst_no'];?>"/>
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Email</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" readonly id="customer_email"
											name="customer_email"
											pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
											placeholder="Enter Customer Email" autocomplete="off" value="<?php echo $invoice_data['invoice']['outward_data']['email_id'];?>"/>
									</div>
								</div>
								
								<div class="form-group">
									<label for="brands" class="col-sm-5 control-label"
										style="text-align: left;">State</label>
									<div class="col-sm-7">
										<input type="text" id="customer_state" readonly name="customer_state"
											class="form-control"
											placeholder="Enter State" value="<?php echo $invoice_data['invoice']['outward_data']['state'];?>"/>
									</div>
								</div>
								<div class="form-group" style="display: none">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">State Code</label>
									<div class="col-sm-7">
										<input type="text" class="form-control"
											id="customer_state_code" name="customer_state_code"
											value="33" placeholder="Enter State Code" readonly autocomplete="off"
											value="<?php echo $invoice_data['invoice']['outward_data']['state_code'];?>"/>
									</div>
								</div>
							</div>
						</div>
						<strong>Outward Details</strong>
						<table class="table table-bordered">
								<thead>
									<tr>
										<th style="width: 8%">Lot No</th> 
										<th style="width: 7%">Lot Date</th>
										<th style="width: 7%">Total Out Box Items</th>
										<th style="width: 7%">Total Out Single Items</th>
										<th style="width: 8%">Rent Amt</th>
										<th style="width: 8%">Total Other Amts</th>  
										<th style="width: 9%">Paid Amount</th>
										<th style="width: 6%">Balance</th>
									</tr>
								</thead>	

								<tbody>
									<tr id="row_1">
										<td><input type="text" readonly name="outDC" id="outDC"
											class="form-control" autocomplete="off">
										</td> 
										<td><input type="text" class="form-control" id="odate"
											name="odate" readonly autocomplete="off"></td>
										<td><input type="text" readonly name="outboxes" id="outboxes"
											class="form-control" autocomplete="off"></td>
										<td><input type="text" name="outsingles" id="outsingles"
											class="form-control" readonly></td> 
										<td><input type="text" readonly name="total_outamt" id="total_outamt" 
											class="form-control" autocomplete="off"></td>
										<td><input type="text" readonly name="outotheramt" id="outotheramt" 
											class="form-control" autocomplete="off"></td>
										<td><input type="text" readonly name="advance" id="advance"
											class="form-control" autocomplete="off"></td>
										<td><input type="text" name="outbalance" id="outbalance"
											class="form-control" readonly autocomplete="off"></td>
									</tr>
								</tbody>
							</table>
							
							<strong>Inward Details</strong>
							<table class="table table-bordered" id="product_info_table_gst">
								<thead>
									<tr>
										<th style="width: 8%">Lot No</th> 
										<th style="width: 7%">Lot Date</th>
										<th style="width: 7%">Total In Box Items</th>
										<th style="width: 7%">Total In Single Items</th>
										<th style="width: 8%">Rent Amt</th> 
										<th style="width: 8%">Total Other Amts</th>  
										<th style="width: 9%">Paid Amount</th>
										<th style="width: 6%">Balance</th>
									</tr>
								</thead>

								<tbody>
									<tr id="row_1">
										<td><input type="text" readonly name="inDC[]" id="inDC_1"
											class="form-control" autocomplete="off">
											<input type="hidden" readonly name="inId[]" id="inId_1"
											class="form-control" autocomplete="off"></td> 
										<td><input type="text" class="form-control" id="in_date_1"
											name="in_date[]" readonly autocomplete="off"></td>
										<td><input type="text" readonly name="inboxes[]" id="inboxes_1"
											class="form-control" autocomplete="off"></td>
										<td><input type="text" name="insingles[]" id="insingles_1"
											class="form-control" readonly></td> 
										<td><input type="text" readonly name="inamount[]" id="inamount_1" 
											class="form-control" autocomplete="off"></td>
										<td><input type="text" readonly name="inotheramt[]" id="inotheramt_1" 
											class="form-control" autocomplete="off"></td>
										<td><input type="text" readonly name="paidamt[]" id="paidamt_1"
											class="form-control" autocomplete="off"></td>
										<td><input type="text" name="inbalance[]" id="inbalance_1"
											class="form-control" readonly autocomplete="off"></td>
									</tr>
								</tbody>
							</table>

							<!-- div>
								&nbsp;
								<button type="button" id="add_row" class="btn btn-default">
									<i class="fa fa-plus"></i>&nbsp; Add New Row
								</button>
							</div-->
							<div>
								<!-- New Format -->
								<div class="col-md-12 col-xs-12 pull-right bottomDiv">
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Total Balance</label> <input
												type="text" class="form-control" id="rent_amount"
												name="rent_amount" readonly autocomplete="off">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Cleaning Charge</label> 
											<input type="text" class="form-control" id="cleaningcharge"
												name="cleaningcharge" autocomplete="off"
												onkeyup="calculateInvBalance()" value="<?php echo $invoice_data['invoice']['cleaningcharge'];?>"/>
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Vehicle Transfer Charge</label> 
											<input type="text" class="form-control" id="vehtranscharge"
												name="vehtranscharge" autocomplete="off"
												onkeyup="calculateInvBalance()" value="<?php echo $invoice_data['invoice']['vehtranscharge'];?>">
										</div>
									</div>
								  
									<div class="col-md-2 col-xs-12">
										<div class="form-group">
											<label for="gross_amount">Discount Amount</label> <input
												type="text" class="form-control" id="disc_amount"
												name="disc_amount" autocomplete="off"
												onkeyup="calculateInvBalance()"
												value="<?php echo $invoice_data['invoice']['disc_amount'];?>">
										</div>
									</div>
								    <div class="col-md-2 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="iamtpaid">Total In + Out Paid</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="iamtpaid"
												name="iamtpaid" readonly autocomplete="off"
												value="<?php echo $invoice_data['invoice']['iamtpaid'];?>">
										</div>
									</div>
									<div class="col-md-2 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="itotalamt">Total In Rent</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="itotalamt"
												name="itotalamt" readonly autocomplete="off"
												value="<?php echo $invoice_data['invoice']['itotalamt'];?>">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="itotalamt">Total In + Out Others</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="itotalothers"
												name="itotalothers" readonly autocomplete="off">
										</div>
									</div>
							
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
						 								<label for="net_amount">Net Amount</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="net_amount"
												name="net_amount" readonly autocomplete="off"
												value="<?php echo $invoice_data['invoice']['net_amount'];?>">
                                                <input type="hidden" class="form-control" id="net_amount_valuedb"
												name="net_amount_valuedb" readonly autocomplete="off"
												value="<?php echo $invoice_data['invoice']['net_amount'];?>">
										</div>
									</div>
									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="amtpaid">Amount Paid</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="hidden" required class="form-control" id="amtpaiddb"
												name="amtpaiddb"  
												autocomplete="off" 
												value="<?php echo $invoice_data['invoice']['amtpaid'];?>">
											<input type="text" required class="form-control" id="amtpaid"
												name="amtpaid" onkeyup="calculateInvBalance()"
												autocomplete="off" 
												value="<?php echo $invoice_data['invoice']['amtpaid'];?>">
										</div>
									</div>

									<div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="balance">Balance</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="hidden" class="form-control" id="balancedb"
												name="balancedb"  autocomplete="off"
												value="<?php echo $invoice_data['invoice']['balance'];?>">
                                                <input type="text" class="form-control" id="balance"
												name="balance" readonly autocomplete="off"
												value="<?php echo $invoice_data['invoice']['ibalance'];?>">
										</div>
									</div>
								    <div class="col-md-10 col-xs-12">
										<div class="form-group"
											style="padding-right: 26px; float: right; padding-top: 8px;">
											<label for="balance">Balance To Return to Customer</label>
										</div>
									</div>
									<div class="col-md-2 col-xs-12 pull-right">
										<div class="form-group">
											<input type="text" class="form-control" id="icbalance"
												name="icbalance" value="0" readonly autocomplete="off"
												value="<?php echo $invoice_data['invoice']['itotalcbal'];?>">
										</div>
									</div>
								</div>
							</div>
						</div> 
					</div>
				<!-- /.box -->
				<div class="box-footer">
					<a target="__blank" href="<?php echo base_url() . 'estimate/printDiv/'.$invoice_data['invoice']['id'] ?>" class="btn btn-success" >Print</a>
				<?php  if($invoice_data['invoice']['invid']=="0" || $invoice_data['invoice']['invid']=="" || $invoice_data['invoice']['invid']=="null"): ?>
					<button type="submit" value="Update" name="update_button" class="btn btn-primary">Update Estimate</button>
				    <button type="submit"  value="ConvertToSales" name="convertSales_button" class="btn btn-warning">Convert to Invoice</a></button>
				<?php endif?>
					<a href="<?php echo base_url('estimate/') ?>" class="btn btn-warning">Back</a>
				</div>
				</form>
					</div>
				<!-- /.box-body -->
			
		</div>
		<!-- col-md-12 -->
            
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>

<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {
    $(".select_group").select2();
    $("#mainEstimateNav>a")[0].click();
    $("#mainEstimateNav").addClass('active');
    $("#manageEstimateNav").addClass('active');

	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	});

	//Disable button and prevent double submitting
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	});
	
	getInwardOutwardDetails();
  });
  

  

</script>