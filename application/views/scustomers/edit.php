
<?php ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage
     Customers 
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Customers</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>


        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Edit Customers</h3>
          </div>
          <!-- /.box-header -->
          <form role="form" action="<?php base_url('scustomers/update') ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">

                <?php echo validation_errors(); ?>
				<div class="col-md-6 col-xs-12 pull pull-left">
        
                <div class="form-group">
                 <label for="supp_name">Customers name*</label>
                  <input type="text" class="form-control" id="supp_name" name="supp_name" placeholder="Enter Customers name" 
                  required value="<?php echo $supplier_data['supplier']['supp_name']; ?>"  autocomplete="off"/>
                </div>
              

                <div class="form-group">
                  <label for="supp_address">Customers Address</label>
                  <input type="text" class="form-control" id="supp_address" name="supp_address" pattern=".{,28}" title="Max limit 28 characters"
		  	placeholder="Address line 1" value="<?php echo $supplier_data['supplier']['supp_address']; ?>" autocomplete="off" style=" border-bottom: 0px solid #ccc;"/>
                 <input type="text" class="form-control" id="supp_address2"  name="supp_address2" pattern=".{,28}" title="Max limit 28 characters"
                  	placeholder="Address line 2" value="<?php echo $supplier_data['supplier']['supp_address2']; ?>" autocomplete="off" style=" border-bottom: 0px solid #ccc; border-top: 0px;"/>
                  <input type="text" class="form-control" id="supp_address3"  name="supp_address3" pattern=".{,28}" title="Max limit 28 characters"
                  	placeholder="Address line 3" value="<?php echo $supplier_data['supplier']['supp_address3']; ?>" autocomplete="off" style="border-bottom: 0px solid #ccc; border-top: 0px;"/>
                  <input type="text" class="form-control" id="supp_address4"  name="supp_address4" pattern=".{,28}" title="Max limit 28 characters"
                  	placeholder="Address line 4" value="<?php echo $supplier_data['supplier']['supp_address4']; ?>" autocomplete="off" style=" border-top: 0px;"/>
            
                </div>
 
		<div class="form-group">
                  <label for="ph_no">Contact Number*</label>
                  <input type="text" class="form-control" id="ph_no" name="ph_no" 
                  placeholder="Enter Phone Number"  pattern="^[0-9]{10,12}$" required value="<?php echo $supplier_data['supplier']['ph_no']; ?>" autocomplete="off" />
                </div>
    <div class="form-group">
                  <label for="email_id">Email Id</label>
                  <input type="text" class="form-control" id="email_id" name="email_id" 
                  placeholder="Enter Email Id" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php echo $supplier_data['supplier']['email_id']; ?>"  autocomplete="off"  />
                </div>
                	<div class="form-group">
                  <label for="gst_no">GST Number</label>
                  <input type="text" class="form-control" id="gst_no" name="gst_no" 
                  placeholder="Enter GST Number Number"  value="<?php echo $supplier_data['supplier']['gst_no']; ?>" autocomplete="off" />
                </div>
    
    
    
   				 </div>
                <div class="col-md-6 col-xs-12 pull pull-left">
                     <div class="form-group">
                            <div class="col-md-8 col-xs-12 pull pull-left" style="padding-left: 0px;">
                         <label for="brands">State</label>
                                <div class="col-sm-12" style="margin-bottom: 10px; padding: 0px;">	
                            <input type="text" id="customer_state" name=customer_state class="form-control"
                                 placeholder="Enter State" value="<?php echo $supplier_data['supplier']['state']; ?>"  >        
                            <ul class="dropdown-menu txtstatename" style="margin-left:15px;margin-right:0px;"
                                role="menu" aria-labelledby="dropdownMenu"  id="DropdownStateName"></ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12 pull pull-right">
                            <label for="state_code">State Code</label>
                                <div class="col-sm-12" style="margin-bottom: 10px; padding: 0px;">	
                                <input type="text" class="form-control" id="customer_state_code" name="customer_state_code" 
                                placeholder="Enter State Code" readonly value="<?php echo $supplier_data['supplier']['state_code']; ?>"  autocomplete="off"  />
                                </div> 
                            </div>
                   
                   
                   <div class="form-group">
                      <label for="credit">Credit</label>
                      <select class="form-control" id="credit" name="credit">
                        <option value="1" <?php if($supplier_data['supplier']['credit'] == 1) { echo "selected='selected'"; } ?>>Yes</option>
                        <option value="2" <?php if($supplier_data['supplier']['credit'] != 1) { echo "selected='selected'"; } ?>>No</option>
                      </select>
                    </div>

                    <div class="form-group">
                       <label for="credit_limit">Credit Limit</label>
                      <input type="text" class="form-control" id="credit_limit" name="credit_limit" placeholder="Enter credit limit" value="<?php echo $supplier_data['supplier']['credit_limit']; ?>"  autocomplete="off"  />
                    </div>
                     <div class="form-group">
                      <label for="credit_days">Credit Days</label>
                      <input type="text" class="form-control" id="credit_days" name="credit_days" placeholder="Enter Credit Days" value="<?php echo $supplier_data['supplier']['credit_days']; ?>"  autocomplete="off"   />
                     </div> 
                            
                    </div>
         		 </div>
                   
              
          		
                
             </div>
              
             <!-- </div>-->
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="<?php echo base_url('scustomers/') ?>" class="btn btn-warning">Back</a>
              </div>
              
            </form>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
  
  $(document).ready(function() {
    $(".select_group").select2();
    $("#description").wysihtml5();
    $("#mainSupplierNav>a")[0].click();
    $("#mainSupplierNav").addClass('active');
    $("#manageSupplierNav").addClass('active');


	//Disable button and prevent double submitting
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	});
  });
//get the product information from the server
  function getCommonProductData(row_id)
  {
	  getProductItemData(row_id);
  }

  
  

</script>