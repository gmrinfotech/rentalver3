<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Customer wise Report
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active">Day wise Report</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
		<?php endif; ?>
		<?php
		$from_date   = array('name'=>'from_date', 'id'=>'from_date', 'value' => set_value('from_date', $from_date), 'class'=>'form-control default-date-picker', 'required' => '');
		$to_date   = array('name'=>'to_date', 'id'=>'to_date', 'value' => set_value('to_date', $to_date), 'class'=>'form-control default-date-picker', 'required' => '');
		?>
		 <div>
          <form class="form-inline" action="<?php echo base_url('scustomers/creport') ?>" method="POST">
            <div class="form-group">
           <label>From :</label>
			<?php echo form_input($from_date); ?>
           <label>To :</label>
		 	<?php echo form_input($to_date); ?>
            </div>
             <label>Customer Name :</label>
            <select class="form-control" name="customer" id="customer" style="width:150px !important">
                 			<option value="" <?php if($fcustomer == "") { echo "selected='selected'"; } ?>>All</option>
                            <?php foreach ($customers as $k => $v): ?> 
                             <option value="<?php echo $v['supl_id'] ?>" 
                             <?php if($fcustomer == $v['supl_id']) { echo "selected='selected'"; } ?> >
                             <?php echo $v['supp_name'] ?></option> 
                            <?php endforeach ?> 
              </select>
            <button btn btn-primary repSubBtntype="submit" class="btn btn-primary repSubBtn">Submit</button>
          </form>
        </div> 
	 </div>
	 </div>
      <br>
	<div class="row">
		<div class="col-sm-12">
			<div class="box">
				<div class="box-body"> 
						<div class="scrollable col-md-12 col-xs-12 pl0">
							 <table id="manageTable" class="table table-bordered table-striped cf">
								<thead class="cf"> 
									<th width="8%">Outward No</th>
									<th width="6%">Out Date</th>
									<th width="6%">Item Type</th> 
									<th width="15%">Box Name</th> 
									<th width="6%">Out Qty</th> 
									<th width="6%">Return Date</th>
									<th width="6%">Return Qty</th>
									<th width="6%">Bal Qty</th> 
							 		<th width="10%">Amount</th>
							 		<th width="10%">Reduction</th>
							 		<th width="10%">Final Amount</th>
								</thead>
								<tbody>
								 <?php if(count($orderlist) > 0): ?>
				                      <?php foreach($orderlist as $key => $row): ?>
				                          <tr>
				                            <td><?php echo $row['odc_no']; ?></td> 
				                            <td><?php echo $row['outdate']; ?></td>
				                            <td><?php echo $row['itype']; ?></td> 
				                            <td><?php echo $row['name']; ?></td> 
				                            <td><?php echo $row['outunit']; ?></td> 
				                            <td><?php echo $row['indate'];?></td>
				                            <td><?php echo $row['inunits'];?></td> 
				                            <td><?php echo $row['outunit'] - $row['inunits']; ?></td>  
				                            <td><?php echo $row['totalrent']; ?></td>
				                            <td><?php echo $row['discount']; ?></td>
				                            <td><?php echo $row['finalamt']; ?></td>
				                          </tr>
				                      <?php endforeach; ?>
				                  <?php endif; ?>
								</tbody>
							</table>
						</div>
					</div> 
				</div>
			</div>	 
		 </div>
	 </div>
	</div>
 
    <script type="text/javascript">
    $(document).ready(function() {
    $("#mainSupplierNav>a")[0].click();
    $("#mainSupplierNav").addClass('active menu-open');
    $("#manageSupplierRep").addClass('active');

    manageTable = $('#manageTable').DataTable({
       pageLength: '100', 
       bSort : false,
 	   dom: 'Bfrtip',
 	    buttons: [
 			'excel'
  		],
         'order': [] 
   });
    
    $(".default-date-picker").datepicker({
		// minView: 2,
	    format: 'dd/mm/yyyy',
	    autoclose: true,
	    todayHighlight: true
	});
 
    });
    </script>
 