
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage
       Customers 
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Customers</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>


        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Add Customer</h3>
          </div>
          <!-- /.box-header -->
          <form role="form" action="<?php base_url('scustomers/create') ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">

                <?php echo validation_errors(); ?>
				<div class="col-md-6 col-xs-12 pull pull-left">
        
                <div class="form-group">
                  <label for="supp_name">Customer name*</label>
                  <input type="text" class="form-control" id="supp_name" name="supp_name" placeholder="Enter Customer name" required autocomplete="off"/>
                </div>

                <div class="form-group">
                  <label for="supp_address">Customer Address</label>
                  <input type="text" class="form-control" id="supp_address" name="supp_address" placeholder="Address line 1" required 
                       pattern=".{,28}" title="Max limit 28 characters"
                  	placeholder="Address line 1" autocomplete="off" style="border-bottom: 0px solid #ccc;"/>
                  <input type="text" class="form-control" id="supp_address2"  name="supp_address2" pattern=".{,28}" title="Max limit 28 characters"
                  	placeholder="Address line 2" autocomplete="off" style=" border-bottom: 0px solid #ccc; border-top: 0px;"/>
                  <input type="text" class="form-control" id="supp_address3"  name="supp_address3" pattern=".{,28}" title="Max limit 28 characters"
                  	placeholder="Address line 3" autocomplete="off" style="border-bottom: 0px solid #ccc; border-top: 0px;"/>
                  <input type="text" class="form-control" id="supp_address4"  name="supp_address4" pattern=".{,28}" title="Max limit 28 characters"
                  	placeholder="Address line 4" autocomplete="off" style=" border-top: 0px;"/>
                </div>
        
                      <div class="form-group">
                  <label for="ph_no">Contact Number*</label>
                  <input type="text" class="form-control" id="ph_no" name="ph_no" pattern="^[0-9]{10,12}$" required placeholder="Enter Phone Number" autocomplete="off" />
                </div>
                
                <div class="form-group">
                  <label for="email_id">Email Id</label>
                  <input type="text" class="form-control" id="email_id" name="email_id" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Enter Email Id" autocomplete="off"   />
                </div>
                 <div class="form-group">
                  <label for="gst_no">GST Number</label>
                  <input type="text" class="form-control" id="gst_no" name="gst_no" placeholder="Enter GST Number" autocomplete="off" />
                </div>
                
                
                
                </div>
                <div class="col-md-6 col-xs-12 pull pull-left">
                           

				
                <div class="form-group">
	                <div class="col-md-8 col-xs-12 pull pull-left" style="padding-left: 0px;">
	                 <label for="brands">State</label>
                      <div class="col-sm-12" style="margin-bottom: 10px; padding: 0px;">	
	                    <input type="text" id="customer_state" name=customer_state class="form-control"
	                    	 placeholder="Enter State" value="Tamil Nadu">        
	                    <ul class="dropdown-menu txtstatename" style="margin-left:15px;margin-right:0px;"
                    	    role="menu" aria-labelledby="dropdownMenu"  id="DropdownStateName"></ul>
                  	   </div>
	                </div>
	                 
	                 <div class="col-md-4 col-xs-12 pull pull-right">
		                   <label for="state_code">State Code</label>
		                    <div class="col-sm-12" style="margin-bottom: 10px; padding: 0px;">	
                  <input type="text" class="form-control" id="customer_state_code" name="customer_state_code" 
                  placeholder="Enter State Code" readonly autocomplete="off"  value="33"  />
	                    </div>  </div>
                        
                        
            <div class="form-group">
            <label for="credit">Credit</label>
            <select class="form-control" id="credit" name="credit">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>    
          <div class="form-group">
                  <label for="credit_limit">Credit Limit</label>
                  <input type="text" class="form-control" id="credit_limit" name="credit_limit" placeholder="Enter Credit Limit" autocomplete="off"   />
           </div>
           <div class="form-group">
                  <label for="credit_days">Credit Days</label>
                  <input type="text" class="form-control" id="credit_days" name="credit_days" placeholder="Enter Credit Days" autocomplete="off"   />
           </div>
          
          <div class="form-group">
                  <label for="credit_days">Old Balance</label>
                  <input type="text" class="form-control" id="old_balance" name="old_balance" placeholder="Enter Old Balance" autocomplete="off"   />
           </div>
          
          
          
            
				</div>

				<h3>&nbsp;</h3>
		        <div class="form-group" style="display: none">
		            <label for="credit">Credit</label>
		            <select class="form-control" id="credit" name="credit">
		              <option value="1">Active</option>
		            </select>
		         </div>
		   <!--        <div class="form-group">
	                  <label for="credit_limit">Credit Limit</label>
	                  <input type="text" class="form-control" id="credit_limit" name="credit_limit" placeholder="Enter Credit Limit" autocomplete="off"   />
		           </div>

		         <div class="form-group">
	                  <label for="credit_limit">Grace Period (In Days)</label>
	                  <input type="text" class="form-control" id="grace_period" name="grace_period"
	                   placeholder="Enter Grace Period" value="0" autocomplete="off"/>
		           </div> -->  
		           
		           </div>  
             </div>
              
             <!-- </div>-->
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save Changes</button>
                <a href="<?php echo base_url('scustomers/') ?>" class="btn btn-warning">Back</a>
              </div>
            </form>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
  $(document).ready(function() {
    $(".select_group").select2();
    $("#description").wysihtml5();
    $("#mainSupplierNav>a")[0].click();
    $("#mainSupplierNav").addClass('active');
    $("#addSupplierNav").addClass('active');
  
	//Disable button and prevent double submitting
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	}); 
});
 

</script>