
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage GMRAdmin Information
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Company</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12 col-xs-12">
          
          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('success'); ?>
            </div>
          <?php elseif($this->session->flashdata('error')): ?>
            <div class="alert alert-error alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('error'); ?>
            </div>
          <?php endif; ?>

          <div class="box">
          <br>
            <form role="form" action="<?php base_url('company/gmradmin') ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">

                <?php echo validation_errors(); ?>
				<div class="col-md-4 col-xs-12 pull pull-left">  
                 <div class="form-group">
                 <input id="showpprice" name="showpprice" value="1" <?php if($showpprice){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable showpprice </b>
                </div>
                   <div class="form-group">
                  <input id="subsidy" name="subsidy" value="1" <?php if($subsidy){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                   <b>Enable subsidy  </b>
                </div>
                   <div class="form-group">
                 <input id="opticals" name="opticals" value="1" <?php if($opticals){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable opticals  </b>
                </div>
                     <div class="form-group">
                 <input id="warehouse" name="warehouse" value="1" <?php if($comwarehouse){echo 'checked="checked"'; } ?>  type="checkbox" />
                   <b>Enable warehouse </b>
                </div>
         			<div class="form-group">
                <input id="enableestimate" name="enableestimate" value="1" <?php if($enableestimate){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable Estimate  </b>
                </div>
                <div class="form-group">
                 <input id="salesinvpayment" name="salesinvpayment" value="1" <?php if($salesinvpayment){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable salesinvpayment  </b>
                </div>
                </div>
  
 				 <div class="col-md-4 col-xs-12 pull pull-left"> 
                 <div class="form-group">
                 <input id="custreports" name="custreports" value="1" <?php if($custreports){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable custreports </b>
                </div>
                   <div class="form-group">
                  <input id="enablepurchase" name="enablepurchase" value="1" <?php if($enablepurchase){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                   <b>Enable purchase  </b>
                </div>
                   <div class="form-group">
                  <input id="enablesales" name="enablesales" value="1" <?php if($sales){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                   <b>Enable Sales  </b>
                </div>
                   <div class="form-group">
                 <input id="custpayment" name="custpayment" value="1" <?php if($custpayment){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable custpayment  </b>
                </div>
                  <div class="form-group">
                 <input id="purpayment" name="purpayment" value="1" <?php if($purpayment){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable Purchase payment  </b>
                </div>
                <div class="form-group">
                 <input id="csales" name="csales" value="1" <?php if($csales){echo 'checked="checked"'; } ?>  type="checkbox" />
                   <b>Enable Convert to sales </b>
                </div> 
                </div>
  
   				<div class="col-md-4 col-xs-12 pull pull-left"> 
   				<div class="form-group">
                <input id="mobservice" name="mobservice" value="1" <?php if($mobservice){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable Mobile service  </b>
                </div>
   				 <div class="form-group">
                 <input id="corder" name="corder" value="1" <?php if($corder){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable C Orders (Retail sales)  </b>
                </div>
                 <div class="form-group">
                 <input id="hotel" name="hotel" value="1" <?php if($hotel){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable Hotel </b>
                </div>
                   <div class="form-group">
                  <input id="attendance" name="attendance" value="1" <?php if($attendance){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                   <b>Enable attendance  </b>
                </div>
                   <div class="form-group">
                 <input id="redeempoints" name="redeempoints" value="1" <?php if($points){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Enable redeempoints  </b>
                </div>
 
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            	<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            	<input id="save" name="save" value="1" <?php echo 'checked="checked"'; ?> type="checkbox" /> 
                  <b>Save</b>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script type="text/javascript">
  $(document).ready(function() { 
    $("#mainSettingsNav>a")[0].click();
    $("#gmradminNav").addClass('active menu-open');
    $("#message").wysihtml5();
 
  });
</script>

