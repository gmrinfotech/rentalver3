
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manage
        Company Information
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Company</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12 col-xs-12">
          
          <?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('success'); ?>
            </div>
          <?php elseif($this->session->flashdata('error')): ?>
            <div class="alert alert-error alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $this->session->flashdata('error'); ?>
            </div>
          <?php endif; ?>

          <div class="box">
          <br>
            <form role="form" action="<?php base_url('company/update') ?>" method="post" enctype="multipart/form-data">
              <div class="box-body">

                <?php echo validation_errors(); ?>
			<div class="col-md-4 col-xs-12 pull pull-left">
                <div class="form-group">
                  <label for="company_name">Company Name*</label>
                  <input type="text" class="form-control" id="company_name" name="company_name" required 
                  placeholder="Enter company name" value="<?php echo $company_data['company_name'] ?>" autocomplete="off">
                </div>
                <div class="form-group">
                  <label for="address">Address*</label>
                  <input type="text" class="form-control" id="address1" name="address1" 
	                  required placeholder="Flat No, Street Address" border-bottom: 0px solid #ccc;"
	                  value="<?php if(isset($company_addr[0]))  echo $company_addr[0] ?>" autocomplete="off">
	                 
                  	<input type="text" class="form-control" id="address2" name="address2" 
	                  required placeholder="Landmark" border-bottom: 0px solid #ccc;"
	                  value="<?php if(isset($company_addr[1]))  echo $company_addr[1] ?>" autocomplete="off">
                    
                    <input type="text" class="form-control" id="address3" name="address3" 
	                  required placeholder="Town" border-bottom: 0px solid #ccc;"
	                  value="<?php if(isset($company_addr[2])) echo $company_addr[2] ?>" autocomplete="off">
                    
                    <input type="text" class="form-control" id="address4" name="address4" 
	                  required placeholder="City-Pincode" border-bottom: 0px solid #ccc;"
	                  value="<?php if(isset($company_addr[3]))  echo $company_addr[3] ?>" autocomplete="off">
	                  
	                 <input type="text" class="form-control" id="address5" name="address5" 
	                   placeholder="State"  
	                  value="<?php if(isset($company_addr[4])) echo $company_addr[4] ?>" autocomplete="off">
	                 
	                
                </div>
               <div class="form-group">
                  <label for="phone">Phone*</label>
                  <input type="text" class="form-control" id="phone" name="phone" required placeholder="Enter phone" value="<?php echo $company_data['phone'] ?>" autocomplete="off">
                </div>	
                <div class="form-group">
                  <label for="phone">Email*</label>
                  <input type="text" class="form-control" id="email" name="email" required 
                   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Enter email" value="<?php echo $company_data['email'] ?>" autocomplete="off">
                </div>	
                  <div class="form-group">
                  <label for="company_gstno">Company GST Number*</label>
                  <input type="text" class="form-control" id="company_gstno" name="company_gstno" required placeholder="Enter company GST number" value="<?php echo $company_data['company_gstno'] ?>" autocomplete="off">
                </div>
                <?php if($corder) :?>
					 
 				<div class="form-group">
                <input id="printcorder" name="printcorder" value="1" <?php if($printcorder){echo 'checked="checked"'; } ?>  type="checkbox" />   <b>Click the box to Print C-Order  </b>
                </div>
               <?php endif; ?>
                   <div class="form-group">
                  <input id="productcodeenable" name="productcodeenable" value="1" <?php if($pcode){echo 'checked="checked"'; } ?>  type="checkbox" />  <b>Click the box to Enable Product code  </b>
                </div>
                   <div class="form-group">
                 <input id="productdescenable" name="productdescenable" value="1" <?php if($enableproddesc){echo 'checked="checked"'; } ?>  type="checkbox" />  <b>Click the box to Enable Product Descriptions  </b>
                </div>
                     <div class="form-group">
                 <input id="showbal" name="showbal" value="1" <?php if($showbal){echo 'checked="checked"'; } ?>  type="checkbox" />  <b>Click the box to Show Balance in Invoice </b>
                </div>
         			<div class="form-group">
                <input id="thermalprint" name="thermalprint" value="1" <?php if($thermalprint){echo 'checked="checked"'; } ?>  type="checkbox" />   <b>Click the box to Enable Thermal Print  </b>
                </div>
                <div class="form-group">
                 <input id="A4print" name="A4print" value="1" <?php if($A4print){echo 'checked="checked"'; } ?>  type="checkbox" />  <b>Click the box to Enable A4 Print  </b>
                </div>
                <div class="form-group">
                 <input id="hsncode" name="hsncode" value="1" <?php if($hsncode){echo 'checked="checked"'; } ?>  type="checkbox" />  <b>Click the box to Enable HSN/SAC Code in Print  </b>
                </div>
                  <div class="form-group">
                 <input id="composition" name="composition" value="1" <?php if($composition){echo 'checked="checked"'; } ?>  type="checkbox" />  
                 <b>Click the box to Enable Composition Scheme </b>
                </div>  
                 <div class="form-group">
                 <input id="taxsplitup" name="taxsplitup" value="1" <?php if($taxsplitup){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Click the box to Show Tax Splitup in Bill </b> 
                </div>
                <div class="form-group">
                  <label for="decimalpoints">Decimal Points in Bill</label>
                  	<select class="form-control" id="decimalpoints" name="decimalpoints" style="width:100%;">
                    <?php if ($company_data['decimalpoints'] =="4")
					 {?>
	                      <option value="4">4</option>
                          <option value="2">2</option>
	                      <option value="3">3</option>
                          
                    <?php } else if ($company_data['decimalpoints'] =="3") {?>
                          <option value="3">3</option>
						  <option value="2">2</option>
	                      <option value="4">4</option>
                      <?php } else { ?>
                          <option value="2">2</option>
	                      <option value="3">3</option>
                          <option value="4">4</option>
                           <?php } ?>
                     </select>
                     
                </div>
                </div>
 
                
                <div class="col-md-4 col-xs-12 pull pull-left">
                <div class="form-group">
                  <label for="currency">Print : Title</label>
                  	<input type="text" class="form-control" id="title" name="title"
                  	 required placeholder="Enter For Name" value="<?php echo $company_data['title'] ?>" 
                  	 autocomplete="off">
                </div>
                 <div class="form-group">
                  <label for="currency">Print : For Name</label>
                  	<input type="text" class="form-control" id="forname" name="forname"
                  	 required placeholder="Enter For Name" value="<?php echo $company_data['forname'] ?>" 
                  	 autocomplete="off">
                </div>
                 
 				<label for="currency">Print : Copy Names</label> 
 				 <div class="form-group"> 
	        			<input type="text" class="form-control" id="printCopy1Title" name="printCopy1Title" 
	                   placeholder="Print Copy Name 1"
	                   value="<?php echo $company_data['printCopy1Title'] ?>" autocomplete="off">
     			</div>
                 
                  <div class="form-group">
	        			Click to Print 2nd Copy : <input id="printCopy2checkid" name="printCopy2checkid" value="1" <?php if($company_data['printCopy2']== "1"){echo 'checked="checked"'; } ?>  type="checkbox" />
	        		<input type="text" class="form-control" id="printCopy2Title" name="printCopy2Title" 
	                   placeholder="Print Copy Name 2"
	                   value="<?php echo $company_data['printCopy2Title'] ?>" autocomplete="off">
	     			
     			</div>
     			 <div class="form-group">
	        			Click to Print 3rd Copy : <input id="printCopy3checkid" name="printCopy3checkid"   type="checkbox" value="1" <?php if($company_data['printCopy3']== "1"){echo 'checked="checked"'; } ?>  />
	        			<input type="text" class="form-control" id="printCopy3Title" name="printCopy3Title" 
	                   placeholder="Print Copy Name 3"
	                   value="<?php echo $company_data['printCopy3Title'] ?>" autocomplete="off">
	     			
     			</div>
     			 <div class="form-group">
	        			Click to Print 4th Copy : <input id="printCopy4checkid" name="printCopy4checkid"   type="checkbox" value="1" <?php if($company_data['printCopy4']== "1"){echo 'checked="checked"'; } ?>  />
	        			<input type="text" class="form-control" id="printCopy4Title" name="printCopy4Title" 
	                   placeholder="Print Copy Name 4"
	                   value="<?php echo $company_data['printCopy4Title'] ?>" autocomplete="off">
	     			
     			</div>
     			 <div class="form-group">
	        			Click to Print 5th Copy : <input id="printCopy5checkid" name="printCopy5checkid"   type="checkbox" value="1" <?php if($company_data['printCopy5']== "1"){echo 'checked="checked"'; } ?>  />
	        			<input type="text" class="form-control" id="printCopy5Title" name="printCopy5Title" 
	                   placeholder="Print Copy Name 5"
	                   value="<?php echo $company_data['printCopy5Title'] ?>" autocomplete="off">
	     			
     			</div>
     			<div class="form-group">
                  <label>Discount Type</label>
                  	<select class="form-control" id="discountType" name="discountType" style="width:100%;">
                  	 <?php if ($discountType)
					 {?>
	                      <option value="1">Percentage</option>
	                      <option value="0">Value</option>
                    <?php } else{ ?>
						<option value="0">Value</option> 
	                    <option value="1">Percentage</option>
                      <?php }  ?>
                     </select>
                </div>
              
              <div class="form-group">
                  <label>Choose Customer By</label>
                  	<select class="form-control" id="chooseCust" name="chooseCust" style="width:100%;">
                  	 <?php if ($chooseCust)
					 {?>
                     <option value="1">Name</option>
                      <option value="0">Phone</option>
                    <?php } else{ ?>
					<option value="0">Phone</option> 
                    <option value="1">Name</option>
                      <?php }  ?>
                     </select>
                </div>
                 
                <div class="form-group">
                  <label for="company_gstno">SMS Key*</label>
                  <input type="text" class="form-control" id="smscode" name="smscode" required placeholder="Enter SMS Key" value="<?php echo $company_data['smscode'] ?>" autocomplete="off">
                </div>
                 <div class="form-group">
                  <label for="company_gstno">SMS TITLE* 6 Charaters EX: GMRINF</label>
                  <input type="text" class="form-control" id="smssenderid" name="smssenderid"  pattern=".{6,6}" required placeholder="Enter SMS Sender ID" value="<?php echo $company_data['smssenderid'] ?>" autocomplete="off">
                </div>
               <div class="form-group">
                 <input id="smsenable" name="smsenable" value="1" <?php if($smsenable){echo 'checked="checked"'; } ?>  type="checkbox" />  
                 <b>Click the box to Send SMS Enable</b>
                </div>  
                 <div class="form-group">
                 <input id="emaildirect" name="emaildirect" value="1" <?php if($emaildirect){echo 'checked="checked"'; } ?>  type="checkbox" /> 
                  <b>Click the box to Send Email Instantly </b> 
                </div>
                
                </div>
                
                  <div class="col-md-4 col-xs-12 pull pull-left">
                   <div class="form-group">
                  <label for="bankname">Bank Name</label> <input id="bankactive" name="bankactive" value="1" <?php if($company_data['bankactive']== "1"){echo 'checked="checked"'; } ?>  type="checkbox" />&nbsp;Click the Box to Print Bank Details on Invoice 
                  	<input type="text" class="form-control" id="bankname" name="bankname"
                  	  placeholder="Enter For Bank Name" value="<?php echo $company_data['bankname'] ?>" 
                  	 autocomplete="off">
                </div>
                 <div class="form-group">
                  <label for="accno">Account Number</label>
                  	<input type="text" class="form-control" id="accno" name="accno"
                  	  placeholder="Enter For Account Number" value="<?php echo $company_data['accno'] ?>" 
                  	 autocomplete="off">
                </div>
                 <div class="form-group">
                  <label for="ifsccode">IFSC Code</label>
                  	<input type="text" class="form-control" id="ifsccode" name="ifsccode"
                  	  placeholder="Enter For IFSC Code" value="<?php echo $company_data['ifsccode'] ?>" 
                  	 autocomplete="off">
                </div>
                 <div class="form-group">
                  <label for="branchname">Branch Name</label>
                  	<input type="text" class="form-control" id="branchname" name="branchname"
                  	  placeholder="Enter For Branch Name" value="<?php echo $company_data['branchname'] ?>" 
                  	 autocomplete="off">
                </div>
                 <div class="form-group">
                  <label for="country">Country</label>
                  	<input type="text" class="form-control" id="country" name="country" 
	                   placeholder="country"
	                   value="<?php echo $company_data['country'] ?>" autocomplete="off">
                </div>
                
                <div class="form-group">
                  <label for="currency">Currency</label>
                  <?php ?>
                  <select class="form-control" id="currency" name="currency">
                    <option value="">~~SELECT~~</option>

                    <?php foreach ($currency_symbols as $k => $v): ?>
                      <option value="<?php echo trim($k); ?>" <?php if($company_data['currency'] == $k) {
                        echo "selected";
                      } ?>><?php echo $k ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
               
                <div class="form-group">
                  <label>Existing Logo Preview: </label>
                  <br>
                  <img src="<?php echo base_url() . $company_data['image'] ?>" width="100" height="100">
                </div>

                <div class="form-group">
                  <label for="product_image">Update Logo</label>
                  <div class="kv-avatar">
                      <div class="file-loading">
                          <input id="product_image" name="product_image" type="file">
                      </div>
                  </div>
                </div>               
                 
                 </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            	<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script type="text/javascript">
  $(document).ready(function() {
    $("#mainSettingsNav>a")[0].click();
    $("#mainSettingsNav").addClass('active menu-open');
    $("#companyNav").addClass('active');
    $("#message").wysihtml5();
    
    var btnCust = '<button type="button" class="btn btn-secondary" title="Add picture tags" ' + 
        'onclick="alert(\'Call your custom code here.\')">' +
        '<i class="glyphicon glyphicon-tag"></i>' +
        '</button>'; 
    $("#product_image").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        // defaultPreviewContent: '<img src="/uploads/default_avatar_male.jpg" alt="Your Avatar">',
        layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "gif","jpeg"]
    });
  });
</script>

