

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('auth/logout') ?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
         <div class="row">
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3><?php echo $total_products; ?></h3>

                <p>Total Rent Items</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?php echo base_url('rentitems/') ?>" class="small-box-footer">More info <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3><?php echo $total_inward; ?></h3>

                <p>Total Inward</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?php echo base_url('inward/') ?>" class="small-box-footer">More info <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>

          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3><?php echo $total_outwards;?></h3>

                <p>Total Outwards</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?php echo base_url('outward/') ?>" class="small-box-footer">More info <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          
           <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red" >
              <div class="inner">
                <h3><?php echo $total_suppliers; ?></h3>

                <p>Total Customers</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-people"></i>
              </div>
              <a href="<?php echo base_url('scustomers/') ?>" class="small-box-footer">More info <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
        </div>
        <!-- /.row -->
         <div class="row">
 
          <!-- ./col -->
          
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3><?php echo $total_estimate; ?></h3>

                <p>Total Estimates</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-people"></i>
              </div>
              <a href="<?php echo base_url('estimate/') ?>" class="small-box-footer">More info <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
           <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3><?php echo $total_invoice; ?></h3>

                <p>Total Invoices</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-people"></i>
              </div>
              <a href="<?php echo base_url('invoice/') ?>" class="small-box-footer">More info <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3><?php echo $total_users; ?></h3>

                <p>Total Users</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-people"></i>
              </div>
              <a href="<?php echo base_url('users/') ?>" class="small-box-footer">More info <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3><?php echo $total_stores; ?></h3>

                <p>Total Stores</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-home"></i>
              </div>
              <a href="<?php echo base_url('stores/') ?>" class="small-box-footer">More info <i class="fa fa-chevron-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          
        </div>
        <!-- /.row -->
        <div class="row">
         <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue totdts">
              <div class="inner">
                <h2>Today Cash Flow</h2>
				<br>
				<h4><i class="fa fa-hand-o-down"></i> Outward Advance Received : Rs. <?php echo $a=$total_sales['invadv']; ?></h4> 
                <h4><i class="fa fa-hand-o-down"></i> Inward Amount Received  : Rs. <?php echo $b=$total_sales['oamtpaid'];?></h4> 
                <h4><i class="fa fa-hand-o-down"></i> Est Amount Received  : Rs. <?php echo $c=$total_sales['eamtpaid'];?></h4>  
                <h4><i class="fa fa-hand-o-down"></i> Outward Payment Received  : Rs. <?php echo $e=$total_sales['outpay'];?></h4> 
                <h4><i class="fa fa-hand-o-down"></i> Est Payment Received  : Rs. <?php echo $f=$total_sales['estpay'];?></h4> 
                <br>
                <h4><i class="fa fa-hand-o-up"></i> Est Amount Returned  : Rs. <?php echo $d=$total_sales['ecbal'];?></h4> 
                <br>
                <h4><i class="fa fa-pie-chart"></i> Cash In Hand  :  <?php echo $a+$b+$c+$e+$f-$d; ?></h4>
              </div>
            </div>
          </div>
        </div>
   
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    $(document).ready(function() {
      $("#dashboardMainMenu").addClass('active');
    }); 
  </script>
