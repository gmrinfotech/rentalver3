<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Manage Credit</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><i
					class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Credit</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12 col-xs-12">

				<div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible"
					role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <div class="box">
					<div class="col-sm-6">
						<div class="box-header">
							<h3 class="box-title">Add Credit</h3>
						</div>
					</div>

					<!-- /.box-header -->
					<form role="form" action="<?php base_url('credit/create') ?>"
						method="post" class="form-horizontal">
						<div class="box-body">

                <?php
																
					echo validation_errors ();
				
				?>
							<div class="col-md-12 col-xs-12 pull pull-left">
							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Credit No*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="invoice_no"
											name="invoice_no" placeholder="Enter Invoice No" required
											value="<?php echo $credit_data['crd_no']; ?>"  readonly autocomplete="off">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Inward DC No*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="inward_no"
											name="inward_no" placeholder="Enter Inward No"
											value="<?php echo $credit_data['idc_no']?>" readonly autocomplete="off">
											
										 <!--<select class="form-control select_group"
											id="inward_no" required name="inward_no"
											style="width:100%;" onchange="getCreditInwardDetails()">
												<option value=""></option>
				                            <?php foreach ($inwardDC as $k => $v): ?>
			                              	<option value="<?php echo $v['id'] ?>" <?php if($credit_data['idc_no'] == $v['id']) 
			                              		{echo "selected='selected'";} ?>><?php echo $v['idc_no'] ?></option>
			                            	<?php endforeach ?>
				                          </select>
				                         input type="text" class="form-control" id="total_balanceqty"
											name="total_balanceqty" required
											readonly autocomplete="off"-->
									</div>
								</div>
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Credit Given Date</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="idate"
											name="idate" value="<?php echo $credit_data['sdate']; ?>" required
											autocomplete="off">
									</div>
								</div>
								
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Repayment Date</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="datepicker"
											name="datepicker" autocomplete="off" 
											value="<?php echo $credit_data['paydate']; ?>">
									</div>
								</div>

								<div class="form-group">

									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Member Name*</label>
									<div class="col-sm-7">
										<input type="hidden" class="form-control" id="customer_id"
											name="customer_id" autocomplete="off"> <input type="text"
											class="form-control" id="customer_name" name="customer_name"
											placeholder="Enter Name" required autocomplete="off" 
											value="<?php echo $credit_data['supplier_name']; ?>"/>
										<a href="#" data-toggle="modal" class="supl" data-target="#myModal">Pick
											Supplier</a>
									</div>
								</div>
								<div class="form-group" style="display: none">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Credit To</label>
									<div class="col-sm-7">
										<select class="form-control" id="outto"
											name="outto" value = "1" style="width: 100%;" required onchange="chooseSupl()">
											<option value="1">Supplier</option>
											<option value="2">Sub Supplier</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Address</label>
									<div class="col-sm-7">

										<input type="text" class="form-control" id="customer_address"
											name="customer_address" placeholder="Enter Address"
											autocomplete="off" value="<?php echo $credit_data['supplier_address']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Phone*</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_phone"
											name="customer_phone" pattern="^[0-9]{10,12}$"
											placeholder="Enter Phone" required
											autocomplete="off" value="<?php echo $credit_data['ph_no']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">GST/PAN</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_gst"
											name="customer_gst" placeholder="Enter GST/PAN"
											autocomplete="off" value="<?php echo $credit_data['gst_no']; ?>">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Email</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="customer_email"
											name="customer_email" value="<?php echo $credit_data['email_id']; ?>"
											pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
											placeholder="Enter Email" autocomplete="off">
									</div>
								</div>
								
								<div class="form-group">
									<label for="brands" class="col-sm-5 control-label"
										style="text-align: left;">State</label>
									<div class="col-sm-7">
										<input type="text" id="customer_state" name="customer_state"
											value="Tamil Nadu" class="form-control"
											placeholder="Enter State" value="<?php echo $credit_data['state']; ?>">
										<ul class="dropdown-menu txtstatename"
											style="margin-left: 15px; margin-right: 0px;" role="menu"
											aria-labelledby="dropdownMenu" id="DropdownStateName"></ul>
									</div>
								</div>
								<div class="form-group" style="display: none">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">State Code</label>
									<div class="col-sm-7" >
										<input type="text" class="form-control"
											id="customer_state_code" name="customer_state_code"
											value="<?php echo $credit_data['state']; ?>"
											placeholder="Enter State Code" autocomplete="off">
									</div>
								</div>

							</div>

							<div class="col-md-4 col-xs-12 pull pull-left">
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Credit Amount</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="creditamt"
											name="creditamt" placeholder="Credit Amount"
											autocomplete="off" value="<?php echo $credit_data['creditamt']; ?>"
											onkeyup="calculateCreditAmount()">
									</div>
								</div>

								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Interest %</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="interest"
											name="interest" placeholder="Interest %"
											value="<?php echo $credit_data['interest']; ?>"
											autocomplete="off" onkeyup="calculateCreditAmount()">
									</div>
								</div>
							
								<div class="form-group">
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Total days</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="totaldays"
											name="totaldays" placeholder="Total days" 
											value="<?php echo $credit_data['totaldays']; ?>" readonly required
											autocomplete="off">
									</div>
								</div>
								<div class="form-group">	
									<label for="gross_amount" class="col-sm-5 control-label"
										style="text-align: left;">Interest Amt</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="interestamt"
											name="interestamt" placeholder="Interest Amount"
											value="<?php echo $credit_data['interestamt']; ?>"
											autocomplete="off">
									</div>
								
								</div>
								<div class="form-group">
										<label for="gross_amount" class="col-sm-5 control-label"
											style="text-align: right; font-size: 16px; height: 34px; 
											padding-top: 5px; background-color: #ec8282;">
											Total Amount</label>
										<div class="col-sm-7">
											<input type="text"
												class="form-control" id="total_amount" name="total_amount"
												autocomplete="off" value="<?php echo $credit_data['total_amount']; ?>"/>
										</div>
									</div>
									
									<div class="form-group">
										<label for="gross_amount" class="col-sm-5 control-label">
											Amount Paid</label>
										<div class="col-sm-7">
											<input type="text"
												class="form-control" id="amtpaid" name="amtpaid"
												autocomplete="off" value="<?php echo $credit_data['amtpaid']; ?>" onkeyup=calculateBalance() />
										</div>
									</div>
									
									<div class="form-group">
										<label for="gross_amount" class="col-sm-5 control-label">
											Balance</label>
										<div class="col-sm-7">
											<input type="text"
												class="form-control" id="balance" name="balance"
												autocomplete="off" value="<?php echo $credit_data['balance']; ?>"/>
										</div>
									</div>
								</div>
							</div>
						</div>
				

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Update Credit</button>
					<a href="<?php echo base_url('credit/') ?>" class="btn btn-warning">Back</a>
				</div>
				</form>
					</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- col-md-12 -->
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
<!-- popup form -->
<div id="myModal" class="modal fade" aria-labelledby="myModalLabel"
	aria-hidden="true" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true"></button>
				<h4 class="modal-title">Pick Supplier</h4>
			</div>
			<div class="modal-body" id="myModalBody">
				<table class="table table-bordered">
					<tr>
						<td>Supplier Name</td>
						<td><select class="form-control select_group1" id="supp_name"
							name="supp_name" style="width: 100%;" onchange="getSuplData()"
							required>
								<option value=""></option>
                            <?php foreach ($suppliers as $k => $v): ?>
                              <option
									value="<?php echo $v['supl_id'] ?>"><?php echo $v['supp_name'] ?></option>
                            <?php endforeach ?>
                         </select></td>
					</tr>
					<tr>
						<td>Supplier Address</td>
						<input type="hidden" name="supp_name_hid" id="supp_name_hid"
							readonly class="form-control">
						<input type="hidden" name="supp_hid" id="supp_hid" readonly
							class="form-control">
						<td><input type="text" name="supp_address" id="supp_address"
							readonly class="form-control"></td>
					</tr>
					<tr>
						<td>GST Number/Pan Number</td>
						<td><input type="text" name="gst_no" id="gst_no"
							class="form-control" readonly autocomplete="off"></td>
					</tr>
					<tr>
						<td>Phone Number</td>
						<td><input type="text" name="ph_no" id="ph_no"
							class="form-control" readonly autocomplete="off"></td>
					</tr>
					<tr>
						<td>Email Id</td>
						<td><input type="text" name="email_id" id="email_id"
							class="form-control" readonly autocomplete="off" /></td>
					</tr>
					<tr>
						<td>State</td>
						<td><input type="text" name="state" id="state"
							class="form-control" readonly autocomplete="off" /></td>
					</tr>
					<tr>
						<td>State Code</td>
						<td><input type="text" name="state_code" id="state_code"
							class="form-control" readonly autocomplete="off" /></td>
					</tr>
				</table>

				<div id="alert-msg"></div>
			</div>
			<div class="modal-footer">
				<input class="btn btn-default" id="submit" name="submit"
					data-dismiss="modal" type="button" value="OK"
					onclick="selectSupl()" /> <input class="btn btn-default"
					type="button" data-dismiss="modal" value="Close"
					onclick="clearCus()" />
			</div>
            <?php echo form_close(); ?>            
        </div>
	</div>
</div>




<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {
    $(".select_group").select2();
    $(".select_group1").select2({
      dropdownParent: $("#myModal")
    });
//  	$('#productname').autocomplete({
//		source: "<?php /*echo base_url + 'orders/search/?';*/ ?>",
//     });
    $("#mainCreditNav>a")[0].click();
    $("#mainCreditNav").addClass('active');
    $("#manageCreditNav").addClass('active');

	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
		}).on('change', function(){
			calDays($('#idate').val(), $('#datepicker').val());
	    });

	$('#idate').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
		}).on('change', function(){
			calDays($('#idate').val(), $('#datepicker').val());
	    });

	//Disable button and prevent double submitting
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	});
	
  });

  /*
  $(document).ready(function () {
    $('#example1').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    });

    //Alternativ way
    $('#example2').datepicker({
      format: "dd/mm/yyyy"
    }).on('change', function(){
        $('.datepicker').hide();
    });

	});
  */

  function calDays(startdate, enddate) {
	  if(startdate && enddate) {
	 	 var days = calculateDays(startdate, enddate);
         $("#totaldays").val(days);
         calculateCreditAmount();
	  }
  }

  function calculateBalance() {
		var net =  converttonumber($("#total_amount").val());
		var paid = converttonumber($("#amtpaid").val());
		var bal = (net - paid).toFixed(2);;
		$("#balance").val(bal);
	}

 	function calculateCreditAmount() {
		var net =  converttonumber($("#total_amount").val());
		var paid = converttonumber($("#amtpaid").val());
		var bal = (net - paid).toFixed(2);
		$("#total_amount").val(bal);
	}

 	function calculateCreditAmount(){
 		var principal = converttonumber($("#creditamt").val());
 		var rate = converttonumber($("#interest").val());
 		var days = converttonumber($("#totaldays").val());
 	 	
 		if(principal && rate && days) {
 		
 		var totalmonths = days/30;

 		var final_si=parseFloat((principal*rate*totalmonths)/100);

 		var total = (principal + final_si).toFixed(2);
 		$("#total_amount").val(total);
 		$("#interestamt").val(final_si.toFixed(2));
 		}
 	}

	

</script>