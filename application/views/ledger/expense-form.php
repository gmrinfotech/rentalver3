<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Manage
      Expenditure 
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Expenditure</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>
        <div class="box"> 
     <div class="box-body">
     <div class="row">
    		<div class="col-sm-12">
    			<div class="panel card-view pt-0">
    				<div class="panel-body row pa-5">
						<div class="button-list mt-0">
							<!-- <a href="<?php echo base_url('Expense_form'); ?>" class="btn btn-expen bg-yellow btn-xs mt-0">Daily Expense</a> -->
							<a href="<?php echo base_url('ledger'); ?>" class="btn  btn-expen bg-yellow  btn-xs mt-0">Manage Ledger</a>
							<a href="<?php echo base_url('Expense_form/daywise_report'); ?>" class="btn  btn-expen bg-yellow  btn-xs mt-0">Day wise report</a>
							<!-- <a href="<?php echo base_url('Expense_form/monthwise_report'); ?>" class="btn  btn-expen bg-yellow  btn-xs mt-0">Month wise report</a>-->
						</div>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-sm-12">
    			<div class="panel card-view pt-0">
					<div class="panel-wrapper collapse in">
						<div class="panel-body row pa-0">
							<div class="sm-data-box data-with-border">
								<div class="container-fluid">
									<div class="row">
										<div class="col-xs-3 text-center pl-0 pr-0 data-wrap-left bg-red btl-30">
											<span class="weight-500 uppercase-font txt-light block">Opening Bal</span>
											<span class="txt-light block counter"><i class="fa fa-inr"></i> <span class="counter-anim"><?php echo $opening_balance; ?></span></span>
										</div>
										<div class="col-xs-3 text-center  pl-0 pr-0 data-wrap-right bg-green">
											<span class="weight-500 uppercase-font txt-light  block">Credit</span>
											<span class="txt-light block counter"><i class="fa fa-inr"></i> <span class="counter-anim"><?php echo $credit_amt; ?></span></span>
										</div>
										<div class="col-xs-3 text-center  pl-0 pr-0 data-wrap-right bg-pink">
											<span class="weight-500 uppercase-font txt-light  block">Debit</span>
											<span class="txt-light block counter"><i class="fa fa-inr"></i> <span class="counter-anim"><?php echo $debit_amt; ?></span></span>
										</div>
										<div class="col-xs-3 text-center  pl-0 pr-0 data-wrap-right bg-aqua bbr-30">
											<span class="weight-500 uppercase-font txt-light  block">closing Bal</span>
											<span class="txt-light block counter"><i class="fa fa-inr"></i> <span class="counter-anim"><?php echo $closing_balance; ?></span></span>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
    		</div>
    	</div>
    	<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Expense Entry</h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<?php if(!empty($message)): ?>
								<div class="alert alert-success alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?php echo $message; ?>.</p> 
									<div class="clearfix"></div>
								</div>
							<?php endif; ?>
							<?php if(!empty($error)): ?>
								<div class="alert alert-warning alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?php echo $error; ?>.</p>
									<div class="clearfix"></div>
								</div>
							<?php endif; ?>

							<form action="<?php echo base_url('Expense_form/index/'.$id); ?>" enctype="multipart/form-data" method="post" data-toggle="validator" role="form">
								<div class="form-body">
									<hr class="light-grey-hr">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label mb-10">Date</label>
												 <input type="text" class="form-control default-date-picker" id="expense_date"
												  required name="expense_date" placeholder="Enter Date" 
												  autocomplete="off" value="<?php echo $expense_date; ?>" />
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label mb-10">Ledger</label>
												<select class="form-control select_group product"
												id="ledger" required name="ledger" style="width:100%;" 
												onchange="getLedgerData()">
												   <option value=""></option>
						                            <?php foreach ($ledgers as $k => $v): ?>
						                              <option value="<?php echo $v['id'] ?>" 
						                              <?php if($ledger_id == $v['id']) 
						                              { echo "selected='selected'"; } ?>><?php echo $v['name'] ?></option>
						                            <?php endforeach ?>
						                          </select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label mb-10">Amount</label>
												 <input type="text" class="form-control" id="amount"
												  required name="amount" placeholder="Enter Amount" 
												  autocomplete="off" value="<?php echo $amount; ?>" />
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label mb-10">Type</label>
												<div class="radio-list">
													<div class="radio-inline pl-0">
														<span class="radio radio-info">
															<input type="radio" name="expense_type" id="expense_credit" <?php echo ($expense_type == 1)?'checked="checked"':''; ?> value="1" required>
														<label for="expense_credit">Credit</label>
														</span>
													</div>
													<div class="radio-inline">
														<span class="radio radio-info">
															<input type="radio" name="expense_type" id="expense_debit" <?php echo ($expense_type == 2)?'checked="checked"':''; ?> value="2" required>
														<label for="expense_debit">Debit</label>
														</span>
													</div>
												</div>
											</div>
										</div>										
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label mb-10">Item Description</label>
												   <textarea type="text" class="form-control" id="item_description"
												    name="item_description" placeholder="Enter 
                              						description" autocomplete="off">
                             						 <?php echo $item_description; ?>
                 								 </textarea>
								        </div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label mb-10">&nbsp;</label><br>
												<button type="submit" name="submit" value="submit" class="btn btn-primary  mr-10"> Save</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>	
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Expense List</h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">							
							<div class="table-wrap">
								<div class="table-responsive">
									<table id="json_table" class="table table-hover display  pb-30" >
										<thead>
											<th>Date</th>
											<th>Category</th>
											<th>Description</th>
											<th>Credit (+)</th>
											<th>Debit (-)</th>
											<th>Balance</th>
											<th>Action</th>
										</thead>
										<tbody>
									  		<tr>
												<td></td>
												<td></td>
												<td></td>
												
												<td><b>Opening Balance</b></td>
												<td></td>
												<td><b><?php echo $opening_balance; ?></b></td>
												<td></td>
											</tr>
											<?php if(sizeof($expense_list) > 0): ?>
											<?php $balance = $opening_balance; ?>
												<?php foreach($expense_list as $row): ?>
													<tr>
														<td><?php echo $row->expense_date; ?></td>
														<td><?php echo $row->name; ?></td>
														<td><?php echo $row->item_description; ?></td>
														<?php if($row->expense_type == 1): ?>
															<td><?php echo $row->amount; ?></td>
															<td></td>
															<td><?php echo $balance = $balance + $row->amount; ?></td>
														<?php endif; ?>
														<?php if($row->expense_type == 2): ?>
															<td></td>
															<td><?php echo $row->amount; ?></td>
															<td><?php echo $balance = $balance - $row->amount; ?></td>
														<?php endif; ?>
														<td>
															<a href="<?php echo base_url('Expense_form/index/'.$row->id); ?>" class="btn btn-default editbutton" title="Edit"><i class="fa fa-pencil"></i></a>
															<a href="<?php echo base_url('Expense_form/delete_expense/'.$row->id); ?>" class="btn btn-danger del-confirm" title="Delete"><i class="fa fa-trash-o"></i></a>
														</td>
													</tr>
												<?php endforeach; ?>
											<?php endif; ?>
										</tbody>
										<tfoot>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td><b>Closing Balance</b></td>
												<td></td>
												<td><b><?php echo $closing_balance; ?></b></td>
												<td></td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->		
	</div>
	</div>
	</div>
	</div></div>
	</section>
	    <script type="text/javascript">

	    $(document).ready(function() {
		$("#mainLedgerJournalNav>a")[0].click();
        $("#mainLedgerJournalNav").addClass('active');
        $("#manageExpenditureNav").addClass('active');

    	$(".default-date-picker").datepicker({
    		format: 'dd/mm/yyyy',
    		autoclose: true
		});
	    });

	    function getLedgerData() {

	    	var ledger_id = $("#ledger").val();
	        if (ledger_id) { 
	            $.ajax({
	    			  url: base_url + 'ledger/getLedgerValueById',
	                //url: base_url + 'orders/getProductValueById',
	                type: 'post',
	                data: {
	                    product_id: ledger_id
	                },
	                dataType: 'json',
	                success: function(response) {
	                  //  $("#amount_" + row_id).val(1);
	    			  if(response.expense_type == 1) 
	    			  {
	                   	  $('#expense_credit').prop('checked', true);
	    			  } else {
	    				  $('#expense_debit').prop('checked', true);
	    			  }
	                 
	                }
	            })
	      }
	    }
    </script>
</body>
</html>