<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     <?php if(in_array('createLedger', $user_permission)): ?>
          <button class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add Ledger</button>
        <?php endif; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Ledger </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Manage Ledger</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div id="flip-scrollmanage">
            <table id="manageTable" class="table table-bordered table-striped cf">
              <thead class="cf">
              <tr>
                <th>Ledger Name</th>
                <th>Under</th>
                <th>CR/DR</th>
                <th>Status</th>
                <?php if(in_array('updateLedger', $user_permission) || in_array('deleteLedger', $user_permission)): ?>
                  <th>Action</th>
                <?php endif; ?>
              </tr>
              </thead>

            </table>
          </div></div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- col-md-12 -->
    </div>
    <!-- /.row -->
    

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php if(in_array('createLedger', $user_permission)): ?>
<!-- create brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="addModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Ledger</h4>
      </div>

      <form role="form" action="<?php echo base_url('ledger/create') ?>" method="post" id="createForm">

        <div class="modal-body">

          <div class="form-group">
            <label for="ledger_name">Ledger Name*</label>
            <input type="text" class="form-control" id="ledger_name" name="ledger_name" required placeholder="Enter Ledger name" autocomplete="off">
          </div>
           <div class="form-group">
            <label for="under_category">Under Category*</label>
           <select class="form-control" id="under_category" name="under_category">
              <option value="1">Income</option>
              <option value="2">Expense</option>
              <option value="3">Capital</option> 
               <?php if($pl): ?>
              <option value="4">Cash-in-Hand</option>
              <option value="5">Current Assets</option>
              <option value="6">Fixed Assets</option>
              <option value="7">Secured Loan</option>
              <option value="8">Unsecured Loan</option>
              <option value="9">Bank Accouts</option>
              <option value="10">Duties and Taxs</option>
              <option value="11">Liabilities</option>
              
              <?php endif ?>
            </select>
           
          </div>
           <div class="form-group">
            <label for="expense_type">CR/DR*</label>
           <select class="form-control" id="expense_type" name="expense_type">
              <option value="1">CR</option>
              <option value="2">DR</option>
           </select>
           
          </div>
          <div class="form-group">
            <label for="active">Status</label>
            <select class="form-control" id="active" name="active">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
         
          <button type="submit" class="btn btn-primary">Save changes</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>


<?php if(in_array('updateLedger', $user_permission)): ?>
<!-- edit brand modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Ledger </h4>
      </div>
		
      <form role="form" action="<?php echo base_url('ledger/update') ?>" method="post" id="updateForm">
		
        <div class="modal-body">
          <div id="messages"></div>
					
          <div class="form-group">
        	<label for="name">Ledger Name*</label>
	        <div class="input-group margin-bottom-10">
	        	  <input type="hidden" id="pneditstatus" name="pneditstatus" value="false"></input>
	        	  <input type="hidden" id="pnoldname" name="pnoldname"></input>
	              <input type="text" class="form-control editField" readonly id="edit_ledger_name" 
	              name="edit_ledger_name" placeholder="Enter Ledger name" required 
	              autocomplete="off"/>
	                <div class="input-group-addon">
	                  <span class="glyphicon glyphicon-pencil pneditMode pneditspan" id="pneditspan"></span>
	                  <span class="glyphicon glyphicon-ok hide pnnonEditMode pnokspan" id="pnokspan"></span>
	                </div>         
	       	 </div>
	        </div>
	        
        	<div class="form-group">
            <label for="edit_under_ledger">Under Category*</label>
             <select class="form-control" id="edit_under_ledger" name="edit_under_ledger">
                 <option value="1">Income</option>
              <option value="2">Expense</option>
              <option value="3">Capital</option> 
               <?php if($pl): ?>
              <option value="4">Cash-in-Hand</option>
              <option value="5">Current Assets</option>
              <option value="6">Fixed Assets</option>
              <option value="7">Secured Loan</option>
              <option value="8">Unsecured Loan</option>
              <option value="9">Bank Accouts</option>
              <option value="10">Duties and Taxs</option>
              <option value="11">Liabilities</option>
              
              <?php endif ?>
            </select>
          </div>
           <div class="form-group">
            <label for="edit_expense_type">CR/DR*</label>
           <select class="form-control" id="edit_expense_type" name="edit_expense_type">
              <option value="1">CR</option>
              <option value="2">DR</option>
           </select>
          </div>
                    <div class="form-group">
            <label for="edit_active">Status</label>
            <select class="form-control" id="edit_active" name="edit_active">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >Update</button>
	  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         
        </div>

      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>

<?php if(in_array('deleteLedger', $user_permission)): ?>
<!-- remove brand modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Remove Ledger</h4>
      </div>

      <form role="form" action="<?php echo base_url('ledger/remove') ?>" method="post" id="removeForm">
        <div class="modal-body">
          <p>Do you really want to remove?</p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Delete</button>
	  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         
        </div>
      </form>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>


<script type="text/javascript">


 	$('.pneditspan').click( function() {
           $(this).parents().siblings('input').prop('readonly', false);
           $('.pneditMode').addClass('hide');
           $('.pnnonEditMode').toggleClass('hide');
    });
	
	 $('.pnokspan').click( function() {
           $(this).parents().siblings('input').prop('readonly', true);
           $('.pneditMode').removeClass('hide');
           $('.pnnonEditMode').toggleClass('hide');
           var pnoldtext = $('#pnoldname').val();
           var pnnewtext = $('#edit_ledger_name').val();
           if(pnoldtext != pnnewtext) {  
          		$('#pneditstatus').val("true");
           } else {
        	   $('#pneditstatus').val("false");
           } 
    	});
var manageTable;

$(document).ready(function() {
    $("#mainLedgerJournalNav>a")[0].click();
    $("#mainLedgerJournalNav").addClass('active menu-open');
    $("#manageLedgerNav").addClass('active');

    //Disable button and prevent double submitting as in ksk
    $('form').submit(function () {
    	$(this).find(':submit').attr('disabled', 'disabled');
    });
    
  // initialize the datatable 
  manageTable = $('#manageTable').DataTable({
    'ajax': 'ledger/fetchLedgerData',
    'order': []
  });

  // submit the create from 
  $("#createForm").unbind('submit').on('submit', function() {
    var form = $(this);

    // remove the text-danger
    $(".text-danger").remove();

    $.ajax({
      url: form.attr('action'),
      type: form.attr('method'),
      data: form.serialize(), // /converting the form data into array and sending it to server
      dataType: 'json',
      success:function(response) {

        manageTable.ajax.reload(null, false); 

        if(response.success === true) {
          $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
            '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
          '</div>');


          // hide the modal
          $("#addModal").modal('hide');

          // reset the form
          $("#createForm")[0].reset();
          $("#createForm .form-group").removeClass('has-error').removeClass('has-success');

        } else {

          if(response.messages instanceof Object) {
            $.each(response.messages, function(index, value) {
              var id = $("#"+index);

              id.closest('.form-group')
              .removeClass('has-error')
              .removeClass('has-success')
              .addClass(value.length > 0 ? 'has-error' : 'has-success');
              
              id.after(value);

            });
          } else {
            $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
            '</div>');
          }
        }
      }
    }); 

    return false;
  });

});

// edit function
function editFunc(id)
{ 
  $('#pneditstatus').val("false");
  $.ajax({
    url: 'ledger/fetchLedgerDataById/'+id,
    type: 'post',
    dataType: 'json',
    success:function(response) {

      $("#edit_ledger_name").val(response.name);
	  $("#pnoldname").val(response.name);
	  $("#edit_under_ledger").val(response.under);
	  $("#edit_expense_type").val(response.expense_type);
      $("#edit_active").val(response.active);

      // submit the edit from 
      $("#updateForm").unbind('submit').bind('submit', function() {
        var form = $(this);

        // remove the text-danger
        $(".text-danger").remove();

        $.ajax({
          url: form.attr('action') + '/' + id,
          type: form.attr('method'),
          data: form.serialize(), // /converting the form data into array and sending it to server
          dataType: 'json',
          success:function(response) {

            manageTable.ajax.reload(null, false); 

            if(response.success === true) {
              $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
              '</div>');


              // hide the modal
              $("#editModal").modal('hide');
              // reset the form 
              $("#updateForm .form-group").removeClass('has-error').removeClass('has-success');

            } else {

              if(response.messages instanceof Object) {
                $.each(response.messages, function(index, value) {
                  var id = $("#"+index);

                  id.closest('.form-group')
                  .removeClass('has-error')
                  .removeClass('has-success')
                  .addClass(value.length > 0 ? 'has-error' : 'has-success');
                  
                  id.after(value);

                });
              } else {
                $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                  '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
                '</div>');
              }
            }
          }
        }); 

        return false;
      });

    }
  });
}

// remove functions 
function removeFunc(id)
{
  if(id) {
    $("#removeForm").on('submit', function() {

      var form = $(this);

      // remove the text-danger
      $(".text-danger").remove();

      $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: { category_id:id }, 
        dataType: 'json',
        success:function(response) {

          manageTable.ajax.reload(null, false); 

          if(response.success === true) {
            $("#messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-ok-sign"></span> </strong>'+response.messages+
            '</div>');

            // hide the modal
            $("#removeModal").modal('hide');

          } else {

            $("#messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              '<strong> <span class="glyphicon glyphicon-exclamation-sign"></span> </strong>'+response.messages+
            '</div>'); 
          }
        }
      }); 

      return false;
    });
  }
}
</script>
