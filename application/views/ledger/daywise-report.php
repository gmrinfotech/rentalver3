<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Day wise Report
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active">Day wise Report</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12 col-xs-12">

        <div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
		<?php endif; ?>
		<?php
		$from_date   = array('name'=>'from_date', 'id'=>'from_date', 'value' => set_value('from_date', $from_date), 'class'=>'form-control default-date-picker', 'required' => '');
		$to_date   = array('name'=>'to_date', 'id'=>'to_date', 'value' => set_value('to_date', $to_date), 'class'=>'form-control default-date-picker', 'required' => '');
		?>
			 <div>
          <form class="form-inline" action="<?php echo base_url('expense_form/daywise_report') ?>" method="POST">
            <div class="form-group">
           <label>Date :</label>
			<?php echo form_input($from_date); ?>
           <label>To :</label>
		 	<?php echo form_input($to_date); ?> 
            </div>  
            <button btn btn-primary repSubBtntype="submit" class="btn btn-primary repSubBtn">Submit</button>
          </form>
        </div>
  
      <br>
	 
				<div class="box">
					<div class="box-body">				
						 
							 	<div id="flip-scroll">
									<table id="manageTable" class="table table-hover display  pb-30" >
										<thead>
											<th>Date</th>
											<th>Category</th>
											<th>Description</th>
											<th>Credit (+)</th>
											<th>Debit (-)</th>
											<th>Balance</th>
										</thead>
										<tbody>
											<tr>
												<td><?php echo $fdate.' - '.$tdate; ?></td>
												<td><b>Outward Cash Received</b></td>
												<td></td> 
												<td><b><?php echo $a=$total_csales['invadv']; ?></b></td>
												<td></td>
												<td></td> 
											</tr>
											<tr>
												<td><?php echo $fdate.' - '.$tdate; ?></td>
												<td><b>Outward Digital Payments Received</b></td>
												<td></td> 
												<td><b><?php echo $b=$total_sales['invadv']; ?></b></td>
												<td></td>
												<td></td> 
											</tr>
											<tr>
												<td></td>
												<td><b>Inward Cash Received </b></td>
												<td></td>  
												<td><b><?php echo $c=$total_csales['oamtpaid']; ?></b></td>
												<td></td>
												<td></td> 
											</tr>
											<tr>
												<td></td>
												<td><b>Inward Digital Payments Received </b></td>
												<td></td>  
												<td><b><?php echo $d=$total_sales['oamtpaid']; ?></b></td>
												<td></td>
												<td></td> 
											</tr>
											<tr>
												<td></td>
												<td><b>Est Cash Received</b></td>
												<td></td>  
												<td><b><?php echo $e=$total_csales['eamtpaid']; ?></b></td>
												<td></td>
												<td></td> 
											</tr>
											<tr>
												<td></td>
												<td><b>Est Digital Payments Received</b></td>
												<td></td>  
												<td><b><?php echo $f=$total_sales['eamtpaid']; ?></b></td>
												<td></td>
												<td></td> 
											</tr> 
											<tr>
												<td></td>
												<td><b> Est Amount Returned </b></td>
												<td></td> 
												<td></td> 
												<td><b><?php echo $d=$total_sales['ecbal']; ?></b></td>
												<td></td> 
											</tr> 
											<tr>
												<td></td>
												<td><b>Outward Payments Received By Cash</b></td>
												<td></td> 
												<td><b><?php echo $g=$total_csales['outpay']; ?></b></td>
												<td></td>
												<td></td> 
											</tr>
											<tr>
												<td></td>
												<td><b>Outward Payments Received By Digital</b></td>
												<td></td> 
												<td><b><?php echo $i=$total_sales['outpay']; ?></b></td>
												<td></td>
												<td></td> 
											</tr>
											<tr>
												<td></td>
												<td><b>Estimate Payments Received By Cash</b></td>
												<td></td> 
												<td><b><?php echo $h=$total_csales['estpay']; ?></b></td>
												<td></td>
												<td></td> 
											</tr>
											<tr>
												<td></td>
												<td><b>Estimate Payments Received By Digital</b></td>
												<td></td> 
												<td><b><?php echo $j=$total_sales['estpay']; ?></b></td>
												<td></td>
												<td></td> 
											</tr>
											<?php
											$tot_credit_amt = 0;
											$tot_debit_amt = 0;
											$tot_closing_balance = 0;
											?>
											<?php if(sizeof($report_data) > 0): ?>
												<?php foreach($report_data as $row_date => $expense_list): ?>
													<?php if(count($expense_list) > 0): ?>
														<?php foreach($expense_list as $key => $row): ?>
															<?php
															if($row['expense_type'] == 1)
															{
																$tot_credit_amt += $row['amount'];
															}
															if($row['expense_type'] == 2)
															{
																$tot_debit_amt += $row['amount'];
															}
															?>
															<tr>
																<td>
																	<?php echo ($key == 0)? $row_date:'' ?>
																</td>
																<td><?php echo $row['category_name']; ?></td>
																<td><?php echo $row['item_description']; ?></td>
																<?php if($row['expense_type'] == 1): ?>
																	<td><?php echo $row['amount']; ?></td>
																	<td></td>
																<?php endif; ?>
																<?php if($row['expense_type'] == 2): ?>
																	<td></td>
																	<td><?php echo $row['amount']; ?></td>
																<?php endif; ?>
																<td></td>
															</tr>
														<?php endforeach; ?>
													<?php endif; ?>
												<?php endforeach; ?>
											<?php endif; ?>

											<?php
											$tot_credit_amt += $a + $b + $c + $d + $e + $f + $g + $h + $i + $j;
											$tot_debit_amt += $d;
											$tot_closing_balance = $tot_credit_amt - $tot_debit_amt;
											?>
											<tr>
												<td></td>
												<td></td>
												<td><strong>Balance</strong></td>
												<td><strong><?php echo number_format($tot_credit_amt, 2, '.', ''); ?></strong></td>
												<td><strong><?php echo number_format($tot_debit_amt, 2, '.', ''); ?></strong></td>
												<td><strong><?php echo number_format($tot_closing_balance, 2, '.', ''); ?></strong></td>
											</tr>
										</tbody>
									</table>
								 </div> 
					</div>
				</div>	
			 
		<!-- /Row -->
		</div>
	 </div>		
	</div>
 
    <script type="text/javascript">

    var manageTable;
    
    $(document).ready(function() {
	$("#mainLedgerJournalNav>a")[0].click();
    $("#mainLedgerJournalNav").addClass('active');
    $("#managedaywiseNav").addClass('active');
 
    $(".default-date-picker").datepicker({
		  	// minView: 2,
    		format: 'dd/mm/yyyy',
    		autoclose: true
		});

    manageTable = $('#manageTable').DataTable({
 	   dom: 'Bfrtip',
 	   "pageLength": 20,
 	    buttons: [
 			'copy', 'csv', 'excel', 'pdf', 'print'
  		],
         'order': []
  
   });
    });
    </script>
 