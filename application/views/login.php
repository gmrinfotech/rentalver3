<?php 
$mysqli = new mysqli("localhost","root","admin","rental");
// Check connection
if ($mysqli -> connect_errno) {
  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
  exit();
}
$sql="SELECT company_name,loginimage,expiredate FROM company";
if ($result=mysqli_query($mysqli,$sql))
  {
  // Seek to row number 15
  mysqli_data_seek($result,0);

  // Fetch row
  $row=mysqli_fetch_row($result);
  $companyname=$row[0];
  $companyimage=$row[1];
  $expiredate=$row[2]; // To be in dd/mm/yyyy Format
  
   $date = date('d/m/Y');  
   $disp = false;
 
$newTime1 = strtotime(trim(str_replace('/', '-', str_replace('-', '', $date))));
$newTime2 = strtotime(trim(str_replace('/', '-', str_replace('-', '', $expiredate))));

$diff = abs($newTime2 - $newTime1);  
$days = floor($diff / (60*60*24)); 

  	 $expired = false;
            if($newTime1 > $newTime2) {
            	$expired = true;
            } else if($days <= 30) {
            	$disp = true;  
            } 
 
     
}
mysqli_close($mysqli); 
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  
  
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/Ionicons/css/ionicons.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css') ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/square/blue.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/custom.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/codebase.min.css') ?>">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
    <body>
        <div id="page-container" class="main-content-boxed">
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="bg-image"  style="background-image: url('assets/images/<?php echo $companyimage?>.jpg');">
                    <div class="row mx-0 bg-black-op">
                        <div class="hero-static col-md-6 col-xl-8 d-none d-md-flex align-items-md-end">
                            <div class="p-30 invisible" data-toggle="appear">
                                <p class="font-size-h3 font-w600 text-white">
                                    
                                </p>
                                <p class="font-italic text-white-op"><br>
                                    GMR Infotech<br>
                                    Support : 9944558110 <br>
                                     Bpartner Copyright &copy; <span class="js-year-copy">2020</span><br>
                                     Ticket : https://support.gmrinfotech.in
                                   
                                </p>
                            </div>
                        </div>
                        <div class="hero-static col-md-6 col-xl-4 d-flex align-items-center bg-white invisible" data-toggle="appear" data-class="animated fadeInRight">
                            <div class="content content-full">
                                <!-- Header -->
                                <p align="center"> <img src="<?php echo base_url('assets/images/logo.JPG') ?>"></p>
                                <div class="px-05 py-01">
                                  
                                    <h1 class="h3 font-w700 mt-30 mb-10">Welcome 
                                      <a class="link-effect font-w700" href="#">
                                       <!-- <i class="si si-fire"></i>-->
                                        <span class="font-size-xl"><?php echo $companyname ?></span>
                                    </a></h1>
                                    <h2 class="h5 font-w400 text-muted mb-0">Please sign in</h2>
                                </div>
                               
                               <form action="<?php echo base_url('auth/login')?>" method="post">
                                 <br/>
							       <div class="form-group has-feedback">
							        <?php echo validation_errors(); ?> 
							        <?php if(!empty($errors)) {?>
							    		 <span style="color: Red;"><?php echo $errors ?></span>
							    	<?php }?>
							      </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input type="email" class="form-control" name="email" id="email" autocomplete="off"> 
                                                <label for="login-username">Username</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input type="password" class="form-control" name="password" id="password" autocomplete="off">
                                                <label for="login-password">Password</label>
                                            </div>
                                        </div>
                                    </div>
                                  	<?php if($expired != true): ?>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-sm btn-hero btn-alt-primary" name="login">
                                            <i class="si si-login mr-10"></i> Sign In
                                        </button>  
                                    </div>
                                     <?php else:?>
                                    	 <label><span style="color: Red;"><b>Your License expired</b></span></label>
                                         <br/><label><span style="color: Red;"><b>Please Contact : <?php echo "9944558110 for renewal" ?></b></span></label>
                                   <?php endif?>
                                   <?php if($disp == true): ?>
                                      <label><span style="color: Red;"><b>Your License will be expired within : <?php echo $days ?> days</b></span></label>
                                      <br/><label><span style="color: #f3951f;"><b>Expiry Date : <?php echo $expiredate ?></b></span></label>
                                      <br/><label><b>Please Contact : <?php echo "9944558110 for renewal" ?></b></label>
                                    <?php endif?>
                                </form>
                                   <div class="px-30 py-10"> 
                                    <h2 class="h5 font-w700  mt-30 mb-10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                      <a class="link-effect font-w700" href="login.php">
                                       <!-- <i class="si si-fire"></i>-->
                                        <span class="font-size-xl"><a href="https://gmrinfotech.in/products.html" target="_blank">www.gmrinfotech.in</a></span>
                                         
                                    	</a>
                                       </h2>
                                  
                                    
                                </div>
                                <!-- END Sign In Form -->
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- END Page Content -->
              
            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
     	<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script>
		<!-- Bootstrap 3.3.7 -->
		<script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/core/popper.min.js')?>"></script>   
        <script src="<?php echo base_url('assets/js/core/jquery.appear.min.js')?>"></script> 
        <script src="<?php echo base_url('assets/js/core/jquery.scrollLock.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/codebase.js')?>"></script>

        <!-- Page JS Plugins -->
        <script src="<?php echo base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>

        <!-- Page JS Code -->
        <script src="<?php echo base_url('assets/js/pages/op_auth_signin.js')?>"></script>
    </body>
</html>
