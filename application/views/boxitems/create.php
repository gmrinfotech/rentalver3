<?php
	$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
		$pcodenew = $company_Prefix['bcode'];
		$bcode = $pcodenew;
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
      <h1>
         Manage
         Box Item 
      </h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><i
					class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Boxitems</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible"
					role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        		<div class="box">
					<div class="col-sm-12">
						<div class="box-header">
                  			<h3 class="box-title">Add Box Item</h3>
						</div>
					</div> 
					<!-- /.box-header -->
					<form role="form" action="<?php base_url('boxitems/create') ?>"
						method="post" class="form-horizontal">
						<div class="box-body"> 
   						  <div class="col-sm-12">
							 <div class="box-header" style="float: left;color:red;font-weight:bold"> 
								<?php echo validation_errors ();?>
								</div>
						 </div>
					  
					 	 <div class="col-md-4 col-xs-12 pull pull-left">
							 <div class="form-group">
		                           <label class="col-sm-5 control-label" style="text-align: left;">Box Item code*</label>
		                           <div class="col-sm-7">
			                   <!--  <div class="input-group margin-bottom-10"> --> 
				                 	<input type="text" class="form-control" id="boxitem_code" name="boxitem_code" 
				                 	value="<?php echo $bcode;?>" 
				                       placeholder="Enter product code" required readonly autocomplete="off" 
				                       onkeypress="handleKeyPress(event)"/> 
				                   
									<input type="hidden" class="form-control" id="boxitem_code_hidden" 
				                         	name="boxitem_code_hidden" value="<?php echo $bcode;?>" 
				                       placeholder="Enter product code" /> 
				                       <!-- <div class="input-group-addon">
				                           <input id="manualcode" name="manualcode" value="1" type="checkbox" title="Click to enter code manually"/> 
					               	  </div>-->          
				               <!--  </div>--> 
				                </div>
					    	 </div> 
					 </div>
							 
					 <div class="col-md-4 col-xs-12 pull pull-left"> 
	                        <div class="form-group">
	                           <label class="col-sm-5 control-label" style="text-align: left;">Box Item name*</label>
	                           <div class="col-sm-7">
	                           <input type="text" class="form-control" id="boxitem_name" name="boxitem_name"
	                            placeholder="Enter box item name" required autocomplete="off"/>
	                            </div>
	                        </div>  
					 </div>
 					  
 					   <div class="col-md-4 col-xs-12 pull pull-left"> 
	                        <div class="form-group">
	                           <label class="col-sm-5 control-label" style="text-align: left;">Min Rental Days*</label>
	                           <div class="col-sm-7">
	                           <input type="text" class="form-control" id="minrentaldays" name="minrentaldays" onkeyup="calculateRatePerDay()" 
	                            placeholder="Enter Min Rental Days" required autocomplete="off"/>
	                            </div>
	                        </div>  
					 </div>
					 
					  <div class="col-md-4 col-xs-12 pull pull-left"> 
	                        <div class="form-group">
	                           <label class="col-sm-5 control-label" style="text-align: left;">Main Box Rate*</label>
	                           <div class="col-sm-7">
	                           <input type="text" class="form-control" id="rate" name="rate" onkeyup="calculateRatePerDay()" 
	                            placeholder="Enter Main Box Rate" required autocomplete="off"/>
	                            </div>
	                        </div>  
					 </div>
					  
					  <div class="col-md-4 col-xs-12 pull pull-left"> 
	                        <div class="form-group">
	                           <label class="col-sm-5 control-label" style="text-align: left;">Box Rate/Day*</label>
	                           <div class="col-sm-7">
	                           <input type="text" class="form-control" id="rateperday" name="rateperday"
	                            placeholder="Box Rate/Day" required readonly autocomplete="off"/>
	                            </div>
	                        </div>  
					 </div>
				   <div id="no-more-tables">
							<h2>&nbsp</h2>
							<table class="table table-bordered" id="product_info_table_gst">
								<thead>
									<tr>
										<th style="width: 20%">Item Code</th>
										<th style="width: 40%">Item Name</th>
										<th style="width: 10%">Qty</th>
										<th style="width: 20%">Type</th> 
										<th style="width: 10%">Remove</th>
									</tr>
								</thead>

								<tbody>
									<tr id="row_1">
										<td data-title="Product Name"> 
											<select class="form-control select_group product"
												data-row-id="row_1" id="rentitem_code_1" required name="rentitem_code[]"
												style="width: 100%;" onchange="getCommonProductCode(1)">
													<option value=""></option>
												    <?php foreach ($products as $k => $v): ?>
						                              <option value="<?php echo $v['id'] ?>"><?php echo $v['code'] ?></option>
						                            <?php endforeach ?>
					                          </select> 
										</td>
										<td data-title="Name"> 
											 <select class="form-control select_group product"
												data-row-id="row_1" id="rentitem_name_1" required name="rentitem_name[]"
												style="width: 100%;" onchange="getCommonProductData(1)">
													<option value=""></option>
												    <?php foreach ($products as $k => $v): ?>
						                              <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
						                            <?php endforeach ?>
					                          </select> 
					                          <input type="hidden" name="rname[]" id="rname_1"
											class="form-control" autocomplete="off"> 
										</td>
										<td data-title="Qty">
											<input type="text" name="qty[]" id="qty_1"
											class="form-control" autocomplete="off" value="1"> 
										</td>
										<td data-title="Type">
										<select class="form-control" id="type_1" name="type[]"
												style="width: 100%;" required>
												<option value="1">Main</option>
												<option value="2">Side</option> 
											</select>
										</td>
										 
										<td data-title="Remove">
										<button type="button" class="btn btn-default"
												onclick="removeRow('1')">
											<i class="fa fa-close"></i>
										</button></td>
									</tr>
								</tbody>
							</table>
							</div>
							<br />
							<div>
								&nbsp;
								<button type="button" id="add_row" disabled class="btn btn-default">
									<i class="fa fa-plus"></i>&nbsp; Add New Row
								</button>
							</div> 
						</div> 
				<!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Save</button>
					<a href="<?php echo base_url('boxitems/') ?>" class="btn btn-warning">Back</a>
				</div>
				</form>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- col-md-12 --> 
	</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
 
<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {
	$("#mainBoxitemsNav>a")[0].click();
    $("#mainBoxitemsNav").addClass('active');
    $("#addBoxitemsNav").addClass('active');

    $('#manualcode').change(function() {
        var curVal = $("#boxitem_code_hidden").val();
    	if(this.checked) {
    		$("#boxitem_code").removeAttr("readOnly");
    		$("#boxitem_code").val("");
    	} else {
    		$("#boxitem_code").attr('readOnly','readOnly');
    		$("#boxitem_code").val(curVal);
    	}    
    });
    
    // Add new row in the table 
    $("#add_row").unbind('click').bind('click', function() {
      addboxitemrow();
   });
  });
  //Gst Operations
  // get the product information from the server
  function getCommonProductCode(row_id)
  {
	   getRentItemDataName(row_id);
	   var btn = $("#add_row");
	   btn.removeAttr("disabled");
  }

  function getCommonProductData(row_id)
  {
	   getRentItemDataCode(row_id);
	   var btn = $("#add_row");
	   btn.removeAttr("disabled");
  }

</script>