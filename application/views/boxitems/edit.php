<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
      <h1>
         Manage
         Box Item 
      </h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard') ?>"><i
					class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Boxitems</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12 col-xs-12">

				<div id="messages"></div>

        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-success alert-dismissible"
					role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php elseif($this->session->flashdata('error')): ?>
          <div class="alert alert-error alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php endif; ?>

        <div class="box">
					<div class="col-sm-12">
						<div class="box-header">
                  			<h3 class="box-title">Edit Box Item</h3>
						</div>
					</div>

					<!-- /.box-header -->
					<form role="form" action="<?php base_url('boxitems/create') ?>"
						method="post" class="form-horizontal">
						<div class="box-body"> 
   						  <div class="col-sm-12">
								<div class="box-header" style="float: left;color:red;font-weight:bold"> 
									<?php echo validation_errors ();?>
								</div>
							</div>
				 
					  <div class="col-md-4 col-xs-12 pull pull-left">
							 <div class="form-group">
		                           <label class="col-sm-5 control-label" style="text-align: left;">Box Item code*</label>
		                           <div class="col-sm-7">
			                  <!--  <div class="input-group margin-bottom-10"> --> 
				                 	<input type="text" class="form-control" id="boxitem_code" name="boxitem_code" pattern=".{4,}"  
				                 		value="<?php echo $box_data['box']['code'] ?>"
				                      	placeholder="Enter product code" required readonly autocomplete="off" onkeypress="handleKeyPress(event)"/> 
				                         	<input type="hidden" class="form-control" id="boxitem_code_hidden" name="boxitem_code_hidden" 
				                         	pattern=".{4,}"  value="<?php echo $box_data['box']['code'] ?>"
				                       		placeholder="Enter product code" required readonly autocomplete="off" onkeypress="handleKeyPress(event)"/> 
				                     <!--   <div class="input-group-addon">
				                           <input id="manualcode" name="manualcode" value="1" type="checkbox" title="Click to enter code manually"/> 
					               	  </div>             
				                </div> --> 
				                </div>
					    	 </div> 
					 </div>
							 
					 <div class="col-md-4 col-xs-12 pull pull-left"> 
	                        <div class="form-group">
	                           <label class="col-sm-5 control-label" style="text-align: left;">Box Item name*</label>
	                           <div class="col-sm-7"> 
	                             	<div class="input-group margin-bottom-10">
						        	  <input type="hidden" id="pneditstatus" name="pneditstatus" value="false"></input>
						        	  <input type="hidden" id="pnoldname" name="pnoldname" value="<?php echo $box_data['box']['name']; ?>"></input>
						              <input type="text" class="form-control editField" readonly id="boxitem_name" 
						              name="boxitem_name" placeholder="Enter box item name" required 
						              value="<?php echo html_escape($box_data['box']['name']); ?>"  autocomplete="off"/>
						                <div class="input-group-addon">
						                  <span class="glyphicon glyphicon-pencil pneditMode pneditspan" id="pneditspan"></span>
						                  <span class="glyphicon glyphicon-ok hide pnnonEditMode pnokspan" id="pnokspan"></span>
						                </div>         
						        	</div> 
						      </div>
						    </div>    
						 </div>   	   
						<div class="col-md-4 col-xs-12 pull pull-left"> 
	                        <div class="form-group">
	                           <label class="col-sm-5 control-label" style="text-align: left;">Min Rental Days*</label>
	                           <div class="col-sm-7">
	                           <input type="text" class="form-control" id="minrentaldays" name="minrentaldays" onkeyup="calculateRatePerDay()" 
	                            placeholder="Enter Min Rental Days" 
	                             value="<?php echo $box_data['box']['minrentaldays']; ?>"  required autocomplete="off"/>
	                            </div>
	                        </div>  
	                      </div>  
					  <div class="col-md-4 col-xs-12 pull pull-left"> 
	                        <div class="form-group">
	                           <label class="col-sm-5 control-label" style="text-align: left;">Box Rate*</label>
	                           <div class="col-sm-7">
	                           <input type="text" class="form-control" id="rate" name="rate" value="<?php echo $box_data['box']['rate']; ?>" 
	                            placeholder="Enter Box Rate" required onkeyup="calculateRatePerDay()" autocomplete="off"/>
	                            </div>
	                        </div>  
					 </div>
 				 
					  <div class="col-md-4 col-xs-12 pull pull-left"> 
	                        <div class="form-group">
	                           <label class="col-sm-5 control-label" style="text-align: left;">Box Rate/Day*</label>
	                           <div class="col-sm-7">
	                           <input type="text" class="form-control" id="rateperday" name="rateperday"
	                            placeholder="Box Rate/Day" value="<?php echo $box_data['box']['rateperday']; ?>" required autocomplete="off"/>
	                            </div>
	                        </div>  
					 </div>
				   <div id="no-more-tables">
							<h2>&nbsp</h2>
							<table class="table table-bordered" id="product_info_table_gst">
								<thead>
									<tr>
										<th style="width: 20%">Item Code</th>
										<th style="width: 40%">Item Name</th>
										<th style="width: 10%">Qty</th>
										<th style="width: 20%">Type</th> 
										<th style="width: 10%">Remove</th>
									</tr>
								</thead>

								<tbody>
								  <?php if(isset($box_data['box_item'])): ?>
			                      <?php $x = 1; ?>
			                      <?php foreach ($box_data['box_item'] as $key => $val): ?> 
                       				<tr id="row_<?php echo $x; ?>">
										<td data-title="Product Code">
					                        <select class="form-control select_group product" data-row-id="row_<?php echo $x; ?>" 
					                         id="rentitem_code_<?php echo $x; ?>" required name="rentitem_code[]" style="width:100%;"
					                         onchange="getCommonProductCode(<?php echo $x; ?>)">
					                        	<option value=""></option>
					                            <?php foreach ($products as $k => $v): ?>
					                          	  <?php if($v['code']): ?>
					                              	<option value="<?php echo $v['id'] ?>"
					                              	 <?php if($val['rentitem_id'] == $v['id'])
					                              	  { echo "selected='selected'"; } ?>><?php echo $v['code'] ?></option>
					                           	   <?php endif; ?>
					                             <?php endforeach ?>
					                          </select> 
                       				 	</td>
										<td data-title="Name"> 
											 <select class="form-control select_group product"
												data-row-id="row_<?php echo $x; ?>" id="rentitem_name_<?php echo $x; ?>" required name="rentitem_name[]"
												style="width: 100%;" onchange="getCommonProductData(<?php echo $x; ?>)">
													<option value=""></option>
						                            <?php foreach ($products as $k => $v): ?>
						                          	  <?php if($v['name']): ?>
						                              	<option value="<?php echo $v['id'] ?>"
						                              	 <?php if($val['rentitem_id'] == $v['id'])
						                              	  { echo "selected='selected'"; } ?>><?php echo $v['name'] ?></option>
						                           	   <?php endif; ?>
						                            <?php endforeach ?>
						                     </select> 
						                      <input type="hidden" name="rname[]" id="rname_<?php echo $x; ?>"
											class="form-control" autocomplete="off" value="<?php echo $val['rentitem_name'] ?>"> 
										</td>
										<td data-title="Qty"> 
											<input type="text" name="qty[]" id="qty_<?php echo $x; ?>"
											class="form-control" autocomplete="off" value="<?php echo $val['qty'] ?>" >
										</td>
										<td data-title="Type"> 
											<select class="form-control" data-row-id="row_<?php echo $x; ?>"
											id="type_<?php echo $x; ?>" name="type[]" style="width: 100%;"
											required>
												<?php if ($val['type'] == 1) {?>
						                     	<option value="<?php echo $val['type'] ?>">Main</option>
												<option value="2">Side</option>  
					                   		<?php } else if($val['type'] == 2) { ?>
												<option value="<?php echo $val['type'] ?>">Side</option>
						                      	<option value="1">Main</option>  
					                   		<?php } ?> 
										</select></td>
										</td>
									 
										<td data-title="Remove">
										<button type="button" class="btn btn-default"
												onclick="removeRow('<?php echo $x; ?>')">
												<i class="fa fa-close"></i>
											</button></td>
									</tr>
									<?php $x++; ?>
                    			<?php endforeach; ?>
                  				<?php endif; ?>
								</tbody>
							</table>
							</div>
							<br />
							<div>
								&nbsp;
								<button type="button" id="add_row" class="btn btn-default">
									<i class="fa fa-plus"></i>&nbsp; Add New Row
								</button>
							</div> 
					</div>
				<!-- /.box-body -->

				<div class="box-footer"> 
					<button type="submit" class="btn btn-primary">Update Box item</button>
					<a href="<?php echo base_url('boxitems/') ?>" class="btn btn-warning">Back</a>
				</div>
				</form>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
 

</div>
<!-- /.row -->


</section>
<!-- /.content -->
</div>
 


<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";

  $(document).ready(function() {
    $(".select_group").select2();
    $(".select_group1").select2({
      dropdownParent: $("#myModal")
    });
    $(".select_group2").select2({
        dropdownParent: $("#myModal1")
    });
    $("#mainBoxitemsNav>a")[0].click();
    $("#mainBoxitemsNav").addClass('active');
    $("#manageBoxitemsNav").addClass('active');
    


    $('.pneditspan').click( function() {
           $(this).parents().siblings('input').prop('readonly', false);
           $('.pneditMode').addClass('hide');
           $('.pnnonEditMode').toggleClass('hide');
    });

    $('.pnokspan').click( function() {
           $(this).parents().siblings('input').prop('readonly', true);
           $('.pneditMode').removeClass('hide');
           $('.pnnonEditMode').toggleClass('hide');
           var pnoldtext = $('#pnoldname').val();
           var pnnewtext = $('#boxitem_name').val();
           if(pnoldtext != pnnewtext) {  
          		$('#pneditstatus').val("true");
           } else {
        	   $('#pneditstatus').val("false");
           } 
    });
    
    // Add new row in the table 
    $("#add_row").unbind('click').bind('click', function() {
    	addboxitemrow();
    });
  });

  //Gst Operations
  // get the product information from the server
  function getCommonProductCode(row_id)
  {
	   getRentItemDataName(row_id);
	   var btn = $("#add_row");
	   btn.removeAttr("disabled");
  }

  function getCommonProductData(row_id)
  {
	   getRentItemDataCode(row_id);
	   var btn = $("#add_row");
	   btn.removeAttr("disabled");
  }
</script>