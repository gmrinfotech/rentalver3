<?php 

class Model_invoice extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the orders data */
	public function getInsertData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM invoice ORDER BY id DESC";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}

	public function countTotalInvoices()
	{
		$sql = "SELECT * FROM invoice";
		$query = $this->db->query($sql, array(1));
		return $query->num_rows();
	}
	
	public function getInvoiceData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM invoice WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	
		$sql = "SELECT * FROM invoice ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	// get the orders item data
	public function getInvoiceItemData($invoice_id = null)
	{
		if(!$invoice_id) {
			return false;
		}

		$sql = "SELECT * FROM invoice_items WHERE invoice_id = ?";
		$query = $this->db->query($sql, array($invoice_id));
		return $query->result_array();
	}

	public function create()
	{
		$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
		$invoiceprefix= $company_Prefix['name'];
		//$preinvoice_id = $this->model_orders->getInsertData(1);
		//$invoicenew= $preinvoice_id['id'];
		//$invoicenew++;
		$invoicenew = $company_Prefix['invno'];
		$bill_no = $invoiceprefix.$invoicenew;
		$eorderno= $this->input->post('eorderno');
		$eorderid= $this->input->post('estimateid');
		$sdate = $this->input->post('selected_date');
		$date = str_replace('/', '-', $sdate);
		
		$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
		$invnonew = $company_Prefix['invno'];
		
		$totalrent = $this->input->post('itotalamt');
		$gstamt = $this->calculategst($totalrent);
		$cgstamt = $gstamt/2;
		$gross_amount = $totalrent + $gstamt;
 
		$net_amount =  $this->input->post('net_amount');
		$data = array(
			'invoice_id' => $bill_no,
			'sdate' =>$sdate,
			'odc_no' => $this->input->post('outward_no'), 
			'odate' => $this->input->post('odate'), 
			'sid'=> $this->input->post('customer_id'),
    		'supplier_name' => $this->input->post('customer_name'),
			'supplier_phone' => $this->input->post('customer_phone'),
			'total_items' => $this->input->post('total_items'),
			'totbalance' =>$sgst=$this->input->post('rent_amount'),
			'cleaningcharge' => $this->input->post('cleaningcharge'),
			'vehtranscharge' => $this->input->post('vehtranscharge'), 
			'disc_amount' => $this->input->post('disc_amount'),
			'net_amount' => $net_amount,
			'advance' => $this->input->post('advance'),
		    'iamtpaid' => $this->input->post('iamtpaid'),
			'ibalance' => $this->input->post('rent_amount') , 
			'balance' => $this->input->post('balance') + $gstamt, 
		    'itotalcbal' => $this->input->post('icbalance'), 
			'amtpaid' => $this->input->post('amtpaid'), 
		    'itotalothers' => $this->input->post('itotalothers'),
			'user_id' => $this->session->userdata('id'),
		    'itotalamt' => $totalrent,
		    'gross_amount' => $net_amount + $gstamt,
		    'gstper' => 5,
		    'gst' => $gstamt,
		    'cgst' => $cgstamt,
		    'sgst' => $cgstamt,
			'eorderno' => $eorderno,
			'eorderid' => $eorderid, 
			'mop' => $this->input->post('mop')			
		);

		$this->db->trans_begin();
		
		$insert = $this->db->insert('invoice', $data);
		$invoice_id = $this->db->insert_id();

		$count_product = count($this->input->post('inDC'));

    	for($x = 0; $x < $count_product; $x++) {
			$items = array(
    			'invoice_id' => $invoice_id, 
    			'inward_id' => $this->input->post('inId')[$x],
    			'idc_no' => $this->input->post('inDC')[$x], 
				'idate' => $this->input->post('in_date')[$x],
    			'inboxes' => $this->input->post('inboxes')[$x],
			    'insingles' => $this->input->post('insingles')[$x], 
    			'totalrent' => $this->input->post('inamount')[$x],
			    'inotheramt' => $this->input->post('inotheramt')[$x],
				'amtpaid' =>  $this->input->post('paidamt')[$x],
				'balance' => $this->input->post('inbalance')[$x]
    		);
			
			$insert = $this->db->insert('invoice_items', $items);
  		}
  		
  		$this->incInvoiceNo($invnonew);
  	    if($eorderid!="0")
		{ 
		    	$eorderupdatenew = array(
				'invid' => $invoice_id,
				'invno' => $bill_no 
			 );
			 
			 $update = $this->updatesordernew($eorderupdatenew, $eorderid);
		 } 
  		if ($this->db->trans_status() === FALSE)
  		{
  			$this->db->trans_rollback();
  		}
  		else
  		{
  			$this->db->trans_commit();
  			$this->updatesms($invoice_id);
  		}
  		
		return ($invoice_id) ? $invoice_id : false;
	}
	
	
	public function calculategst($price) {
		$gst = 5; 
		$gst_amount = $price*$gst/100; 
		return $gst_amount;
	}
		
	    
	public function updatesordernew($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('eorders', $data);
			return ($update == true) ? true : false;
		}
	}
	public function incInvoiceNo($invoicenew) {
		$invoicenew++;
		$data = array(
				'invno' => $invoicenew,
		);
		$update = $this->model_invoiceprefixs->update($data, 1);
	}
	
	public function updatesms($id)
	{
		$cusdata = $this->getInvoiceData($id);
		$invoice_id= $cusdata['invoice_id'];
		$total_items= $cusdata['total_items'];
		$totalamount= $cusdata['net_amount'];
		$balance= $cusdata['ibalance'];
		$mobile= $cusdata['supplier_phone'];
		$custname= $cusdata['supplier_name'];
		//sms start
		$authKey = "m249554A8hh0UI35bffa311";
		$mobileNumber = $mobile;
		$senderId = "KSK Cold storage";
	
		$message = urlencode("Dear ".$custname.", Your Inward No: ".$invoice_id.",
				Total Items: ".$total_items.",
				Total Amount: Rs.".$totalamount. ",Balance Amount: Rs.".$balance. " By KSK Cold Storage");
		$route = 4;
		//Prepare you post parameters
		$postData = array(
				'mobiles' => $mobileNumber,
				'message' => $message,
				'sender' => $senderId,
				'route' => $route
		);
	
		$url="http://api.msg91.com/api/v2/sendsms";
	
		$curl = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_URL => "$url",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $postData,
				CURLOPT_HTTPHEADER => array(
						"authkey: $authKey",
						"content-type: multipart/form-data"
				),
		));
	
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
	
		if ($err) {
			//echo "cURL Error #:" . $err;
			return false;
		} else {
			return true;
		}
		//sms end;
	}
	
	public function update($id)
	{
		if($id) {
	
		$sdate = $this->input->post('selected_date');
		$date = str_replace('/', '-', $sdate);

		// fetch the order data 
		
		$data = array(
			'sdate' => $sdate,
			'totbalance' => $this->input->post('rent_amount'),
			'cleaningcharge' => $this->input->post('cleaningcharge'),
			'vehtranscharge' => $this->input->post('vehtranscharge'), 
			'disc_amount' => $this->input->post('disc_amount'),
			'net_amount' => $this->input->post('net_amount'),
			'amtpaid' => $this->input->post('amtpaid'),
		    'iamtpaid' => $this->input->post('iamtpaid'),
		    'itotalothers' => $this->input->post('itotalothers'),
			'ibalance' => $this->input->post('balance'), 
		 	'itotalcbal' => $this->input->post('icbalance'), 
			'balance' => $this->input->post('balance'), 
			'itotalamt' => $this->input->post('itotalamt'),
			'amtpaid' => $this->input->post('amtpaid'),
			'user_id' => $this->session->userdata('id'),
			'mop' => $this->input->post('mop')		 
		);
		$this->db->trans_begin();
		
		$this->db->where('id', $id);
		$update = $this->db->update('invoice', $data);
	
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
		}
		
		return true;
		}
	}

}