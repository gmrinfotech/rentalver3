<?php 

class Model_rentitems extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the brand data */
	public function getRentitemsData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM rentitems where id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM  rentitems Order BY Name ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getTypeRentitemsData($type)
	{ 
		$sql = "SELECT id,code,name FROM  rentitems where type=$type Order BY Name ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function check_product_name($id = '', $product_name) {
		$this->db->where('name', $product_name);
		if($id) {
			$this->db->where_not_in('id', $id);
		}
        return $this->db->get('rentitems')->num_rows();
	}
	

	function check_product_code($id = '', $product_code) {
		$this->db->where('code', $product_code);
		if($id) {
			$this->db->where_not_in('id', $id);
		}
        return $this->db->get('rentitems')->num_rows();
	}
	
	public function getActiveProductData()
	{
		$sql = "SELECT * FROM rentitems WHERE availability = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}

	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('rentitems', $data);
			return ($insert == true) ? true : false;
		}
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('rentitems', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$product_data = $this->getProductImage($id);
			if($product_data['image'] != "<p>You did not select a file to upload.</p>")
				unlink($product_data['image']);
			$delete = $this->db->delete('rentitems');
			return ($delete == true) ? true : false;
		}
	}

	public function countTotalProducts()
	{
		$sql = "SELECT id FROM rentitems";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

}