<?php 

class Model_credit extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the orders data */
	public function getInsertData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM credit ORDER BY id DESC";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}
	
	public function getCreditData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM credit WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	
		$sql = "SELECT * FROM credit ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function create()
	{

		$sdate = $this->input->post('idate');
		$date = str_replace('/', '-', $sdate);
		$crd_no = $this->input->post('invoice_no');
		
		if($crd_no == null) {
			$preinvoice_id = $this->model_credit->getInsertData ( 1 );
			$invoicenew = $preinvoice_id ['id'];
			$invoicenew ++;
			$crd_no = 'CRD-' . $invoicenew;
		}
		
		$data = array(
			'crd_no' =>$crd_no,
			'idc_no' => $this->input->post('inward_no'),
			'sid'=> $this->input->post('customer_id'),
    		'supplier_name' => $this->input->post('customer_name'),
    		'supplier_address' => $this->input->post('customer_address'),
    		'gst_no' => $this->input->post('customer_gst'),
			'ph_no' => $this->input->post('customer_phone'),
			'email_id' => $this->input->post('customer_email'),
			'state' => $this->input->post('customer_state'),
			'sdate' =>$sdate,
			'paydate' =>$this->input->post('datepicker'),
			'totaldays' =>$sgst=$this->input->post('totaldays'),	
			'date_time' => strtotime($date),
			'creditamt' =>$this->input->post('creditamt'),
			'interest' => $this->input->post('interest'),
			'interestamt' => $this->input->post('interestamt'),
			'total_amount' => $this->input->post('total_amount'),	
    		'amtpaid' => $this->input->post('amtpaid'),
    		'balance' => $this->input->post('balance'),
    		'user_id' => $this->session->userdata('id')
		);

		$insert = $this->db->insert('credit', $data);
		$credit_id = $this->db->insert_id();

		return ($credit_id) ? $credit_id : false;
	}

	public function countPOOrderItem($outward_id)
	{
		if($outward_id) {
			$sql = "SELECT id FROM outward_item WHERE outward_id = ?";
			$query = $this->db->query($sql, array($outward_id));
			return $query->num_rows();
		}
	}

	public function update($id)
	{
		if($id) {

		$sdate = $this->input->post('idate');
		$date = str_replace('/', '-', $sdate);

		// fetch the order data 
		
		$data = array(
			'crd_no' => $this->input->post('invoice_no'),
			'idc_no' => $this->input->post('inward_no'),
			'sid'=> $this->input->post('customer_id'),
    		'supplier_name' => $this->input->post('customer_name'),
    		'supplier_address' => $this->input->post('customer_address'),
    		'gst_no' => $this->input->post('customer_gst'),
			'ph_no' => $this->input->post('customer_phone'),
			'email_id' => $this->input->post('customer_email'),
			'state' => $this->input->post('customer_state'),
			'sdate' =>$sdate,
			'paydate' =>$this->input->post('datepicker'),
			'totaldays' =>$sgst=$this->input->post('totaldays'),	
			'date_time' => strtotime($date),
			'creditamt' =>$this->input->post('creditamt'),
			'interest' => $this->input->post('interest'),
			'interestamt' => $this->input->post('interestamt'),
			'total_amount' => $this->input->post('total_amount'),	
    		'amtpaid' => $this->input->post('amtpaid'),
    		'balance' => $this->input->post('balance'),
    		'user_id' => $this->session->userdata('id')
    	);
			
			$this->db->where('id', $id);
			$update = $this->db->update('credit', $data);
			return true;
		}
	}
}