<?php 

class Model_invoiceprefixs extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getPrefixData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM prefix WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	
		$sql = "SELECT * FROM prefix";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('prefix', $data);
			return ($insert == true) ? true : false;
		}
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('prefix', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('prefix');
			return ($delete == true) ? true : false;
		}
	}

}