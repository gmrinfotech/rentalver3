<?php 

class Model_paymentbyestimate extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the orders data */
	public function getPaymentData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM paymentbyestimate WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM paymentbyestimate ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getcustnames() {
		$sql = "select distinct(custid), cname from paymentbyestimate ORDER BY cname ASC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array(); 
	}
	
	// get the orders item data
	public function getOrdersItemData($order_id = null)
	{
		if(!$order_id) {
			return false;
		}

		$sql = "SELECT * FROM paymentbyestimate WHERE payment_id = ?";
		$query = $this->db->query($sql, array($order_id));
		return $query->result_array();
	}

	public function create()
	{
	$user_id = $this->session->userdata('id');
	$a=$this->input->post('totalrent');
	$b=$this->input->post('advance');
	
	$this->db->trans_begin();
	
		if($b<=$a)
		{
        $customer_id= $this->input->post('custid');
		$estimate_id= $this->input->post('estimatenoid');
		$estimate_no= $this->input->post('estimateno');
		//$orderwallet = $this->model_estimate->getOrdersDatabillno($invoice_id);
		$totalrent= $this->input->post('totalrent');
		$advance= $this->input->post('advance');
		$balance= $this->input->post('balance');
		$payment= $this->input->post('payment');
		$newpayment = $advance + $payment;
		$newbalance = $balance - $payment;
		$mop = $this->input->post('mop');
 		 /*?>echo "totalrent".$totalrent."<br>";
		echo "advance".$advance."<br>";
		echo "balance".$balance."<br>";
		echo "payment".$payment."<br>";
		echo "newpayment".$newpayment."<br>";
		echo "newbalance".$newbalance."<br>";
		echo "mop".$mop."<br>";<?php */
		//die();
		$orderfin = array(
			 	'amtpaid' => $newpayment,
				'balance' => $newbalance,
				'mop'=>$mop
				
			);
		
		$update = $this->model_estimate->updatenew($orderfin, $estimate_id);
	}
		else {
			$this->session->set_flashdata('error', 'Payment exceeds the balance amount. Payment Failed!!');
			redirect('paymentbyestimate/create', 'refresh');
		}
	
	$sdate = $this->input->post('selected_date');
	$date = str_replace('/', '-', $sdate);
	$data = array(
			'payment_id' =>$this->input->post('payment_no'),
			'estimateno' =>$estimate_no,
			'custid' =>$this->input->post('custid'),
			'cname' => $this->input->post('custname') ,
			'cmob' =>$this->input->post('mobile'),
			'cbal' =>$this->input->post('balance'),
			'cpaid' => $this->input->post('payment') ,
			'mop' => $this->input->post('mop'),
			'others' => $this->input->post('others'),
			'pdate' =>$sdate,
			'date_time' =>strtotime($date) ,
			'user_id' => $user_id
			
    	);
	
		$insert = $this->db->insert('paymentbyestimate', $data);
		$payment_id = $this->db->insert_id();
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		
   		return ($payment_id) ? $payment_id : false;
	}


	public function countTotalEstimate()
	{
		$sql = "SELECT * FROM paymentbyestimate";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	public function getInsertData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM paymentbyestimate ORDER BY id DESC";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}
	
	public function update($id)
	{
	
	// m-d-Y;
	//$date= date("m-d-Y", strtotime($var) ); working but reverse 2-08-2018 taking as 2nd month but 26/08/2018 working fine
	/*$date = $this->input->post('selected_date');*/
		
		$sdate = $this->input->post('selected_date');
		$date = str_replace('/', '-', $sdate);
		
	/*$datetime = strtotime($date);*/
	/*	if ($datetime === FALSE) {

	  $datetime = strtotime(str_replace('/', '-', $date));
	}*/

		if($id) {
			$user_id = $this->session->userdata('id');
			// fetch the order data 
			$custtype= $this->input->post('custType');

		 	$data = array(
				'receipt_no' =>$this->input->post('receipt_no'),
				'sdate' =>$this->input->post('selected_date'),
				'others' => $this->input->post('others') ,
    		);
		
			$this->db->where('id', $id);
			$update = $this->db->update('paymentbyestimate', $data);
			return true;
			
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('ereciept');

			$this->db->where('payment_id', $id);
			$delete_item = $this->db->delete('payment_item');
			return ($delete == true && $delete_item) ? true : false;
		}
	}

	public function countTotalPaidOrders()
	{
		$sql = "SELECT * FROM paymentbyestimate WHERE paid_status = ?";
		$query = $this->db->query($sql, array(1));
		return $query->num_rows();
	}

}