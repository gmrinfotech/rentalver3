<?php 

class Model_driver extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	// get active brand infromation 
	public function getActiveDriver()
	{
		$sql = "SELECT * FROM driver";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function GetRow($keyword) {
		$this->db->order_by('id', 'DESC');
		$this->db->like("driverphone", $keyword);
		return $this->db->get('driver')->result_array();
	}

	public function getDriverData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM driver WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM driver";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('driver', $data);
			return ($insert == true) ? true : false;
		}
	}

	function check_truck_no($id = '', $truck_no) {
		$this->db->where('truckno', $truck_no);
		if($id) {
			$this->db->where_not_in('id', $id);
		}
		return $this->db->get('driver')->num_rows();
	}
	
	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('driver', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('driver');
			return ($delete == true) ? true : false;
		}
	}

}