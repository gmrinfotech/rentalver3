<?php 

class Model_scustomers extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function countTotalSuppliers()
	{
		$sql = "SELECT supl_id FROM scustomers";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
 
 
	/* get the brand data */
	public function getSupplierData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM scustomers where supl_id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM scustomers ORDER BY supl_id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	function check_unique_phone($phone_number) {
		$this->db->where('ph_no', $phone_number);
		return $this->db->get('scustomers')->num_rows();
	}
	
	function check_unique_gst($gst_no) {
		$this->db->where('gst_no', $gst_no);
		return $this->db->get('scustomers')->num_rows();
	}

	public function createcustomer($data)
	{
		$gst_no = $data['gst_no'];
		if($gst_no != 0) {
			$uniqgst = $this->check_unique_gst($gst_no);
			if($uniqgst > 0) {
				return '000'; // Error number to check unique gst number
			}
		}
	    $result = $this->check_unique_phone($data['ph_no']);
		if($result > 0 ) {
			return '0000'; // Error number to check unique phone number
		}
		
		if($result == 0) {
			if($data) {
				$insert = $this->db->insert('scustomers', $data);
				$insert_id = $this->db->insert_id();
				return ($insert == true) ? $insert_id : false;
			}
		} 
	}
	
	public function getSupplierAddress($id = null)
	{
		if($id) {
			$sql = "SELECT supp_address,supp_address2,supp_address3,supp_address4 FROM scustomers where supl_id = ?";
	
			$query = $this->db->query($sql, array($id));
			return array_filter($query->row_array());
		}
	}

	public function create()
	{
		$gst_no = $this->input->post('gst_no');
		$this->form_validation->set_rules('cust_name', 'Customer name', 'trim|required');
		$this->form_validation->set_rules('ph_no', 'Phone Number', 'trim|required|is_unique[scustomers.ph_no]');
	    $this->form_validation->set_rules('gst_no', 'Enter GST/Pan Number', 'trim|required');
		
		if($gst_no != 0) {
			$this->form_validation->set_rules('gst_no', 'GST/Pan Number', 'is_unique[scustomers.gst_no]');
		}
		$this->form_validation->set_error_delimiters('<p class="val-error">','</p>');
		
		$data = array(
				'supp_name' => $this->input->post('supp_name'),
				'supp_address' => $this->input->post('supp_address'),
				'supp_address2' => $this->input->post('supp_address2'),
				'supp_address3' => $this->input->post('supp_address3'),
				'supp_address4' => $this->input->post('supp_address4'),
				'gst_no' => $this->input->post('gst_no'),
				'ph_no' => $this->input->post('ph_no'),
				'email_id' => $this->input->post('email_id'),
				'state' => $this->input->post('customer_state'),
				'credit' => $this->input->post('credit'),
				'credit_limit' => $this->input->post('credit_limit'),
				'credit_days' => $this->input->post('credit_days'),
			   	'old_balance' => $this->input->post('old_balance'),
				'wallet' => $this->input->post('credit_limit'),
				'state_code' => $this->input->post('customer_state_code') 
				 
		);
	
		$this->db->trans_begin();
	
		$insert = $this->db->insert('scustomers', $data);
 
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		 
		return ($insert) ? $insert : false;
	}

	public function update($data, $id)
	{
		if($data && $id) {

			$this->db->trans_begin();
			
			$this->db->where('supl_id', $id);
			$update = $this->db->update('scustomers', $data); 
			
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
				return false;
			}
			else
			{
				$this->db->trans_commit();
				return true;
			}

		}
		return false;
	}
	
	function get_report_list($filter = array())
    { 
        if(count($filter) > 0)
        {
            if(!empty($filter['from_date']) && !empty($filter['to_date']))
            {
                $from_date = $filter['from_date'];
                $to_date = $filter['to_date'];

                $customer = $filter['customer'];
 
               if($customer == "") {
                	$sql = "SELECT DISTINCT O.id, O.odc_no, O.sdate AS outdate, O.sid, O.supplier_name, O.advance, 
							IFNULL(O.totalrent, 0) AS totalrent, OI.itype, OI.boxitem_name, OI.noofunits as outunit, I.idate AS indate,
							IFNULL(II.noofunits, 0) AS inunits,  IFNULL(I.amtpaid , 0) AS iamtpaid, I.advance AS iadv, 
							IFNULL(OI.balanceqty, 0) AS balqty, 
							 IFNULL(I.discount, 0) as discount, IFNULL(O.totalrent - I.discount , 0) AS finalamt FROM outward O 
							 inner join outward_item OI 
							 ON O.id = OI.outward_id LEFT outer join inward I ON O.id = I.odc_no 
							 LEFT outer  JOIN inward_item II ON II.inward_id = I.id 
							where 
							str_to_date(O.sdate, '%d/%m/%Y')
					    	BETWEEN str_to_date('$from_date','%d/%m/%Y') AND 
							str_to_date('$to_date', '%d/%m/%Y') "; 
                	 
                } else {
                	$sql = "SELECT DISTINCT O.id, O.odc_no, O.sdate AS outdate, O.sid, O.supplier_name, O.advance, 
							IFNULL(O.totalrent, 0) AS totalrent, OI.itype, OI.boxitem_name, OI.noofunits as outunit, I.idate AS indate,
							IFNULL(II.noofunits, 0) AS inunits,  IFNULL(I.amtpaid , 0) AS iamtpaid, I.advance AS iadv, 
							IFNULL(OI.balanceqty, 0) AS balqty, 
							 IFNULL(I.discount, 0) as discount, IFNULL(O.totalrent - I.discount , 0) AS finalamt FROM outward O 
							 inner join outward_item OI 
							 ON O.id = OI.outward_id LEFT outer join inward I ON O.id = I.odc_no 
							 LEFT outer  JOIN inward_item II ON II.inward_id = I.id 
						    where 
							str_to_date(O.sdate, '%d/%m/%Y')
					  	    BETWEEN str_to_date('$from_date','%d/%m/%Y') AND 
							str_to_date('$to_date', '%d/%m/%Y') and O.sid = $customer"; 
                	  
                }   
				$query = $this->db->query($sql); 
            }
			else
			{
				$sql = "SELECT * FROM outward Order By id DESC ";
				$query = $this->db->query($sql);
				
			}
        }
		return $query->result_array();
    }

	public function remove($id)
	{
		if($id) {
			$this->db->where('supl_id', $id);
			$delete = $this->db->delete('scustomers');
			return ($delete == true) ? true : false;
		}
	}
}
