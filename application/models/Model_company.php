<?php 

class Model_company extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the brand data */
	public function getCompanyData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM company WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}
	
	public function getAdminData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM gmradmin WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}
	public function getAdminProductcode()
	{
		
			$sql = "SELECT productcodeenable FROM company";
			$query = $this->db->query($sql);
			return $query->row_array();
		
	}
	public function getDiscoutType()
	{
		$sql = "SELECT discountType FROM company";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	/*public function getInsertData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM orders ORDER BY id DESC";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}*/

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('company', $data);
			return ($update == true) ? true : false;
		}
	}

	public function updateadmin($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('gmradmin', $data);
			return ($update == true) ? true : false;
		}
	}

}