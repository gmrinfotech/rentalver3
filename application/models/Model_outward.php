<?php 

class Model_outward extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the orders data */
	public function getInsertData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM outward ORDER BY id DESC";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}

	public function countTotalOutwards()
	{
		$sql = "SELECT * FROM outward";
		$query = $this->db->query($sql, array(1));
		return $query->num_rows();
	}

	public function getOutwardData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM outward WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	
		$sql = "SELECT * FROM outward ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	} 
	
	public function getOutwardDCNo($id = null)
	{
		if($id) {
			$sql = "SELECT odc_no,supplier_name FROM outward WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}
	
	public function getAvailableOutwardItems($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM outward_item WHERE outward_id = ? and balanceqty > 0 order by id ASC";
			$query = $this->db->query($sql, array($id));
			return $query->result_array();
		}
	}

	public function getPresentOutwarditemsData()
	{
		$sql = "SELECT id, odc_no, total_balanceqty,ph_no FROM outward where total_balanceqty > 0";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
 
	public function getClosedOutwarditemsData()
	{
		$sql = "select id, odc_no from outward where total_balanceqty = 0 and id not in (select odc_no from eorders)";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	// get the orders item data
	public function getOutwardItemData($outward_id = null)
	{
		if(!$outward_id) {
			return false;
		}

		$sql = "SELECT * FROM outward_item WHERE outward_id = ? order by id ASC";
		$query = $this->db->query($sql, array($outward_id));
		return $query->result_array();
	}
	
	public function getInBoxNos($outward_id = null)
	{
		if(!$outward_id) {
			return false;
		}
	
		$sql = "select SUM(CASE WHEN itype = 1 THEN noofunits ELSE 0 END) AS outboxes,
				SUM(CASE WHEN itype = 2 THEN noofunits ELSE 0 END) AS outsingles 
				FROM outward_item where outward_id = ?";
		$query = $this->db->query($sql, array($outward_id));
		return $query->row_array();
	}
 
	public function getOutwardItemDatabyId($outward_item_id, $outward_id)
	{
		if(!$outward_id) {
			return false;
		}

		$sql = "SELECT * FROM outward_item WHERE outward_id = ? and id = ?";

		$query = $this->db->query($sql, array($outward_id, $outward_item_id));
		return $query->row_array();
	}
	
	public function create()
	{
 
		$sdate = $this->input->post('selected_date');
		$date = str_replace('/', '-', $sdate);

		$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
		$outnonew = $company_Prefix['outno'];
 
		$data = array(
			'odc_no' => $this->input->post('invoice_no'),
			'sid'=> $this->input->post('customer_id'),
    		'supplier_name' => $this->input->post('customer_name'),
    		'supplier_address' => $this->input->post('customer_address'),
			'sitelocation' => $this->input->post('sitelocation'),
    		'gst_no' => $this->input->post('customer_gst'),
			'ph_no' => $this->input->post('customer_phone'),
			'email_id' => $this->input->post('customer_email'),
			'state' => $this->input->post('customer_state'),
    		'state_code' => $this->input->post('customer_state_code'),
    		'did' => $this->input->post('driver_id'),
			'drivername' => $this->input->post('driver_name'),
			'driverphone' => $this->input->post('driver_phone') ,
			'contNo' => $this->input->post('cont_no'),
			'empid' => $this->input->post('employee'),
			'tod' => $this->input->post('tod'),
			'mop' => $this->input->post('mop'),
			'others' => $this->input->post('others'),
			'sdate' => $sdate,
			'ndate' => $sdate,
			'date_time' => strtotime($date),
			'outwardcharge' => $this->input->post('rent_amount'),
			'wages' => $this->input->post('wages'),
			'cleaningcharge' => $this->input->post('cleaningcharge'),
			'vehtranscharge' => $this->input->post('vehtranscharge'), 
    		'untimedloading' => $this->input->post('untimedloading'), 
			'totalamount'=>	$this->input->post('total_amount'),
    		'discount' => $this->input->post('disc_amount'),
    		'totalrent' => $this->input->post('net_amount'),
			'advance' => $this->input->post('amtpaid'),
			'total_items' => $this->input->post('total_items'),
			'total_balanceqty' => $this->input->post('total_balanceqty'), 
    		'user_id' => $this->session->userdata('id') 
			 
		);
 
		$this->db->trans_begin();
		
		$insert = $this->db->insert('outward', $data);
		$outward_id = $this->db->insert_id();

		if($outward_id == false) 
			return false;
 
		$count_product = count($this->input->post('boxitem_name'));
    	for($x = 0; $x < $count_product; $x++) {

    		$box_id = $this->input->post('boxitem_name')[$x]; 
    		$noofunits = $this->input->post('noofunits')[$x]; 
    		$itype = $this->input->post('itype')[$x];
 
    		$items = array(
    			'outward_id' => $outward_id,
    			'itype' => $itype,
    		    'boxitem_id' => $box_id,
    		    'boxitem_name' => $this->input->post('bname')[$x], 
    			'type' => $this->input->post('type')[$x], 
				'units' => $this->input->post('units')[$x], 
    			'minrentaldays' => $this->input->post('minrentaldays')[$x],
    			'rate'=> $this->input->post('rate')[$x],  
    			'rateperday'=> $this->input->post('rateperday')[$x],
				'noofunits' => $noofunits, 
    			'sdate' => $sdate,
    			'ndate' => $sdate,
    			'period' => $this->input->post('period')[$x],
				'totalrent' => $this->input->post('totamount')[$x],
    			'balanceqty' => $this->input->post('balanceqty')[$x]
    		);
 
    		$this->db->insert('outward_item', $items);  
    		
    		if($itype == 1) {
	    		$get_box_item = $this->getBoxitemsRentQty($box_id);
					foreach ($get_box_item as $k => $v) {  
						$product_id = $v['rentitem_id'];
						$qty = $v['qty']; 
						
						$totunits = $noofunits * $qty;
						
						$sql = "UPDATE rentitems SET rentedqty = rentedqty + $totunits, 
						    	availableqty = availableqty - $totunits WHERE id = $product_id";
						
					    if($noofunits != 0)
							$query = $this->db->query($sql);
				}
    		} else { 
			 	$sql = "UPDATE rentitems SET rentedqty = rentedqty + $noofunits, 
				    	availableqty = availableqty - $noofunits WHERE id = $box_id"; 
	    		if($noofunits != 0)
	    			$query = $this->db->query($sql);
    		} 	
    	}
  
    	$this->incInvoiceNo($outnonew);
    	
    	if ($this->db->trans_status() === FALSE)
    	{
    		$this->db->trans_rollback();
    	}
    	else
    	{
    		$this->db->trans_commit();
    		//$this->updatesms($outward_id);
    	}
 
		return ($outward_id) ? $outward_id : false;
	}
	public function getPendingOutwardNos()
	{
		$sql = "SELECT id, odc_no FROM outward where totalrent > advance";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function updatenew($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('outward', $data);
			return ($update == true) ? true : false;
		}
	}
	public function search($id) {
		$this->db->like('id',$id,'both');
		return $this->db->get('outward')->row_array();
	}
	public function getBoxitemsRentQty($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM boxitems_data where boxitems_id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->result_array();
		} 
	}
	
	public function incInvoiceNo($invoicenew) {
		$invoicenew++;
		$data = array(
			'outno' => $invoicenew,
		);
		$update = $this->model_invoiceprefixs->update($data, 1);
	}

	public function updatesms($outward_id)
	{
		$cusdata = $this->getOutwardData($outward_id);
		$odc_no= $cusdata['odc_no'];
		$total_items= $cusdata['total_items'];
		$contNo= $cusdata['contNo'];
		$totalamount= $cusdata['totalamount'];
		$balance= $cusdata['balance'];
		$mobile= $cusdata['ph_no'];
		$custname= $cusdata['supplier_name'];
		//sms start
		$authKey = "m249554A8hh0UI35bffa311";
		$mobileNumber = $mobile;
		$senderId = "KSK Cold storage";
	
		$message = urlencode("Dear ".$custname.", Your Outward No: ".$odc_no.", 
				Total Items: ".$total_items.", Cont No: ".$contNo.", 
				Total Amount: Rs.".$totalamount. ",Balance Amount: Rs.".$balance. " By APS Traders");
		$route = 4;
		//Prepare you post parameters
		$postData = array(
				'mobiles' => $mobileNumber,
				'message' => $message,
				'sender' => $senderId,
				'route' => $route
		);
	
		$url="http://api.msg91.com/api/v2/sendsms";
	
		$curl = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_URL => "$url",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $postData,
				CURLOPT_HTTPHEADER => array(
						"authkey: $authKey",
						"content-type: multipart/form-data"
				),
		));
	
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
	
		if ($err) {
			//echo "cURL Error #:" . $err;
			return false;
		} else {
			return true;
		}
		//sms end;
	}
	
	public function countPOOrderItem($outward_id)
	{
		if($outward_id) {
			$sql = "SELECT id FROM outward_item WHERE outward_id = ?";
			$query = $this->db->query($sql, array($outward_id));
			return $query->num_rows();
		}
	}
	
	public function countTotalPurchase()
	{
		$sql = "SELECT bill_no FROM outward";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	public function update($id)
	{
		if($id) {
	

		$sdate = $this->input->post('selected_date');
		$date = str_replace('/', '-', $sdate);
		$odc_no = $this->input->post('invoice_no');

		// fetch the order data 
		
		$data = array( 
			'sid'=> $this->input->post('customer_id'),
    		'supplier_name' => $this->input->post('customer_name'),
    		'supplier_address' => $this->input->post('customer_address'),
			'sitelocation' => $this->input->post('sitelocation'),
    		'gst_no' => $this->input->post('customer_gst'),
			'ph_no' => $this->input->post('customer_phone'),
			'email_id' => $this->input->post('customer_email'),
			'state' => $this->input->post('customer_state'),
    		'state_code' => $this->input->post('customer_state_code'),
    		'did' => $this->input->post('driver_id'),
			'drivername' => $this->input->post('driver_name'),
			'driverphone' => $this->input->post('driver_phone') ,
			'contNo' => $this->input->post('cont_no'),
			'empid' => $this->input->post('employee'),
			'tod' => $this->input->post('tod'),
			'mop' => $this->input->post('mop'),
			'others' => $this->input->post('others'),
			'sdate' =>$sdate,
			'ndate' => $sdate,
			'date_time' => strtotime($date),
			'outwardcharge' =>$sgst=$this->input->post('rent_amount'),
			'wages' => $this->input->post('wages'),
			'cleaningcharge' => $this->input->post('cleaningcharge'),
			'vehtranscharge' => $this->input->post('vehtranscharge'), 
    		'untimedloading' => $this->input->post('untimedloading'), 
			'totalamount'=>	$this->input->post('total_amount'),
    		'discount' => $this->input->post('disc_amount'),
    		'totalrent' => $this->input->post('net_amount'),
			'advance' => $this->input->post('amtpaid'),
			'total_items' => $this->input->post('total_items'),
			'total_balanceqty' => $this->input->post('total_balanceqty'),
    		'user_id' => $this->session->userdata('id')
		); 
		$this->db->trans_begin();
		
		$this->db->where('id', $id);
		$update = $this->db->update('outward', $data);
		
		if($update == false)
			return false;
 
		$get_order_item = $this->getOutwardItemData($id);
		foreach ($get_order_item as $k => $v) {  
			$box_id = $v['boxitem_id'];
			$noofunits = $v['noofunits']; 
			$itype = $v['itype']; 
			
			if($itype == 1) {
				$get_box_item = $this->getBoxitemsRentQty($box_id);
					foreach ($get_box_item as $k => $v) {  
						$product_id = $v['rentitem_id'];
						$qty = $v['qty']; 
						
						$totunits = $noofunits * $qty;
						
						$sql = "UPDATE rentitems SET rentedqty = rentedqty - $totunits, 
						    	availableqty = availableqty + $totunits WHERE id = $product_id";
						
					    if($noofunits != 0)
							$query = $this->db->query($sql);
				}
			} else { 
				$sql = "UPDATE rentitems SET rentedqty = rentedqty - $noofunits, 
				    	availableqty = availableqty + $noofunits WHERE id = $box_id";
				
			    if($noofunits != 0)
					$query = $this->db->query($sql);
			}
		}

			// now remove the order item data 
			$this->db->where('outward_id', $id);
			$this->db->delete('outward_item');

			// now decrease the product qty
			$count_product = count($this->input->post('boxitem_name'));
    		for($x = 0; $x < $count_product; $x++) {

    		$box_id = $this->input->post('boxitem_name')[$x]; 
    		$noofunits = $this->input->post('noofunits')[$x]; 
    		$itype = $this->input->post('itype')[$x];
    		
    		$items = array(
    			'outward_id' => $id, 
    		    'itype' => $itype,
    		    'boxitem_id' => $box_id,
    		    'boxitem_name' => $this->input->post('bname')[$x], 
    			'type' => $this->input->post('type')[$x],
				'units' => $this->input->post('units')[$x], 
    			'minrentaldays' => $this->input->post('minrentaldays')[$x],
    			'rate'=> $this->input->post('rate')[$x],  
    			'rateperday'=> $this->input->post('rateperday')[$x],
				'noofunits' => $noofunits, 
    			'sdate' => $sdate,
    			'ndate' => $sdate,
    			'period' => $this->input->post('period')[$x],
				'totalrent' => $this->input->post('totamount')[$x],
    			'balanceqty' => $this->input->post('balanceqty')[$x]
    		);
    		$this->db->insert('outward_item', $items);
  
    		if($itype == 1) {
    			$get_box_item = $this->getBoxitemsRentQty($box_id);
				foreach ($get_box_item as $k => $v) {  
					$product_id = $v['rentitem_id'];
					$qty = $v['qty']; 
					
					$totunits = $noofunits * $qty;
					
					$sql = "UPDATE rentitems SET rentedqty = rentedqty + $totunits, 
					    	availableqty = availableqty - $totunits WHERE id = $product_id";
					
				    if($noofunits != 0)
						$query = $this->db->query($sql);
				}
    		} else {
				$sql = "UPDATE rentitems SET rentedqty = rentedqty + $noofunits, 
				    	availableqty = availableqty - $noofunits WHERE id = $box_id";
	    		
	    		if($noofunits != 0)
	    			$query = $this->db->query($sql);
    		}
    	}
    	
    	if ($this->db->trans_status() === FALSE)
    	{
    		$this->db->trans_rollback();
    	}
    	else
    	{
    		$this->db->trans_commit();
    	}
    	/* End of For */
			return true;
		}
	}

	public function getInOutwardData($id)
	{
		if($id) {
			$sql = "SELECT id FROM inward WHERE odc_no = ?";
			$query = $this->db->query($sql, array($id));
			return $query->num_rows();
		}
	}
	
	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('outward');

			$this->db->where('outward_id', $id);
			$delete_item = $this->db->delete('outward_item');
			return ($delete == true && $delete_item) ? true : false;
		}
	}
}