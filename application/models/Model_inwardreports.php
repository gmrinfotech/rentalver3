<?php 

class Model_inwardreports extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function get_report_list($filter = array())
    { 
        if(count($filter) > 0)
        {
            if(!empty($filter['from_date']) && !empty($filter['to_date']))
            {
                $from_date = $filter['from_date'];
                $to_date = $filter['to_date'];

                $customer = $filter['customer'];
 
               if($customer == "") {
                	$sql = "select id,odc_no,idc_no, idate, odate, supplier_name,ph_no,email_id,
						totaldays,totalrent,advance,total_items,total_balanceqty,amtpaid FROM inward 
						WHERE str_to_date(idate, '%d/%m/%Y')
					    BETWEEN str_to_date('$from_date','%d/%m/%Y') AND 
						str_to_date('$to_date', '%d/%m/%Y') "; 
                	 
                } else if($customer == '000') {
                	$sql = "select id,odc_no,idc_no, idate, odate, supplier_name,ph_no,email_id,
						totaldays,totalrent,advance,total_items,total_balanceqty,amtpaid FROM inward 
						WHERE str_to_date(idate, '%d/%m/%Y')
					    BETWEEN str_to_date('$from_date','%d/%m/%Y') AND 
						str_to_date('$to_date', '%d/%m/%Y') and sid = ''";  
                } else {
                	$sql = "select id,odc_no,idc_no, idate, odate, supplier_name,ph_no,email_id,
						totaldays,totalrent,advance,total_items,total_balanceqty,amtpaid FROM inward 
						WHERE str_to_date(idate, '%d/%m/%Y')
					    BETWEEN str_to_date('$from_date','%d/%m/%Y') AND 
						str_to_date('$to_date', '%d/%m/%Y') and sid = $customer"; 
                	  
                } 
				$query = $this->db->query($sql); 
            }
			else
			{
				$sql = "SELECT * FROM inward Order By id DESC ";
				$query = $this->db->query($sql);
				
			}
        }
		return $query->result_array();
    }
}