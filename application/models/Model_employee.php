<?php 

class Model_employee extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	// get active brand infromation 
	public function getActiveEmployee()
	{
		$sql = "SELECT * FROM employee";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getEmployeeData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM employee WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM employee";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('employee', $data);
			return ($insert == true) ? true : false;
		}
	}

	function check_employee_name($id = '', $employee_name) {
		$this->db->where('employeename', $employee_name);
		if($id) {
			$this->db->where_not_in('id', $id);
		}
		return $this->db->get('employee')->num_rows();
	}
	
	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('employee', $data);
			return ($update == true) ? true : false;
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('employee');
			return ($delete == true) ? true : false;
		}
	}

}