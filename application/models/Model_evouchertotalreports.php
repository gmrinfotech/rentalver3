<?php 

class Model_evouchertotalreports extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}



	function get_report_list($filter = array())
    { 
    	$cmpdata = $this->model_company->getAdminData(1);
    	$ledgergmr = $cmpdata['ledgergmr'];
        if(count($filter) > 0)
        {
	        if($ledgergmr=="0")
			{
	            if(!empty($filter['from_date']) && !empty($filter['to_date']))
	            {
	                $from_date = $filter['from_date'];
	                $to_date = $filter['to_date'];
					$sql = "select T1.id,T1.voucher_no,T1.others,T2.sdate,T2.under,T2.ledger_id,T1.user_id,sum(T2.amount) as amount FROM evoucher AS T1
	       				 INNER JOIN evoucher_item AS T2  ON T1.id = T2.voucher_id WHERE str_to_date(T2.sdate, '%d/%m/%Y') BETWEEN str_to_date('$from_date', '%d/%m/%Y') AND str_to_date('$to_date', '%d/%m/%Y') Group by T2.ledger_id "; 
					$query = $this->db->query($sql);
	
	            }
				else
				{
					$sql = "SELECT T1.id,T1.voucher_no,T1.others,T2.sdate,T2.under,T2.ledger_id,T2.amount FROM ereciept AS T1 INNER JOIN ereciept_item AS T2  
					ON T1.id = T2.voucher_id GROUP BY id";
					$query = $this->db->query($sql);
					
				}
	        } else{ 
				if(!empty($filter['from_date']) && !empty($filter['to_date']))
	            {
	                $from_date = $filter['from_date'];
	                $to_date = $filter['to_date'];
					$sql = "SELECT T1.id, T1.expense_date,T2.under,
							T1.ledger_id,sum(T1.amount) as amount FROM expense_log AS T1 INNER JOIN ledger AS T2  
							ON T2.id = T1.ledger_id WHERE T1.expense_type = 2 and STR_TO_DATE(T1.expense_date, '%d/%m/%Y')
							BETWEEN str_to_date('$from_date', '%d/%m/%Y') AND str_to_date('$to_date', '%d/%m/%Y') Group BY T1.ledger_id"; 
					//$sql = "select * FROM ereciept WHERE date_format(str_to_date(pdate, '%d/%m/%Y'), '%m/%d/%Y') BETWEEN date_format(str_to_date('$from_date', '%d/%m/%Y'), '%m/%d/%Y') AND date_format(str_to_date('$to_date', '%d/%m/%Y'), '%m/%d/%Y')"; 
	
					$query = $this->db->query($sql);
					 
					//$sql = "SELECT * FROM porders WHERE sdate BETWEEN ? AND ? ";
					//$query = $this->db->query($sql, array($from_date, $to_date ));
	            }
				else
				{
					$sql = "SELECT T1.id, T1.expense_date,T2.under,
							T1.ledger_id,sum(T1.amount) as amount FROM expense_log AS T1 INNER JOIN ledger AS T2  
							ON T2.id = T1.ledger_id WHERE T1.expense_type = 2 GROUP BY T1.ledger_id";
				 
					$query = $this->db->query($sql);
					
				}  
			}
        }
		return $query->result_array();
    }
	// get the orders item data
	/*public function getOrdersItemData($order_id = null)
	{
		if(!$order_id) {
			return false;
		}

		$sql = "SELECT * FROM orders_item WHERE order_id = ?";
		$query = $this->db->query($sql, array($order_id));
		return $query->result_array();
	}

	public function countOrderItem($order_id)
	{
		if($order_id) {
			$sql = "SELECT * FROM orders_item WHERE order_id = ?";
			$query = $this->db->query($sql, array($order_id));
			return $query->num_rows();
		}
	}

	public function countTotalPaidOrders()
	{
		$sql = "SELECT * FROM orders WHERE paid_status = ?";
		$query = $this->db->query($sql, array(1));
		return $query->num_rows();
	}*/

}