<?php
Class Model_expenditure extends CI_Model
{
	function get_expense_category($id)
    {
        return $this->db->get_where('ledger', array('id'=>$id))->row();
    }

    function delete_expense_category($id)
    {
    	$this->db->where('id', $id);
		$this->db->delete('ledger');
	
		// delete children
		// $this->remove_product($id);
    }

    function check_expense_category($str, $id=false)
    {
        $this->db->select('name');
        $this->db->from('ledger');
        $this->db->where('name', $str);
        if ($id)
        {
            $this->db->where('id !=', $id);
        }
        $count = $this->db->count_all_results();
        
        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    function save_expense_category($save)
    {
        if ($save['id'])
        {
            $this->db->where('id', $save['id']);
            $this->db->update('ledger', $save);
            return $save['id'];
        }
        else
        {
            $this->db->insert('ledger', $save);
            return $this->db->insert_id();
        }
    }
    function get_expense_categories($category_status = 'enabled')
    {
        if($category_status == 'enabled')
        {
            $this->db->where('status', 1);
        }
        if($category_status == 'disabled')
        {
            $this->db->where('status', 0);
        }
        return $this->db->get('ledger')->result();
    }

    // Expense
    function get_expense($id)
    {
        return $this->db->get_where('expense_log', array('id'=>$id))->row();
    }

    function delete_expense($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('expense_log');
    }
    
    function save_expense($save)
    {
        if ($save['id'])
        {
            $this->db->where('id', $save['id']);
            $this->db->update('expense_log', $save);
            return $save['id'];
        }
        else
        {
            $this->db->insert('expense_log', $save);
            return $this->db->insert_id();
        }
    }
    
    function get_expense_list($filter = array())
    {

    
        $this->db->select('expense_log.*');
        $this->db->select('ledger.name');
        $this->db->join('ledger', 'expense_log.ledger_id = ledger.id', 'left');
        if(count($filter) > 0)
        {
            if(!empty($filter['expense_date']))
            {
                $this->db->where("str_to_date(expense_date,'%d/%m/%Y') = str_to_date('".$filter['expense_date']."','%d/%m/%Y')");
            }
            if(!empty($filter['from_date']) && !empty($filter['to_date']))
            { 
                $from_date = $filter['from_date']; 
                $to_date = $filter['to_date']; 
                $this->db->where("str_to_date(expense_date,'%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y')");
                $this->db->order_by("expense_date", "asc");
            }
        }
        return $this->db->get('expense_log')->result();
    }
    
	public function countTotalCashSumOrders($filter = array())
	{ 
		$from_date = $filter['from_date']; 
        $to_date = $filter['to_date']; 
		$sql = "select 
		(select COALESCE(sum(advance),0) FROM outward WHERE str_to_date(sdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop=1) as invadv,
		(select COALESCE(sum(amtpaid),0) FROM inward WHERE str_to_date(idate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop=1) as oamtpaid,
		(select COALESCE(sum(amtpaid),0) FROM eorders WHERE str_to_date(sdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop=1) as eamtpaid,
		(select COALESCE(sum(itotalcbal),0) FROM eorders WHERE str_to_date(sdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop=1) as ecbal,
		(select COALESCE(sum(cpaid),0) FROM paymentbyoutward WHERE str_to_date(pdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop=1) as outpay,
		(select COALESCE(sum(cpaid),0) FROM paymentbyestimate WHERE str_to_date(pdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop=1) as estpay";
 
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function countTotalSumOrders($filter = array())
	{ 
		$from_date = $filter['from_date']; 
        $to_date = $filter['to_date']; 
		$sql = "select 
				(select COALESCE(sum(advance),0) FROM outward WHERE str_to_date(sdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop!=1) as invadv,
				(select COALESCE(sum(amtpaid),0) FROM inward WHERE str_to_date(idate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop!=1) as oamtpaid,
				(select COALESCE(sum(amtpaid),0) FROM eorders WHERE str_to_date(sdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop!=1) as eamtpaid,
				(select COALESCE(sum(itotalcbal),0) FROM eorders WHERE str_to_date(sdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop!=1) as ecbal,
				(select COALESCE(sum(cpaid),0) FROM paymentbyoutward WHERE str_to_date(pdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop!=1) as outpay,
				(select COALESCE(sum(cpaid),0) FROM paymentbyestimate WHERE str_to_date(pdate, '%d/%m/%Y') between str_to_date('".$from_date."','%d/%m/%Y') and str_to_date('".$to_date."','%d/%m/%Y') AND mop!=1) as estpay";
 
		$query = $this->db->query($sql);
		return $query->row_array();
	}
    
    function get_opening_closing($date)
    {
		$sdate = str_replace('/', '-', $date);
    	$previous_date = date('d/m/Y', strtotime('-1 day', strtotime($sdate)));
    
    	$sql = "select  sum(case when expense_type = '1' and str_to_date(expense_date,'%d/%m/%Y') <= str_to_date('".$previous_date."','%d/%m/%Y')
    			then amount else 0 end) - sum(case when expense_type = '2' and str_to_date(expense_date,'%d/%m/%Y') <= str_to_date('".$previous_date."','%d/%m/%Y')
    			then amount else 0 end) as opening, sum(case when expense_type = '1' and str_to_date(expense_date,'%d/%m/%Y') <= str_to_date('".$date."','%d/%m/%Y')
    			then amount else 0 end) - sum(case when expense_type = '2' and str_to_date(expense_date,'%d/%m/%Y') <= str_to_date('".$date."','%d/%m/%Y')
    			then amount else 0 end) as closing from expense_log";
				
    	//echo $sql;
    	$query = $this->db->query($sql);
    	return $query->row_array();
    }
 
    function get_opening_balance($date)
    {
        $previous_date = date('Y-m-d', strtotime('-1 day', strtotime($date)));
       // $previous_date = date('Y-m-d', strtotime($date));

        $this->db->select('expense_log.*');
        $this->db->select('ledger.name');
        $this->db->join('ledger', 'expense_log.ledger_id = ledger.id', 'left');
        $this->db->where('expense_date', $previous_date);
        //$this->db->where("expense_date = (select max(expense_date) from expense_log where expense_date < '$previous_date')");
        $expense_list = $this->db->get('expense_log')->result();
        $credit_amt = 0;
        $debit_amt = 0;
        if(sizeof($expense_list) > 0)
        {
            foreach ($expense_list as $row) {
                if($row->expense_type == 1)
                {
                    $credit_amt += $row->amount;
                }
                if($row->expense_type == 2)
                {
                    $debit_amt += $row->amount;
                }
            }
        }
        $balance = $credit_amt - $debit_amt;
        return $balance;
    }
}