<?php 

class Model_inward extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the orders data */
	public function getInsertData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM inward ORDER BY id DESC";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}
	
	public function countTotalInwards()
	{
		$sql = "SELECT * FROM inward";
		$query = $this->db->query($sql, array(1));
		return $query->num_rows();
	}
	
	public function getInwardData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM inward WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	
		$sql = "SELECT * FROM inward ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getInEstData($id)
	{
		if($id) {
			$sql = "SELECT id FROM eorders_items WHERE inward_id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->num_rows();
		}
	}
	
	
	public function getInwardItemData($inward_id = null)
	{
		if(!$inward_id) {
			return false;
		}
	
		$sql = "SELECT * FROM inward_item WHERE inward_id = ?";
		$query = $this->db->query($sql, array($inward_id));
		return $query->result_array();
	}
	
	public function countTotalSumOrders()
	{ 
		$sql = "select  (select COALESCE(sum(advance),0) FROM outward WHERE str_to_date(sdate, '%d/%m/%Y') =
						str_to_date(now(),'%Y-%m-%d')) as invadv,
					  	(select COALESCE(sum(amtpaid),0) FROM inward WHERE str_to_date(idate, '%d/%m/%Y') =
						str_to_date(now(),'%Y-%m-%d')) as oamtpaid,
						(select COALESCE(sum(amtpaid),0) FROM eorders WHERE str_to_date(sdate, '%d/%m/%Y') =
						str_to_date(now(),'%Y-%m-%d')) as eamtpaid,
					  	(select COALESCE(sum(itotalcbal),0) FROM eorders WHERE str_to_date(sdate, '%d/%m/%Y') =
						str_to_date(now(),'%Y-%m-%d')) as ecbal,
						(select COALESCE(sum(cpaid),0) FROM paymentbyoutward WHERE str_to_date(pdate, '%d/%m/%Y') =
						str_to_date(now(),'%Y-%m-%d')) as outpay,
						(select COALESCE(sum(cpaid),0) FROM paymentbyestimate WHERE str_to_date(pdate, '%d/%m/%Y') =
						str_to_date(now(),'%Y-%m-%d')) as estpay";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function create()
	{

		$sdate = $this->input->post('selected_date');
		$date = str_replace('/', '-', $sdate);
    	$outward_id = $this->input->post('outward_no');
 
    	$totalbalqty  = $this->input->post('total_balanceqty');
		$data = array(
			'idc_no' => $this->input->post('invoice_no'),
			'odc_no' => $outward_id, 
			'sid'=> $this->input->post('sid'),
    		'supplier_name' => $this->input->post('isupl_name'),
    		'supplier_address' => $this->input->post('customer_address'), 
	    	'ph_no' => $this->input->post('isupl_phone'), 
    		'gst_no' => $this->input->post('customer_gst'),
			'ph_no' => $this->input->post('isupl_phone'),
			'email_id' => $this->input->post('semail'), 
    		'did' => $this->input->post('driver_id'),
			'drivername' => $this->input->post('driver_name'),
			'driverphone' => $this->input->post('driver_phone') ,
			'contNo' => $this->input->post('cont_no'),
			'empid' => $this->input->post('employee'),
			'tod' => $this->input->post('tod'),
			'mop' => $this->input->post('mop'),
			'others' => $this->input->post('others'),
			'odate' =>$this->input->post('odate'),
			'idate' =>$sdate,
			'totaldays' =>$sgst=$this->input->post('totaldays'), 
			'date_time' => strtotime($date),
			'inwardcharge' =>$sgst=$this->input->post('rent_amount'),
			'wages' => $this->input->post('wages'),
			'cleaningcharge' => $this->input->post('cleaningcharge'),
			'vehtranscharge' => $this->input->post('vehtranscharge'), 
    		'untimedloading' => $this->input->post('untimedloading'),
			'totalamount'=>	$this->input->post('total_amount'), 
    		'discount' => $this->input->post('disc_amount'),
    		'totalrent' => $this->input->post('net_amount'),
			'total_items' => $this->input->post('total_items'),
			'amtpaid' => $this->input->post('amtpaid'),
			'advance' => $this->input->post('advance'),
			'balance' => $this->input->post('balance'), 
    		'user_id' => $this->session->userdata('id'),
			'total_balanceqty' => $totalbalqty
		);

		$this->db->trans_begin();
		
		$insert = $this->db->insert('inward', $data);
		$inward_id = $this->db->insert_id();

		$count_product = count($this->input->post('boxitem_name'));
    	for($x = 0; $x < $count_product; $x++) {
 
    		$outward_item_id = $this->input->post('boxitem_name')[$x]; 
    		$box_id = $this->input->post('boxitem_id')[$x]; 
    		$noofunits = $this->input->post('noofunits')[$x]; 
    		$itype = $this->input->post('itype')[$x];

    		$items = array(
    			'inward_id' => $inward_id,
    			'outward_item_id' => $outward_item_id,  
    			'itype' => $itype,
    		    'boxitem_id' => $box_id,
    		    'boxitem_name' => $this->input->post('bname')[$x], 
    			'type' => $this->input->post('type')[$x], 
				'units' => $this->input->post('units')[$x], 
    			'minrentaldays' => $this->input->post('minrentaldays')[$x],
    			'rate'=> $this->input->post('rate')[$x],  
    			'rateperday'=> $this->input->post('rateperday')[$x],
    		    'outunits' => $this->input->post('outunits')[$x],
				'noofunits' => $noofunits, 
    			'period' => $this->input->post('period')[$x],
    			'gperiod' => $this->input->post('grace_period')[$x],
				'totalrent' => $this->input->post('totamount')[$x],
    			'balanceqty' => $this->input->post('balanceqty')[$x]
    		);
 
    		   if($noofunits != 0) { 
				 	$sql = "UPDATE rentitems SET rentedqty = rentedqty - $noofunits, 
					    	availableqty = availableqty + $noofunits WHERE id = $box_id"; 
		    		
		    		$query = $this->db->query($sql); 
		    		
				    $insql = "UPDATE outward_item SET balanceqty = balanceqty - $noofunits, ndate = '$sdate'
				              WHERE outward_id = $outward_id AND id = $outward_item_id";
				    $query = $this->db->query($insql);
		
		    		$this->db->insert('inward_item', $items); 
    		   } 
    	}
    	
		$insql = "UPDATE outward SET total_balanceqty = $totalbalqty, ndate = '$sdate' WHERE id = $outward_id";
    	$query = $this->db->query($insql);
    	
    	$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
		$innonew = $company_Prefix['inno'];
    	
    	$this->incInvoiceNo($innonew);
    	
    	if ($this->db->trans_status() === FALSE)
    	{
    		$this->db->trans_rollback();
    	}
    	else
    	{
    		$this->db->trans_commit();
    		//$this->updatesms($inward_id);
    	}
    	
		return ($inward_id) ? $inward_id : false;
	}
	
	public function getBoxitemsRentQty($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM boxitems_data where boxitems_id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->result_array();
		} 
	}
	
	public function incInvoiceNo($invoicenew) {
		$invoicenew++;
		$data = array(
			'inno' => $invoicenew,
		);
		$update = $this->model_invoiceprefixs->update($data, 1);
	}
	
	public function updatesms($inward_id)
	{
		$cusdata = $this->getInwardData($inward_id);
		$idc_no= $cusdata['idc_no'];
		$total_items= $cusdata['total_items'];
		$contNo= $cusdata['contNo'];
		$totalamount= $cusdata['totalamount'];
		$balance= $cusdata['balance'];
		$mobile= $cusdata['ph_no'];
		$custname= $cusdata['supplier_name'];
		//sms start
		$authKey = "m249554A8hh0UI35bffa311";
		$mobileNumber = $mobile;
		$senderId = "KSK Cold storage";
	
		$message = urlencode("Dear ".$custname.", Your Inward No: ".$idc_no.",
				Total Items: ".$total_items.", Cont No: ".$contNo.",
				Total Amount: Rs.".$totalamount. ",Balance Amount: Rs.".$balance. " By KSK Cold Storage");
		$route = 4;
		//Prepare you post parameters
		$postData = array(
				'mobiles' => $mobileNumber,
				'message' => $message,
				'sender' => $senderId,
				'route' => $route
		);
	
		$url="http://api.msg91.com/api/v2/sendsms";
	
		$curl = curl_init();
		curl_setopt_array($curl, array(
				CURLOPT_URL => "$url",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $postData,
				CURLOPT_HTTPHEADER => array(
						"authkey: $authKey",
						"content-type: multipart/form-data"
				),
		));
	
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
	
		if ($err) {
			//echo "cURL Error #:" . $err;
			return false;
		} else {
			return true;
		}
		//sms end;
	}

	public function countPOOrderItem($inward_id)
	{
		if($inward_id) {
			$sql = "SELECT id FROM inward_item WHERE inward_id = ?";
			$query = $this->db->query($sql, array($inward_id));
			return $query->num_rows();
		}
	}

	public function getInwardDataForOutward($outward_id = null)
	{
		$sql = "SELECT * FROM inward WHERE odc_no = $outward_id"; 
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getInwardItemsForOutwardId($outward_id)
	{
        	$result = array();
        	$inward_data = $this->getInwardDataForOutward($outward_id);
			
        	$balance = 0;
        	
			foreach($inward_data as $k1 => $ot) {
    		
			$result['inward'][$k1] = $ot;
 
    		$inward_items = $this->model_inward->getInwardItemData($ot['id']);
    		
    		$balance = $balance + $ot['balance'];
    		
    		$inboxes = 0;
    		$insingles = 0; 
    		foreach($inward_items as $k => $v) {
    			 switch ($v['itype']) {
        			case 1:
        				$inboxes = $inboxes + $v['noofunits'];
        				break;
        			case 2:
        				$insingles = $insingles + $v['noofunits'];
        				break; 
    			 }
    		}
    		
 
    		$result['inward'][$k1]['inboxes'] = $inboxes;
    		$result['inward'][$k1]['insingles'] = $insingles; 

			}

			$result['inwardbalance'] = $balance;
			return $result;
    		//"select SUM(noofunits) as sboxes from inward_item where units=2 and inward_id=3 ";
	}
	
	public function countTotalPurchase()
	{
		$sql = "SELECT bill_no FROM inward";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	public function update($id)
	{
		if($id) {
	
		$sdate = $this->input->post('selected_date');
		$date = str_replace('/', '-', $sdate);
 
		$outward_no = $this->input->post('outward_no');
        $outward_id = $this->input->post('outward_id');
		
    	$totalbalqty  = $this->input->post('total_balanceqty');
		$data = array(
			'idc_no' => $this->input->post('invoice_no'),
			'odc_no' => $outward_id, 
			'sid'=> $this->input->post('sid'),
    		'supplier_name' => $this->input->post('isupl_name'),
    		'supplier_address' => $this->input->post('customer_address'), 
			'ph_no' => $this->input->post('isupl_phone'), 
    		'gst_no' => $this->input->post('customer_gst'),
			'ph_no' => $this->input->post('isupl_phone'),
			'email_id' => $this->input->post('semail'), 
    		'did' => $this->input->post('driver_id'),
			'drivername' => $this->input->post('driver_name'),
			'driverphone' => $this->input->post('driver_phone') ,
			'contNo' => $this->input->post('cont_no'),
			'empid' => $this->input->post('employee'),
			'tod' => $this->input->post('tod'),
			'mop' => $this->input->post('mop'),
			'others' => $this->input->post('others'),
			'odate' =>$this->input->post('odate'),
			'idate' =>$sdate,
			'totaldays' =>$sgst=$this->input->post('totaldays'), 
			'date_time' => strtotime($date),
			'inwardcharge' =>$sgst=$this->input->post('rent_amount'),
			'wages' => $this->input->post('wages'),
			'cleaningcharge' => $this->input->post('cleaningcharge'),
			'vehtranscharge' => $this->input->post('vehtranscharge'), 
    		'untimedloading' => $this->input->post('untimedloading'),
			'totalamount'=>	$this->input->post('total_amount'), 
    		'discount' => $this->input->post('disc_amount'),
    		'totalrent' => $this->input->post('net_amount'),
			'total_items' => $this->input->post('total_items'),
			'amtpaid' => $this->input->post('amtpaid'),
			'advance' => $this->input->post('advance'),
			'balance' => $this->input->post('balance'), 
    		'user_id' => $this->session->userdata('id'),
			'total_balanceqty' => $totalbalqty
		);

		$this->db->trans_begin();
			
		$this->db->where('id', $id);
		$update = $this->db->update('inward', $data);
 
		if($update == false)
		return false;
 
		$get_order_item = $this->getInwardItemData($id);
		foreach ($get_order_item as $k => $v) {  
			$box_id = $v['boxitem_id'];
			$noofunits = $v['noofunits']; 
			$itype = $v['itype']; 
			$outward_item_id = $v['outward_item_id'];
			
			if($itype == 1) {
				$get_box_item = $this->getBoxitemsRentQty($box_id);
					foreach ($get_box_item as $k => $v) {   
						$product_id = $v['rentitem_id'];
						$qty = $v['qty']; 
						
						$totunits = $noofunits * $qty;
						
						$sql = "UPDATE rentitems SET rentedqty = rentedqty + $totunits, 
						    	availableqty = availableqty - $totunits WHERE id = $product_id";
						
					    $query = $this->db->query($sql);
				}
			} else { 
				$sql = "UPDATE rentitems SET rentedqty = rentedqty + $noofunits, 
				    	availableqty = availableqty - $noofunits WHERE id = $box_id";
				
			   	$query = $this->db->query($sql);
			}
			
			    $insql = "UPDATE outward_item SET balanceqty = balanceqty + $noofunits 
			              WHERE outward_id = $outward_id AND id = $outward_item_id";
			    $query = $this->db->query($insql);
		}
		
			$insql = "UPDATE outward SET total_balanceqty = $totalbalqty WHERE id = $outward_id";
    		$query = $this->db->query($insql);
    

			// now remove the order item data 
			$this->db->where('inward_id', $id);
			$this->db->delete('inward_item');
		
		$count_product = count($this->input->post('boxitem_name'));
    	for($x = 0; $x < $count_product; $x++) {
 
    		$outward_item_id = $this->input->post('boxitem_name')[$x]; 
    		$box_id = $this->input->post('boxitem_id')[$x]; 
    		$noofunits = $this->input->post('noofunits')[$x]; 
    		$itype = $this->input->post('itype')[$x];

    		$items = array(
    			'inward_id' => $id,
    			'outward_item_id' => $outward_item_id,  
    			'itype' => $itype,
    		    'boxitem_id' => $box_id,
    		    'boxitem_name' => $this->input->post('bname')[$x], 
    			'type' => $this->input->post('type')[$x], 
				'units' => $this->input->post('units')[$x], 
    			'minrentaldays' => $this->input->post('minrentaldays')[$x],
    			'rate'=> $this->input->post('rate')[$x],  
    			'rateperday'=> $this->input->post('rateperday')[$x],
    		    'outunits' => $this->input->post('outunits')[$x],
				'noofunits' => $noofunits, 
    			'period' => $this->input->post('period')[$x],
    			'gperiod' => $this->input->post('grace_period')[$x],
				'totalrent' => $this->input->post('totamount')[$x],
    			'balanceqty' => $this->input->post('balanceqty')[$x]
    		);
 
    		 if($noofunits != 0) {
	    		if($itype == 1) {
		    		$get_box_item = $this->getBoxitemsRentQty($box_id);
						foreach ($get_box_item as $k => $v) {  
							$product_id = $v['rentitem_id'];
							$qty = $v['qty']; 
							
							$totunits = $noofunits * $qty;
							
							$sql = "UPDATE rentitems SET rentedqty = rentedqty - $totunits, 
							    	availableqty = availableqty + $totunits WHERE id = $product_id";
							
						 
							$query = $this->db->query($sql);
					}
	    		} else { 
				 	$sql = "UPDATE rentitems SET rentedqty = rentedqty - $noofunits, 
					    	availableqty = availableqty + $noofunits WHERE id = $box_id"; 
		    		 
		    		$query = $this->db->query($sql);
	    		}
	  		
			    $insql = "UPDATE outward_item SET balanceqty = balanceqty - $noofunits 
			              WHERE outward_id = $outward_id AND id = $outward_item_id";
			    $query = $this->db->query($insql);
	
	    		$this->db->insert('inward_item', $items); 
    		 }
    	}
    	
		$insql = "UPDATE outward SET total_balanceqty = $totalbalqty WHERE id = $outward_id";
    	$query = $this->db->query($insql);
    	
		
    	if ($this->db->trans_status() === FALSE)
    	{
    		$this->db->trans_rollback();
    		return false;
    	}
    	else
    	{
    		$this->db->trans_commit();
    	}
			return true;
		}
	}

 
	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('inward');

			$this->db->where('inward_id', $id);
			$delete_item = $this->db->delete('inward_item');
			return ($delete == true && $delete_item) ? true : false;
		}
	}
}