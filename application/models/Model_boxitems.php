<?php 

class Model_boxitems extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the brand data */
	public function getBoxitemsData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM boxitems where id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM  boxitems Order BY Name ASC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
 
	public function getBoxitems($boxitems_id = null)  
	{
		if(!$boxitems_id) {
			return false;
		}

		$sql = "SELECT B2.name AS bname, B1.* FROM boxitems_data AS B1 inner join boxitems
 				B2 ON B1.boxitems_id=B2.id where boxitems_id = $boxitems_id";
		//echo $sql;
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getBoxedRentItemData($product_id, $boxitems_id)  
	{
		if(!$boxitems_id) {
			return false;
		}

		$sql = "SELECT B1.*, B2.qty  FROM rentitems AS B1 inner join boxitems_data
 				B2 ON B2.rentitem_id=B1.id WHERE B1.id = $product_id AND B2.boxitems_id = $boxitems_id";
 		$query = $this->db->query($sql);
		return $query->row_array();
	}

	function check_product_name($id = '', $product_name) {
		$this->db->where('name', $product_name);
		if($id) {
			$this->db->where_not_in('id', $id);
		}
        return $this->db->get('boxitems')->num_rows();
	}
	

	public function getActiveProductData()
	{
		$sql = "SELECT * FROM boxitems WHERE availability = ? ORDER BY id DESC";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	} 
	
	public function create()
	{ 
		$data = array(
			'code' => $this->input->post('boxitem_code'), 
			'name' => $this->input->post('boxitem_name'),
			'minrentaldays' => $this->input->post('minrentaldays'),  
			'rate' => $this->input->post('rate'), 
			'rateperday' => $this->input->post('rateperday')
		);

		$this->db->trans_begin();
		
		$insert = $this->db->insert('boxitems', $data);
		$boxitems_id = $this->db->insert_id();

		$count_product = count($this->input->post('rentitem_code'));
    	for($x = 0; $x < $count_product; $x++) {  
    		$items = array(
    			'boxitems_id' => $boxitems_id, 
    			'rentitem_id' => $this->input->post('rentitem_code')[$x], 
    			'rentitem_name' => $this->input->post('rname')[$x],
    			'qty' => $this->input->post('qty')[$x], 
    			'type' => $this->input->post('type')[$x] 
    		);  
    		$this->db->insert('boxitems_data', $items);
    		
    	} 
     
		$company_Prefix = $this->model_invoiceprefixs->getPrefixData(1);
		$bcode= $company_Prefix['bcode'];
		$this->bcodenewno($bcode);
 
    	if ($this->db->trans_status() === FALSE)
    	{
    		$this->db->trans_rollback();
    	}
    	else
    	{
    		$this->db->trans_commit(); 
    	}
    	
		return ($boxitems_id) ? $boxitems_id : false;
	}
	
	public function bcodenewno($bcode) {

		$bcode++;
		$data = array(
			'bcode' => $bcode,
		);
		$update = $this->model_invoiceprefixs->update($data, 1);
	}
	
	public function update($data,$id)
	{
		if($data && $id) {
			$this->db->trans_begin();
			
			$this->db->where('id', $id);
			$update = $this->db->update('boxitems', $data);
			
			if($update == false)
				return false;
			
			// now remove the order item data 
			$this->db->where('boxitems_id', $id);
			$this->db->delete('boxitems_data');
			
			$count_product = count($this->input->post('rentitem_code'));
	    	for($x = 0; $x < $count_product; $x++) {  
	    		$items = array(
	    			'boxitems_id' => $id, 
	    			'rentitem_id' => $this->input->post('rentitem_code')[$x], 
	    			'rentitem_name' => $this->input->post('rname')[$x],
	    			'qty' => $this->input->post('qty')[$x], 
	    			'type' => $this->input->post('type')[$x]
	    		);  
	    		$this->db->insert('boxitems_data', $items); 
	    	}
    	
			if ($this->db->trans_status() === FALSE)
	    	{
	    		$this->db->trans_rollback();
	    		return false;
	    	}
	    	else
	    	{
	    		$this->db->trans_commit();
	    		return true;
	    	} 
		}
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$product_data = $this->getProductImage($id);
			if($product_data['image'] != "<p>You did not select a file to upload.</p>")
				unlink($product_data['image']);
			$delete = $this->db->delete('boxitems');
			return ($delete == true) ? true : false;
		}
	}

	public function getProductImage($id = null)
	{
		if($id) {
			$sql = "SELECT image FROM boxitems where id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
		return "";
	}
	
	public function countTotalProducts()
	{
		$sql = "SELECT id FROM boxitems";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

}