<?php 

class Model_ledger extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get active brand infromation */
	public function getActiveCategroy()
	{
		$sql = "SELECT * FROM ledger WHERE active = ?";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}
	
	public function getAllActiveLedger()
	{
		$sql = "SELECT * FROM ledger WHERE active = ? "; 
		$query = $this->db->query($sql,array(1));
		return $query->result_array();
	}
	public function getActiveLedgernameData($id = null)
	{
	
		if($id) {
			$sql = "SELECT * FROM ledger_name where id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM ledger_name";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getActiveLedger($under)
	{
		$sql = "SELECT * FROM ledger WHERE active = ? and under in (". $under .")"; 
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}
	
	public function getActiveLedgerCrDr($expense_type)
	{
		$sql = "SELECT * FROM ledger WHERE active = ? and expense_type in (". $expense_type .")"; 
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}
	
	function check_ledger_name($id = '', $edit_ledger_name) {
		$this->db->where('name', $edit_ledger_name);
		if($id) {
			$this->db->where_not_in('id', $id);
		}
		return $this->db->get('ledger')->num_rows();
	}
	
	/* get the brand data */
	public function getLedgerData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM ledger WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM ledger";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function create($data)
	{
		if($data) {
			$insert = $this->db->insert('ledger', $data);
			return ($insert == true) ? true : false;
		}
	}

	public function update($data, $id)
	{
		if($data && $id) {
			$this->db->where('id', $id);
			$update = $this->db->update('ledger', $data);
			
			return ($update == true) ? true : false;
		}
		
	}

	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('ledger');
			return ($delete == true) ? true : false;
		}
	}

}