<?php 

class Model_invoicereports extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function get_report_list($filter = array())
    { 
        if(count($filter) > 0)
        {
            if(!empty($filter['from_date']) && !empty($filter['to_date']))
            {
                $from_date = $filter['from_date'];
                $to_date = $filter['to_date'];

				$sql = "select * FROM invoice WHERE str_to_date(sdate, '%d/%m/%Y')
					    BETWEEN str_to_date('$from_date','%d/%m/%Y') AND 
						str_to_date('$to_date', '%d/%m/%Y')"; 

				$query = $this->db->query($sql);

            }
			else
			{
				$sql = "SELECT * FROM invoice Order By id DESC ";
				$query = $this->db->query($sql);
				
			}
        }
		return $query->result_array();
    }
}