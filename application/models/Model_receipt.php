<?php 

class Model_receipt extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/* get the orders data */
	public function getReceiptData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM ereciept WHERE id = ?";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}

		$sql = "SELECT * FROM ereciept ORDER BY id DESC";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getReceiptDataByLedger()
	{
		$sql = "SELECT T1.id,T2.receipt_id, T1.receipt_no,T1.sdate,T1.user_id, T1.others,
				    SUM(CASE
				        WHEN T2.under = 1 THEN T2.amount
				        ELSE 0
				    END) AS 'income',
				    SUM(CASE
				        WHEN T2.under = 2 THEN T2.amount
				        ELSE 0
				    END) AS 'expense',
				    SUM(CASE
				        WHEN  T2.under = 3 THEN T2.amount
				        ELSE 0
				    END) AS 'capital' 
				FROM ereciept AS T1
				 INNER JOIN ereciept_item AS T2 ON T1.id = T2.receipt_id group by T1.receipt_no";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getInsertData($id = null)
	{
		if($id) {
			$sql = "SELECT * FROM ereciept ORDER BY id DESC";
			$query = $this->db->query($sql, array($id));
			return $query->row_array();
		}
	}
	// get the orders item data
	public function getOrdersItemData($order_id = null)
	{
		if(!$order_id) {
			return false;
		}

		$sql = "SELECT * FROM ereciept_item WHERE receipt_id = ?";
		$query = $this->db->query($sql, array($order_id));
		return $query->result_array();
	}

	public function create()
	{
	$user_id = $this->session->userdata('id');
	$sdate = $this->input->post('selected_date');
	$date = str_replace('/', '-', $sdate);
/*	if ($datetime === FALSE) {
	  $datetime = strtotime(str_replace('/', '-', $date));
	}*/

	$this->db->trans_begin();
	
	$data = array(
			'receipt_no' =>$this->input->post('receipt_no'),
			'sdate' =>$this->input->post('selected_date'),
			'others' => $this->input->post('others') ,
			'user_id' => $user_id
			
    	);
	
		$insert = $this->db->insert('ereciept', $data);
		$receipt_id = $this->db->insert_id();
		//$sdate=$this->input->post('selected_date');
		//$this->load->model('model_products');
		$count_product = count($this->input->post('product'));
    	for($x = 0; $x < $count_product; $x++) {
    		$items = array(
    			'receipt_id' => $receipt_id,
				'sdate' =>$sdate,
    			'ledger_id' => $this->input->post('product')[$x],
    			'under' => $this->input->post('under_value')[$x],
				'amount' => $this->input->post('amount')[$x],
				'date_time' => strtotime($date),
    	);
		$this->db->insert('ereciept_item', $items);

    	}

    	if ($this->db->trans_status() === FALSE)
    	{
    		$this->db->trans_rollback();
    	}
    	else
    	{
    		$this->db->trans_commit();
    	//	$this->updatesms($order_id);
    	}
		return ($receipt_id) ? $receipt_id : false;
	}

	public function countOrderItem($order_id)
	{
		if($order_id) {
			$sql = "SELECT * FROM ereciept_item WHERE receipt_id = ?";
			$query = $this->db->query($sql, array($order_id));
			return $query->num_rows();
		}
	}
	public function countTotalEstimate()
	{
		$sql = "SELECT * FROM ereciept";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	public function update($id)
	{
	
	// m-d-Y;
	//$date= date("m-d-Y", strtotime($var) ); working but reverse 2-08-2018 taking as 2nd month but 26/08/2018 working fine
	/*$date = $this->input->post('selected_date');*/
		$user_id = $this->session->userdata('id');
	$sdate = $this->input->post('selected_date');
		$date = str_replace('/', '-', $sdate);
		
	/*$datetime = strtotime($date);*/
/*	if ($datetime === FALSE) {

	  $datetime = strtotime(str_replace('/', '-', $date));
	}*/

		if($id) {
			$user_id = $this->session->userdata('id');
			// fetch the order data 
			$custtype= $this->input->post('custType');

	 $data = array(
			'receipt_no' =>$this->input->post('receipt_no'),
			'sdate' =>$this->input->post('selected_date'),
			'others' => $this->input->post('others') ,
			'user_id' => $user_id
			
    	);
		
			$this->db->where('id', $id);
			$update = $this->db->update('ereciept', $data);


			// now remove the order item data 
			$this->db->where('receipt_id', $id);
			$this->db->delete('ereciept_item');
			//$receipt_id=$this->input->post('receipt_no');
			$sdate=$this->input->post('selected_date');
			// now decrease the product qty
			$count_product = count($this->input->post('product'));
	    	for($x = 0; $x < $count_product; $x++) {
	    		$items = array(
	    		'receipt_id' => $id,
				'sdate' =>$sdate,
    			'ledger_id' => $this->input->post('product')[$x],
    			'under' => $this->input->post('under_value')[$x],
				'amount' => $this->input->post('amount')[$x],
				'date_time' => strtotime($date),
	    		);
	    		$this->db->insert('ereciept_item', $items);
	    	}

	    	if ($this->db->trans_status() === FALSE)
	    	{
	    		$this->db->trans_rollback();
	    		return false;
	    	}
	    	else
	    	{
	    		$this->db->trans_commit();
	    		return true;
	    	}
		}
	}



	public function remove($id)
	{
		if($id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('ereciept');

			$this->db->where('receipt_id', $id);
			$delete_item = $this->db->delete('ereciept_item');
			return ($delete == true && $delete_item) ? true : false;
		}
	}

	public function countTotalPaidOrders()
	{
		$sql = "SELECT * FROM ereciept WHERE paid_status = ?";
		$query = $this->db->query($sql, array(1));
		return $query->num_rows();
	}

}