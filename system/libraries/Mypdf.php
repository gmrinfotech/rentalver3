<?php

require_once(BASEPATH."libraries/tcpdf/tcpdf.php");

class CI_MYPDF extends TCPDF {
	
	public $page_counter = 1;
	
    public function Header() {
        $headerData = $this->getHeaderData();
        $this->SetFont('times', '', 12);
        $this->writeHTML($headerData['string']);
    }
	public function Footer() {
		if ($this->last_page == true) {
			$this->setCellPaddings(1, 1, 0, 0);

			$this->SetY(-20);
			// Set font
			$this->SetFont('times', '', 12); 
			$this->writeHTMLCell(40, 15, '', '', 'Customer Signature' , 0, 0, 0, true, 'L',true);
            $this->writeHTMLCell(140, 5, '', '', $this->forname , 0, 0, 0, true, 'R',true);
			
		}
	}
	
	public function createinvoicepdf($id, $pdf, $order_data, $outward_data, $orders_items, $company_info, $buyeraddr, 
					$title,$invoice) { 
		$company_name= $company_info['company_name'];
		$company_address= $company_info['address'];
		$company_phone= $company_info['phone'];
 		$company_gst= $company_info['company_gstno'];
		$companyaddrArray = explode(',', $company_address);
		//$buyeraddr= $this->model_scustomers->getSupplierData($order_data['sid']);
	 	if (empty($buyeraddr)) {
			$buyeraddr[] = $outward_data['supplier_address'];
	
		}
		//$buyeraddr = $order_data['customer_address'];
		//$buyeraddrArray = explode(',', $buyeraddr);
		$numCItems = count($companyaddrArray);
		$numBItems = count($buyeraddr);
	
		$pdf->forname = $company_info['forname'];
 
		$tbl = '';
		$tbl .= '
			<table cellspacing="0" cellpadding="1" border="0">
			    <tr> 
				<td width="20%">
					<img src="assets/images/logo.jpg" style="width:75px;height:60px;">
				</td>
				<td width="80%">
					<div style="text-align:right;font-size:20px;"><br>'.$title.'</div>
				</td> 
				</tr>
			</table>
			<table cellspacing="0" cellpadding="7" border="1">
			    <tr>
			        <td style="line-height: 150%;"><font size="11px"><b>'.$company_name.'</b><br/>';
		$i = 0;
		foreach ($companyaddrArray as $string) {
			if(++$i === $numCItems) {
				$tbl .= $string.'<br />';
			} else {
				$tbl .= $string.',<br />';
			}
		}
	
		$tbl .= '<b>Phone</b> : +91 '.$company_phone;
		if($company_gst!=null)
		{
			$tbl .='<br/><b>GST No : </b>'.$company_gst;
		}
		$tbl .= '</font></td>
			        <td style="line-height: 150%;"><b>Customer</b>
	  					<br/><font size="11px">'.$order_data['supplier_name'].'<br/>';
		$j = 0;
		foreach ($buyeraddr as $string) {
			if(++$j === $numBItems) {
				$tbl .= $string.'<br />';
			} else {
				$tbl .= $string.',<br />';
			}
		}
		if($order_data['supplier_phone']!=null)
		{
			$tbl .='Phone No:'.$order_data['supplier_phone'];
		}
		
		$tbl .='</font></td>';
		$tod = "By Transport";
		if($outward_data['tod'] == 2) {
			$tod = "By Hand";
		}

		$tbl .= '
	        	<td style="line-height: 150%;font-size:14px;"><b>Out No &nbsp;&nbsp;&nbsp;: </b>'.$outward_data['odc_no'].'
			            <br/><b>Out Date : </b> '.$outward_data['sdate'].'
			            <br/><b>Bill No : </b> '.$order_data['invoice_id'].'
			            <br/><b>Bill Date : </b> '.$order_data['sdate'].'
			            <br/><b>Terms of Delivery</b> : '.$tod.'
						<br/><b>Truck No</b> : '.$outward_data['contNo'].' 
						<br/><b>Driver Name</b> : '.$outward_data['drivername'].'
				</td>
		    </tr>
		</table>';
	
		$pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$tbl, $tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+43, PDF_MARGIN_RIGHT);
	
		// set header and footer fonts
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		// set margins
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER+1);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-60);
	
		// set auto page breaks
		//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
	
		// add a page
		$pdf->AddPage();
	
		$pdf->SetFont('times', '', 10);
	
		// NON-BREAKING ROWS (nobr="true")
		$pdf->SetFont('times', '', 9);
		
		if(!$invoice) {
			$header = array('Out No', 'Date','Box Items','Single Items', 'Rent', 'Other Amts','Paid','Balance'); 
			$tbl = '';
			$w = array(20, 20, 20, 25, 28, 20,26,21); 
			$tbl .= $this->ColoredOutwardTable($pdf,$header,$outward_data,$w);
		}
		
		$tbl .= '
		<table>
		 <tbody cellspacing="0" cellpadding="7">'; 
		$tbl .= '</tbody></table>';
 
		//$header = array('Product name', 'HSN/SAC Code', 'Rate', 'Quantity','Amount','Dis%','GST%','CGST %','SGST %','Total Amount');
	
		if($invoice) {
			$iheader = array('In No', 'Date','Box Items','Single Items', 'Rent', 'Paid','Balance');
			$w = array(24, 25, 25, 25, 28, 28,25); 	 
		} else {
			$iheader = array('In No', 'Date','Box Items','Single Items', 'Rent', 'Other Amts','Paid','Balance');
			$w = array(20, 20, 20, 25, 28, 20,26,21);  
		}
		$tbl = $this->ColoredInvTable($pdf,$iheader,$orders_items,$w,$invoice); 
		$pdf->writeHTML($tbl, true, false, false, false, '');
		
		$balance = $order_data['itotalcbal'] > 0 ? '-'.$order_data['itotalcbal'] : ($order_data['balance'] > 0 ? $order_data['balance'] : 0) ;
		$tbl = '';
		$tbl .= '
		 <table cellspacing="0" cellpadding="5" border="1">';
		$tbl .= '<tr>
		
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Total Rent </b></td>
		 <td style="border:1px thin #666"; align="right">'.number_format($a=$order_data['itotalamt'],2).'</td>
		 </tr>'; 
		 if($invoice) {
		 	$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>GST amount</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($d=$order_data['gst'],2).'</td>
		 </tr> 
		 <tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Net amount</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($a+$d,2).'</td>
		 </tr> 
		  <tr>
		 <td colspan="6" style="border:1px thin #666"; align="left"><b>Amount In Words (INR) : '. $pdf->getIndianCurrency(abs($a+$d)).'</b></td>
		 </tr>';
		 } else {
		$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Veh Transfer Charge</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($b=$order_data['vehtranscharge'],2).'</td>
		 </tr>';
		$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Miscellaneous Charges</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($c=$order_data['cleaningcharge'] + $order_data['itotalothers'],2).'</td>
		 </tr>';
		 $tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Sub Amount </b></td>
		 <td style="border:1px thin #666"; align="right">' .number_format($a+$b+$c,2).'</td>
		 </tr>';
		 	$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Total Outward + Inward Paid Amount</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($order_data['iamtpaid'],2).'</td>
		 </tr>';

		$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Discount Amount</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($order_data['disc_amount'],2).'</td>
		 </tr>';
	
		$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Total Amount</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($order_data['net_amount'],2).'</td>
		 </tr>
         <tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Amount Paid</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($order_data['amtpaid'],2).'</td>
		 </tr>
		 <tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Balance Amount</b></td>
		 <td style="border:1px thin #666"; align="right">'.number_format($balance,2).'</td>
		 </tr> 
		 <tr>
		 <td colspan="6" style="border:1px thin #666"; align="left"><b>Amount In Words (INR) : '. $pdf->getIndianCurrency(abs($order_data['net_amount'])).'</b></td>
		 </tr>';
		  }
		$tbl .= '</table>';
		$pdf->writeHTML($tbl, true, false, false, false, ''); 
	}
	
	public function createreceiptbyinvpdf($id, $pdf, $order_data, $company_info, $buyeraddr, $printTitle, $invtitle, $billno)
	{
		if(empty($printTitle)) {
			$printTitle = "Receipt Copy";
		}
	
		$company_name= $company_info['company_name'];
		 
		$order_date = date('d/m/Y', $order_data['date_time']);
	
		$pdf->forname = $company_info['forname'];
		$mop = "Cash";
		switch ($order_data['mop']) {
			case "1":
				$mop = "Cash";
				break;
			case "2":
				$mop = "Cheque";
				break;
			case "3":
				$mop = "Card";
				break;
			case "4":
				$mop = "Credit";
				break;
			case "5":
				$mop = "Netbanking";
				break;
			case "6":
				$mop = "UPI";
				break;
				
			default:
				$mop = "Cash";
		} 			
		$image = "";
		if (file_exists($company_info['image']))
			$image = $company_info['image'];
	
		$tbl ='';
		$tbl .= '
			<table cellspacing="0" cellpadding="1" border="0">
			    <tr>
				<td width="20%">
					<img src="'.$image.'" alt="" style="width:150px;height:60px;">
				</td>
				<td width="60%">
					<div style="text-align:center;font-size:20px;"><br>'.$printTitle.'</div>
				</td>
	
				</tr>
			</table>
			<table cellspacing="0" cellpadding="7" border="1">
			    <tr>
			        <td style="line-height: 150%;font-size:14px;"><b>'.$company_name.'</b>
			        <br/>';
	
		if($company_info['phone'] != null)
		{
			$tbl .='<b>Phone : </b>'.$company_info['phone'].'<br />';
		}
		if(($company_info['company_gstno'] != null) && ($company_info['company_gstno'] != '0'))
		{
			$tbl .='<b>GST NO : </b>'.$company_info['company_gstno'];
		}
		$tbl .= '</td>
			        <td style="line-height: 150%;"><b>Payee</b>
	  					<br/><font size="11px"><b>Name : </b>'.$order_data['cname'].'<br/>';
	
		if(isset($order_data['cmob']) && $order_data['cmob'] != null)
		{
			$tbl .='<b>Phone : </b>'.$order_data['cmob'].'<br />';
		}

		$tbl .='</font></td>';
	
		$tbl .= '<td style="line-height: 150%;font-size=14px "><b>'.$invtitle.'</b>'.$order_data['payment_id'].'
				   <br/><b>Date:</b> '.$order_data['pdate'].
				   '<br/><b>Payment Mode</b> :'.$mop;
 		if($order_data['others'] != null)
		{
			$tbl .='<br/><b>Details : </b>'.$order_data['others'];
		}
	
		$tbl .= ' </td>
			    </tr>
			</table>';
	
		$pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$tbl, $tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+23, PDF_MARGIN_RIGHT);
	
		// set header and footer fonts
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		// set margins
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER+1);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-60);
	
		// set auto page breaks
		//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
	
		// add a page
		$pdf->AddPage();
	
		$pdf->SetFont('times', '', 10);
	
		// NON-BREAKING ROWS (nobr="true")
		$pdf->SetFont('times', '', 9);
		$tbl = '';
		$tbl .= '
		<table>
		 <thead>
		    <tr>
		      <th style="width:110px;text-align:center;">Invoice Number</th>
			  <th style="width:100px;text-align:center;">Old Balance</th>
		      <th style="width:70px;text-align:center;">Payment</th>
		      <th style="width:30px;text-align:center;">New Balance</th>
			</tr>
		 </thead>
		 <tbody cellspacing="0" cellpadding="7">';
		$tbl .= '</tbody></table>';
	
		$header = array('Invoice Number', 'Old Balance', 'Payment', 'New Balance');
		
		$w = array(60, 42, 45, 33);
		
		$tbl = $pdf->DrawHeader($pdf,$header, $w);
		$pdf->last_page = true;

		$pdf->Multicell($w[0], 10, $order_data[$billno], 'L', 'C', false, 0);
		$pdf->Multicell($w[1], 10, number_format($order_data['cbal'],2), 'L', 'R', false, 0);
		$pdf->Multicell($w[2], 10, number_format($order_data['cpaid'],2), 'L', 'R', false, 0);
		$pdf->Multicell($w[3], 10, number_format($order_data['cbal']- $order_data['cpaid'],2), 'LR', 'R', false, 0);
		$pdf->Ln();
		$pdf->Cell(array_sum($w), 0, '', 'T');

	    $pdf->writeHTML($tbl, true, false, false, false, '');
		$tblf = '';
		$tblf .= '
		 <table cellspacing="0" cellpadding="5" border="1">';
	
		$tblf .= '
		 <tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Total Payment</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($order_data['cpaid'],2).'</td>
		 </tr>
	
		 <tr>
		 <td colspan="6" style="border:1px thin #666"; align="left"><b>Amount In Words (INR) : '. $pdf->getIndianCurrency($order_data['cpaid']).'</b></td>
		 </tr>
		 </table>';
		$pdf->writeHTML($tblf, true, false, false, false, '');
	}
	
	public function createpdf($id, $pdf, $order_data, $orders_items, $company_info, $buyeraddr,$title,$dc_no,$outward) {
		
		$order_date = date('d/m/Y', $order_data['date_time']);
		$company_name= $company_info['company_name'];
		$company_address= $company_info['address'];
		$company_phone= $company_info['phone'];
		$company_gst= $company_info['company_gstno'];
		$companyaddrArray = explode(',', $company_address);
		//$buyeraddr= $this->model_scustomers->getSupplierData($order_data['sid']);
	 
		if (empty($buyeraddr)) {
			$buyeraddr[] = $order_data['supplier_address']; 
		}
		
		$pdf->forname = $company_info['forname'];
 		$pdf->last_page = true;
		$numCItems = count($companyaddrArray);
		$numBItems = count($buyeraddr);
		
		$tbl = '';
		$tbl .= '
			<table cellspacing="0" cellpadding="1" border="0">
			    <tr> 
				<td width="20%">
					<img src="assets/images/logo.jpg" style="width:75px;height:60px;">
				</td>
				<td width="80%">
					<div style="text-align:right;font-size:20px;"><br>'.$title.'</div>
				</td> 
				</tr>
			</table>
			<table cellspacing="0" cellpadding="7" border="1">
			    <tr>
			        <td style="line-height: 150%;"><b>'.$company_name.'</b><br/>';
						$i = 0;
		        		foreach ($companyaddrArray as $string) {
		        			if(++$i === $numCItems) {
		        				$tbl .= $string.'<br />';
		        			} else {
		        				$tbl .= $string.',<br />';
		        			}
		        		}
		        		
			        $tbl .= '<b>Phone</b> : +91 '.$company_phone.'
			                <br/><font size="10px"><b>GST No</b> : '.$company_gst.'</font></td>
			        <td style="line-height: 150%;"><b>Customer</b>
	  					<br/><font size="10px">'.$order_data['supplier_name'].'<br/>';
			       		$j = 0;
				        foreach ($buyeraddr as $string) {
				        	if(++$j === $numBItems) {
		        				$tbl .= $string.'<br />';
				        	} else {
	        					$tbl .= $string.',<br />';
	        				}
		        		}
		        		if($order_data['ph_no']!=null)
						{
							$tbl .='<b>Phone No : </b>'.$order_data['ph_no'].'<br/>';
						}
						if($outward) {
						if($order_data['sitelocation']!=null)
						{
						$tbl .='<b>Site Location : </b>'.$order_data['sitelocation'].'<br/>';
						}
						}
						if($order_data['gst_no']!=null)
						{
							$tbl .='<b>GST No : </b>'.$order_data['gst_no'];
						}
	        		$tbl .='</font></td>';
		        	$tod = "By Hand";
		        	if($order_data['tod'] == 2) {
		        		$tod = "By Courier";
		        	} 
		        	
		        	$mop = "Cash";
		        	switch ($order_data['mop']) {
		        		case "1":
		        			$mop = "Cash";
		        			break;
		        		case "2":
		        			$mop = "Cheque";
		        			break;
		        		case "3":
		        			$mop = "Card";
		        			break;
	        			case "4":
	        				$mop = "Credit";
	        				break;
	        			case "5":
	        				$mop = "UPI";
	        				break;
	        			case "6":
	        				$mop = "Netbanking";
	        				break;
		        		default:
		        			$mop = "Cash";
		        	}
		        	
					$tbl .= '
		        	<td style="line-height: 150%;font-size:13px;"><b>Lot No : </b>'.$dc_no;
		        	     if($outward) {
				        	 $tbl .= '<br/><b>Outward Date : </b> '.$order_data['sdate'];
				          } else {
				          	 $tbl .= '<br/><b>Outward Date : </b> '.$order_data['odate'].'
				          	          <br/><b>Inward Date : </b> '.$order_data['idate']; 	
				          }
				          $tbl .= '<br/><b>Terms of Delivery</b> : '.$tod.'
							<br/><br/><b>Truck No</b> : '.$order_data['contNo'].'
							<br/><b>Driver Name</b> : '.$order_data['drivername'];
							
							if($order_data['others'] != "") {
							$tbl .= '<br/><b>Labour</b> : '.$order_data['others'];
							}
					$tbl .='</td>
			    </tr>
			</table>';
		
		$pdf->setHeaderData($ln='', $lw=0, $ht='', $hs=$tbl, $tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+45, PDF_MARGIN_RIGHT);

		// set header and footer fonts
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER+1);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER-60);
		
		// set auto page breaks
		//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// add a page
		$pdf->AddPage();
	
		$pdf->SetFont('times', '', 10);
		
		// NON-BREAKING ROWS (nobr="true")
		$pdf->SetFont('times', '', 9);
		$tbl = ''; 
		$tbl .= ' 
		<table>
		 <tbody cellspacing="0" cellpadding="7">';

		$tbl .= '</tbody></table>';  
		
		//$header = array('Product name', 'HSN/SAC Code', 'Rate', 'Quantity','Amount','Dis%','GST%','CGST %','SGST %','Total Amount');

		if($outward) {
			$header = array('Item Type','Box Name','Item Name','No of Units', 'Rent', 'Est Period', 'Est Rent');
			$w = array(20, 32, 47, 20, 17, 21, 23);
		} else {
			$header = array('Item Type','Box Name','Item Name','Out Units', 'In Units', 'Rent', 'G-Period', 'Period', 'Rent');
			$w = array(20, 30, 35, 15, 15, 15, 15, 15, 20);
		}
		$tbl = $this->ColoredTable($pdf,$header,$orders_items,$w, $outward);
		
		$pdf->writeHTML($tbl, true, false, false, false, '');
		$tbl = '';
		$tbl .= '
		 <table cellspacing="0" cellpadding="5" border="1">';
		 if($outward) { 
		 $tbl .= '<tr> 
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Total Est Rent</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($esr=$order_data['outwardcharge'],2).'</td>
		 </tr>';  
		} else {
			$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Inward Charge</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($ic=$order_data['inwardcharge'],2).'</td>
		 </tr>';
		}
		$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Vehicle Transfer Charge</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($vc=$order_data['vehtranscharge'],2).'</td>
		 </tr>';
		$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Total Wages</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($tw=$order_data['wages'],2).'</td>
		 </tr>';
		if(!$outward) {
		$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Cleaning Charge</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($cc=$order_data['cleaningcharge'],2).'</td>
		 </tr>';
			}
		
		$tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Untimed Loading Charge</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($ul=$order_data['untimedloading'],2).'</td>
		 </tr>'; 
		 if($outward) {
		 
	   $tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Total Estimated Rent Amount</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($ta=$esr+$vc+$tw+$ul,2).'</td>
		 </tr>';
		  $tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Total Amount Paid (Rent + Vehicle  Charge + Total Wages + Loading Charge) </b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($order_data['advance'],2).'</td>
		 </tr>';
		  
		   $tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Vehicle  Charge + Total Wages + Loading Charges </b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($vc+$ul+$tw,2).'</td>
		 </tr>';
	   $tbl .= '<tr> 
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Advance Paid</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($order_data['advance']-$vc-$ul-$tw,2).'</td>
		 </tr>'; 
		 } else {
	  $tbl .= '<tr>
		 <td colspan="5" style="border:1px thin #666"; align="right"><b>Total Amount</b></td>
		 <td style="border:1px thin #666"; align="right">'. number_format($ta=$ic+$vc+$tw+$cc+$ul,2).'</td>
		 </tr>';}
		if(!$outward) {
			$tbl .= '<tr>
			 <td colspan="5" style="border:1px thin #666"; align="right"><b>Amount Paid</b></td>
			 <td style="border:1px thin #666"; align="right">'. number_format($order_data['amtpaid'],2).'</td>
			 </tr>';
			 $balanceinward=$order_data['totalrent']-$order_data['amtpaid'];
			 $tbl .= '<tr>
			 <td colspan="5" style="border:1px thin #666"; align="right"><b>Balance</b></td>
			 <td style="border:1px thin #666"; align="right">'. $balanceinward.'</td>
			 </tr>';
		 }  
	    $tbl .= '<tr>
		 <td colspan="6" style="border:1px thin #666"; align="left"><b>Amount In Words (INR) : '. $pdf->getIndianCurrency(abs($ta)).'</b></td>
		 </tr>
		 <tr>
		 <td style="text-align:left;font-size:12px;" colspan="6" style="border:1px thin #666"; align="left"><b><u>Terms & Conditions :- </u></b>'.
			 '<div style="text-align:left;font-size:10px;">
			 	1. I Agree to take above Materials From A.P.S Traders to the Rules and Regulations Accourding to the Company.<br>
			 	2. 1 Day Charge is calculated as Morning 8.00 Am to Next day Morning 11.00 Am<br>
			 	3. A.P.S Traders have full rights to take any materials from your site if Customer not paid the daily rents reugalarly.<br>
			 	4. If any physical damages or missing in counting numbers of Items, Customer is fully responsible . 
			 </div></td>
		 </tr> 
		 </table>';	
		$pdf->writeHTML($tbl, true, false, false, false, ''); 	
	}
	
	// Colored table
	public function ColoredInvTable($pdf,$header,$data,$w,$invoice) {
 
		// Header width
	
		$pdf->DrawHeader($pdf,$header, $w);
	
		// Data
		$fill = 0;
		$i = 0;
		foreach($data as $row) {
		 	
			$i++;
			$num_pages = $pdf->getNumPages();
			$pdf->startTransaction();
			
			$pdf->Multicell($w[0], 10, $row['idc_no'], 'L', 'C', false, 0); 
			$pdf->Multicell($w[1], 10, $row['idate'], 'L', 'C', false, 0);
			$pdf->Multicell($w[2], 10, $row['inboxes'], 'L', 'C', false, 0);
			$pdf->Multicell($w[3], 10, $row['insingles'], 'L', 'C', false, 0);
			$pdf->Multicell($w[4], 10, $row['totalrent'], 'L', 'R', false, 0);
			if($invoice) { 
				$pdf->Multicell($w[5], 10, $row['amtpaid'], 'LR', 'R', false, 0);
				$pdf->Multicell($w[6], 10, $row['balance'], 'LR', 'R', false, 0);
			} else {
				$pdf->Multicell($w[5], 10, $row['inotheramt'], 'L', 'R', false, 0); 
				$pdf->Multicell($w[6], 10, $row['amtpaid'], 'LR', 'R', false, 0);
				$pdf->Multicell($w[7], 10, $row['balance'], 'LR', 'R', false, 0);
			}
			//$pdf->Multicell($w[6], 10, number_format($row['totalrent'],2), 'LR', 'R', false, 0);
			$pdf->Ln();
	
			if($i > 8) // no of rows to break table to next page
			{
				$i = 0;
				//Undo adding the row.
				$pdf->rollbackTransaction(true);
				$pdf->last_page = false;
				//Adds a bottom line onto the current page.
				//Note: May cause page break itself.
				 
				$pdf->Cell(array_sum($w), 0, '', 'T');
				$pdf->writeHTML("Continued...", true, false, false, true, 'L');
				//$pdf->writeHTMLCell(5, 5, '', '', 'Continued...', 0, 0, 0, true, 'L',true);
				//Add a new page.
				$pdf->AddPage();
				//Draw the header.
				$pdf->DrawHeader($pdf, $header, $w);
				 
				//Re-do the row.
				$pdf->Multicell($w[0], 10, $row['idc_no'], 'L', 'C', false, 0); 
				$pdf->Multicell($w[1], 10, $row['idate'], 'L', 'C', false, 0);
				$pdf->Multicell($w[2], 10, $row['inboxes'], 'L', 'C', false, 0);
				$pdf->Multicell($w[3], 10, $row['insingles'], 'L', 'C', false, 0);
				$pdf->Multicell($w[4], 10, $row['totalrent'], 'L', 'R', false, 0);
				if($invoice) { 
					$pdf->Multicell($w[5], 10, $row['amtpaid'], 'LR', 'R', false, 0);
					$pdf->Multicell($w[6], 10, $row['balance'], 'LR', 'R', false, 0);
				} else {
					$pdf->Multicell($w[5], 10, $row['inotheramt'], 'L', 'R', false, 0); 
					$pdf->Multicell($w[6], 10, $row['amtpaid'], 'LR', 'R', false, 0);
					$pdf->Multicell($w[7], 10, $row['balance'], 'LR', 'R', false, 0);
				}
				//$pdf->Multicell($w[6], 10, number_format($row['totalrent'],2), 'LR', 'R', false, 0);
				$pdf->Ln();
			}
			else
			{
				//Otherwise we are fine with this row, discard undo history.
				$pdf->commitTransaction();
				$pdf->last_page = true;
			}
			$fill=!$fill;
		}
		$pdf->Cell(array_sum($w), 0, '', 'T');
	}
	
	public function ColoredOutwardTable($pdf, $header, $data, $w) {
			
		// Header width

		$pdf->DrawHeader($pdf,$header, $w);
			$num_pages = $pdf->getNumPages();
			$pdf->startTransaction();
			$pdf->Multicell($w[0], 10, $data['odc_no'], 'L', 'C', false, 0);
			$pdf->MultiCell($w[1], 10, $data['sdate'], 'L', 'L', false, 0);
			$pdf->Multicell($w[2], 10, $data['outwardData']['outboxes'], 'L', 'C', false, 0);
			$pdf->Multicell($w[3], 10, $data['outwardData']['outsingles'], 'L', 'C', false, 0);
			$pdf->Multicell($w[4], 10, $data['outwardcharge'], 'L', 'R', false, 0);
			$pdf->Multicell($w[5], 10, $data['totalamount'] - $data['outwardcharge'], 'L', 'R', false, 0);
			$pdf->Multicell($w[6], 10, $data['advance'], 'L', 'R', false, 0); 
			$pdf->Multicell($w[7], 10, '-', 'LR', 'C', false, 0);
			//$pdf->Multicell($w[6], 10, number_format($row['totalrent'],2), 'LR', 'R', false, 0);
			$pdf->Ln();
			//Otherwise we are fine with this row, discard undo history.
			$pdf->commitTransaction();
			$pdf->last_page = false;

		//$pdf->Cell(array_sum($w), 0, '', 'T');
	}
 	 // Colored table
    public function ColoredTable($pdf,$header,$data, $w, $outward) {
       
        // Header width 
        $pdf->DrawHeader($pdf,$header, $w);
 
        // Data
        $fill = 0;
        $i = 0;
        foreach($data as $row) {
        	$i++;
        	$num_pages = $pdf->getNumPages();
        	$pdf->startTransaction(); 
        	if($outward) {
        		$pdf->MultiCell($w[0], 8, $row['itype'] , 'L', 'L', false, 0);
            	$pdf->Multicell($w[1], 8, $row['boxname'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[2], 8, $row['name'], 'L', 'C', false, 0); 
            	$pdf->Multicell($w[3], 8, $row['noofunits'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[4], 8, $row['rate'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[5], 8, $row['period'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[6], 8, $row['totalrent'], 'LR', 'C', false, 0); 
        	} else {
            	$pdf->MultiCell($w[0], 8, $row['itype'] , 'L', 'L', false, 0);
            	$pdf->Multicell($w[1], 8, $row['boxname'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[2], 8, $row['name'], 'L', 'C', false, 0); 
            	$pdf->Multicell($w[3], 8, $row['outunits'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[4], 8, $row['noofunits'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[5], 8, $row['rate'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[6], 8, $row['gperiod'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[7], 8, $row['period'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[8], 8, $row['totalrent'], 'LR', 'C', false, 0); 
        	}
            $pdf->Ln();
            
            if($i > 11)
            {
            	$i = 0;
            	//Undo adding the row.
            	$pdf->rollbackTransaction(true);
            	$pdf->last_page = false;
            	//Adds a bottom line onto the current page.
            	//Note: May cause page break itself.
            	
            	$pdf->Cell(array_sum($w), 0, '', 'T');
            	$pdf->writeHTML("Continued...", true, false, false, true, 'L');
            	//$pdf->writeHTMLCell(5, 5, '', '', 'Continued...', 0, 0, 0, true, 'L',true);
            	//Add a new page.
            	$pdf->AddPage();
            	//Draw the header.
            	$pdf->DrawHeader($pdf, $header, $w);
            	
            	//Re-do the row.
          		$pdf->MultiCell($w[0], 10, $row['itype'] , 'L', 'L', false, 0);
            	$pdf->Multicell($w[1], 10, $row['boxname'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[2], 10, $row['name'], 'L', 'C', false, 0); 
            	$pdf->Multicell($w[3], 10, $row['noofunits'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[4], 10, $row['rate'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[5], 10, $row['period'], 'L', 'C', false, 0);
            	$pdf->Multicell($w[6], 10, $row['totalrent'], 'LR', 'C', false, 0); 
            	$pdf->Ln();
            }
            else
            {
                //Otherwise we are fine with this row, discard undo history.
                $pdf->commitTransaction();
                $pdf->last_page = true;
            }
            $fill=!$fill;
        }
        $pdf->Cell(array_sum($w), 0, '', 'T');
    }
	
	function getIndianCurrency($number)
	{
		$decimal = round($number - ($no = floor($number)), 2) * 100;
		$hundred = null;
		$digits_length = strlen($no);
		$i = 0;
		$str = array();
		$str1 = array();
		$words = array(0 => '', 1 => 'One', 2 => 'Two',
				3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
				7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
				10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
				13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
				16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
				19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
				40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
				70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
		$digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
		while( $i < $digits_length ) {
			$divider = ($i == 2) ? 10 : 100;
			$number = floor($no % $divider);
			$no = floor($no / $divider);
			$i += $divider == 10 ? 1 : 2;
			if ($number) {
				$plural = (($counter = count($str)) && $number > 9) ? '' : null;
				$hundred = ($counter == 1 && $str[0]) ? ' And ' : null;
				$str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
			} else $str[] = null;
		}
		$Rupees = implode('', array_reverse($str));
		//$paise = ($decimal) ? " " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
		return ($Rupees ? $Rupees . 'Rupees Only' : '');
	}
	
	public function DrawHeader($pdf, $header, $w) {
		// Colors, line width and bold font
		// Header
		$pdf->SetFillColor(224, 227, 232);
		$pdf->SetTextColor(0);
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetLineWidth(0.2);
		$pdf->SetFont('', 'B',10);
		$num_headers = count($header);
		for($i = 0; $i < $num_headers; ++$i) {
			$pdf->Cell($w[$i], 10, $header[$i], 1, 0, 'C', 1);
		}
		$pdf->Ln();
		// Color and font restoration
		$pdf->SetFillColor(224, 235, 255);
		$pdf->SetTextColor(0);
		$pdf->SetFont('');
	}
	
}
?>