-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2021 at 07:53 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentalver1`
--

-- --------------------------------------------------------

--
-- Table structure for table `boxitems`
--

CREATE TABLE `boxitems` (
  `id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `minrentaldays` int(11) DEFAULT NULL,
  `rate` varchar(50) DEFAULT NULL,
  `rateperday` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `boxitems_data`
--

CREATE TABLE `boxitems_data` (
  `id` int(11) NOT NULL,
  `boxitems_id` int(11) NOT NULL DEFAULT 0,
  `rentitem_id` int(11) NOT NULL DEFAULT 0,
  `rentitem_name` varchar(100) NOT NULL DEFAULT '0',
  `qty` decimal(10,0) NOT NULL DEFAULT 0,
  `type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `active`) VALUES
(1, 'Box', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `expiredate` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_gstno` varchar(50) DEFAULT NULL,
  `address` varchar(100) NOT NULL,
  `hsncode` int(11) NOT NULL DEFAULT 0,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `country` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `currency` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `forname` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `printCopy1Title` varchar(50) DEFAULT NULL,
  `printCopy2` int(11) NOT NULL,
  `printCopy2Title` varchar(50) DEFAULT NULL,
  `printCopy3` int(11) NOT NULL,
  `printCopy3Title` varchar(50) DEFAULT NULL,
  `printCopy4` int(11) NOT NULL,
  `printCopy4Title` varchar(50) DEFAULT NULL,
  `printCopy5` int(11) NOT NULL,
  `printCopy5Title` varchar(50) DEFAULT NULL,
  `bankactive` int(11) NOT NULL,
  `bankname` varchar(100) NOT NULL,
  `branchname` varchar(100) NOT NULL,
  `accno` varchar(100) NOT NULL,
  `ifsccode` varchar(100) NOT NULL,
  `discountType` int(11) NOT NULL,
  `chooseCust` int(11) NOT NULL,
  `composition` int(11) NOT NULL,
  `productcodeenable` int(11) NOT NULL,
  `enableproddesc` int(11) NOT NULL,
  `taxsplitup` int(11) NOT NULL,
  `showbal` int(11) NOT NULL,
  `thermalprint` int(11) NOT NULL,
  `A4print` int(11) NOT NULL,
  `loginimage` int(11) NOT NULL,
  `smscode` varchar(50) NOT NULL,
  `smsenable` int(11) NOT NULL,
  `smssenderid` varchar(50) NOT NULL,
  `emaildirect` int(11) NOT NULL,
  `printcorder` int(11) NOT NULL,
  `decimalpoints` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `expiredate`, `company_name`, `company_gstno`, `address`, `hsncode`, `phone`, `email`, `country`, `message`, `currency`, `image`, `forname`, `title`, `printCopy1Title`, `printCopy2`, `printCopy2Title`, `printCopy3`, `printCopy3Title`, `printCopy4`, `printCopy4Title`, `printCopy5`, `printCopy5Title`, `bankactive`, `bankname`, `branchname`, `accno`, `ifsccode`, `discountType`, `chooseCust`, `composition`, `productcodeenable`, `enableproddesc`, `taxsplitup`, `showbal`, `thermalprint`, `A4print`, `loginimage`, `smscode`, `smsenable`, `smssenderid`, `emaildirect`, `printcorder`, `decimalpoints`) VALUES
(1, '1/1/2022', 'APS Traders', '33BVLPM4267J1Z7', '137/5 Bharathi Nagar, Near KG School,Kovai Road  -Annur ,Office Number : 9384147460', 0, '9965547460', 'aps7887@gmail.com', '', '', 'INR', 'assets/images/logo/logo.jpg', 'For APS Traders', 'APS Traders', '', 0, '', 0, '', 0, '', 0, '', 1, 'Tamilnad Mercantile Bank', 'Avinachi Branch', '245700050900152', 'TMBL0000245 ', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', 0, 'GMRINF', 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `id` int(11) NOT NULL,
  `drivername` varchar(50) DEFAULT NULL,
  `driverphone` varchar(50) DEFAULT NULL,
  `truckno` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`id`, `drivername`, `driverphone`, `truckno`) VALUES
(1, 'SATHYARAJ', '9597125708', 'INTRA'),
(2, 'PRABHU', '8760626793', 'OLD PICK UP'),
(3, 'DINESH JOHN', '7339297498', 'NEW PICK UP'),
(4, 'MOHAN', '9751249882', 'TATA ACE'),
(5, 'JOHN', '9965547460', 'CARRY'),
(6, 'KRISHNASAMY SENIANDAVAR', '9865480757', 'TN 38 BX 9111'),
(7, 'SUNDRAM', '9894806510', 'TN 42 H 2428');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `employeename` varchar(50) DEFAULT NULL,
  `employeephone` varchar(50) DEFAULT NULL,
  `sboxwage` varchar(50) DEFAULT NULL,
  `mboxwage` varchar(50) DEFAULT NULL,
  `lboxwage` varchar(50) DEFAULT NULL,
  `bagwage` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `employeename`, `employeephone`, `sboxwage`, `mboxwage`, `lboxwage`, `bagwage`) VALUES
(1, 'ARUMUGAM', '9944815794', NULL, NULL, NULL, NULL),
(2, 'PRABU DRIVER', '8760626793', NULL, NULL, NULL, NULL),
(3, 'JOHN DRIVER', '7339297498', NULL, NULL, NULL, NULL),
(4, 'MOHAN DRIVER', '9751249882', NULL, NULL, NULL, NULL),
(5, 'ANAND', '9894859496', NULL, NULL, NULL, NULL),
(6, 'PRABHAKARAN', '9788585420', NULL, NULL, NULL, NULL),
(7, 'SATHYARAJ DRIVER', '9597125708', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `eorders`
--

CREATE TABLE `eorders` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(50) DEFAULT NULL,
  `sdate` varchar(50) NOT NULL,
  `odc_no` varchar(100) NOT NULL,
  `odate` varchar(100) NOT NULL,
  `sid` varchar(50) NOT NULL,
  `supplier_name` varchar(50) NOT NULL,
  `supplier_phone` varchar(50) DEFAULT NULL,
  `total_items` varchar(50) DEFAULT NULL,
  `totbalance` decimal(10,0) DEFAULT NULL,
  `mop` int(11) DEFAULT NULL,
  `cleaningcharge` decimal(10,0) DEFAULT NULL,
  `vehtranscharge` decimal(10,0) DEFAULT NULL,
  `disc_amount` decimal(10,0) DEFAULT NULL,
  `net_amount` decimal(10,0) DEFAULT NULL,
  `iamtpaid` decimal(10,0) DEFAULT NULL,
  `amtpaid` decimal(10,0) DEFAULT NULL,
  `ibalance` decimal(10,0) DEFAULT NULL,
  `advance` decimal(10,2) DEFAULT NULL,
  `itotalamt` decimal(10,2) DEFAULT NULL,
  `itotalcbal` decimal(10,2) DEFAULT NULL,
  `itotalothers` decimal(10,2) DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `invid` int(11) DEFAULT NULL,
  `invno` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `eorders_items`
--

CREATE TABLE `eorders_items` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(50) NOT NULL,
  `inward_id` varchar(100) NOT NULL,
  `idc_no` varchar(100) NOT NULL,
  `contNo` varchar(100) NOT NULL,
  `idate` varchar(50) NOT NULL,
  `inboxes` varchar(50) NOT NULL,
  `insingles` varchar(100) NOT NULL,
  `totalrent` decimal(10,0) NOT NULL,
  `inotheramt` decimal(10,0) NOT NULL,
  `amtpaid` decimal(10,0) DEFAULT NULL,
  `balance` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `ereciept`
--

CREATE TABLE `ereciept` (
  `id` int(11) NOT NULL,
  `sdate` varchar(100) NOT NULL,
  `receipt_no` varchar(100) NOT NULL,
  `others` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='gst_no,ph_no,email_id,state,state_code' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `ereciept_item`
--

CREATE TABLE `ereciept_item` (
  `id` int(11) NOT NULL,
  `sdate` varchar(50) NOT NULL,
  `receipt_id` int(11) NOT NULL,
  `ledger_id` int(11) NOT NULL,
  `under` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `date_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='order_id`, `product_id`, `qty`, `rate`, `amount`, `discount`, `gst`, `cgst`, `sgst`, `totamount`' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `evoucher`
--

CREATE TABLE `evoucher` (
  `id` int(11) NOT NULL,
  `sdate` varchar(100) NOT NULL,
  `voucher_no` varchar(100) NOT NULL,
  `others` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='gst_no,ph_no,email_id,state,state_code' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `evoucher_item`
--

CREATE TABLE `evoucher_item` (
  `id` int(11) NOT NULL,
  `sdate` varchar(50) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `ledger_id` int(11) NOT NULL,
  `under` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `date_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='order_id`, `product_id`, `qty`, `rate`, `amount`, `discount`, `gst`, `cgst`, `sgst`, `totamount`' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `expense_log`
--

CREATE TABLE `expense_log` (
  `id` int(11) NOT NULL,
  `expense_date` varchar(50) NOT NULL,
  `date_time` varchar(50) NOT NULL DEFAULT '',
  `ledger_id` int(11) NOT NULL,
  `item_description` varchar(100) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `expense_type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `gmradmin`
--

CREATE TABLE `gmradmin` (
  `id` int(11) NOT NULL,
  `expiredate` varchar(150) NOT NULL DEFAULT '0',
  `ledgergmr` int(11) NOT NULL,
  `barcodeprint` int(11) NOT NULL,
  `userpermissiongmr` int(11) NOT NULL,
  `enableestimate` int(11) NOT NULL,
  `csales` int(11) NOT NULL,
  `warehouse` int(11) NOT NULL,
  `opticals` int(11) NOT NULL,
  `enablesales` int(11) NOT NULL,
  `enablepurchase` int(11) NOT NULL,
  `salesinvpayment` int(11) NOT NULL,
  `custreports` int(11) NOT NULL,
  `custpayment` int(11) NOT NULL,
  `purpayment` int(11) NOT NULL,
  `mobservice` int(11) NOT NULL,
  `corder` int(11) NOT NULL,
  `hotel` int(11) NOT NULL,
  `attendance` int(11) NOT NULL,
  `redeempoints` int(11) NOT NULL,
  `showpprice` int(11) NOT NULL,
  `billing` int(11) DEFAULT NULL,
  `subsidy` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `avlqty` int(11) NOT NULL,
  `pl` int(11) NOT NULL,
  `so` int(11) DEFAULT NULL,
  `netdiscount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `group_name` text NOT NULL,
  `permission` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `permission`) VALUES
(1, 'Administrator', 'a:48:{i:0;s:10:\"createUser\";i:1;s:10:\"updateUser\";i:2;s:8:\"viewUser\";i:3;s:11:\"createGroup\";i:4;s:11:\"updateGroup\";i:5;s:9:\"viewGroup\";i:6;s:14:\"createCategory\";i:7;s:14:\"updateCategory\";i:8;s:12:\"viewCategory\";i:9;s:14:\"createEmployee\";i:10;s:14:\"updateEmployee\";i:11;s:12:\"viewEmployee\";i:12;s:13:\"createVehicle\";i:13;s:13:\"updateVehicle\";i:14;s:11:\"viewVehicle\";i:15;s:13:\"createProduct\";i:16;s:13:\"updateProduct\";i:17;s:11:\"viewProduct\";i:18;s:14:\"createCustomer\";i:19;s:14:\"updateCustomer\";i:20;s:12:\"viewCustomer\";i:21;s:13:\"createOutward\";i:22;s:13:\"updateOutward\";i:23;s:11:\"viewOutward\";i:24;s:12:\"createInward\";i:25;s:12:\"updateInward\";i:26;s:10:\"viewInward\";i:27;s:14:\"createEstimate\";i:28;s:14:\"updateEstimate\";i:29;s:12:\"viewEstimate\";i:30;s:11:\"createOrder\";i:31;s:11:\"updateOrder\";i:32;s:9:\"viewOrder\";i:33;s:12:\"createLedger\";i:34;s:12:\"updateLedger\";i:35;s:10:\"viewLedger\";i:36;s:14:\"createPayments\";i:37;s:14:\"updatePayments\";i:38;s:12:\"viewPayments\";i:39;s:11:\"createStore\";i:40;s:11:\"updateStore\";i:41;s:9:\"viewStore\";i:42;s:11:\"viewReports\";i:43;s:13:\"updateCompany\";i:44;s:11:\"viewProfile\";i:45;s:12:\"updatePrefix\";i:46;s:10:\"viewPrefix\";i:47;s:13:\"updateSetting\";}'),
(2, 'Staff', 'a:48:{i:0;s:10:\"createUser\";i:1;s:10:\"updateUser\";i:2;s:8:\"viewUser\";i:3;s:11:\"createGroup\";i:4;s:11:\"updateGroup\";i:5;s:9:\"viewGroup\";i:6;s:14:\"createCategory\";i:7;s:14:\"updateCategory\";i:8;s:12:\"viewCategory\";i:9;s:14:\"createEmployee\";i:10;s:14:\"updateEmployee\";i:11;s:12:\"viewEmployee\";i:12;s:13:\"createVehicle\";i:13;s:13:\"updateVehicle\";i:14;s:11:\"viewVehicle\";i:15;s:13:\"createProduct\";i:16;s:13:\"updateProduct\";i:17;s:11:\"viewProduct\";i:18;s:14:\"createCustomer\";i:19;s:14:\"updateCustomer\";i:20;s:12:\"viewCustomer\";i:21;s:13:\"createOutward\";i:22;s:13:\"updateOutward\";i:23;s:11:\"viewOutward\";i:24;s:12:\"createInward\";i:25;s:12:\"updateInward\";i:26;s:10:\"viewInward\";i:27;s:14:\"createEstimate\";i:28;s:14:\"updateEstimate\";i:29;s:12:\"viewEstimate\";i:30;s:11:\"createOrder\";i:31;s:11:\"updateOrder\";i:32;s:9:\"viewOrder\";i:33;s:12:\"createLedger\";i:34;s:12:\"updateLedger\";i:35;s:10:\"viewLedger\";i:36;s:14:\"createPayments\";i:37;s:14:\"updatePayments\";i:38;s:12:\"viewPayments\";i:39;s:11:\"createStore\";i:40;s:11:\"updateStore\";i:41;s:9:\"viewStore\";i:42;s:11:\"viewReports\";i:43;s:13:\"updateCompany\";i:44;s:11:\"viewProfile\";i:45;s:12:\"updatePrefix\";i:46;s:10:\"viewPrefix\";i:47;s:13:\"updateSetting\";}');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(50) DEFAULT NULL,
  `sdate` varchar(50) NOT NULL,
  `odc_no` varchar(100) NOT NULL,
  `odate` varchar(100) NOT NULL,
  `sid` varchar(50) NOT NULL,
  `supplier_name` varchar(50) NOT NULL,
  `supplier_phone` varchar(50) DEFAULT NULL,
  `total_items` varchar(50) DEFAULT NULL,
  `totbalance` decimal(10,0) DEFAULT NULL,
  `mop` int(11) DEFAULT NULL,
  `cleaningcharge` decimal(10,0) DEFAULT NULL,
  `vehtranscharge` decimal(10,0) DEFAULT NULL,
  `disc_amount` decimal(10,0) DEFAULT NULL,
  `net_amount` decimal(10,0) DEFAULT NULL,
  `gross_amount` decimal(10,0) DEFAULT NULL,
  `gstper` decimal(10,0) DEFAULT NULL,
  `gst` decimal(10,0) DEFAULT NULL,
  `cgst` decimal(10,0) DEFAULT NULL,
  `sgst` decimal(10,0) DEFAULT NULL,
  `iamtpaid` decimal(10,0) DEFAULT NULL,
  `amtpaid` decimal(10,0) DEFAULT NULL,
  `ibalance` decimal(10,0) DEFAULT NULL,
  `advance` decimal(10,2) DEFAULT NULL,
  `itotalamt` decimal(10,2) DEFAULT NULL,
  `itotalcbal` decimal(10,2) DEFAULT NULL,
  `itotalothers` decimal(10,2) DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `eorderid` int(11) DEFAULT NULL,
  `eorderno` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(50) NOT NULL,
  `inward_id` varchar(100) NOT NULL,
  `idc_no` varchar(100) NOT NULL,
  `contNo` varchar(100) NOT NULL,
  `idate` varchar(50) NOT NULL,
  `inboxes` varchar(50) NOT NULL,
  `insingles` varchar(100) NOT NULL,
  `totalrent` decimal(10,0) NOT NULL,
  `inotheramt` decimal(10,0) NOT NULL,
  `amtpaid` decimal(10,0) DEFAULT NULL,
  `balance` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `inward`
--

CREATE TABLE `inward` (
  `id` int(11) NOT NULL,
  `odc_no` varchar(100) NOT NULL,
  `idc_no` varchar(100) NOT NULL,
  `sid` varchar(100) DEFAULT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `supplier_address` varchar(100) DEFAULT NULL,
  `gst_no` varchar(100) DEFAULT NULL,
  `ph_no` varchar(100) DEFAULT NULL,
  `email_id` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `state_code` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `drivername` varchar(50) NOT NULL,
  `driverphone` varchar(50) NOT NULL,
  `contNo` varchar(50) NOT NULL,
  `empid` varchar(50) NOT NULL,
  `tod` int(11) NOT NULL,
  `mop` int(11) NOT NULL,
  `odate` varchar(50) NOT NULL,
  `idate` varchar(50) NOT NULL DEFAULT '',
  `totaldays` varchar(50) NOT NULL,
  `date_time` varchar(50) NOT NULL,
  `inwardcharge` varchar(50) DEFAULT NULL,
  `wages` decimal(10,0) NOT NULL DEFAULT 0,
  `vehtranscharge` decimal(10,0) DEFAULT NULL,
  `cleaningcharge` decimal(10,0) DEFAULT NULL,
  `untimedloading` decimal(10,0) DEFAULT NULL,
  `totalamount` decimal(10,0) NOT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `totalrent` decimal(10,0) NOT NULL,
  `advance` decimal(10,0) DEFAULT NULL,
  `amtpaid` decimal(10,0) DEFAULT NULL,
  `total_items` bigint(20) NOT NULL,
  `paid_status` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `balance` decimal(10,0) DEFAULT NULL,
  `total_balanceqty` int(11) NOT NULL,
  `others` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `inward`
--

INSERT INTO `inward` (`id`, `odc_no`, `idc_no`, `sid`, `supplier_name`, `supplier_address`, `gst_no`, `ph_no`, `email_id`, `state`, `state_code`, `did`, `drivername`, `driverphone`, `contNo`, `empid`, `tod`, `mop`, `odate`, `idate`, `totaldays`, `date_time`, `inwardcharge`, `wages`, `vehtranscharge`, `cleaningcharge`, `untimedloading`, `totalamount`, `discount`, `totalrent`, `advance`, `amtpaid`, `total_items`, `paid_status`, `user_id`, `balance`, `total_balanceqty`, `others`) VALUES
(1, '1', 'Lot-19', '51', 'PONKALIYAMMAN TEXTILES', NULL, NULL, '9597144468', '', '', 0, 1, 'SATHYARAJ', '9597125708', 'INTRA', '1', 1, 1, '01/04/2021', '03/04/2021', '3 Days', '1617400800', '1828.50', '0', '0', '0', '0', '1829', '0', '1829', '0', '1829', 4, NULL, 1, '0', 21, ''),
(2, '1', 'Lot-20', '51', 'PONKALIYAMMAN TEXTILES', NULL, NULL, '9597144468', '', '', 0, 2, 'PRABHU', '8760626793', 'OLD PICK UP', '1', 1, 1, '03/04/2021', '05/04/2021', '3 Days', '1617573600', '74.25', '0', '0', '0', '0', '74', '0', '74', '0', '74', 4, NULL, 1, '0', 18, ''),
(3, '2', 'Lot-21', '51', 'PONKALIYAMMAN TEXTILES', NULL, NULL, '9597144468', '', '', 0, 1, 'SATHYARAJ', '9597125708', 'INTRA', '1', 1, 1, '07/07/2021', '07/07/2021', '1 Days', '1625608800', '20.00', '0', '0', '0', '0', '20', '0', '20', '0', '0', 1, NULL, 1, '20', 1, ''),
(4, '1', 'Lot-22', '51', 'PONKALIYAMMAN TEXTILES', NULL, NULL, '9597144468', '', '', 0, 1, 'SATHYARAJ', '9597125708', 'INTRA', '1', 1, 1, '05/04/2021', '07/07/2021', '94(3 Months 3 Days)', '1625608800', '10645.50', '0', '0', '0', '0', '10646', '0', '10646', '0', '0', 4, NULL, 1, '10646', 18, '');

-- --------------------------------------------------------

--
-- Table structure for table `inward_item`
--

CREATE TABLE `inward_item` (
  `id` int(11) NOT NULL,
  `inward_id` int(11) NOT NULL,
  `outward_item_id` int(11) NOT NULL,
  `itype` int(11) NOT NULL,
  `boxitem_id` int(11) NOT NULL,
  `boxitem_name` varchar(50) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT 0,
  `units` varchar(100) NOT NULL,
  `minrentaldays` bigint(20) NOT NULL,
  `rate` bigint(20) NOT NULL,
  `rateperday` varchar(50) NOT NULL DEFAULT '',
  `noofunits` bigint(20) NOT NULL,
  `outunits` bigint(20) NOT NULL,
  `period` varchar(100) NOT NULL,
  `gperiod` varchar(100) NOT NULL,
  `totalrent` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `balanceqty` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='order_id`, `product_id`, `qty`, `rate`, `amount`, `discount`, `gst`, `cgst`, `sgst`, `totamount`' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `inward_item`
--

INSERT INTO `inward_item` (`id`, `inward_id`, `outward_item_id`, `itype`, `boxitem_id`, `boxitem_name`, `type`, `units`, `minrentaldays`, `rate`, `rateperday`, `noofunits`, `outunits`, `period`, `gperiod`, `totalrent`, `balanceqty`) VALUES
(1, 1, 9, 1, 60, '1 X 2 MAIN PART', 1, 'Nos', 1, 50, '50.00', 10, 10, '3', '0', 1500, 0),
(2, 1, 10, 1, 62, '1 X 2F SIDE PART', 1, 'Nos', 1, 20, '20.00', 5, 10, '3', '0', 300, 5),
(3, 1, 11, 2, 80, '1 X 3 ROOF', 1, 'Nos', 1, 1, '1.00', 2, 10, '3', '0', 6, 8),
(4, 1, 12, 2, 34, '1 X 3 SIDE SHEET', 1, 'Nos', 1, 4, '3.75', 2, 10, '3', '0', 23, 8),
(5, 2, 10, 1, 62, '1 X 2F SIDE PART', 1, 'Nos', 1, 20, '20.00', 1, 5, '3', '0', 60, 4),
(6, 2, 11, 2, 80, '1 X 3 ROOF', 1, 'Nos', 1, 1, '1.00', 1, 8, '3', '0', 3, 7),
(7, 2, 12, 2, 34, '1 X 3 SIDE SHEET', 1, 'Nos', 1, 4, '3.75', 1, 8, '3', '0', 11, 7);

-- --------------------------------------------------------

--
-- Table structure for table `ledger`
--

CREATE TABLE `ledger` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `under` varchar(100) NOT NULL,
  `active` int(11) NOT NULL,
  `expense_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `ledger_name`
--

CREATE TABLE `ledger_name` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active` int(11) NOT NULL,
  `expense_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `ledger_name`
--

INSERT INTO `ledger_name` (`id`, `name`, `active`, `expense_type`) VALUES
(1, 'Income', 1, 1),
(2, 'Expense', 1, 2),
(3, 'Capital', 1, 1),
(4, 'Cash-in-Hand', 1, 1),
(5, 'Current Assets', 1, 3),
(6, 'Fixed Assets', 1, 3),
(7, 'Secured Loan', 1, 3),
(8, 'Unsecured Loan', 1, 3),
(9, 'Bank Accouts', 1, 3),
(10, 'Duties and Taxs', 1, 2),
(11, 'Liabilities', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `outward`
--

CREATE TABLE `outward` (
  `id` int(11) NOT NULL,
  `odc_no` varchar(100) NOT NULL,
  `sid` varchar(100) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(100) NOT NULL,
  `sitelocation` varchar(100) DEFAULT NULL,
  `gst_no` varchar(100) NOT NULL,
  `ph_no` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `state_code` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `drivername` varchar(50) NOT NULL,
  `driverphone` varchar(50) NOT NULL,
  `contNo` varchar(50) NOT NULL,
  `empid` varchar(50) NOT NULL,
  `tod` int(11) NOT NULL,
  `mop` int(11) NOT NULL,
  `sdate` varchar(50) NOT NULL,
  `ndate` varchar(50) NOT NULL,
  `date_time` varchar(50) NOT NULL,
  `outwardcharge` decimal(10,0) DEFAULT NULL,
  `wages` decimal(10,0) NOT NULL DEFAULT 0,
  `vehtranscharge` decimal(10,0) DEFAULT NULL,
  `cleaningcharge` decimal(10,0) DEFAULT NULL,
  `untimedloading` decimal(10,0) DEFAULT NULL,
  `totalamount` decimal(10,0) NOT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `totalrent` decimal(10,0) NOT NULL,
  `advance` decimal(10,0) DEFAULT NULL,
  `total_items` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_balanceqty` int(11) NOT NULL,
  `others` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `outward`
--

INSERT INTO `outward` (`id`, `odc_no`, `sid`, `supplier_name`, `supplier_address`, `sitelocation`, `gst_no`, `ph_no`, `email_id`, `state`, `state_code`, `did`, `drivername`, `driverphone`, `contNo`, `empid`, `tod`, `mop`, `sdate`, `ndate`, `date_time`, `outwardcharge`, `wages`, `vehtranscharge`, `cleaningcharge`, `untimedloading`, `totalamount`, `discount`, `totalrent`, `advance`, `total_items`, `user_id`, `total_balanceqty`, `others`) VALUES
(1, 'Out-48', '51', 'PONKALIYAMMAN TEXTILES', 'KUMARAGOUNDEN PUDUR', '', '', '9597144468', '', 'Tamil Nadu', 33, 1, 'SATHYARAJ', '9597125708', 'INTRA', '1', 1, 1, '01/04/2021', '07/07/2021', '1617228000', '0', '0', '0', '0', '0', '0', '0', '0', '0', 4, 1, 18, ''),
(2, 'Out-49', '51', 'PONKALIYAMMAN TEXTILES', 'KUMARAGOUNDEN PUDUR', '', '', '9597144468', '', 'Tamil Nadu', 33, 1, 'SATHYARAJ', '9597125708', 'INTRA', '2', 1, 1, '07/07/2021', '07/07/2021', '1625608800', '0', '0', '0', '0', '0', '0', '0', '0', '0', 1, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `outward_item`
--

CREATE TABLE `outward_item` (
  `id` int(11) NOT NULL,
  `outward_id` int(11) NOT NULL,
  `itype` int(11) NOT NULL,
  `boxitem_id` int(11) NOT NULL,
  `boxitem_name` varchar(50) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT 0,
  `units` varchar(100) NOT NULL,
  `sdate` varchar(100) NOT NULL,
  `ndate` varchar(100) NOT NULL,
  `avlqty` bigint(20) NOT NULL,
  `minrentaldays` bigint(20) NOT NULL,
  `rate` bigint(20) NOT NULL,
  `rateperday` varchar(50) NOT NULL DEFAULT '',
  `noofunits` bigint(20) NOT NULL,
  `period` varchar(100) NOT NULL,
  `gperiod` varchar(100) NOT NULL,
  `totalrent` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `balanceqty` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='order_id`, `product_id`, `qty`, `rate`, `amount`, `discount`, `gst`, `cgst`, `sgst`, `totamount`' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `outward_item`
--

INSERT INTO `outward_item` (`id`, `outward_id`, `itype`, `boxitem_id`, `boxitem_name`, `type`, `units`, `sdate`, `ndate`, `avlqty`, `minrentaldays`, `rate`, `rateperday`, `noofunits`, `period`, `gperiod`, `totalrent`, `balanceqty`) VALUES
(9, 1, 1, 60, '1 X 2 MAIN PART', 1, 'Nos', '01/04/2021', '03/04/2021', 0, 1, 50, '50.00', 10, '0', '', 0, 0),
(10, 1, 1, 62, '1 X 2F SIDE PART', 1, 'Nos', '01/04/2021', '05/04/2021', 0, 1, 20, '20.00', 10, '0', '', 0, 4),
(11, 1, 2, 80, '1 X 3 ROOF', 1, 'Nos', '01/04/2021', '05/04/2021', 0, 1, 1, '1.00', 10, '0', '', 0, 7),
(12, 1, 2, 34, '1 X 3 SIDE SHEET', 1, 'Nos', '01/04/2021', '05/04/2021', 0, 1, 4, '3.75', 10, '0', '', 0, 7),
(14, 2, 1, 62, '1 X 2F SIDE PART', 1, 'Nos', '07/07/2021', '07/07/2021', 0, 1, 20, '20.00', 1, '0', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `paymentbyestimate`
--

CREATE TABLE `paymentbyestimate` (
  `id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `estimateno` varchar(50) NOT NULL,
  `cname` varchar(100) NOT NULL,
  `custid` varchar(100) NOT NULL,
  `cmob` varchar(100) NOT NULL,
  `cbal` varchar(100) NOT NULL,
  `cpaid` varchar(100) NOT NULL,
  `mop` int(11) DEFAULT NULL,
  `others` varchar(100) DEFAULT NULL,
  `date_time` varchar(100) DEFAULT NULL,
  `pdate` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `paymentbyoutward`
--

CREATE TABLE `paymentbyoutward` (
  `id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `outwardno` varchar(50) NOT NULL,
  `cname` varchar(100) NOT NULL,
  `custid` varchar(100) NOT NULL,
  `cmob` varchar(100) NOT NULL,
  `cbal` varchar(100) NOT NULL,
  `cpaid` varchar(100) NOT NULL,
  `mop` int(11) DEFAULT NULL,
  `others` varchar(100) DEFAULT NULL,
  `date_time` varchar(100) DEFAULT NULL,
  `pdate` varchar(100) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `prefix`
--

CREATE TABLE `prefix` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active` int(11) NOT NULL,
  `inname` varchar(50) NOT NULL,
  `outname` varchar(50) NOT NULL DEFAULT '0',
  `invno` int(50) NOT NULL,
  `inno` int(50) NOT NULL,
  `outno` int(50) NOT NULL,
  `pcode` int(50) NOT NULL,
  `bcode` int(50) NOT NULL,
  `esti` varchar(50) NOT NULL DEFAULT '',
  `estno` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `prefix`
--

INSERT INTO `prefix` (`id`, `name`, `active`, `inname`, `outname`, `invno`, `inno`, `outno`, `pcode`, `bcode`, `esti`, `estno`) VALUES
(1, 'Bill-', 1, 'Lot-', 'Out-', 2, 23, 50, 82, 32, 'Est-', 10);

-- --------------------------------------------------------

--
-- Table structure for table `rentitems`
--

CREATE TABLE `rentitems` (
  `id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `units` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `rentedqty` varchar(100) NOT NULL,
  `totalqty` varchar(100) NOT NULL,
  `availableqty` varchar(100) NOT NULL,
  `availability` int(11) NOT NULL,
  `minrentaldays` int(11) NOT NULL,
  `rate` varchar(50) NOT NULL DEFAULT '',
  `rateperday` varchar(50) NOT NULL DEFAULT '',
  `categoryid` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `rentitems`
--

INSERT INTO `rentitems` (`id`, `code`, `name`, `units`, `type`, `rentedqty`, `totalqty`, `availableqty`, `availability`, `minrentaldays`, `rate`, `rateperday`, `categoryid`, `description`) VALUES
(1, '1', '3 X 2', '1', '2', '0', '8440', '8440', 1, 20, '26', '1.30', 0, ''),
(2, '2', 'SPAN', '1', '2', '0', '1478', '1478', 1, 20, '70', '3.50', 1, ''),
(3, '3', '19 JOCKEY', '1', '2', '0', '562', '562', 1, 20, '100', '5.00', 1, ''),
(4, '4', '16 JOCKEY', '1', '2', '0', '475', '475', 1, 20, '70', '3.50', 1, ''),
(5, '5', '12 PROPS', '1', '2', '0', '994', '994', 1, 30, '18', '0.60', 1, ''),
(6, '6', '11 PROPS', '1', '2', '0', '560', '560', 1, 30, '17', '0.57', 1, ''),
(7, '7', '10 PROPS', '1', '2', '0', '1638', '1638', 1, 30, '15', '0.50', 1, ''),
(8, '8', '9 PROPS', '1', '2', '0', '1890', '1890', 1, 30, '14', '0.47', 1, ''),
(9, '9', '8 PROPS', '1', '2', '0', '990', '990', 1, 30, '13', '0.43', 1, ''),
(10, '10', '7 PROPS', '1', '2', '0', '893', '893', 1, 30, '12', '0.40', 1, ''),
(11, '11', '2 X 8 SIDE SHEET', '1', '2', '0', '2', '2', 1, 1, '16', '16.00', 0, ''),
(12, '12', '2 X 7 SIDE SHEET', '1', '2', '0', '15', '15', 1, 1, '14', '14.00', 2, ''),
(13, '13', '2 X 6 SIDE SHEET', '1', '2', '0', '19', '19', 1, 1, '12', '12.00', 1, ''),
(14, '14', '2 X 5 SIDE SHEET', '1', '2', '0', '0', '0', 1, 1, '10', '10.00', 2, ''),
(15, '15', '2 X 4 SIDE SHEET', '1', '2', '0', '20', '20', 1, 1, '8', '8.00', 2, ''),
(16, '16', '8 JOCKEY', '1', '2', '0', '1381', '1381', 1, 20, '50', '2.50', 1, ''),
(17, '17', '1.5 X 8 SIDE SHEET', '1', '2', '0', '-92', '-92', 1, 1, '12', '12.00', 2, ''),
(18, '18', '1.5 X 7 SIDE SHEET', '1', '2', '0', '30', '30', 1, 1, '10.5', '10.50', 2, ''),
(19, '19', '1.5 X 6 SIDE SHEEET', '1', '2', '0', '3', '3', 1, 1, '9', '9.00', 2, ''),
(20, '20', '1.5 X 5 SIDE SHEET', '1', '2', '0', '-2', '-2', 1, 1, '7.5', '7.50', 2, ''),
(21, '21', '1.5 X 4 SIDE SHEET', '1', '2', '0', '-38', '-38', 1, 1, '6', '6.00', 2, ''),
(22, '22', '1.5 X 3 SIDE SHEET', '1', '2', '0', '42', '42', 1, 1, '4.5', '4.50', 2, ''),
(23, '23', '1.5 X 2 SIDE SHEET', '1', '2', '0', '24', '24', 1, 1, '3', '3.00', 2, ''),
(24, '24', '1.1/4 X 3 SIDE SHEET', '1', '2', '0', '0', '0', 1, 1, '4.5', '4.50', 2, ''),
(25, '25', '1.1/4 X 4 SIDE SHEET', '1', '2', '0', '64', '64', 1, 1, '6', '6.00', 2, ''),
(26, '26', '1.1/4 X 5 SIDE SHEET', '1', '2', '0', '29', '29', 1, 1, '7.5', '7.50', 2, ''),
(27, '27', '1.1/4 X 8 SIDE SHEET', '1', '2', '0', '48', '48', 1, 1, '12', '12.00', 2, ''),
(28, '28', '1.1/4 X 6 SIDE SHEET', '1', '2', '0', '20', '20', 1, 1, '9', '9.00', 2, ''),
(29, '29', '1.1/4 X 7 SIDE SHEET', '1', '2', '0', '38', '38', 1, 1, '10.5', '10.50', 2, ''),
(30, '30', '3/4 X 8 SIDE SHEET', '1', '2', '0', '15', '15', 1, 1, '8', '8.00', 2, ''),
(31, '31', '3/4 X 7 SIDE SHEET', '1', '2', '0', '3', '3', 1, 1, '7', '7.00', 2, ''),
(32, '32', '1 X 7 SIDE SHEET', '1', '2', '0', '17', '17', 1, 1, '8.75', '8.75', 1, ''),
(33, '33', '1 X 8 SIDE SHEET', '1', '2', '0', '40', '40', 1, 1, '10', '10.00', 2, ''),
(34, '34', '1 X 3 SIDE SHEET', '1', '2', '7', '47', '40', 1, 1, '3.75', '3.75', 2, ''),
(35, '35', 'ROUND BOX 3/4 DIA X 5F', '1', '2', '0', '2', '2', 1, 1, '125', '125.00', 0, ''),
(36, '36', 'ROUND BOX DIA 1 X 5', '1', '2', '0', '0', '0', 1, 1, '125', '125.00', 3, ''),
(37, '37', '2 X 5F BOX  MAIN PART', '1', '1', '0', '20', '20', 1, 1, '10', '10', 3, ''),
(38, '38', '1.5 X 5F MAIN PART', '1', '1', '0', '0', '0', 1, 1, '20', '20', 3, ''),
(39, '39', '1 X 5F MAIN PART', '1', '1', '0', '-36', '-36', 1, 1, '30', '30', 3, ''),
(40, '40', '3/4 X 5F SIDE PART', '1', '1', '0', '-20', '-20', 1, 1, '40', '40', 3, ''),
(41, '41', '1 X 5F SIDE PART', '1', '1', '0', '14', '14', 1, 1, '50', '50', 3, ''),
(42, '42', '1.1/4 X 5F SIDE PART', '1', '1', '0', '10', '10', 1, 1, '60', '60', 3, ''),
(43, '43', '1.1/4 X 5 BOX SIDE', '1', '1', '0', '10', '10', 1, 1, '10', '10', 1, ''),
(44, '44', '1 X 3F MAIN PART', '1', '1', '0', '0', '0', 1, 1, '10', '10.00', 1, ''),
(45, '45', '1 X 3F SIDE PART', '1', '1', '0', '0', '0', 1, 1, '5', '5.00', 1, ''),
(50, '50', '1 X 4F MAIN PART', '1', '1', '0', '4', '4', 1, 1, '60', '60', 3, ''),
(51, '51', '3/4 X 4F SIDE PART', '1', '1', '0', '55', '55', 1, 1, '50', '50', 3, ''),
(52, '52', '1 X 4 SIDE PART', '1', '1', '0', '24', '24', 1, 1, '60', '60.00', 1, ''),
(53, '53', '2 X 3/4 SHOE', '1', '2', '0', '6', '6', 1, 1, '15', '15.00', 3, ''),
(54, '54', '2 X 1 SHOE', '1', '2', '0', '6', '6', 1, 1, '15', '15.00', 3, ''),
(55, '55', '1.5 X 1 SHOE', '1', '2', '0', '6', '6', 1, 1, '10', '10.00', 3, ''),
(56, '56', '1 X 3/4 SHOE', '1', '2', '1', '-10', '-11', 1, 1, '10', '10.00', 3, ''),
(57, '57', '1.5 X 3/4 SHOE', '1', '2', '0', '18', '18', 1, 1, '10', '10.00', 3, ''),
(60, '60', '1 X 2 MAIN PART', '1', '1', '-10', '-10', '0', 2, 1, '50', '50.00', 1, ''),
(61, '61', '1 X 2FF MAIN PART', '1', '1', '0', '20', '20', 1, 1, '10', '10.00', 1, ''),
(62, '62', '1 X 2F SIDE PART', '1', '1', '-6', '10', '16', 1, 1, '20', '20.00', 1, ''),
(80, '80', '1 X 3 ROOF', '1', '2', '7', '1', '-6', 1, 1, '1', '1.00', 1, ''),
(81, '81', 'EARTH RAMMER', '1', '2', '0', '0', '0', 1, 1, '800', '800.00', 1, ''),
(83, '83', '6 F LADDER', '1', '2', '0', '1', '1', 1, 1, '60', '60.00', 1, ''),
(84, '84', '7 F LADDER', '1', '2', '0', '1', '1', 1, 1, '100', '100.00', 1, ''),
(85, '85', '8F LADDER', '1', '2', '0', '1', '1', 1, 1, '120', '120.00', 1, ''),
(86, '86', 'KALAVA SHEET', '1', '2', '0', '1', '1', 1, 1, '50', '50.00', 1, ''),
(87, '87', 'CONCRETE SHEET', '1', '2', '0', '2', '2', 1, 1, '60', '60.00', 4, ''),
(88, '88', '1 X 4', '1', '2', '0', '60', '60', 1, 1, '5', '5.00', 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `scustomers`
--

CREATE TABLE `scustomers` (
  `supl_id` int(11) NOT NULL,
  `supp_name` varchar(100) NOT NULL,
  `supp_address` varchar(100) NOT NULL,
  `supp_address2` varchar(100) NOT NULL,
  `supp_address3` varchar(100) NOT NULL,
  `supp_address4` varchar(100) NOT NULL,
  `contactpname` varchar(100) NOT NULL,
  `gst_no` varchar(100) NOT NULL,
  `ph_no` varchar(100) NOT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  `credit_limit` int(11) DEFAULT NULL,
  `credit_days` int(11) DEFAULT NULL,
  `old_balance` int(11) DEFAULT NULL,
  `wallet` varchar(50) DEFAULT NULL,
  `state_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='credit,credit_limit,credit_days,old_balance,wallet' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `scustomers`
--

INSERT INTO `scustomers` (`supl_id`, `supp_name`, `supp_address`, `supp_address2`, `supp_address3`, `supp_address4`, `contactpname`, `gst_no`, `ph_no`, `email_id`, `state`, `credit`, `credit_limit`, `credit_days`, `old_balance`, `wallet`, `state_code`) VALUES
(1, 'Ramesh Pathuvampalli', 'Pathuvampalli', '', '', '', 'Own Reference', '', '8586230166', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(2, 'Ramasamy Jeevanagar', 'JEEVA NAGAR', '', '', '', 'Own Reference', '', '9842283692', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(3, 'AC Shanmugam', 'Sellappampalayam', '', '', '', 'Own Reference', '', '9865850250', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(4, 'Murasoli Kumaran', 'Amman Tex', '', '', '', 'Kalisamy Thalaivar', '', '9384258957', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(5, 'Arumugam Puliyampatti', 'Puliyampatti', '', '', '', '', '', '9965019995', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(6, 'EN Shanmugam', 'Sirumugai', '', '', '', '', '', '9655607544', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(7, 'Ganesan Sokkampalayam', 'Sokkampalayam', '', '', '', '', '', '9843843938', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(8, 'Hari NM', 'Annur', '', '', '', '', '', '9790462299', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(9, 'Hussain Bhai', 'Annur', '', '', '', 'Mushtaq Er', '', '9080701629', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(10, 'Iyyappan Puliyampatti', 'Puliyampatti', '', '', '', '', '', '9750476077', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(11, 'Karthi Kumarapalayam', 'Kumarapalayam', '', '', '', '', '', '9976987846', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(12, 'Kesavamoorthi Kumarapalayam', 'Kumarapalayam', '', '', '', '', '', '9965902480', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(13, 'RAMESH ACHAMPALAYAM', 'ACHAMPALAYAM', '', '', '', 'SELF REF', '', '7558184118', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(14, 'DHARAMARAJ CS', 'PETHANAIKAMPALAYAM', '', '', '', 'SATHISH', '', '9952177127', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(15, 'MOHAN JEEVA NAGAR', 'JEEEVA NAGAR', '', '', '', 'SELF ', '', '9942366201', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(16, 'SIVA KUMAR MUDUKANDURAI', 'MUDUKANDURAI', '', '', '', '9976568495', '', '9976568495', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(17, 'UMAPATHY', 'KMPL', '', '', '', 'OWN KMPL', '', '8838145939', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(18, 'RANJITH KUMAR', 'KARUPUSAMY REFF', '', '', '', 'MURUGASAMY / KARUPUSAMY', '', '9345890142', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(19, 'BHAGYARAJ VADAKALAUR', 'VADAKALUR', '', '', '', '', '', '9976656612', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(20, 'ESSAR THIRU', 'COIMBATORE ', '', '', '', 'KALISAMY THALAIVAR REFFERENCE', '', '7402313436', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(21, 'SUBRAMANI SELLAPAMPALAYAM', 'SELLAPAMPALAYAM', '', '', '', 'OWN REFFERENCE', '', '9976828406', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(22, 'SUBRAMANI AMTP', 'ANNUR MTP', '', '', '', 'OWN', '', '7373946065', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(23, 'SAMINATHANAN ANNUR', 'ANNUR', '', '', '', 'SELF', '', '9952774222', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(24, 'VINEETH ER', 'ANNUR', '', '', '', 'OWN', '', '9842273735', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(25, 'Rajan', 'seniandavar', '', '', '', 'seniandavar', '', '9965453871', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(26, 'SHIVA KUMAR NAMBIYAMPALAYAM', 'NAMBIYAMPALAYAM', '', '', '', 'OWN REF', '', '7373562956', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(27, 'AMUL RAJ', 'PASUR', '', '', '', 'OWN REF', '', '9976762914', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(28, 'SENTHIL GANESAN', 'PULIYAMPATTI', '', '', '', 'OWN', '', '9524957062', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(29, 'MANI - SENTHIL GANESAN', 'KARUVALUR', '', '', '', '', '', '9942241060', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(30, 'JOHN RAJ', 'BOYANOOR', '', '', '', '', '', '9976080722', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(31, 'RAJENDRAN AVINACHI', 'AVINACHI', '', '', '', '', '', '9842564536', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(32, 'CHINNASAMY PONNEGOUNDEN PUDUR', 'PONNEGOUNDEN PUDUR', '', '', '', '', '', '9865004979', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(33, 'SENTHIL KARANUR', 'KARANUR', '', '', '', '', '', '9952803449', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(34, 'RAVIKUMAR ER', 'KOVILPALAYAM', '', '', '', '', '', '9790041119', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(35, 'R PALANISAMY', 'KUMARAPALAYAM', '', '', '', '', '', '9942450559', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(36, 'KARUPPUSAMY UG MAHAL', 'ANNUR', '', '', '', '', '', '9842276889', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(37, 'SEKAR', 'THASARAPALAYAM', '', '', '', '', '', '8248015872', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(38, 'VEERAMATHI & CO', 'GANESAPURAM', '', '', '', '', '', '8122726399', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(39, 'KUMAR', 'KUMARAPALAYAM', '', '', '', '', '', '9443794419', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(40, 'RAVI SELLAPPAMPALAYAM', 'SELLAPPAMPALAYAM', '', '', '', '', '', '8012870809', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(41, 'NANDHA KUNIYUR', 'KUNIYYUR', '', '', '', '', '', '9095135905', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(42, 'YOGESH ', 'ALAMELUMANGAPALAYAM', '', '', '', '', '', '9789391241', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(43, 'ESWARAN KANJAPALI', 'KANJAPALAI', '', '', '', '', '', '9843504108', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(44, 'MANIKANDAN SINGARAJA', 'SINGARAJA REFF', '', '', '', '', '', '9500264806', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(45, 'RAYAPPAN', 'KARIYAGOUNDENNUR', '', '', '', '', '', '9659891643', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(46, 'SHANMUGAM KARAMADAI', 'ANNUR POST OFF', '', '', '', '', '', '9578339883', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(47, 'JAYA PRASATH', 'KEMANAIKAMPALAYAM', '', '', '', '', '', '9994905600', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(48, 'SEKAR PATHUVAMPALLI', 'PATHUVAMPALLI', '', '', '', '', '', '8072679591', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(49, 'KALISWARAN', 'KOTTAIPALAYAM', '', '', '', '', '', '9894636111', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(50, 'KRISHNAMOORTHY', 'AYEMAPUDUR', '', '', '', '', '', '8144185505', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33'),
(51, 'PONKALIYAMMAN TEXTILES', 'KUMARAGOUNDEN PUDUR', '', '', '', '', '', '9597144468', '', 'Tamil Nadu', 1, 10, NULL, NULL, '10000', '33');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `statename` varchar(100) DEFAULT NULL,
  `statecode` int(11) DEFAULT NULL,
  `stateabb` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `statename`, `statecode`, `stateabb`) VALUES
(1, 'Jammu and Kashmir', 1, 'JK'),
(2, 'Himachal Pradesh', 2, 'HP'),
(3, 'Punjab', 3, 'PB'),
(4, 'Chandigarh', 4, 'CH'),
(5, 'Uttarakhand', 5, 'UK'),
(6, 'Haryana', 6, 'HR'),
(7, 'Delhi', 7, 'DL'),
(8, 'Rajasthan', 8, 'RJ'),
(9, 'Uttar Pradesh', 9, 'UP'),
(10, 'Bihar', 10, 'BR'),
(11, 'Sikkim', 11, 'SK'),
(12, 'Arunachal Pradesh', 12, 'AR'),
(13, 'Nagaland', 13, 'NL'),
(14, 'Manipur', 14, 'MN'),
(15, 'Mizoram', 15, 'MZ'),
(16, 'Tripura', 16, 'TR'),
(17, 'Meghalaya', 17, 'ML'),
(18, 'Assam', 18, 'AS'),
(19, 'West Bengal', 19, 'WB'),
(20, 'Jharkhand', 20, 'JH'),
(21, 'Orissa', 21, 'OD'),
(22, 'Chattisgarh', 22, 'CG'),
(23, 'Madhya Pradesh', 23, 'MP'),
(24, 'Gujarat', 24, 'GJ'),
(25, 'Daman and Diu', 25, 'DD'),
(26, 'Dadra and Nagar Haveli', 26, 'DN'),
(27, 'Maharashtra', 27, 'MH'),
(28, 'Andhra Pradesh (Old)', 28, 'ADO'),
(29, 'Karnataka', 29, 'KA'),
(30, 'Goa', 30, 'GA'),
(31, 'Lakshadweep Islands', 31, 'LD'),
(32, 'Kerala', 32, 'KL'),
(33, 'Tamil Nadu', 33, 'TN'),
(34, 'Pondicherry', 34, 'PY'),
(35, 'Andaman and Nicobar Islands', 35, 'AN'),
(36, 'Telangana', 36, 'TS'),
(37, 'Andhra Pradesh', 37, 'ADN');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `active`) VALUES
(1, 'Bangalore', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `gender` int(11) NOT NULL,
  `cacheStore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `firstname`, `lastname`, `phone`, `gender`, `cacheStore`) VALUES
(1, 'Lakshmanan', '$2y$10$0qGeTZD/QLslsHX4eMhrLOzWDZpIoqRKxOiJ5C722JQBF0AiuEJSK', 'admin@gmrinfotech.in', 'Lakshmanan', 'Murugan', '9944558110', 1, NULL),
(2, 'demouser', '$2y$10$ZMYoMBNBk9uUjoBDdVeiKuFwyDWgm8lyAu3UR/WSmv5eikDFr5TjC', 'demo@gmrinfotech.com', 'Demo', 'User', '9944558110', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boxitems`
--
ALTER TABLE `boxitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boxitems_data`
--
ALTER TABLE `boxitems_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eorders`
--
ALTER TABLE `eorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eorders_items`
--
ALTER TABLE `eorders_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ereciept`
--
ALTER TABLE `ereciept`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ereciept_item`
--
ALTER TABLE `ereciept_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evoucher`
--
ALTER TABLE `evoucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evoucher_item`
--
ALTER TABLE `evoucher_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_log`
--
ALTER TABLE `expense_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gmradmin`
--
ALTER TABLE `gmradmin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inward`
--
ALTER TABLE `inward`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inward_item`
--
ALTER TABLE `inward_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ledger`
--
ALTER TABLE `ledger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ledger_name`
--
ALTER TABLE `ledger_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outward`
--
ALTER TABLE `outward`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outward_item`
--
ALTER TABLE `outward_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paymentbyestimate`
--
ALTER TABLE `paymentbyestimate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paymentbyoutward`
--
ALTER TABLE `paymentbyoutward`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prefix`
--
ALTER TABLE `prefix`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rentitems`
--
ALTER TABLE `rentitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scustomers`
--
ALTER TABLE `scustomers`
  ADD PRIMARY KEY (`supl_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boxitems`
--
ALTER TABLE `boxitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `boxitems_data`
--
ALTER TABLE `boxitems_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `eorders`
--
ALTER TABLE `eorders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eorders_items`
--
ALTER TABLE `eorders_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ereciept`
--
ALTER TABLE `ereciept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ereciept_item`
--
ALTER TABLE `ereciept_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evoucher`
--
ALTER TABLE `evoucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evoucher_item`
--
ALTER TABLE `evoucher_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense_log`
--
ALTER TABLE `expense_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gmradmin`
--
ALTER TABLE `gmradmin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inward`
--
ALTER TABLE `inward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `inward_item`
--
ALTER TABLE `inward_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ledger`
--
ALTER TABLE `ledger`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ledger_name`
--
ALTER TABLE `ledger_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `outward`
--
ALTER TABLE `outward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `outward_item`
--
ALTER TABLE `outward_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `paymentbyestimate`
--
ALTER TABLE `paymentbyestimate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paymentbyoutward`
--
ALTER TABLE `paymentbyoutward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prefix`
--
ALTER TABLE `prefix`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rentitems`
--
ALTER TABLE `rentitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `scustomers`
--
ALTER TABLE `scustomers`
  MODIFY `supl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
