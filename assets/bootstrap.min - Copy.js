function isNumber(evt, eleId) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 47 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57 || iKeyCode == 46))
        IsNumeric(e, eleId);
    return !0
}
var specialKeys = new Array();
specialKeys.push(8);

function IsNumeric(e, eleId) {
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 46 && keyCode <= 57 && keyCode != 47) || specialKeys.indexOf(keyCode) != -1);
    document.getElementById(eleId).style.display = ret ? "none" : "inline";
    return ret
}

function Trim(nStr) {
    return nStr.replace(/(^\s*)|(\s*$)/g, "")
}

function fnPaste() {
    event.returnValue = !1
}

function isNull(obj, msg) {
    if (msg != "Password")
        obj1 = Trim(obj.value);
    else obj1 = obj.value;
    if (obj1 == "") {
        alert("Please enter the " + msg);
        obj.focus();
        return !0
    } else return !1
}

function isLen(obj, siz, msg) {
    if (msg != "Password")
        obj1 = Trim(obj.value);
    else obj1 = obj.value;
    if (obj1 != "") {
        var strLen = obj.value;
        if (strLen.length < siz) {
            alert(msg + " should be atleast " + siz + " characters");
            obj.focus();
            return !0
        }
    } else return !1
}

function isSame(obj1, obj2, msg1, msg2) {
    if ((Trim(obj1.value)) == (Trim(obj2.value))) {
        alert(msg1 + " is matched with the " + msg2);
        obj2.focus();
        return !0
    } else return !1
}

function isNotSame(obj1, obj2, msg1, msg2) {
    if ((Trim(obj1.value)) != (Trim(obj2.value))) {
        alert(msg1 + " does not match");
        obj2.focus();
        return !0
    } else return !1
}

function isCorrect(obj1, obj2, msg1, msg2) {
    if ((Trim(obj1.value)) >= (Trim(obj2.value))) {
        alert(msg1 + " should be less than " + msg2);
        obj2.focus();
        return !0
    } else return !1
}

function isTxtareaNull(obj, msg) {
    if (Trim(obj.innerText) == "") {
        alert("Please enter " + msg);
        obj.focus();
        return !0
    } else return !1
}

function isTxtareaLen(obj, msg) {
    if (obj.innerHTML.length > 255) {
        alert("Please enter below 256 characters in " + msg);
        obj.focus();
        return !0
    } else return !1
}

function notEmail(obj, msg) {
    var exp = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
    if (!exp.test(obj.value)) {
        alert("Please enter valid " + msg);
        obj.focus();
        return !0
    } else return !1
}

function notZipcode(obj, msg) {
    exp = /[a-zA-Z|\d]-{1}/;
    if (!exp.test(obj.value)) {
        alert("Please enter valid " + msg);
        obj.focus();
        return !0
    } else return !1
}

function notChecked(obj, msg) {
    checked = !1;
    if (obj.length) {
        for (i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                checked = !0;
                break
            }
        }
    } else if (obj.checked)
        checked = !0;
    if (!(checked)) {
        alert("Please select the " + msg);
        if (obj.length)
            obj[0].focus();
        else obj.focus();
        return !0
    }
}

function notSelected(obj, msg) {
    if (obj.options[obj.selectedIndex].value == "") {
        alert("Please select the " + msg);
        obj.focus();
        return !0
    } else return !1
}

function notImageFile(obj, msg) {
    var exp = /^.+\.(jpg|gif|jpeg|JPG|JPEG|GIF)$/;
    if (!exp.test((obj.value).toLowerCase())) {
        alert("Please choose jpg or gif file for " + msg);
        obj.focus();
        return !0
    } else return !1
}

function notDocFile(obj, msg) {
    if (Trim(obj.value) != "") {
        var exp = /^.+\.(RTF|rtf|DOC|doc|TXT|txt)$/;
        if (!exp.test((obj.value).toLowerCase())) {
            alert("Please choose doc or txt file for " + msg);
            obj.value = "";
            obj.focus();
            return !0
        } else return !1
    } else return !1
}

function notPdfDocFile(obj, msg) {
    var exp = /^.+\.(pdf|doc|PDF|DOC)$/;
    if (!exp.test((obj.value).toLowerCase())) {
        alert("Please choose pdf or doc file for " + msg);
        obj.value = "";
        obj.focus();
        return !0
    } else return !1
}

function notPdfFile(obj, msg) {
    var exp = /^.+\.(pdf|PDF)$/;
    if (!exp.test((obj.value).toLowerCase())) {
        alert("Please choose pdf file for " + msg);
        obj.value = "";
        obj.focus();
        return !0
    } else return !1
}

function notPrice(obj, msg) {
    exp = /^[\d]*[\.]{0,1}[\d]{1,2}$/;
    if (!exp.test(obj.value)) {
        alert("Please enter valid " + msg);
        obj.focus();
        return !0
    } else return !1
}

function fnChkNum(obj, msg) {
    exp = /^[\d]/;
    if (!exp.test(obj.value)) {
        alert("Please Select " + msg);
        obj.focus();
        return !0
    } else return !1
}

function fnChkNum1(obj) {
    exp1 = /[^0-9]/;
    if (exp1.test(obj.value)) {
        alert("Only  numbers(0-9) are allowed");
        obj.value = "";
        obj.focus();
        return !0
    }
}

function fnChkAlpha(obj, msg) {
    exp = (/(^([a-z]|[A-Z]|["."]|[\s])*$)/);
    if (!exp.test(obj.value)) {
        alert("Please enter only alphabets in " + msg);
        obj.focus();
        return !0
    } else return !1
}

function fnChkAlphaNum(obj, msg) {
    exp = (/(^([a-z]|[A-Z]|[0-9])*$)/);
    if (!exp.test(obj.value)) {
        alert("Please enter only alphanumeric in " + msg);
        obj.focus();
        return !0
    } else return !1
}

function fnChkAlphaNumeric(obj, msg) {
    var alpha = /[a-zA-Z|]/;
    var Num = /[\d]/;
    if (!(Alpha.test(obj.value) && Num.test(obj.value))) {
        alert("Please enter only alphanumeric in " + msg);
        obj.focus();
        return !0
    } else return !1
}

function setHeaderTitle(pgtitle) {
	var user = $(".headername>span");
	if(user[0])
		user[0].innerHTML = pgtitle;	
}

function chooseTable() {
    if ($("#custType").val() == 2) {
        $(".igst").hide();
        $(".gsth").hide();
        $(".igsth").show();
        $("#gstsublbl").hide();
        $("#igstsublbl").show()
    } else {
        $(".igst").show();
        $(".gsth").show();
        $(".igsth").hide();
        $("#igstsublbl").hide();
        $("#gstsublbl").show()
    }
    subAmount()
}

function chooseTableBody() {
    if ($("#custType").val() == 2) {
        $(".igst").hide();
        $(".gsth").hide();
        $(".igsth").show();
        $("#gstsublbl").hide();
        $("#igstsublbl").show()
    } else {
        $(".igst").show();
        $(".gsth").show();
        $(".igsth").hide();
        $("#igstsublbl").hide();
        $("#gstsublbl").show()
    }
}

function chooseSupl() {
	 if ($("#outto").val() == 1) {
		  $(".supl").show();
	      $(".subsupl").hide();
	      clearCus();
	 } else {
		  $(".subsupl").show();
	      $(".supl").hide();
	      clearCus();
	 }
}
 
function getPrevProd() {
	var tableProductLength = $("#product_info_table_gst tbody tr").length;

	var prodlist = new Array();
    for (x = 0; x < tableProductLength; x++) {
        var tr = $("#product_info_table_gst tbody tr")[x];
        var count = $(tr).attr('id');
        row = count.substring(4);
        var prod = Number($("#product_" + row).val());

        prodlist.push(prod); 
    }
    
    return prodlist;
}

function checkSameProd(prodid) {
    var prodlist = getPrevProd(); 
    var noitems = prodlist.filter(i => i == prodid).length 
    
    if(noitems > 1) 
    	return true;
    else 
    	return false;
}

function checkPrevProd(prodid) {
    var prodlist = getPrevProd(); 

    if(prodlist.includes(Number(prodid))) 
    	return true;
    else 
    	return false;
}

function calculateBalance() {
	var net =  Number($("#net_amount").val());
	var paid = Number($("#amtpaiddb").val());
	var bal = (net - paid).toFixed(2);
	$("#balance").val(bal);
	
	var newpay =  Number($("#newPay").val());
	var balance = Number($("#balance").val());

	var nbal = (net - (paid + newpay)).toFixed(2);
	
	$("#nbalance").val(nbal);
}

function getWages() {
	var tableProductLength = $("#product_info_table_gst tbody tr").length;
    for (xi = 0; xi < tableProductLength; xi++) {
        var tr = $("#product_info_table_gst tbody tr")[xi];
        var count = $(tr).attr('id');
        row = count.substring(4);

        calculateWages(row);
    }
}

function getRents() {
	var tableProductLength = $("#product_info_table_gst tbody tr").length;
    for (xj = 0; xj < tableProductLength; xj++) {
        var tr = $("#product_info_table_gst tbody tr")[xj];
        var count = $(tr).attr('id');
        row = count.substring(4);

        getRent(row);
    }
}


function calculateWages(row) {
	if(row) {
	    var sboxwages = converttonumber($("#sbox_wage").val());
		var mboxwages = converttonumber($("#mbox_wage").val());
		var lboxwages = converttonumber($("#lbox_wage").val());
		var bagwages = converttonumber($("#bag_wage").val());
		var units = 1;
		
		units = converttonumber($("#units_" + row).val());
		
		if(isNaN(units)) {
			units = converttonumber($("#units_id_" + row).val());
		}
		var noofunits = converttonumber($("#noofunits_" + row).val());
		
		/* Calculate Wages */
	    var wages = 0;
	    if(units == 1) {
	    	wages = noofunits * sboxwages;
	    } else if (units == 2) {
	    	wages = noofunits * mboxwages;
	    } else if (units == 3) {
	    	wages = noofunits * lboxwages;
		} else if (units == 4) {
			wages = noofunits * bagwages;
	    }
	    
	    $("#wage_" + row).val(wages);
	    subRAmount();
	}
}

function getTotalRent(row = null) {
    if (row) {
    	var avlqty = converttonumber($("#avlqty_" + row).val());
    	var noofunits = converttonumber($("#noofunits_" + row).val());
        var rate = converttonumber($("#rate_" + row).val()).toFixed(2);
        var period = converttonumber($("#period_" + row).val());
      
        var balanceqty = avlqty - noofunits;
        if (noofunits > avlqty) { 
			showpopupdetails('popup_detail_model_body', 'popup_detail_model', "<b>No of units cannot exceed Available Qty</b>s");
			$("#noofunits_" + row).val(avlqty); 
        }
 
        $("#balanceqty_" + row).val(balanceqty);
        
        var total = noofunits * rate * period; 
        total = total.toFixed(2); 
        $("#totamount_" + row).val(total); 
        subRAmount();
    } else {
        alert('no row !! please refresh the page')
    }
}

function getInEditTotalRent(row = null, rackcheck = true, update = false) {
    if (row) {
				
        var rackavlspc = converttonumber($("#tsboxes").val());

        var tableProductLength = $("#product_info_table_gst tbody tr").length;
    	var units = converttonumber($("#units_" + row).val());
        var kg = converttonumber($("#kg_" + row).val()).toFixed(2);
        var noofunits = converttonumber($("#noofunits_" + row).val());
        var rentunit = converttonumber($("#rentunit_" + row).val());
        var rent = converttonumber($("#rent_" + row).val()).toFixed(2);
        var period = converttonumber($("#period_" + row).val());
        var oldnoofunits = converttonumber($("#onoofsboxes_" + row).val());
        
        $("#noofsboxes_" + row).val("");

        var noofsboxes = noofunits;
        if(units == 1) {
        	noofsboxes = noofunits;
        } else if (units == 2) {
        	noofsboxes = noofunits * 2;
        } else if (units == 3 || units == 4) {
        	noofsboxes = noofunits * 4;
        }

        var cfloor = $("#floor_" + row).val();
        var cchamber = $("#chamber_" + row).val();
        var crack = $("#rack_" + row).val();
        var enterednoofsboxes = noofsboxes;
        var oenterednoofsboxes = 0;
        for (x = 0; x < tableProductLength; x++) {
            var tr = $("#product_info_table_gst tbody tr")[x];
            var count = $(tr).attr('id');
            count = count.substring(4);
            var floor = $("#floor_" + count).val();
            var chamber = $("#chamber_" + count).val();
            var rack = $("#rack_" + count).val();
            
            if(cfloor == floor && cchamber == chamber && crack == rack) {
                enterednoofsboxes = converttonumber(enterednoofsboxes) + converttonumber($("#noofsboxes_" + count).val());
                oenterednoofsboxes = converttonumber(oenterednoofsboxes) + converttonumber($("#onoofsboxes_" + count).val());
            }
        }
        $("#enterednoofsboxes").val(enterednoofsboxes);
        
    	if(update)
		{
			rackavlspc = rackavlspc + oenterednoofsboxes;
		}
        
        if(rackcheck) {
	        if (enterednoofsboxes > rackavlspc) {
	        	//var message = "<label for=\"gross_amount\" class=\"col-sm-5 control-label\"
					//style=\"text-align: left;font-size: 19px;height: 34px;padding-top: 5px;background-color: #ec8282;\">Total Items</label>\"
			
				//showpopupdetails('popup_detail_model_body', 'popup_detail_model', message)

				showpopupdetails('popup_detail_model_body', 'popup_detail_model', "<b>Rack limit Exceed for </b> - <br> <br> Floor :  \"" + getFloorName(cfloor) + "\" \n  <br> Chamber :  \"" + getChamberName(cchamber) +"\"  \n  <br> Rack :  \"" + crack + "\"");
	       
				enterednoofsboxes = converttonumber(enterednoofsboxes) - noofsboxes;
	        	$("#noofunits_" + row).val("");
	        	$("#noofsboxes_" + row).val("");
	        }
	        
	        $("#enterednoofsboxes").val(enterednoofsboxes);
        }
        
        $("#noofsboxes_" + row).val(noofsboxes);
        $("#balanceqty_" + row).val(noofunits);
        
        var total = 0;
        if(rentunit == 3) {
        	total = kg * rent * noofunits * period;
        } else {
        	total = noofunits * rent * period;
        }
        total = total.toFixed(2);
   
        $("#totamount_" + row).val(total);
        calculateWages(row);
    } else {
        alert('no row !! please refresh the page')
    }
}

function fillRack(row_id) {
	  $("#rack_" + row_id).find('option').remove(); 
	  $("#rackbal_" + row_id).find('option').remove(); 
	  
	  var floor = $("#floor_"+ row_id).val();
	  var chamber = $("#chamber_"+ row_id).val();
	    if (floor == "" || chamber == "" ) {
	    	floor = 1;
	    	chamber = 1;
	    } else {
	        $.ajax({
	            url: base_url + 'inward/getAvailableRacks',
	            type: 'post',
	            data: {
	            	floor : floor,
	    	    	chamber : chamber
	            },
	            dataType: 'json',
	            success: function(response) {
	            	for (var i = 0; i < response.length; i++) {
	          	      $('#rack_'+ row_id).append('<option value="' + response[i].rack + '">' + response[i].rack + '</option>');
	          	   	  $('#rackbal_'+ row_id).append('<option value="' + response[i].rack + '">' + response[i].rackbalspace + '</option>');
	          	    }
	            	//var sboxes = response[0].rackbalspace;
					//var mboxes = (Number(sboxes)/2).toFixed(0);
					//var lboxes = (Number(sboxes)/4).toFixed(0);

					//$("#tsboxes").val(sboxes);
					//$("#tmboxes").val(mboxes);
					//$("#tlboxes").val(lboxes);
					//$("#tbags").val(lboxes);

					fillAvailSpace(row_id);

		        }
	        })
	    }
}

function fillAvailSpace(row_id) {
	  var rack = Number($("#rack_"+ row_id).val());
	  var rackbal = Number($("#rackbal_"+ row_id+" option[value='"+rack+"']").text());

	  var sboxes = rackbal;
	  var mboxes = parseInt((Number(sboxes)/2));
	  var lboxes = parseInt((Number(sboxes)/4));

	  $("#tsboxes").val(sboxes);
	  $("#tmboxes").val(mboxes);
	  $("#tlboxes").val(lboxes);
	  $("#tbags").val(lboxes);  
	  $("#noofunits_" + row_id).val("");
	  $("#noofsboxes_" + row_id).val("");
	  $("#enterednoofsboxes").val("");
}

function showpopupdetails(model_body, model, message) {
	$('#'+model_body).html(message);
    $('#'+model).modal('show');
}

function getOutTotalRent(row = null) {
    if (row) {

    	var avlunits = converttonumber($("#avlunits_" + row).val());
    	var noofunits = converttonumber($("#noofunits_" + row).val());

    	var total_noofsboxes = $("#total_noofsboxes").val();
    	if (noofunits > avlunits) {
    		showpopupdetails('popup_detail_model_body', 'popup_detail_model', "No of units cannot exceed Available units");
    		//alert("No of units cannot exceed Available units");
    		total_noofsboxes = total_noofsboxes - converttonumber($("#noofsboxes_" + row).val());
    		$("#total_noofsboxes").val(total_noofsboxes)
    		$("#noofunits_" + row).val("");
    		$("#noofsboxes_" + row).val("");
    		return false;
    	}
    	
    	var units = converttonumber($("#units_id_" + row).val());
        var kg = converttonumber($("#kg_" + row).val()).toFixed(2);
       
        var rentunit = converttonumber($("#rentunit_" + row).val());
        var rent = converttonumber($("#rent_" + row).val()).toFixed(2);
        var period = converttonumber($("#period_" + row).val());
        
        var noofsboxes = noofunits;
        if(units == 1) {
        	noofsboxes = noofunits;
        } else if (units == 2) {
        	noofsboxes = noofunits * 2;
        } else if (units == 3 || units == 4) {
        	noofsboxes = noofunits * 4;
        }

        $("#noofsboxes_" + row).val(noofsboxes);
        $("#balanceqty_" + row).val(noofunits);
        
        var total = 0;
        if(rentunit == 3) {
        	total = kg * rent * noofunits * period;
        } else {
        	total = noofunits * rent * period;
        }
        total = total.toFixed(2);
   
        $("#totamount_" + row).val(total);
        calculateWages(row);
    } else {
        alert('no row !! please refresh the page')
    }
}

function subRAmount() {
    var tableProductLength = $("#product_info_table_gst tbody tr").length;
    var totalAmount = 0;
    var total_noofsboxes = 0;
    var totdiscountAmount = 0;
    var taxableamount = 0;
    var total_balanceqty = 0;

    var total_wages = 0;
    
    for (x = 0; x < tableProductLength; x++) {
        var tr = $("#product_info_table_gst tbody tr")[x];
        var count = $(tr).attr('id');
        count = count.substring(4);
        totalAmount = converttonumber(totalAmount) + converttonumber($("#totamount_" + count).val());  
        total_balanceqty = converttonumber(total_balanceqty) + converttonumber($("#balanceqty_" + count).val());
        total_wages = converttonumber(total_wages) + converttonumber($("#wage_" + count).val());
    }

    totalAmount = totalAmount.toFixed(2);
    $("#rent_amount").val(totalAmount); 
    $("#total_balanceqty").val(total_balanceqty);
    $("#wages").val(total_wages);
    
    calculateNet();
}

function calculateNet() {
	var rent_amount =  converttonumber($("#rent_amount").val());
	var wages =  converttonumber($("#wages").val());
	var cleaningcharge =  converttonumber($("#cleaningcharge").val());
	var vehtranscharge =  converttonumber($("#vehtranscharge").val());
	var comcharge =  converttonumber($("#comcharge").val());
	var untimedloading =  converttonumber($("#untimedloading").val());
	var disc_amount =  converttonumber($("#disc_amount").val());
	
	//var inbalance =  converttonumber($("#inbalance").val());
	
	var total = (rent_amount + wages + cleaningcharge + vehtranscharge + comcharge + untimedloading).toFixed(2);
	var net = (total - disc_amount).toFixed(2);
	
	$("#total_amount").val(total);
	$("#net_amount").val(net);
	
	calculateRBalance();
}

function calculateInvNet() {
	var rent_amount =  converttonumber($("#itotalamt").val());
	var others =  converttonumber($("#itotalothers").val());
	var iamtpaid =  converttonumber($("#iamtpaid").val());
	
	var wages =  converttonumber($("#wages").val());
	var cleaningcharge =  converttonumber($("#cleaningcharge").val());
	var vehtranscharge =  converttonumber($("#vehtranscharge").val());
	var comcharge =  converttonumber($("#comcharge").val());
	var untimedloading =  converttonumber($("#untimedloading").val());
	var disc_amount =  converttonumber($("#disc_amount").val());
	
	//var inbalance =  converttonumber($("#inbalance").val());
	
	var total = (rent_amount + others + wages + cleaningcharge + vehtranscharge + comcharge + untimedloading - iamtpaid).toFixed(2);
	var net = (total - disc_amount).toFixed(2);
	
	$("#total_amount").val(total);
	$("#net_amount").val(net);
	
	calculateRBalance();
}

function calculateRBalance() {
	var net =  Number($("#net_amount").val());
	var paid = Number($("#amtpaid").val());
	var bal = (net - paid).toFixed(2);;
	$("#balance").val(bal);
}

function paidAmount() {
    var grandTotal = $("#net_amount").val();
    if (grandTotal) {
        var dueAmount = converttonumber($("#net_amount").val()) - converttonumber($("#paid_amount").val());
        dueAmount = dueAmount.toFixed(2);
        $("#remaining").val(dueAmount);
        $("#remaining_value").val(dueAmount)
    }
}

function removeRow(tr_id) {
    $("#product_info_table_gst tbody tr#row_" + tr_id).remove();
	var rowid = $("#row_index").val();
	var newRowid = converttonumber(rowid) - 1;
	$("#row_index").val(newRowid);
	$("#total_items").val(newRowid);
	var btn = $("#add_row");
	btn.removeAttr("disabled");
	subAmount()
}

function removeRRow(tr_id) {
    $("#product_info_table_gst tbody tr#row_" + tr_id).remove();
	var rowid = $("#row_index").val();
	var newRowid = converttonumber(rowid) - 1;
	$("#row_index").val(newRowid);
	$("#total_items").val(newRowid);
	subRAmount()
}

function getCustData() {
    var cust_id = $("#ph_no").val();
    if (cust_id == "") {
        $("#cust_address").val("");
        $("#gst_no").val("");
        $("#cust_name").val("");
        $("#email_id").val("")
    } else {
        $.ajax({
            url: base_url + 'orders/getCustomerData',
            type: 'post',
            data: {
                cust_id: cust_id
            },
            dataType: 'json',
            success: function(response) {
            	
            	$("#ph_no_hid").val(response.ph_no);
				$("#cust_hid").val(response.cust_id);
                $("#cust_address").val(response.cust_address);
                $("#gst_no").val(response.gst_no);
                $("#cust_name").val(response.cust_name);
                $("#email_id").val(response.email_id);
                $("#state").val(response.state);
                $("#state_code").val(response.state_code)
            }
        })
    }
}

function clearCus() {
    $("#customer_name").val("");
    $("#customer_address").val("");
    $("#customer_gst").val("");
    $("#customer_phone").val("");
    $("#customer_email").val("");
    $("#customer_state").val("");
    $("#customer_state_code").val("")
}

function selectCus() {
    var cname = $("#cust_name").val();
    var caddr = $("#cust_address").val();
	var cid = $("#cust_hid").val();
    var gst = $("#gst_no").val();
    var phone = $("#ph_no_hid").val();
    var email = $("#email_id").val();
    var state = $("#state").val();
    var statecode = $("#state_code").val();
    $("#customer_name").val(cname);
	$("#customer_id").val(cid);
    $("#customer_address").val(caddr);
    $("#customer_gst").val(gst);
    $("#customer_phone").val(phone);
    $("#customer_email").val(email);
    $("#customer_state").val(state);
    $("#customer_state_code").val(statecode)
}
function getRentItemDataName(row_id) {
    var product_id = $("#rentitem_code_" + row_id).val();
    if (product_id) {
        $.ajax({
            url: base_url + 'rentitems/getRentItemValueById',
            type: 'post',
            async: false,
            data: {
                product_id: product_id
            },
            dataType: 'json',
            success: function(response) {
				var row_items = $("#row_index").val();
				$("#total_items").val(row_items);
			 
				$("#rentitem_name_" + row_id).val(response.id);
				//Set text to display in select
				$("#select2-rentitem_name_" + row_id +"-container").text(response.name);
            }
        })
    }
}

function getRentItemDataCode(row_id) {
    var product_id = $("#rentitem_name_" + row_id).val();
    if (product_id) {
        $.ajax({
            url: base_url + 'rentitems/getRentItemValueById',
            type: 'post',
            async: false,
            data: {
                product_id: product_id
            },
            dataType: 'json',
            success: function(response) {
				var row_items = $("#row_index").val();
				$("#total_items").val(row_items);
				$("#rentitem_code_" + row_id).val(response.id);
				//Set text to display in select
				$("#select2-rentitem_code_" + row_id +"-container").text(response.code); 
            }
        })
    } 
}

function getRentItemData(row_id) {
    var product_id = $("#rentitem_name_" + row_id).val();
    if (product_id) {
        $.ajax({
            url: base_url + 'rentitems/getRentItemValueById',
            type: 'post',
            async: false,
            data: {
                product_id: product_id
            },
            dataType: 'json',
            success: function(response) {
				var row_items = $("#row_index").val();
				$("#total_items").val(row_items);
			 
                $("#units_" + row_id).val(response.units);
                $("#avlqty_" + row_id).val(response.availableqty);
                $("#minrentaldays_" + row_id).val(response.minrentaldays);
                $("#rate_" + row_id).val(response.rate); 
            }
        })
    } else {
    	 $("#units_" + row_id).val("");
         $("#avlqty_" + row_id).val("");
         $("#minrentaldays_" + row_id).val("");
         $("#rate_" + row_id).val(""); 
    }
}

function getrespectivekg(unit, row_id, response) {
	var product_id = $("#product_" + row_id).val();
	var kgresp = response;
	var kg = 0;
	if (!kgresp && product_id) {
        $.ajax({
            url: base_url + 'rentitems/getRentItemValueById',
            type: 'post',
            async: false,
            data: {
                product_id: product_id
            },
            dataType: 'json',
            success: function(response) {
            	kgresp = response;
            }
        })
	}
	
	if(kgresp) {
		kg = kgresp.skg;
		switch (converttonumber(unit)) {
		  case 1:
			  kg = kgresp.skg;
		    break;
		  case 2:
			  kg = kgresp.mkg;
		    break;
		  case 3:
			  kg = kgresp.lkg;
		    break;
		  case 4:
			  kg = kgresp.bkg;
		    break;
		}
	}
	return kg;
}

function getProductItemData(row_id) {
    var product_id = $("#product_" + row_id).val();
    if (product_id) {
        $.ajax({
            url: base_url + 'rentitems/getRentItemValueById',
            type: 'post',
            data: {
                product_id: product_id
            },
            dataType: 'json',
            success: function(response) {
				$("#name_" + row_id).val(response.name);
            }
        })
    } else {
     	$("#name_" + row_id).val("");
    }
}

function getRent(row_id) {
	var product_id = $("#product_" + row_id).val();
	var supplier_id = $("#customer_id").val();
	var unit = $("#units_" + row_id).val();
	var defaultVal = $("#default").val();
	
    if (product_id == "") {
        $("#rent_" + row_id).val("");
    } else {
        $.ajax({
            url: base_url + 'scustomers/getRentValue',
            type: 'post',
            async: false,
            data: {
                product_id: product_id,
                supplier_id: supplier_id
            },
            dataType: 'json',
            success: function(response) {
            	var rent = response.snormal;
            	if(defaultVal == 1) {
            		switch (converttonumber(unit)) {
            		  case 1:
            			  rent = response.snormal;
            		    break;
            		  case 2:
            			  rent = response.mnormal;
            		    break;
            		  case 3:
            			  rent = response.lnormal;
            		    break;
            		  case 4:
            			  rent = response.bagnormal;
            		    break;
            		}
            	} else {
            		switch(converttonumber(unit)) {
	          		  case 1:
	          			rent = response.sseason;
	          		    break;
	          		  case 2:
	          			rent = response.mseason;
	          		    break;
	          		  case 3:
	          			rent = response.lseason;
	          		    break;
	          		  case 4:
	          			rent = response.bagseason;
	          		    break;
	          		}
            	}

				$("#rent_" + row_id).val(rent);
            }
        })
    }
}

function getRent1(row_id) {
	var product_id = $("#product_" + row_id).val();
	var supplier_id = $("#customer_id").val();
	var unit = $("#units_" + row_id).val();
	
    if (product_id == "") {
        $("#rent_" + row_id).val("");
    } else {
        $.ajax({
            url: base_url + 'suppliers/getRentValue',
            type: 'post',
            data: {
                product_id: product_id,
                supplier_id: supplier_id
            },
            dataType: 'json',
            success: function(response) {
            	var rent = response.snormal;
            	if(response.default == 1) {
            		switch (converttonumber(unit)) {
            		  case 1:
            			  rent = response.snormal;
            		    break;
            		  case 2:
            			  rent = response.mnormal;
            		    break;
            		  case 3:
            			  rent = response.lnormal;
            		    break;
            		  case 4:
            			  rent = response.bagnormal;
            		    break;
            		}
            	} else {
            		switch (unit) {
	          		  case 1:
	          			rent = response.sseason;
	          		    break;
	          		  case 2:
	          			rent = response.mseason;
	          		    break;
	          		  case 3:
	          			rent = response.lseason;
	          		    break;
	          		  case 4:
	          			rent = response.bagseason;
	          		    break;
	          		}
            	}

				$("#rent_" + row_id).val(rent);
            }
        })
    }
}

function getInwardItemData(row_id) {
    var inward_item_id = $("#product_" + row_id).val();
    var inward_id = $("#inward_no").val();

    var itemcnt = 0;
    if (inward_item_id == "") {
        $("#units_" + row_id).val(1);
        $("#kg_" + row_id).val("");
	} else {
		 if(checkSameProd(inward_item_id)) {
		    	$("#product_" + row_id).val("");
				//Set text to display in select
				$("#select2-product_" + row_id +"-container").text("");
				
				$("#name_" + row_id).val("");
		        $("#floor_" + row_id).val("");
		        $("#floor_id_" + row_id).val("");
		        $("#chamber_" + row_id).val("");
		        $("#chamber_id_" + row_id).val("");
		        $("#rack_" + row_id).val("");
		        $("#units_id_" + row_id).val("");
		        $("#units_" + row_id).val("");
		        $("#unitsinp_" + row_id).val("");
		        $("#rent_" + row_id).val("");
		        $("#kg_" + row_id).val("");
		        $("#avlunits_" + row_id).val("");
		        $("#period_" + row_id).val("");
			      
		        showpopupdetails('popup_detail_model_body', 'popup_detail_model',
		        		"Cannot pick same item already existing in table");
		    } else {
		        $.ajax({
		            url: base_url + 'inward/getInwardItemValueById',
		            type: 'post',
		            data: {
		            	inward_item_id: inward_item_id,
		                inward_id : inward_id,
		            },
		            dataType: 'json',
		            success: function(response) {
						var row_items = $("#row_index").val();
						$("#total_items").val(row_items);
						$("#name_" + row_id).val(response.product_name);
						$("#product_id_" + row_id).val(response.product_id);
		                $("#floor_" + row_id).val(getFloorName(response.floor));
		                $("#floor_id_" + row_id).val(response.floor);
		                $("#chamber_" + row_id).val(getChamberName(response.chamber));
		                $("#chamber_id_" + row_id).val(response.chamber);
		                $("#rack_" + row_id).val(response.rack);
		                $("#units_id_" + row_id).val(response.units);
		                $("#units_" + row_id).val(getUnitsName(response.units));
		                $("#unitsinp_" + row_id).val(response.units);
		                $("#rent_" + row_id).val(response.rent);
		                $("#kg_" + row_id).val(response.kg);
		                $("#avlunits_" + row_id).val(response.balanceqty);

		            }
	        })
	    }
    }
}

function getProductData(row_id, service_charge, vat_charge, sales) {
    var product_id = $("#product_" + row_id).val();
    if (product_id == "") {
        $("#rate_" + row_id).val("");
        $("#qty_" + row_id).val("");
        $("#amount_" + row_id).val("");
    } else {
        $.ajax({
            url: base_url + 'poorders/getProductValueById',
            type: 'post',
            data: {
                product_id: product_id
            },
            dataType: 'json',
            success: function(response) {
				var row_items = $("#row_index").val();
				var disc = 1;
				var price = response.price;
				$("#total_items").val(row_items);
				
				$("#pcode_" + row_id).val(response.code);
				if(sales == true) {
				   $("#rate_" + row_id).val(response.price);
		           $("#discount_" + row_id).val(response.discount);
		           disc = response.discount;
				} else {
				   $("#rate_" + row_id).val(response.pprice);
		           price = response.pprice;
				}
             
                $("#qty_" + row_id).val(1);
                $("#qty_value_" + row_id).val(1);
                $("#hsnsac_" + row_id).val(response.sku);
                $("#units_" + row_id).val(response.units);
                $("#cft_" + row_id).val(response.cft);
                $("#gst_" + row_id).val(response.gst);
                $("#cgst_" + row_id).val(response.cgst);
                $("#sgst_" + row_id).val(response.sgst);
                var total = converttonumber(price) * 1;
                total = total.toFixed(4);
                $("#amount_" + row_id).val(total);
                $("#discount_value_" + row_id).val(total);
                $("#taxamount_" + row_id).val(total);
                var gst = converttonumber($("#gst_" + row_id).val());
                var gtotal = converttonumber(((gst / 100) * total) + converttonumber(total));
                gtotal = gtotal.toFixed(4);
                $("#totamount_" + row_id).val(gtotal);
                calcDis(row_id);
            }
        })
    }
}

function getProductCodeData(product_code, row_id, service_charge, vat_charge) {
    if (product_code == "") {
        $("#rate_" + row_id).val("");
        $("#qty_" + row_id).val("");
        $("#amount_" + row_id).val("");
    } else {
        $.ajax({
            url: base_url + 'poorders/getProductValueByCode',
            type: 'post',
            data: {
                product_code: product_code
            },
            dataType: 'json',
            success: function(response) {
				 if(response)
				 {
					 var existingRowProdName = $("#select2-product_" + row_id +"-container").text()			
					 if(existingRowProdName != "")
					 {
						$("#add_row")[0].click(); 
						row_id = $("#row_index").val();
					 }
					 setTimeout(function() {
						var row_id = $("#row_index").val();
						$("#total_items").val(row_id);
						
						$("#pcode_" + row_id).val(product_code);		 
						$("#product_" + row_id).val(response.id);
						//Set text to display in select
						$("#select2-product_" + row_id +"-container").text(response.name);
						$("#rate_" + row_id).val(response.price);
					    $("#discount_" + row_id).val(response.discount);
						$("#qty_" + row_id).val(1);
						$("#qty_value_" + row_id).val(1);
						$("#hsnsac_" + row_id).val(response.sku);
						$("#units_" + row_id).val(response.units);
						$("#cft_" + row_id).val(response.cft);
						$("#gst_" + row_id).val(response.gst);
						$("#cgst_" + row_id).val(response.cgst);
						$("#sgst_" + row_id).val(response.sgst);
						var total = converttonumber(response.price) * 1;
						total = total.toFixed(4);
						$("#amount_" + row_id).val(total);
						$("#discount_value_" + row_id).val(total);
						$("#taxamount_" + row_id).val(total);
						var gst = converttonumber($("#gst_" + row_id).val());
						var gtotal = converttonumber(((gst / 100) * total) + converttonumber(total));
						gtotal = gtotal.toFixed(4);
						$("#totamount_" + row_id).val(gtotal);
						calcDis(row_id);

						$("#product_code").val("");
						 
					}, 1201);
	/*}, 500);*/
				} else 
				{
					document.getElementById("error2").style.display = "inline";
				}
			}
        })          
    }
}

function getInwardDetails() {
    var inward_no = $("#inward_no").val();
    if (inward_no == "") {
    	
    	$("#total_balanceqty").val("");
        $("#idate").val("");
        $("#isupl_name").val("");
        $("#isupl_phone").val("");
        $("#outto").val(1);
        $("#floor").val(1);
        $("#chamber").val(1);
        $("#rack").val("");
		$("#semail").val("");
        $("#total_items").val("");
        $("#totaldays").val("");
    	chooseSupl();
    } else {
        $.ajax({
            url: base_url + 'inward/getInwardDetailsbyId',
            type: 'post',
            data: {
            	inward_no: inward_no
            },
            dataType: 'json',
            success: function(response) {
				$("#total_balanceqty").val(response.total_balanceqty);
                $("#idate").val(response.sdate);
                $("#isupl_name").val(response.supplier_name);
                $("#isupl_phone").val(response.ph_no);
                $("#semail").val(response.email_id);
                $("#customer_name").val(response.supplier_name);
                $("#customer_address").val(response.supplier_address);
                $("#customer_phone").val(response.ph_no);
                $("#customer_gst").val(response.gst_no);
                $("#customer_email").val(response.email_id);
                $("#customer_state").val(response.state);
                $("#customer_state_code").val(response.state_code);
                $("#grace_period").val(response.graceperiod);
                
                $("#total_items").val(response.total_items);
                
                var days = calculateDays($("#idate").val(), $('#datepicker').val());
               
                $('#product_1').find('option').remove(); 
                
                var opts = response.availInward;
                $.each(opts, function(i, d) {
                     $('#product_1').append('<option value="' + d.id + '">' + d.product_name + '</option>');
                });
                getInwardItemData(1);
            }
        })
    }
}


function getCreditInwardDetails() {
    var inward_no = $("#inward_no").val();
    if (inward_no == "") {
        $("#idate").val("");
        $("#isupl_name").val("");
        $("#isupl_phone").val("");
		$("#semail").val("");
        $("#totaldays").val("");
    } else {
        $.ajax({
            url: base_url + 'inward/getInwardDetailsbyId',
            type: 'post',
            data: {
            	inward_no: inward_no
            },
            dataType: 'json',
            success: function(response) {
                $("#idate").val(response.sdate);
                $("#customer_name").val(response.supplier_name);
                $("#customer_address").val(response.supplier_address);
                $("#customer_phone").val(response.ph_no);
                $("#customer_gst").val(response.gst_no);
                $("#customer_email").val(response.email_id);
                $("#customer_state").val(response.state);
                $("#customer_state_code").val(response.state_code);
                
                $("#creditamt").val(response.credit);
                $("#interest").val(response.interest);
            }
        })
    }
}

function calculateDays1(start, end) {
    var startdate = new Date(formatDate(start));
    var enddate = new Date(formatDate(end));
    
    // end - start returns difference in milliseconds 
    var diff = enddate - startdate;

    // get days
    var days = diff/1000/60/60/24;

    $("#totaldays").val(days);
    return days;
}

function calculateDays(start, end) {
	 var d1 = new Date(formatDate(start));
	 var d2 = new Date(formatDate(end));
	    
	  var m = moment(d2);
	  var years = m.diff(d1, 'years');
	  m.add(-years, 'years');
	  var months = m.diff(d1, 'months');
	  m.add(-months, 'months');
	  var days = m.diff(d1, 'days');
 
	   var diff = d2 - d1;

	    // get days
	    var tdays = diff/1000/60/60/24;
	    $('[name="period[]"]').val(tdays); 
	    
		if(months == 0) {
			 $("#totaldays").val(days+' Days');
			//return '('+days+')';
		} else {
			 $("#totaldays").val(tdays +'('+months+' Months '+days+' Days)');
			//return months+'('+days+')';
		}
		return tdays;
	}

function calculateDaysd(start, end) {
    var startdate = new Date(formatDate(start));
    var enddate = new Date(formatDate(end));
    
    var date1_ms = startdate.getTime();
    var date2_ms = enddate.getTime();

    // Calculate the difference in milliseconds
    var diff = date2_ms - date1_ms;
    
    // get days
    var tdays = diff/1000/60/60/24;
    
    $('[name="period[]"]').val(tdays); 

	var day = 1000 * 60 * 60 * 24;
	var days = Math.floor(diff/day);
    var months = Math.floor(days/31);
    var years = Math.floor(months/12);
    
	if(months == 0) {
		 $("#totaldays").val(days+' Days');
		//return '('+days+')';
	} else {
		 $("#totaldays").val(tdays +'('+months+' Months '+days+' Days)');
		//return months+'('+days+')';
	}
	return tdays;
   
   // return days;
}

function getUnitsName(unitid) {
	 var unit = "S-Box";
    if(unitid == 1) {
    	unit = "S-Box";
    } else if(unitid == 2) {
    	unit = "M-Box";
    } else if(unitid == 3) {
    	unit = "L-Box";
    } else if(unitid == 4) {
    	unit = "Bag";
    } else {
    	unit = "S-Box";
    }
    return unit;
}

function getChamberName(chamberid) {
	 var chamber = "Chamber 1";
     if(chamberid == 1) {
     	chamber = "Chamber 1";
     } else if(chamberid == 2) {
     	chamber = "Chamber 2";
     } else if(chamberid == 3) {
     	chamber = "Chamber 3";
     } else if(chamberid == 4) {
     	chamber = "Chamber 4";
     } else {
     	chamber = "Chamber 1";
     }
     return chamber;
}

function getFloorName(floorid) {
    var floor = "G - Floor";
    if(floorid == 1) {
    	floor = "G - Floor";
    } else if(floorid == 2) {
    	floor = "1 - Floor";
    } else if(floorid == 3) {
    	floor = "2 - Floor";
    } else if(floorid == 4) {
    	floor = "3 - Floor";
    } else {
    	floor = "G - Floor";
    }
	return floor;
}

function formatDate(date) {
	var d=new Date(date.split("/").reverse().join("-"));
	var dd=d.getDate();
	var mm=d.getMonth()+1;
	var yy=d.getFullYear();
	var newdate=yy+"/"+mm+"/"+dd;
	return newdate;
}

function getSuplData() {
    var supl_id = $("#supp_name").val();
    if (supl_id == "") {
        $("#supp_address").val("");
        $("#gst_no").val("");
        $("#ph_no").val("");
        $("#email_id").val("")
    } else {
        $.ajax({
            url: base_url + 'scustomers/getSupplierData',
            type: 'post',
            data: {
                supl_id: supl_id
            },
            dataType: 'json',
            success: function(response) {
				$("#supp_hid").val(response.supl_id);
                $("#supp_name_hid").val(response.supp_name);
                $("#supp_address").val(response.supp_address);
                $("#gst_no").val(response.gst_no);
                $("#ph_no").val(response.ph_no);
                $("#email_id").val(response.email_id);
                $("#state").val(response.state);
                $("#state_code").val(response.state_code);
                $("#graceperiod").val(response.grace_period)
            }
        })
    }
}

function getSubSuplData(){
    var supl_id = $("#sub_supp_name").val();
    if (supl_id == "") {
        $("#sub_supp_address").val("");
        $("#sub_gst_no").val("");
        $("#sub_ph_no").val("");
        $("#sub_email_id").val("")
    } else {
        $.ajax({
            url: base_url + 'subscustomers/getSupplierData',
            type: 'post',
            data: {
                supl_id: supl_id
            },
            dataType: 'json',
            success: function(response) {
				$("#sub_supp_hid").val(response.supl_id);
                $("#sub_supp_name_hid").val(response.supp_name);
                $("#sub_supp_address").val(response.supp_address);
                $("#sub_gst_no").val(response.gst_no);
                $("#sub_ph_no").val(response.ph_no);
                $("#sub_email_id").val(response.email_id);
                $("#sub_state").val(response.state);
                $("#sub_state_code").val(response.state_code)
            }
        })
    }
}

function getDriverData() {
    var id = $("#truckno").val();
    if (id == "") {
        $("#driverphone").val("");
        $("#driver_hid").val("");
        $("#drivername").val("");
        $("#truckno").val("");
    } else {
        $.ajax({
            url: base_url + 'driver/getDriverDataById',
            type: 'post',
            data: {
            	id: id
            },
            dataType: 'json',
            success: function(response) {
            	$("#driver_hid").val(response.id);
            	$("#truck_no_hid").val(response.truckno);
            	$("#driverphone").val(response.driverphone);
                $("#drivername").val(response.drivername);
            }
        })
    }
}

function getemployeeData() {
    var id = $("#employee").val();
    if (id == "") {
        $("#driverphone").val("");
        $("#driver_hid").val("");
        $("#drivername").val("");
        $("#truckno").val("");
    } else {
        $.ajax({
            url: base_url + 'employee/getEmployeeDataById',
            type: 'post',
            data: {
            	id: id
            },
            dataType: 'json',
            success: function(response) {
            	$("#sbox_wage").val(response.sboxwage);
                $("#mbox_wage").val(response.mboxwage);
                $("#lbox_wage").val(response.lboxwage);
                $("#bag_wage").val(response.bagwage);
                
                getWages();
            } 
        })
    }
}


function selectDriver() {
    var did = $("#driver_hid").val();
	var drivername = $("#drivername").val();
    var truckno = $("#truck_no_hid").val();
    var driverphone = $("#driverphone").val();

    $("#driver_phone").val(driverphone);
	$("#driver_id").val(did);
    $("#driver_name").val(drivername);
    $("#cont_no").val(truckno);
}

function clearDri() {
    $("#driverphone").val("");
    $("#driver_hid").val("");
    $("#drivername").val("");
    $("#truckno").val("");
}

function converttonumber(value) {
	return (typeof value != 'undefined') ? Number(value) : 0;
}


function selectSupl() {
    var cname = $("#supp_name_hid").val();
	var cid = $("#supp_hid").val();
    var caddr = $("#supp_address").val();
    var gst = $("#gst_no").val();
    var phone = $("#ph_no").val();
    var email = $("#email_id").val();
    var state = $("#state").val();
    var statecode = $("#state_code").val();
    var graceperiod = $("#graceperiod").val();
    $("#customer_name").val(cname);
	$("#customer_id").val(cid);
    $("#customer_address").val(caddr);
    $("#customer_gst").val(gst);
    $("#customer_phone").val(phone);
    $("#customer_email").val(email);
    $("#customer_state").val(state);
    $("#customer_state_code").val(statecode);
    $("#grace_period").val(graceperiod)
}

function selectinwSupl() {
    var cname = $("#supp_name_hid").val();
	var supplier_id = $("#supp_hid").val();
    var caddr = $("#supp_address").val();
    var gst = $("#gst_no").val();
    var phone = $("#ph_no").val();
    var email = $("#email_id").val();
    var state = $("#state").val();
    var statecode = $("#state_code").val();
    $("#customer_name").val(cname);
	$("#customer_id").val(supplier_id);
    $("#customer_address").val(caddr);
    $("#customer_gst").val(gst);
    $("#customer_phone").val(phone);
    $("#customer_email").val(email);
    $("#customer_state").val(state);
    $("#customer_state_code").val(statecode)
    
    if(supplier_id) {
	    $.ajax({
            url: base_url + 'scustomers/getSupplierProducts',
            type: 'post',
            data: {
            	supplier_id: supplier_id
            },
            dataType: 'json',
            success: function(response) {
                $('#product_1').find('option').remove(); 
                $('#product_1').append('<option value=""></option>');
                
                var opts = response;
                $.each(opts, function(i, d) {
                     $('#product_1').append('<option value="' + d.id + '">' + d.name + '</option>');
                });
                getRentItemData(1);
            }
        })
      
    }
}

function selectSubSupl() {
    var cname = $("#sub_supp_name_hid").val();
	var cid = $("#sub_supp_hid").val();
    var caddr = $("#sub_supp_address").val();
    var gst = $("#sub_gst_no").val();
    var phone = $("#sub_ph_no").val();
    var email = $("#sub_email_id").val();
    var state = $("#sub_state").val();
    var statecode = $("#sub_state_code").val();
    $("#customer_name").val(cname);
	$("#customer_id").val(cid);
    $("#customer_address").val(caddr);
    $("#customer_gst").val(gst);
    $("#customer_phone").val(phone);
    $("#customer_email").val(email);
    $("#customer_state").val(state);
    $("#customer_state_code").val(statecode)
}

function splitGST() {
    var total = converttonumber(document.getElementById("gst").value) / 2;
    document.getElementById("cgst").value = total
    document.getElementById("sgst").value = total
}

function fnChkFolderName() {
    if (((window.event.keyCode < 48) || (window.event.keyCode > 57)) && ((window.event.keyCode < 65) || (window.event.keyCode > 90)) && ((window.event.keyCode < 97) || (window.event.keyCode > 122)) && (window.event.keyCode != 95)) {
        alert("Only Alphabets(A-Z, a-z), Numbers(0-9) and Underscore(_) are allowed");
        window.event.keyCode = 0;
        return !0
    }
}

function GetCountry(defaultValue, isNotWithSelect) {
    var sCountry = "Afghanistan,Albania,Algeria,American Samoa,Andorra,Angola,Anguilla,Antarctica,Antigua and Barbuda,Argentina,Armenia,Aruba,Australia,Austria,Azerbaidjan,Bahamas,Bahrain,Bangladesh,Barbados,Belarus,Belgium,Belize,Benin,Bermuda,Bolivia,Bosnia-Herzegovina,Botswana,Bouvet Island,Brazil,British Indian O. Terr.,Brunei Darussalam,Bulgaria,Burkina Faso,Burundi,Buthan,Cambodia,Cameroon,Canada,Cape Verde,Cayman Islands,Central African Rep.,Chad,Chile,China,Christmas Island,Cocos (Keeling) Isl.,Colombia,Comoros,Congo,Cook Islands,Costa Rica,Croatia,Cuba,Cyprus,Czech Republic,Czechoslovakia,Denmark,Djibouti,Dominica,Dominican Republic,East Timor,Ecuador,Egypt,El Salvador,Equatorial Guinea,Estonia,Ethiopia,Falkland Isl.(Malvinas),Faroe Islands,Fiji,Finland,France,France (European Ter.),French Southern Terr.,Gabon,Gambia,Georgia,Germany,Ghana,Gibraltar,Great Britain (UK),Greece,Greenland,Grenada,Guadeloupe (Fr.),Guam (US),Guatemala,Guinea,Guinea Bissau,Guyana,Guyana (Fr.),Haiti,Heard & McDonald Isl.,Honduras,Hong Kong,Hungary,Iceland,India,Indonesia,Iran,Iraq,Ireland,Israel,Italy,Ivory Coast,Jamaica,Japan,Jordan,Kazachstan,Kenya,Kirgistan,Kiribati,Korea (North),Korea (South),Kuwait,Laos,Latvia,Lebanon,Lesotho,Liberia,Libya,Liechtenstein,Lithuania,Luxembourg,Macau,Madagascar,Malawi,Malaysia,Maldives,Mali,Malta,Marshall Islands,Martinique (Fr.),Mauritania,Mauritius,Mexico,Micronesia,Moldavia,Monaco,Mongolia,Montserrat,Morocco,Mozambique,Myanmar,Namibia,Nauru,Nepal,Netherland Antilles,Netherlands,Neutral Zone,New Caledonia (Fr.),New Zealand,Nicaragua,Niger,Nigeria,Niue,Norfolk Island,Northern Mariana Isl.,Norway,Oman,Pakistan,Palau,Panama,Papua New,Paraguay,Peru,Philippines,Pitcairn,Poland,Polynesia (Fr.),Portugal,Puerto Rico (US),Qatar,Reunion (Fr.),Romania,Russian Federation,Rwanda,Saint Lucia,Samoa,San Marino,Saudi Arabia,Senegal,Seychelles,Sierra Leone,Singapore,Slovak Republic,Slovenia,Solomon Islands,Somalia,South Africa,Spain,Sri Lanka,St. Helena,St. Pierre & Miquelon,St. Tome and Principe,St.Kitts Nevis Anguilla,St.Vincent & Grenadines,Sudan,Suriname,Svalbard & Jan Mayen Is,Swaziland,Sweden,Switzerland,Syria,Tadjikistan,Taiwan,Tanzania,Thailand,Togo,Tokelau,Tonga,Trinidad & Tobago,Tunisia,Turkey,Turkmenistan,Turks & Caicos Islands,Tuvalu,Uganda,Ukraine,United Arab Emirates,United Kingdom,United States,Uruguay,US Minor outlying Isl.,Uzbekistan,Vanuatu,Vatican City State,Venezuela,Vietnam,Virgin Islands (British)";
    var xCountry = sCountry.split(",");
    var str = "";
    if (!isNotWithSelect) str += "<option value='' selected>Select Country</option>\n";
    else str += "<option value='' selected>Doesn't Matter</option>\n";
    for (i = 0; i < xCountry.length; i++)
        if (xCountry[i] == defaultValue) str += "<option value='" + xCountry[i] + "' selected>" + xCountry[i] + "</option>\n";
        else str += "<option value='" + xCountry[i] + "'>" + xCountry[i] + "</option>\n";
    document.write(str)
}

function fnShowDate(obj, msg) {
    var retdate = window.showModalDialog("includes/calender.htm", "", "dialogHeight: 219px; dialogWidth: 273px;  center: Yes; help: No; resizable: No; status: No;titlebar:No");
    obj.value = retdate
}

function fnShowDate1(obj, msg) {
    var retdate = window.showModalDialog("calender1.htm", "", "dialogHeight: 219px; dialogWidth: 273px;  center: Yes; help: No; resizable: No; status: No;titlebar:No");
    obj.value = retdate
}

$(document).ready(function() {
	$('form').submit(function () {
	    $(this).find(':submit').attr('disabled', 'disabled');
	});

	$('#datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	});

    $(".select_group").select2();
    $(".select_group1").select2({
      dropdownParent: $("#myModal")
    });
    
    $(".select_group2").select2({
        dropdownParent: $("#myModal1")
    });
    
	// Open view invoice modal as 
	$(document).on('click', '[data-toggle="ajax-modal"]', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link).done(function(data) {
            $('#viewInvModal').html(data)
            // .append("<script src='"+assets+"js/modal.js' />")
            .modal();
        });
        return false;
    });
	
	
    // Brand Drowdown populate
    $("#brands").keyup(function () {
    	$("#brands").removeClass("errortextboxcolor");
        $.ajax({
            type: "POST",
            url: base_url + 'brands/getBrandNames',
            data: {
                keyword: $("#brands").val()
            },
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    $('#DropdownBrandName').empty();
                    $('#brands').attr("data-toggle", "dropdown");
                    $('#DropdownBrandName').show();
                }
                else if (data.length == 0) {
                    $('#brands').attr("data-toggle", "");
                    $("#brands").addClass("errortextboxcolor");
                    $('#DropdownBrandName').hide();
                }
                $.each(data, function (key,value) {
                    if (data.length >= 0)
                        $('#DropdownBrandName').append('<li role="displayCountries" ><a role="menuitem dropdownCountryli" class="dropdownlivalue">' + value['name'] + '</a></li>');
                });
            }
        });
    });

    $("#brands").dblclick(function () {
    	$("#brands").removeClass("errortextboxcolor");
        $.ajax({
            type: "POST",
            url: base_url + 'brands/getBrandData',
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    $('#DropdownBrandName').empty();
                    $('#brands').attr("data-toggle", "dropdown");
                    $('#DropdownBrandName').show();
                }
                else if (data.length == 0) {
                    $('#brands').attr("data-toggle", "");
                    $("#brands").addClass("errortextboxcolor");
                    $('#DropdownBrandName').hide();
                }
                $.each(data, function (key,value) {
                    if (data.length >= 0)
                        $('#DropdownBrandName').append('<li role="displayCountries" class="displayCountries"><a role="menuitem dropdownCountryli" class="dropdownlivalue">' + value['name'] + '</a></li>');
                });
            }
        });
    });
    
    //Brand dropdown click
    $('ul.txtbrandname').on('click', 'li a', function () {
        $('#brands').val($(this).text());
        $('#DropdownBrandName').hide();
    });

    //Category Dropdown populate
    $("#category").keyup(function () {
    	$("#category").removeClass("errortextboxcolor");
        $.ajax({
            type: "POST",
            url: base_url + 'category/getCategoryNames',
            data: {
                keyword: $("#category").val()
            },
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    $('#DropdowncategoryName').empty();
                    $('#category').attr("data-toggle", "dropdown");
                    $('#DropdowncategoryName').show();
                }
                else if (data.length == 0) {
                    $('#category').attr("data-toggle", "");
                    $("#category").addClass("errortextboxcolor");
                    $('#DropdowncategoryName').hide();
                }
                $.each(data, function (key,value) {
                    if (data.length >= 0)
                        $('#DropdowncategoryName').append('<li role="displayCountries" class="displayCountries"><a role="menuitem dropdownCountryli" class="dropdownlivalue">' + value['name'] + '</a></li>');
                });
            }
        });
    });

    $("#category").dblclick(function () {
    	$("#category").removeClass("errortextboxcolor");
        $.ajax({
            type: "POST",
            url: base_url + 'category/getCategoryData',
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    $('#DropdowncategoryName').empty();
                    $('#category').attr("data-toggle", "dropdown");
                    $('#DropdowncategoryName').show();
                }
                else if (data.length == 0) {
                    $('#category').attr("data-toggle", "");
                    $("#category").addClass("errortextboxcolor");
                    $('#DropdowncategoryName').hide();
                }
                $.each(data, function (key,value) {
                    if (data.length >= 0)
                        $('#DropdowncategoryName').append('<li role="displayCountries" class="displayCountries"><a role="menuitem dropdownCountryli" class="dropdownlivalue">' + value['name'] + '</a></li>');
                });
            }
        });
    });
    
    //Category dropdown click
    $('ul.txtcategoryname').on('click', 'li a', function () {
        $('#category').val($(this).text());
        $('#DropdowncategoryName').hide();
    });

    //Store Dropdown populate
    $("#store").keyup(function () {
    	$("#store").removeClass("errortextboxcolor");
        $.ajax({
            type: "POST",
            url: base_url + 'stores/getStoreNames',
            data: {
                keyword: $("#store").val()
            },
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    $('#DropdownstoreName').empty();
                    $('#store').attr("data-toggle", "dropdown");
                    $('#DropdownstoreName').show();
                }
                else if (data.length == 0) {
                    $('#store').attr("data-toggle", "");
                    $("#store").addClass("errortextboxcolor");
                    $('#DropdownstoreName').hide();
                }
                $.each(data, function (key,value) {
                    if (data.length >= 0)
                        $('#DropdownstoreName').append('<li role="displayCountries" class="displayCountries"><a role="menuitem dropdownCountryli" class="dropdownlivalue">' + value['name'] + '</a></li>');
                });
            }
        });
    });

    //Store dropdown click
    $('ul.txtstorename').on('click', 'li a', function () {
        $('#store').val($(this).text());
        $('#DropdownstoreName').hide();
    });

  // State Drowdown populate
    $("#customer_state").keyup(function () {
    	$("#customer_state").removeClass("errortextboxcolor");
        $.ajax({
            type: "POST",
            url: base_url + 'stores/getStateNames',
            data: {
                keyword: $("#customer_state").val()
            },
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    $('#DropdownStateName').empty();
                    $('#customer_state').attr("data-toggle", "dropdown");
                    $('#DropdownStateName').show();
                }
                else if (data.length == 0) {
                    $('#customer_state').attr("data-toggle", "");
                    $("#customer_state").addClass("errortextboxcolor");
                    $('#DropdownStateName').hide();
                }
                $.each(data, function (key,value) {
                    if (data.length >= 0)
                        $('#DropdownStateName').append('<li role="displayCountries" ><a role="menuitem dropdownCountryli" class="dropdownlivalue">' + value['statename'] + '</a></li>');
                });
            }
        });
    });

    $("#customer_state").dblclick(function () {
    	$("#customer_state").removeClass("errortextboxcolor");
        $.ajax({
            type: "POST",
            url: base_url + 'stores/getStateNames',
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    $('#DropdownStateName').empty();
                    $('#customer_state').attr("data-toggle", "dropdown");
                    $('#DropdownStateName').show();
                }
                else if (data.length == 0) {
                    $('#customer_state').attr("data-toggle", "");
                    $("#customer_state").addClass("errortextboxcolor");
                    $('#DropdownStateName').hide();
                }
                $.each(data, function (key,value) {
                    if (data.length >= 0)
                        $('#DropdownStateName').append('<li role="displayCountries" class="displayCountries"><a role="menuitem dropdownCountryli" class="dropdownlivalue">' + value['statename'] + '</a></li>');
                });
            }
        });
    });
    
    //Brand dropdown click
    $('ul.txtstatename').on('click', 'li a', function () {
        $('#customer_state').val($(this).text());
        $('#DropdownStateName').hide();
        var customer_state = $('#customer_state').val();
        if (customer_state == "") {
            $("#customer_state_code").val("");
        } else {
            $.ajax({
                url: base_url + 'stores/getStoreCode',
                type: 'post',
                data: {
                	statename: customer_state
                },
                dataType: 'json',
                success: function(response) {
                	$("#schemeplan").removeClass("errortextboxcolor");
                    $("#customer_state_code").val(response.statecode);
                }
            })
        }
    });

    $(document).click(function(){
   	 $("#DropdownBrandName").hide(); 
   	 $("#DropdowncategoryName").hide(); 
   	 $("#DropdownstoreName").hide(); 
	 $("#DropdownStateName").hide(); 
   	});

});