function addoutwardrow() {
	 var btn = $("#add_row");
     btn.attr("disabled", "disabled");

     var count_table_tbody_tr = $("#product_info_table_gst tbody tr").length;
     var row_id = getlastrowid('#product_info_table_gst') + 1;
	 $("#row_index").val(row_id);
   
     $.ajax({
         url: base_url + 'rentitems/getRentitemsData/',
         type: 'post',
         dataType: 'json', 
         async: false,
         success:function(response) {  
              var html = '<tr id="row_'+row_id+'">';
              html += '<td data-title="Type"><select class="form-control" id="itype_'+row_id+'" name="itype[]" onchange="getItems('+row_id+')"'+
				'style="width: 100%;" required>'+
				'<option value="1">Box</option>'+
				'<option value="2">Single</option>'+ 
			'</select></td>'+
                  '<td data-title="Box Name"> <select class="form-control select_group product" data-row-id="'+row_id+'" id="boxitem_name_'+row_id+'" name="boxitem_name[]" style="width:100%;"'+
				   ' onchange="getCommonBoxItemData('+row_id+')">'+
                       '<option value=""></option>';
                       $.each(response.boxitems, function(index, value) {
                        if(value.name)
                        	 html += '<option value="'+value.id+'">'+value.name+'</option>';             
                       });
                       
                    html += '<input type="hidden" name="bname[]" id="bname_'+row_id+'"'+
					'class="form-control" autocomplete="off"></select></td>'; 
                /*	html +=  '<td data-title="Item Name"> <select class="form-control select_group product" data-row-id="'+row_id+'"'+
                        'id="rentitem_name_'+row_id+'" name="rentitem_name[]" style="width:100%;"'+
    				   ' onchange="getCommonProductData('+row_id+')">'+
                           '<option value=""></option>';
                           $.each(response, function(index, value) {
                            if(value.name)
                            	 html += '<option value="'+value.id+'">'+value.name+'</option>';             
                           });
                           
                        html += '<input type="hidden" name="rname[]" id="rname_'+row_id+'"'+
							'class="form-control" autocomplete="off"></select>'+ 
                           ' </td>'+  */
                      html += '<td data-title="Type"><select class="form-control" id="type_'+row_id+'" name="type[]"'+
							'style="width: 100%;" required>'+
							'<option value="1">Main</option>'+
							'<option value="2">Side</option>'+ 
						'</select></td>'+
						'<td data-title="Unit"><input type="text" name="units[]" id="units_'+row_id+'" value="Nos"' +
							'class="form-control" autocomplete="off"></td>' +  
						'<td data-title="Min R-Days">'+
								'<input type="text" name="minrentaldays[]" id="minrentaldays_'+row_id+'"' +
							'class="form-control" readonly autocomplete="off"></td>' +
						 '<td data-title="Rate"><input type="text" name="rate[]" id="rate_'+row_id+'" class="form-control"' +
							'autocomplete="off" readonly></td>' +
						 '<td data-title="Rate/Day"><input type="text" name="rateperday[]" id="rateperday_'+row_id+'" class="form-control"' +
							'autocomplete="off" readonly></td>' +
						'<td data-title="No.Units">' +
							'<input type="text" name="noofunits[]" id="noofunits_'+row_id+'" value="1"' +
						'class="form-control" required autocomplete="off"  onkeyup="getTotalRent('+row_id+')"> ' +
						'<input type="hidden" name="balanceqty[]" id="balanceqty_'+row_id+'"' +
							'class="form-control" placeholder="balanceqty" value="1"></td> ' + 		
						'<td data-title="Period"><input type="text" value="0" name="period[]" id="period_'+row_id+'"' +
							'class="form-control" autocomplete="off" onkeyup="getTotalRent('+row_id+')"></td>' +
						'<td data-title="Rent"><input type="text" value="0" name="totamount[]" id="totamount_'+row_id+'"' +
							'class="form-control" readonly autocomplete="off"></td>' +
                   '<td data-title="Remove"><button type="button" class="btn btn-default" onclick="removeRow(\''+row_id+'\')"><i class="fa fa-close"></i></button></td>'+
                   '</tr>';

               if(count_table_tbody_tr >= 1) {
               $("#product_info_table_gst tbody tr:last").after(html);   
             }
             else {
               $("#product_info_table_gst tbody").html(html);
             }

             $(".product").select2();
         }
       });
     return false; 
}

function addinwardrow(days) {
	 var btn = $("#add_row");
    btn.attr("disabled", "disabled");

    var count_table_tbody_tr = $("#product_info_table_gst tbody tr").length;
    var row_id = getlastrowid('#product_info_table_gst') + 1;
	 $("#row_index").val(row_id);
	 var outward_no = $("#outward_no").val();
    $.ajax({
        url: base_url + 'outward/getAvailableOutwardItems/',
        type: 'post',
        dataType: 'json', 
        data: {
          	outward_no: outward_no
        },
        async: false,
        success:function(response) {  
             var html = '<tr id="row_'+row_id+'">';
             html += '<td data-title="Type"><select class="form-control" id="itype_'+row_id+'" name="itype[]"'+
				'style="width: 100%;" required>'+
				'<option value="1">Box</option>'+
				'<option value="2">Single</option>'+ 
			'</select></td>'+
                 '<td data-title="Box Name"> '+
                    '<input type="hidden" name="boxitem_name[]" id="boxitem_name_'+row_id+'"'+
					'class="form-control" readonly autocomplete="off">'+
					'<input type="hidden" name="boxitem_id[]" id="boxitem_id_'+row_id+'"'+
					'class="form-control" readonly autocomplete="off">	'+
                     '<input type="text" name="bname[]" id="bname_'+row_id+'"'+
					'class="form-control" autocomplete="off"></td>'; 
               /*	html +=  '<td data-title="Item Name"> <select class="form-control select_group product" data-row-id="'+row_id+'"'+
                       'id="rentitem_name_'+row_id+'" name="rentitem_name[]" style="width:100%;"'+
   				   ' onchange="getCommonProductData('+row_id+')">'+
                          '<option value=""></option>';
                          $.each(response, function(index, value) {
                           if(value.name)
                           	 html += '<option value="'+value.id+'">'+value.name+'</option>';             
                          });
                          
                       html += '<input type="hidden" name="rname[]" id="rname_'+row_id+'"'+
							'class="form-control" autocomplete="off"></select>'+ 
                          ' </td>'+  */
                     html += '<td data-title="Type"><select class="form-control" id="type_'+row_id+'" name="type[]"'+
							'style="width: 100%;" required>'+
							'<option value="1">Main</option>'+
							'<option value="2">Side</option>'+ 
						'</select></td>'+
						'<td data-title="Unit"><input type="text" name="units[]" id="units_'+row_id+'"' +
							'class="form-control" readonly autocomplete="off"></td>' +  
						'<td data-title="Min R-Days">'+
								'<input type="text" name="minrentaldays[]" id="minrentaldays_'+row_id+'"' +
							'class="form-control" readonly autocomplete="off"></td>' +
						 '<td data-title="Rate"><input type="text" name="rate[]" id="rate_'+row_id+'" class="form-control"' +
							'autocomplete="off" readonly></td>' +
						 '<td data-title="Rate/Day"><input type="text" name="rateperday[]" id="rateperday_'+row_id+'" class="form-control"' +
							'autocomplete="off" readonly></td>' +
						'<td data-title="In Units">' +
							'<input type="text" name="outunits[]" id="outunits_'+row_id+'" readonly value="1"' +
						'class="form-control" required onkeyup="getTotalRent('+row_id+')"></td> ' + 
						'<td data-title="In Units">' +
						'<input type="text" name="noofunits[]" id="noofunits_'+row_id+'" value="1"' +
						'class="form-control" required autocomplete="off" onkeyup="getTotalRent('+row_id+')"> ' +
						'<input type="hidden" name="balanceqty[]" id="balanceqty_'+row_id+'"' +
						'class="form-control" placeholder="balanceqty"></td> ' + 	
						'<td data-title="Grace Period"><input type="text" value="0" name="grace_period[]" id="grace_period_'+row_id+'"' +
						'class="form-control" autocomplete="off" value="0" onkeyup="getTotalRent('+row_id+')"></td>' +
						'<td data-title="Period"><input type="text" value="'+days+'" name="period[]" id="period_'+row_id+'"' +
							'class="form-control" autocomplete="off" onkeyup="getTotalRent('+row_id+')"></td>' +
						'<td data-title="Rent"><input type="text" value="0" name="totamount[]" id="totamount_'+row_id+'"' +
							'class="form-control" readonly autocomplete="off"></td>' +
                    '</tr>';

              if(count_table_tbody_tr >= 1) {
              $("#product_info_table_gst tbody tr:last").after(html);   
            }
            else {
              $("#product_info_table_gst tbody").html(html);
            }

            $(".product").select2();
        }
      });
    return false; 
}

function addboxitemrow() {
	var btn = $("#add_row");
    btn.attr("disabled", "disabled");

    var count_table_tbody_tr = $("#product_info_table_gst tbody tr").length;
    var row_id = count_table_tbody_tr + 1;
	$("#row_index").val(row_id);
 
    $.ajax({
        url: base_url + '/rentitems/getRentitemsData/',
        type: 'post',
        dataType: 'json', 
        async: false,
        success:function(response) {
          
            // console.log(reponse.x);
             var html = '<tr id="row_'+row_id+'">'+
                 '<td data-title="Rent Item code">'+
					'<select class="form-control select_group product" data-row-id="'+row_id+'" id="rentitem_code_'+row_id+'" name="rentitem_code[]" style="width:100%;"'+
				   ' onchange="getCommonProductCode('+row_id+')">'+
                      '<option value=""></option>';
                      $.each(response, function(index, value) {
                       if(value.name)
                       	 html += '<option value="'+value.id+'">'+value.code+'</option>';             
                      });
                      
                   html += '</select>'+ 
                      ' </td>'+ 
						'<td data-title="Name"><select class="form-control select_group product" data-row-id="'+row_id+'"'+ 
							'id="rentitem_name_'+row_id+'" name="rentitem_name[]" style="width:100%;"'+
							   ' onchange="getCommonProductData('+row_id+')">'+
		                        '<option value=""></option>';
		                        $.each(response, function(index, value) {
		                         if(value.name)
		                         	 html += '<option value="'+value.id+'">'+value.name+'</option>';             
		                        });
		                        
		             html += '</select>'+
		            	 '<input type="hidden" name="rname[]" id="rname_'+row_id+'"' +
								'class="form-control" autocomplete="off"></td>' +
						'<td data-title="Qty"><input type="text" name="qty[]" id="qty_'+row_id+'"' +
							'class="form-control" autocomplete="off" value="1"></td>' + 
						'<td data-title="Type"><select class="form-control" id="type_'+row_id+'" name="type[]"'+
								'style="width: 100%;" required>'+
								'<option value="1">Main</option>'+
								'<option value="2">Side</option>'+ 
							'</select>'+
						'</td>'+  
                  '<td data-title="Remove"><button type="button" class="btn btn-default" onclick="removeRow(\''+row_id+'\')"><i class="fa fa-close"></i></button></td>'+
                  '</tr>';

              if(count_table_tbody_tr >= 1) {
              $("#product_info_table_gst tbody tr:last").after(html);  
            
            }
            else {
              $("#product_info_table_gst tbody").html(html);
            } 
            $(".product").select2();
        }
      });
    return false; 
}